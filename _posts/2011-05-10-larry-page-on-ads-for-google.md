---
layout: post
title: "Larry Page on ads for Google"
date: 2011-05-10 14:06
---

Larry Page, [after approving the first ad campaign for Google in ten
years](http://www.stevenlevy.com/index.php/05/08/the-sophie-choice):

> It’s obviously very contrary to what we normally do, and I think part
> of the reason we wanted to do it is for that reason. It sort of
> violates every known principle that we have, and every once in a
> while, you should test that you really have the right principles. You
> don’t want to end up too rigid.
