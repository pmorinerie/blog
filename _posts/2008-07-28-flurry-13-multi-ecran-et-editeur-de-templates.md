---
layout: post
title: "Flurry 1.3 - Multi-écran et éditeur de templates"
date: 2008-07-28 11:23
---

[![Capture d'écran de
Flurry](/images/divers/flurry1.3_thumb.jpg)](/images/divers/flurry1.3.jpg)Vous
connaissez sans doute, ne serait-ce que pour l'avoir déjà vu sur un Mac,
l'économiseur d'écran **Flurry**. Cette animation légère et superbe qui
remplit votre écran d'arabesques fluides et colorées a une longue
histoire : initialement écrit pour Mac par [Calum
Robinson](http://homepage.mac.com/calumr/flurry.html), puis porté sous
Windows par [Matt Ginzton](http://www.maddogsw.com/flurry/), il est
maintenu depuis quelques années par [Julien
Templier](http://www.templier.info/). Il n'avait toutefois pas connu de
nouvelle version depuis plusieurs années.

Mais aujourd'hui, behold ! [Julien annonce la sortie de Flurry
1.3](http://www.aqua-soft.org/board/showthread.php?p=506073). Au menu
des nouveautés :

-   Support du multi-écran: il est désormais possible d'assigner un
    Flurry différent à chaque écran
-   Flurry Editor : un éditeur de templates pour vos Flurry
-   Quelques corrections de bogues et options supplémentaires

Le multi-écran, c'est une bonne nouvelle, qui permettra de plus avoir un
Flurry s'étendant bizarrement sur plusieurs écrans. Mais la grande
nouvelle, c'est l'éditeur de templates : il est maintenant possible de
paramétrer chaque aspect de ses Flurry : nombre de faisceaux, cycle des
couleurs, vitesse, intensité, et j'en passe. On peut également exporter
ses templates, et les partager avec d'autres.

A vrai dire, j'ai toujours trouvé que la version Windows de Flurry
produisait des arabesques un peu moins élégantes que la version Mac —
bien que les deux versions partagent le même moteur. Ceci étant
manifestement dû à une configuration légèrement différente d'une version
à l'autre, j'avais même jeté un coup œil aux sources, pour voir s'il
n'était pas possible de tweaker un peu tout cela (la réponse était en
gros "oui, mais pas facilement"). Donc joie, je vais maintenant pouvoir
bricoler les paramètres pour produire un résultat aussi impressionnant
que sur Mac :)

Vous pouvez **télécharger la dernière version de Flurry sur la [page
Wincustomize de
Julien](http://julien.wincustomize.com/skins.aspx?skinid=75&libid=40)**.
Have fun !
