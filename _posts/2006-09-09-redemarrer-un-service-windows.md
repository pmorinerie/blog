---
layout: post
title: "Redémarrer un Service Windows"
date: 2006-09-09 12:04
---

Ceux d'entre vous qui utilisent la commande `net` de Windows pour
démarrer/arrêter des Services ont dû souvent pester contre l'absence
d'option `net restart`. On ne peut pas demander directement le
redémarrage d'un Service, il faut d'abord l'arrêter
(`net stop service_name`) puis le relancer (`net start service_name`).
Lorque on est en train de tester une configuration serveur un peu fine,
et que l'on a besoin de redémarrages fréquents, cela devient rapidement
agaçant.

J'ai donc écrit un micro-script, `restart`, qui permet de redémarrer
facilement un service. Il s'agit d'un petit batch contenant le code
suivant :

    [dos]@echo offrem Restart the given servicenet stop %1net start %1@echo on

Comme vous voyez, rien de transcendant, juste une astuce pratique. Pour
utiliser ce script, il vous suffit de copier le code ci-dessus dans un
fichier texte nommé "restart.bat", puis de placer ce fichier dans un
dossier du PATH - par exemple "C:\\Windows". Pour redémarrer un service,
vous n'avez alors plus qu'à faire `restart service_name`.
