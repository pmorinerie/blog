---
layout: post
title: Nouvelle tête
date: 2015-02-28 17:45
---

Amis lecteurs du flux RSS de ce blog, si tout se passe bien, vous n’avez rien remarqué.
Mais si vous accéder à ces pages via le Web, il y a quelques changements visibles, et une
nouvelle tête pour ce vieux blog.

Jusqu’ici, les articles étaient servis par un Dotclear version 1 datant de 2006.
La grande époque, celle où Dotclear se différentiait de Wordpress et de SPIP par son
interface d’administration accessible via `/ecrire/`, et où les forums pestaient contre
les extensions PHP5 nécessaires pour installer Dotclear 2 et manquantes chez OVH.

2006, ça fait un bail. Dotclear 1 a courageusement tenu la durée jusque ici, mais je savais
qu’un jour il faudrait prendre le temps de passer à quelque chose de plus pérenne. Et enfin
la grande migration a eu lieu, vers le truc à la mode du moment, `jekyll`. Juste une liste
de fichiers texte et des pages statiques ; sans PHP, sans commentaires. Simple, transportable.

### Attends, je t’ai perdu, là. Un **blog** ?

Aujourd’hui Twitter est plus efficace que jamais pour partager de l’information ou son
humeur du moment, et Instagram bien plus efficate pour poster ses photos de chats. Et les
commentaires comme espace de discussion ont montrés leur limites.

Alors, pourquoi prendre le temps de maintenir un blog en 2015 ?

Principalement parce qu’il me semble important d’avoir sous la main **un espace d’expression public qui
ne dépende pas d’un medium privé**. Dans quelques années, lorsque les réseaux sociaux à la mode
auront été remplacés par d’autres, cet espace sera toujours là. Je n’ai pas à craindre que
les textes ou les images que je publie contreviennent aux Conditions d’Utilisation d’une
plateforme de publication. Plus pérenne, plus libre, pas de question à me poser, pas
d’autocensure.

D’autre part, si l’information se diffuse très bien sur Twitter, elle a souvent pour source
un article, un reportage, un texte plus long hébergé quelque part. Et j’ai envie d’investir
ces espace où on peut commenter un article en plus de 140 caractères, ou **écrire des bouts de
réflexion plus cotonneuses**.

### Où me trouver

Alors voilà, je vais continuer à poster irrégulièrement ici des
bouts d’idées ; ça parlera de ce qui importe dans ma vie. De technique, bien sûr, mais
aussi d’événements, de projets, de réflexions diverses.

Je suis donc ici : vous pouvez jeter de temps en temps un œil à ce site, ou vous abonner
au [flux RSS](http://kemenaran.winosx.com/atom.xml).

Il y a également une liste d’articles partagés que j’alimente fréquemment ; c’est accessible
sur [Delicious](https://delicious.com/kemenaran) ; vous pouvez également vous abonner au [flux RSS de cette liste](http://feeds.delicious.com/v2/rss/kemenaran).

Et enfin vous me trouverez aussi en train de discutailler sur
[Twitter](https://twitter.com/pmorinerie), ou de pousser du code sur
[GitHub](https://github.com/kemenaran).