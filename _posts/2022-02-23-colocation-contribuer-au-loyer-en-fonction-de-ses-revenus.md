---
layout: post
title: "Colocation : contribuer au loyer en fonction de ses revenus"
date: 2022-04-13T07:04:20.625Z
lang: fr
---

Dans notre colocation, on contribue aux loyer et aux dépenses en fonction de nos revenus. Plutôt que de payer chaque mois un loyer fixe, on met au pot en fonction de l'argent avec lequel on vit ce mois-ci.

Pourquoi on a eu envie de mettre ça en place ? Comment on en est arrivé à ce fonctionnement ? De quelle manière ça se passe en pratique ? Cet article raconte tout ça.

![Photographie d’une rue de Paris](/images/uploads/rue-de-paris.jpg)
_Mettre au pot en fonction de ses rentrées d’argent, une pratique commune à beaucoup d’habitats partagés ? Sans doute que oui._

## Envies

Au début, comme tout le monde, chaque personne dans la coloc payait le même loyer (ou presque, avec juste de légères variations en fonction de la taille de la chambre).

Et puis tout ça est parti d'une impression persistante qu'**un euro de loyer n'a pas la même valeur pour tout le monde**. Sortir 500 € quand on vit avec 650 € par mois, ce n'est pas pareil que quand on a un salaire de 1500 € qui tombe tous les mois.

Dans mon cas à moi, c'était aussi un moment où j'étais malheureux là où je travaillais ; mais j'hésitais à changer de boulot, et à devenir indépendant, un peu par peur d'une perte de revenus dans l'intervalle. Rien de très important – mais en regardant mieux, on s'est mis à voir autour de nous plein de situations comme ça, où des gens restent dans un travail qui les rend malheureux ou qui fait du mal au monde, pour une raison simple : la peur de ne plus pouvoir payer le loyer.

On voyait aussi les gens qui avaient des revenus irréguliers : certains mois, ça passe, mais c'est parfois suivi de mois de creux, où sortir toute la thune du loyer devient compliqué.

## Discuter ensemble

On a fini, un soir de février, par s'asseoir tous autour de la table de la cuisine, et par en discuter ensemble. On s'est parlé de nos expériences, déjà : de ce que ça nous fait de payer un loyer, ce que ça peut freiner ou rendre difficile, et commencer à imaginer comment ça pourrait être autrement.

**Est-ce que c'est une bonne idée d'avoir des loyers variables ?** On pourrait faire ça en fonction de la situation matérielle des gens (boulot, chômage, travail précaire). Et si on se retrouvait chaque mois et qu'on discutait de combien on met chacun·e ? Ou alors est-ce qu'on ferait une formule fixe ? Mais est-ce que ça varierait chaque mois ou pas ?

La discussion a fait émerger plein de sujets intéressants. Par exemple, que **la question du logement est anxiogène pour pas mal de gens**. Payer un loyer fixe, même élevé, c'est quelque part l'assurance d'avoir un toit – et passer à des loyers variables envoie un peu d'incertitude là dessus.

On s'est aussi rendu compte en discutant que quelque part, **en habitant tous ensemble, il est parfois plus facile de donner que de prendre**. Les gens dont la situation actuelle fait qu'iels contribueraient moins ne se sentaient pas toujours à l'aise avec ça. On s'est demandé comment faire émerger un sentiment de justice là dessus, qui fasse que tout le monde ait l'impression de participer à un système juste, même aux moments de moindre contribution.

Quelques discussions aussi autour de « qu'est-ce qu'un revenu ? » Si on module le loyer en fonction de ce avec quoi les gens vivent, comment est-ce qu'on le définit ? Est-ce que les allocations chômage rentrent dedans ? Les aides au transport ? Les étrennes de la grand-mère ?

Et de l'autre côté, qu'est-ce qui rentre dans le loyer ? Le loyer général de l'appart, évidemment. Mais les charges, l'internet, les assurances s'il y en a : qu'est-ce que qu'on veut aussi faire rentrer dans un budget commun ?

En commençant à explorer des modes de répartition, on s'est aussi rendu compte de **notre inégalité face aux chiffres**. Si certain·es d'entre nous voyaient immédiatement les enjeux et les impacts d'une formule simple pour calculer les revenus, c'était beaucoup moins immédiat pour beaucoup d'autres. Pourtant, pour que tout le monde ait le sentiment d'un système juste, il fallait que le mode de répartition soit vraiment compréhensible intuitivement par tout le monde.

## Explorer les effets

Assez vite, il est donc apparu que, plutôt que de décider chaque mois de combien on met au pot, **on préférait une formule de répartition fixe**. Ça donne un sentiment de stabilité et d'absence de surprise qui semblait convenir à tout le monde.

Mais quelle formule, et selon quels critères ? Et surtout, comment rendre les différents enjeux de différentes formules vraiment accessibles à tout le monde ?

On a commencé par dessiner des grands graphiques, et à disséquer des formules possibles – mais au final l'un d'entre nous a proposé de faire des **simulateurs interactifs de répartition**.

Ça ressemblait par exemple à ça :

[![Une capture d'écran du simulateur interactif d'une des formules.](/images/uploads/simulateur-de-loyers.gif "En déplaçant les chiffres de la formule, le résultat est mis à jour immédiatement.")](https://pmorinerie.gitlab.io/coloc-loyers-solidaires/)
_Un des simulateurs qu'on a mis en place ([voir la version interactive](https://pmorinerie.gitlab.io/coloc-loyers-solidaires/))._

L'idée, c'est que si on peut changer les paramètres d'une formule, et voir le résultat en temps réel, hébin ça aide beaucoup à comprendre comment la formule fonctionne, et quel effet elle pourrait avoir sur soi. Et puis ça aide aussi à voir combien il faudrait que chacun·e mette pour équilibrer le budget ; ou encore à simuler rapidement ce qui se passe si quelqu'un·e n'a pas de revenus pendant quelques mois.

Pour évaluer les différents moyens de répartir un loyer, on a créé plusieurs simulateurs de ce que ça donnerait. Par exemple, un des simulateurs explorait un loyer qui soit exactement un pourcentage des revenus. Un autre représentait la situation avec une part fixe de loyer, et une part variable. Et encore un autre l'idée d'un revenu de base assuré par la coloc (versé quand on n'a vraiment rien pour vivre ce mois-ci).

## Fonctionnement actuel

Au final, le fonctionnement actuel utilise un **revenu de base**. Concrètement, chaque début de mois :

* On reçoit 100 € de la coloc,
* On verse 35% de son revenu à la coloc.

Ça veut dire que **les gros revenus contribuent plus que les petits** (ce qui est l'effet désiré). Mais surtout, si un mois vous êtes court·e pour le loyer, ou complètement à sec, non seulement vous ne payez pas (ou peu) de loyer, mais en plus **la coloc vous verse de l'argent**. Histoire d'assurer le minimum.

*(On a préféré ça à un système progressif par tranches, parce que ça permet que les mois se compensent entre eux. Par exemple si on attend des allocs qui ne viennent pas, et qu'un mois on est à zéro, mais qu'on reçoit le double le mois suivant, pas de souci : ça fait pile comme si on avait contribué les deux mois.)*

Avec ces sous, on paye le loyer de la partie de la coloc qu'on loue – mais aussi les charges (eau, électricité, internet), les assurances habitation, et les achats ponctuels de matériel pour la coloc. (En revanche, pour la lessive et le papier-toilette, chacun continue à se débrouiller.)

Au final, ce moyen de répartition permet d'être prévisible. Même si le loyer est variable chaque mois, la formule de calcul est fixe : pas besoin de discuter de la répartition des contributions chaque mois. En revanche, quand la situation à long-terme des habitant·es change (par exemple une nouvelle personne dans la coloc', un changement de revenus, ou une fin de droits au chômage), là on se pose à nouveau autour d'une table avec le simulateur pour ré-équilibrer le budget, en mettant à jour la formule.

## En pratique

Concrètement, chaque mois on doit verser nos contributions. Pour cela, on a essayé de simplifier les choses au maximum.

1. **On estime avec combien on vit ce mois-ci.** Salaire, chômage, allocations, économies perso… On ne flique pas les gens, c'est autogéré.
2. **On indique ses revenus dans un tableur partagé**. La contribution à verser est automatiquement calculée en fonction de la formule en cours.
3. **On fait un virement sur le compte de la coloc** si on doit verser des sous – ou un virement du compte vers soi si on récupère des sous ce mois-ci.

Le tableur du budget a deux intérêts :

* Il calcule automatiquement la contribution de chaque mois en fonction de la formule,
* Il permet de suivre l'état du budget.

**Tout le monde peut voir l'état des finances** à n'importe quel moment. Ça nous a semblé important pour l'auto-gestion que la responsabilité d'équilibrer les finances soit partagée par tout le monde – et que l'info soit donc visible et compréhensible facilement.

[![Une capture d'écran du tableur utilisé pour l'autogestion du budget](/images/uploads/tableur-budget-coloc.png "Les courbes sont mises à jour automatiquement chaque mois.")](/images/uploads/tableur-budget-coloc.png)
_Notre tableur partagé des finances. On l'utilise pour calculer nos contributions, et suivre l'état de la trésorerie. [Consulter ou copier le tableur d’exemple](https://www.icloud.com/numbers/0k6LqmyCW_Qjq_HUOcgfzt6RA#Budget_coloc)_

## Où ça en est aujourd'hui 

Après quelques années d'utilisation de ce système, on a l'impression qu'il y a des choses qui fonctionnent particulièrement bien :

* Ça permet effectivement une plus grande justice dans la contribution au loyer 🙌
* Quand une nouvelle personne entre dans la coloc, les discussions pour mettre à jour la formule se passent bien et rapidement. Le simulateur aide beaucoup pour ça.
* Les gens sont à l'aise avec l'idée de contribuer plus que les autres ou moins que les autres (même si ne pas contribuer du tout reste parfois difficile).
* On a pu gérer les cas particuliers sans problème (colocs présent·es seulement une semaine sur deux, par exemple). Dans ce cas on s'est généralement mis d'accord pour ajuster les choses – par exemple pour que la personne déduise N € de ses revenus mensuels, ce qui ajuste automatiquement la contribution sans changer la formule générale.

Et puis évidemment il y a parfois des points de friction :

* La colocation a eu un léger déficit chronique, qu'on a dû combler par de la trésorerie externe. Ça va mieux depuis qu'on prévoit une formule qui met _un peu plus_ que juste ce qu'il faut chaque mois pour atteindre l'équilibre.
* Le loyer de certaines personnes dans la coloc est payé par d'autres gens (typiquement leurs parents). Dans ce cas, sur quels revenus on se base ?

Mais tout ça se discute bien collectivement.

En tout cas l'expérience est vraiment positive. Ca fait du bien de sentir qu'on tend vers plus de justice (même à toute petite échelle), les discussions pour mettre en place les règles se passent bien, les contributions arrivent en temps voulu. A priori on va continuer comme ça pendant un bon moment !

Normalement ce fonctionnement et les outils qui vont avec sont assez réutilisable. Si vous envisagez de monter le même système dans une coloc’ ou un habitat partagé à vous, n’hésitez pas à vous en servir.

## Ressources

- Le simulateur de contributions au loyer :<br>
  [https://pmorinerie.gitlab.io/coloc-loyers-solidaires/](https://pmorinerie.gitlab.io/coloc-loyers-solidaires/)
- Un exemple de feuille de calcul du budget :<br>
  [https://www.icloud.com/numbers/0k6LqmyCW_Qjq_HUOcgfzt6RA](https://www.icloud.com/numbers/0k6LqmyCW_Qjq_HUOcgfzt6RA#Budget_coloc)
