---
layout: post
title: "Testez vos applications web avec Selenium IDE"
date: 2006-06-22 22:28
---

Un - relativement - ancien billet d'[Olivier
Meunier](http://www.neokraft.net/) m'a fait découvrir comment
automatiser les tests d'applications Web à l'aide de Selenium, et de son
extension pour Firefox, [Selenium
IDE](http://www.openqa.org/selenium-ide/). J'ai testé, c'est simple,
efficace, et peut sans doute épargner beaucoup de travail de test.
Adopté.

[Testez vos applications web - Neokraft
Blog](http://www.neokraft.net/post/2006/04/29/Testez-vos-applications-web)
