---
layout: post
title: "Why so socialist ?"
date: 2009-08-04 16:32
---

Il y a des affiches curieuses qui se baladent en ce moment dans les rues
de Los Angeles… En bon fan du Joker de Heath Ledger, je ne pouvais pas
laisser passer ça.

![Why so
socialist](http://i141.photobucket.com/albums/r66/pechuna/Joker.jpg)

On trouve [quelques autres
images](http://www.flickr.com/photos/26962760@N00/3785465075/) [du même
genre](http://www.flickr.com/photos/26962760@N00/3787023006/) sur
Flicker.

Apparemment, c'est lié à la difficile réforme de l'assurance maladie.
Souvenez-vous, aux Etats-Unis, "socialiste", c'est une grosse insulte —
équivalent, dans l'intensité, à "fasciste" chez nous.

*[Source]: [20
Minutes](http://www.20minutes.fr/article/340777/Monde-De-mysterieux-posters-d-Obama-version-Joker-placardes-a-Los-Angeles.php)*
