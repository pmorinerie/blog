---
layout: post
title: "Squatconf"
date: 2014-11-24 09:21
---

Quelques notes prises pendant la [Squatconf](http://squatconf.eu/), par
Oli Evans. On y parle de vie privée, de chiffrement, de
décentralisation, des outils techniques et théoriques pour y arriver.

**<http://blog.tableflip.io/squatconf-information-wants-to-be-free/>**
