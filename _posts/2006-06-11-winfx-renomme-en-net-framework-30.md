---
layout: post
title: "WinFX renommé en .NET Framework 3.0"
date: 2006-06-11 16:58
---

Il semble que trop de développeurs se soient demandés quelle allait être
l'évolution du [.NET Framework](http://msdn.microsoft.com/netframework/)
et de [WinFX](http://msdn.microsoft.com/winfx/) : un développement
parallèle, des fonctionnalités qui se complètent, l'un est une évolution
de l'autre ?

Microsoft a donc décidé officiellement de renommer la plateforme WinFX
en .NET Framework 3.0, comme S. Somasegar , le vice-président de la
section développement, [l'annonce sur son
blog](http://blogs.msdn.com/somasegar/archive/2006/06/09/624300.aspx).
Cela incrit l'architecture WinFX dans la continuité de la technologie
.NET. Cette nouvelle version du Framework sera incluse dans Windows
Vista, et sera disponible sous forme de paquetage indépendant pour
Windows XP et Server 2003. Ce changement ne devrait pas modifier la date
de sortie de Vista ou du .NET Framework 3.0.
