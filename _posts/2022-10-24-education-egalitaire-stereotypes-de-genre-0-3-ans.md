---
layout: post
title: "Éducation égalitaire garçons / filles – 0-3 ans"
date: 2022-10-24 21:33
---

Les stéréotypes de genre apparaissent très tôt dans la vie d’un bébé. Dès la naissance, les personnes autour de lui projettent des choses différentes sur un bébé en fonction du sexe assigné à la naissance – et adoptent des comportements différents.

Pourtant, à bien regarder les bébés, les différences de genre sont loin d’être visibles en soi. C’est sans doute la raison pour laquelle on tient autant à habiller les filles en roses et les garçons en bleu : en dehors de ça, il n'y a pas grand chose pour faire la différence.

Mais les recherches montrent depuis des décennies, avec certitude, que dès la petite enfance, nos comportements d’éducateurs suivent des schémas sexistes inconscients et causent des inégalités et incapacités pour toute la vie.

Comme on n’a pas trouvé ailleurs de **listes étoffées de ces stéréotypes de genre**, voici donc une liste (incomplète) de quelque uns de ces stéréotypes. Elle est centrée principalement sur les premières années de la vie (0 - 3 ans), histoire d’y faire attention dans nos comportements.

## En version PDF

_Pour consulter ou imprimer, voici les deux fiches d'une page, au format PDF, réalisée par [@elise](https://mastodon.xyz/web/@elise@esperanto.masto.host)._

<figure style="text-align: center">
  <a href="/images/education-egalitaire/fiche-education-egalitaire-garcons-filles.pdf">
    <img src="/images/education-egalitaire/fiche-education-egalitaire-garcons-filles.jpeg"
         height="250px"
         alt="Une capture d’écran de la fiche complète."
         title="Cliquez pour télécharger la fiche complète au format PDF.">
    <figcaption style="font-style: italic">
      Une fiche complète sur les stéréotypes et biais de genre (une page).
    </figcaption>
  </a>
</figure>

<figure style="text-align: center">
  <a href="/images/education-egalitaire/fiche-synthetique.pdf">
    <img src="/images/education-egalitaire/fiche-synthetique.jpeg"
         height="250px"
         alt="Une capture d’écran de la fiche synthétique."
         title="Cliquez pour télécharger la fiche synthétique au format PDF.">
    <figcaption style="font-style: italic">
      Une autre fiche plus synthétique (une page).
    </figcaption>
  </a>
</figure>

## En version HTML

_Pour mettre dans ses marques-pages et copier-coller, une version de la fiche complète au format HTML est disponible ci-dessous._

**Quand…** on complimente toujours une fille sur son apparence (« jolie/mignonne ») ou ses vêtements ;<br>
**Alors…** on lui signifie que ces caractéristiques sont importantes, voire nécessaires pour être appréciée ;<br>
**Que faire ?** Il est vrai que cela correspond à une attente de notre société envers les femmes, mais essayons de varier les qualités que l’on valorise !

**Quand…** On propose à une fille essentiellement des jeux relationnels et émotionnels, et à un garçon essentiellement des jeux de construction et déplacement ;<br>
**Alors…** On ne leur permet pas de développer leurs différentes compétences de manière équilibrée ; les garçons se trouvent amputés dans l’expression des émotions, les filles dans leur représentation de l’espace et la motricité ;<br>
**Que faire ?** Proposer des jeux variés et développer des compétences diverses permet à tous les enfants de se construire des personnalités plus équilibrées !

**Quand…** On fait plus de câlins aux filles et on passe plus de temps avec elles à la maison ;<br>
**Alors…** On leur enseigne la relation et à exprimer leurs émotions ;<br>
**Que faire ?** Est-ce que les garçons ne méritent pas aussi cet apprentissage ? La société entière en bénéficierait !

**Quand…** On assigne à un bébé fille la peur dès qu’elle exprime une émotion négative ;<br>
**Alors…** On lui retire la prise sur sa colère, qui est une ressource pour réagir et se défendre ; on lui prescrit un comportement anxieux face à la vie ;<br>
**Que faire ?** Tous les enfants peuvent apprendre à reconnaître et à vivre les émotions dans leur grande diversité, sans se laisser envahir ou écraser par elles ; apprenons-leur à reconnaître et accepter toute la palette des émotions !

**Quand…** On assigne à un bébé garçon la colère dès qu’il exprime une émotion négative ;<br>
**Alors…** On lui interdit d’exprimer sa peur, et de vivre la tristesse ; toute émotion négative est ramenée à la colère, qui devient explosive et incontrôlable ;<br>
**Que faire ?** Tous les enfants peuvent apprendre à reconnaître et à vivre les émotions dans leur grande diversité, sans se laisser envahir ou écraser par elles ; apprenons-leur à reconnaître et accepter toute la palette des émotions !

**Quand…** On projette un rapport plus fort entre mère et fille ou entre père et fils, par exemple en les décrivant comme très complices (« tel père, tel fils », « avec sa mère, elles n’arrêtent pas de parler ») ;<br>
**Alors…** On encourage chez l’enfant une sociabilité non-mixte ; chez le parent, on risque d’intensifier le phénomène d’identification, qui crée une plus grande exigence envers l’enfant du même sexe, donc une relation plus difficile par la suite ;<br>
**Que faire ?** Pour un enfant de l’autre genre, dans la même situation, on décrirait plutôt : « quand ils parlent de botanique, ça peut durer longtemps » ; pourquoi ne pas s’en inspirer ?

**Quand…** On recourt plus souvent à la force face au refus d’un garçon dans des gestes quotidiens (changer une couche, mettre son manteau, etc) ;<br>
**Alors…** On lui enseigne que la contrainte physique est une manière acceptable et courante d’interagir avec les autres ;<br>
**Que faire ?** Voulons-nous vraiment cela ?

**Quand…** On a tendance à faire parler, raconter sa journée, élaborer ses refus à une fille ;<br>
**Alors…** On lui apprend à verbaliser, à mieux comprendre ses émotions et ses expériences ;<br>
**Que faire ?** Pourquoi ne pas proposer la même chose aux garçons ?

**Quand…** On réagit moins vite, voire pas du tout aux besoins exprimés par les petites filles ou on les qualifie plus facilement de « caprices » ;<br>
**Alors…** On leur apprend à patienter, mais aussi dans des cas plus extrêmes à négliger leurs propres besoins, ne plus se faire confiance, faire passer les autres avant elles-mêmes ;<br>
**Que faire ?** La patience et l’empathie, c’est bien, mais pas la soumission : il est important aussi de reconnaître ses propres besoins ; faisons place à ceux des petites filles !

**Quand…** Dès la crèche, on accorde plus d’importance aux besoins des garçons ;<br>
**Alors…** À 18 mois, les petits garçons sont déjà plus attentifs aux demandes des autres garçons qu’à celles des filles ! ;<br>
**Que faire ?** Mieux vaut signifier à tous que les besoins de chacun et chacune sont aussi importants les uns que les autres !

**Quand…** On interroge plus les garçons sur des questions de mathématiques et de logique… ou en général on leur accorde plus de temps en contexte scolaire ;<br>
**Alors…** Ils développent mieux ces compétences et osent plus prendre la parole ; c’est documenté notamment entre 4 et 7 ans, et cause un retard scolaire en sciences ;<br>
**Que faire ?** Proposons aussi ces activités aux filles, qui en auront tout autant besoin dans leur vie !

**Quand…** On désigne les enfants par des mots qui insistent sur les catégories de genre : « les garçons ! » /« les filles » ;<br>
**Alors…** On renforce et on essentialise les différences fantasmées, assignées ou déjà assumées par les enfants ;<br>
**Que faire ?** Dire « les enfants », « les élèves », ou encore « les rapides qui ont déjà fini », ça ne mange pas de pain !

_Et en bonus, suite à une discussion avec [@celineb](@celineb@mamot.fr), deux éléments qui ne sont pas sur les fiches :_

**Quand…** On projette du romantisme sur les relations entre entre garçons et filles, quand on dit des choses comme « C’est son amoureux ? » ou « Déjà un charmeur ! » ;<br>
**Alors…** Si garçons et filles ne peuvent se lier qu’à travers un prisme romantique hétérosexuel, on encourage chaque genre à joue un rôle (les garçons dragueurs et les filles coquettes et passives), et on décourage une sociabilité mixte ;<br>
**Que faire ?** Supposer l’amitié (plutôt que du romantisme), quel que soit le genre, permettra ensuite aux enfants de construire les formes relations qu’ils veulent !

**Quand…** On propose aux petits garçons de faire la course, de jouer à la bagarre (pour gagner), et aux filles de collaborer et de prendre soin ;<br>
**Alors…** Valoriser la compétition chez un garçon n’est pas seulement mettre en avant ses besoins avant ceux des autres, c’est aussi lui dire que la société est fondée sur des valeurs hiérarchiques et qu’il se doit de convoiter la première place ;<br>
**Que faire ?** Proposons aux garçons de eux aussi collaborer (et de gagner <em>avec</em> les autres) plutôt que de toujours gagner <em>sur</em> les autres !

