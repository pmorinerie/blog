---
layout: post
title: "Content-Aware Image Resizing in Python"
date: 2009-11-28 23:33
---

![Enlarged picture](/images/screenshots/enlarge_300.jpg)

We have a newcomer in the [zoo](http://winosx.com/zoo/) ! There are a
few versions of **[Seam Carving Image
Resizing](http://www.seamcarving.com/)** in Python, but [here is my shot
at this](http://winosx.com/zoo/SeamResizer/). It's a simple single-file
script that can seamlessly resize (reduce or expand) pictures.
Additionally, you can choose to save partial results, to see how the
algorithm works or to produce a video. The syntax would be :

`python seamresize.py <resize|enlarge> <picture> <number_of_pixels> [save_partials]`

[The script itself](http://winosx.com/zoo/SeamResizer/seamresize.py) is
short, heavily commented, and probably a good start if you want to add
more features — oh, at it requires the Python Image Library. And you
also can watch
[some](http://winosx.com/zoo/SeamResizer/demos/Enlarge.mp4)
[demos](http://winosx.com/zoo/SeamResizer/demos/Resize%20and%20enlarge.mp4).
