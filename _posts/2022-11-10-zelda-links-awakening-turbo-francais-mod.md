---
layout: post
title: "Zelda: Link’s Awakening Turbo-Français"
date: 2022-11-14 13:03
---

La traduction française de _Zelda: Link’s Awakening_ a un charme particulier. Le texte de [Véronique Chantel](https://www.squarepalace.com/secret-of-mana-veronique-chantel-traduction-presse/secrets-de-traduction-de-secret-of-mana-magazine) est plein de rimes, raccourcis, bizarreries et étrangetés, qui collent parfaitement à l'esprit « Twin Peaks » du jeu. Tout cela en respectant les contraintes techniques de la Game Boy, qui obligeait par exemple le texte français à être à peu près de la même longueur que le texte anglais ou japonais – d'où la nécessité de faire passer les informations essentielles en peu de mots.

<span class="pixel-art">
  ![Une capture d’écran du jeu, avec un pêcheur expliquant à Link « Sois un peu plus motivé ! »](/images/zelda-links-awakening-turbo-francais/pecheur.png)<br>
  _« Vis ta vie ! Sois un peu plus motivé ! » Ce pêcheur m’a longtemps fasciné._
</span>

Mais le texte français comporte malgré tout une imperfection : **les lettres majuscules n’ont pas d’accent.** C'est dommage, car au delà de la lisibilité, le sens de certains mots change parfois en fonction des accents : « OEUF SACRE » n'est pas la même chose que « OEUF SACRÉ », ni « NAUFRAGE » que « NAUFRAGÉ ».

<span class="pixel-art">
  ![Une capture d’écran du jeu, avec un texte « VAS VOIR L’OEUF SACRE », sans accent sur le E de sacré.](/images/zelda-links-awakening-turbo-francais/oeuf-sacre-sans-accent.png)<br>
  _Un Œuf Sacre, c'est quand même pas la même chose._
</span>

Cette omission est dûe à un **compromis technique**.

Pour chaque traduction, les programmeurs du jeu ont ajusté le code du jeu aux nécessités typographiques de chaque langue. Par exemple, la version japonaise permet d'afficher des diacritiques sur certains caractères ; et la version allemande gère les lettres comportant des trémas. Et le code utilisé pour afficher les diacritiques sur les lettres majuscules peut utiliser au maximum deux signes différents (par exemple un tréma et un accent).

Mais le script français aurait besoin de **trois accents** : aigu, grave et circonflexe. Plutôt que de passer un temps précieux à supprimer cette limitation, l'équipe de traduction a donc préféré désactiver la gestion des accents sur les majuscules. Vu les contraintes de temps de développement, on les comprend.

## Restaurer la gestion des accents

Toutefois, en explorant le [script français](https://github.com/zladx/LADX-Disassembly/tree/main/revisions/F0/src/text), il s'avère qu'une [unique ligne de texte](https://github.com/zladx/LADX-Disassembly/blob/916e58eec0a0ced3a1e26b796ba4d0b5abf1d0dc/revisions/F0/src/text/dialog_dx.asm#L23) comporte une majuscule sur un accent : il est écrit « NAUFRAGÉ ». Et de fait, dans les graphismes stockés en mémoire, on trouve bien deux accents : `◌́` et `◌̀`.

Comme la gestion des diacritiques est désactivée pour le français, dans le jeu cette lettre est simplement affichée comme un « E » majuscule, sans accent. Mais cette lettre est un **vestige des tentatives** d'intégrer la gestion des diacritiques à la version française, avant que soit finalement entérinée l'absence d'accents sur les majuscules.

Heureusement, en utilisant le [code-source restauré de Zelda: Link's Awakening](https://github.com/zladx/LADX-Disassembly), il est possible de compiler une version française qui ré-active la gestion des diacritiques. Cela permet à cet accent de s'afficher dans le jeu.

<span class="pixel-art">
  ![Une capture d’écran du jeu, avec un dialogue « NAUFRAGÉ » avec un accent sur le E de naufragé.](/images/zelda-links-awakening-turbo-francais/naufrage-avec-accent.png)<br>
  _Cet accent, désactivé dans le jeu original, n'avait pas été vu depuis 29 ans._
</span>

En modifiant le reste du texte, pour ajouter des accents aux autres majuscules, il devient alors possible d'afficher des accents sur toutes les majuscules du jeu !

<span class="pixel-art">
  ![Une capture d’écran du jeu, avec un dialogue « TOUT SUR L’ÉPÉE TOURNOYANTE » avec deux accent sur les E de épée.](/images/zelda-links-awakening-turbo-francais/E-avec-accent.png)
</span>

## Accents circonflexes

Toutes ? Presque. La limitation technique originale, qui empêche de gérer plus de deux types d'accents, est toujours présente. Comme les accents aigus et graves sont déjà présents, il manque l'accent circonflexe – ce qui empêche d'écrire un mot comme « POISSON-RÊVE ». Fâcheux.

La solution serait de trouver un espace inutilisé dans la mémoire graphique où stocker les pixels de l'accent circonflexe. Ce n'est pas simple : sur la Game Boy, la mémoire graphique est très limitée – et les accents doivent occuper le précieux espace des graphismes qui sont chargés en permanence en mémoire.

Comment faire ? Après quelques recherches, il s'avère qu'un emplacement n'est utilisé par le jeu que lorsque l'inventaire est ouvert. Il serait donc théoriquement possible, quand le jeu ouvre l'inventaire, de remplacer l'accent circonflexe par le graphisme dont l'inventaire a besoin – puis de restaurer l'accent circonflexe à la fermeture de l'inventaire.

Et ça marche ! Grâce à cette astuce, il est maintenant possible d'afficher les trois types d'accents dans le jeu.

<img src="/images/zelda-links-awakening-turbo-francais/swap.gif" width="318px" height="223" style="width: 100%; height: auto; max-width: 450px;" alt="Une animation du jeu, montrant l'accent circonflexe être remplacé en mémoire graphique lorsque l'inventaire s'ouvre."><br>
_Avec de bon yeux, on peut voir dans la mémoire graphique l’accent circonflexe être remplaçé par un autre symbole lorsque l’inventaire s’ouvre._

Il est donc enfin possible d'afficher correctement le texte ci-dessous :

<span class="pixel-art">
  ![Une capture d’écran du jeu, avec un dialogue « POISSON-RÊVE » avec un accent sur le E de poisson-rêve.](/images/zelda-links-awakening-turbo-francais/poisson-reve-avec-accent.png)<br>
  _Hourra !_
</span>

## Gérer les ligatures

Un autre souci de la version originale est que les _e-dans-l’o_ est affiché comme deux lettres séparées. Le jeu affiche donc par exemple « Coeur » et « Oeuf », sans faire la ligature.

Techniquement, il est relativement facile d’ajouter un caractère supplémentaire à la fonte du jeu. Reste à dessiner le nouveau caractère dans une police similaire à celle du jeu – ce qu’a fait avec brio **merwok** sur Discord.

Une fois ce nouveau caractère intégré, on peut facilement afficher le mot « cœur » correctement.

<span class="pixel-art">
  ![Une capture d’écran du jeu, avec un dialogue « cœur » avec le e-dans-l’o affiché comme une ligature.](/images/zelda-links-awakening-turbo-francais/coeur-avec-ligature.png)<br>
  _Haut les cœurs !_
</span>

Le cas du « Œ » majuscule est un peu plus délicat : chaque lettre doit s'inscrire dans une case de 8x8 pixels – et le Œ majuscule est un peu trop large pour ça.

Heureusement il est possible d'ajouter à la fonte non pas un, mais deux caractères supplémentaires – ce qui rend donc 8x16 pixels disponibles pour dessiner la majuscule correctement.

<span class="pixel-art">
  ![Une capture d’écran du jeu, avec un dialogue « ŒUF » avec le e-dans-l’o affiché comme une ligature.](/images/zelda-links-awakening-turbo-francais/OEUF-avec-ligature.png)<br>
  _Ce « Œ » utilise deux tuiles de 8x8 pixels._
</span>

## Zelda: Link's Awakening Turbo-Français

Le résultat, c'est le mod le plus futile de tous les internets.

_[Zelda: Link’s Awakening Turbo-Français](https://www.romhacking.net/hacks/7281/)_ ajuste le script français de Zelda DX, pour ajouter des accents et des ligatures partout où cela est nécessaire. Parfaitement in-dis-pen-sable pour les fans français du jeu.

Vous pouvez télécharger :
- Soit le **patch IPS** sur romhacking.net  (à appliquer vous-même) :<br>
  [ https://www.romhacking.net/hacks/7281/](https://www.romhacking.net/hacks/7281/)
- Soit la **version compilée** (prête à l'usage dans un émulateur Game Boy, ou sur une vraie console) :<br>
  [📎 alzf-r1-turbo-francais-v1.4.gbc](/assets/files/zelda-links-awakening-turbo-francais/azlf-r1-turbo-francais-v1.4.gbc)

Pour celleux que ça intéresse, le [code-source de ces modifications](https://github.com/kemenaran/LADX-Disassembly/commits/mod/turbo-francais) est également disponible.

Bon jeu !

<span class="pixel-art">
  ![Une capture d’écran du jeu, avec un dialogue « Bon baisers de Kyoto! VÉRO ❤️ » avec un accent sur le E de Véro.](/images/zelda-links-awakening-turbo-francais/kyoto-vero.png)
</span>
