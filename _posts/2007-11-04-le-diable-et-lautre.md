---
layout: post
title: "Le diable et l'autre"
date: 2007-11-04 21:42
---

![Le diable en partage](/images/divers/file.jpeg)L'autre samedi — il y
a longtemps, trop longtemps déjà —, je suis allé voir **[le Diable en
partage](http://www.billetreduc.com/18020/evt.htm)**, au Théâtre du
soleil. Cette pièce se donne dans le cadre du festival [Premiers
pas](http://www.premiers-pas.org/), organisé par la Cartoucherie : on
donne à quelques troupes un théâtre pour monter leur premier spectacle ;
chaque soir, une ou deux troupes jouent, les autres tiennent le théâtre,
le bar où l'on peut grignoter un morceau, s'occupent des deux chats
noirs, etc. Si vous avez envie, allez donc voir le site du festival et
lire le manifeste, ça mérite d'être vu. Chouette projet, de ceux qui
font dire qu'il y a des gens bien de part le monde.

Déjà, c'est l'atmosphère de la Cartoucherie le soir : la nuit, la
navette qui nous emmène à travers le bois de Vincennes, les cinq minutes
de marche entre les différents théâtres — puis on passe à travers une
lourde toile tendue en guise de rempart contre le froid, et on se
retrouve dans une grande salle. De hautes toiles délimitent l'espace ;
au centre, des tables et des bancs ronds ; quelques grandes lampes à
gaz, qui diffusent une chaleur calme, réchauffent les dîneurs. De part
et d'autre des tables, un bar rudimentaire, et une femme en boubou qui
propose des tisanes à l'hibiscus ou au gingembre, au dessus de panneaux
en vantant les mérites sur les rêves et la santé. On peut prendre un
plat ou un verre en attendant le spectacle, ou simplement s'assoir sur
un des confortables bancs et laisser un des chats noir ronronner sur ses
genoux.

Venons en au spectacle. Le Diable en partage, donc : une pièce
contemporaine, sur le destin d'une famille yougoslave un peu éclatée.
Les réactions de chacun face à la guerre : ceux des fils qui combattent,
ceux qui fuient la guerre, les parents qui rient ou pleurent, ou font
comme si de rien n'était, la fiancée musulmane qui doit résister à la
violence qui rampe. Parfois drôle, parfois tragique, et toujours ce
contraste entre la guerre, qui détruit les choses et les gens, et la vie
qui continue, les gens qui se font à ses situations absurdes et les
vivent comme le quotidien.

Le texte est très bien écrit, et très bien joué. On disait autour de moi
que la pièce méritait salle comble tous les soirs, et je soutiens : on
passe une très bonne soirée. Et savez-vous quoi : ça se donne toujours,
du 23 au 29 novembre. Si vous êtes sur Paris, réservez une de vos
soirée, ça vaut vraiment le coup.
