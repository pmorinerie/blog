---
layout: post
title: "Grand moment d'émotion"
date: 2006-09-13 10:15
---

Installation d'une Linux [Ubuntu 6.06](http://www.ubuntu-fr.org/), sur
mon ordinateur portable, ce matin. J'avais déjà eu l'occasion de tester
Ubuntu sur un fixe, avec les déboires habituels de Wifi, de résolution
d'écran et de pilotes graphiques... Comme généralement, sur un
ordinateur portable, c'est pire, j'avais quelques craintes. Moralité :

-   Le Wifi fonctionne *right out of the box*. C'est la première
    installation Linux en 5 ans qui me fait ça, et Dieu sait que j'en ai
    testé - toutes les autres nécessitaient quelques heures en
    recherches de pilotes officieux, y compris la même Ubuntu sur mon PC
    fixe.
-   L'écran 16:10 est à la bonne résolution (1280x800) dès le démarrage.
-   Le trackpad marche impeccablement, avec toutes les fonctionnalités
    (clic central, zones de défilement, clic au toucher)
-   Les paquetages français s'installent sans aucun heurt.

Bref, rien à signaler - le meilleur compliment qui puisse se faire. A
toute l'équipe d'Ubuntu, chapeau bas.
