---
layout: post
title: "Un lecteur RSS/Twitter/Google Reader sympa ? Que oui !"
date: 2010-05-09 12:36
---

Amis utilisateurs de Google Reader et de Twitter, ou simplement à la
recherche d'un aggrégateur RSS sympa, j'ai peut-être quelque chose pour
vous. J'ai découvert hier [Feedly](http://www.feedly.com/), un lecteur
RSS basé (initialement) sur Google Reader, mais proposant une interface
et des options bien plus sympa.

[![L'interface principale de Feedly, qui présente vos articles à la
manière d'un
journal](/images/screenshots/feedly-secreenshot.png)](http://www.feedly.com/ "L'interface principale de Feedly, qui présente vos articles à la manière d'un journal")

Vous pouvez aggréger vos flux RSS, Twitter (et quelques autres sources),
ça se synchronise avec Google Reader si vous l'utilisez, et vous
retrouvez les fonctions de partage habituel (Partager sur Google Reader,
Twitter, bloguer, faire une note sur Facebook, envoyer par courriel,
etc).

Pour parcourir d'un coup d'œil une grande quantité d'articles, ou des
rubriques particulières, il faut dire que ce genre de présentation, ça
fonctionne bien ; et ça change de la vue Liste de Google Reader. Je ne
l'ai pour l'instant testé que quelques heures — mais vu d'ici, ça a
l'air d'être du tout bon. À tester.

**Edit:** Ouep, y'a des options sympa. Je recommande "*Mark as Read:
Older than one week/Older than one day*", ou la personnalisation de
l'affichage des articles, ou l'intégration avec Google Buzz (qui en
devient presque pratique)… Pas mal, pas mal.
