---
layout: post
title: "script.aculo.us"
date: 2006-03-28 11:49
---

Ce billet présente la librairie Javascript
[script.aculo.us](http://script.aculo.us). Cette librairie permet très
facilement :

-   D'animer des éléments dans une page web
-   D'ajouter des contrôles modernes : éléments triables par drag/drop,
    sliders, champs de texte auto-éditables
-   D'utiliser des fonctionnalités
    [Ajax](http://fr.wikipedia.org/wiki/AJAX) prédéfinies
    (autocomplétion, tri) ou personnalisées

Bref, une merveille, simple d'utilisation, qui peut donner à un site web
un réel plus en terme d'ergonomie. Lisez le billet complet pour une
présentation détaillée et quelques exemples d'utilisation de la
librairie.

#### Les animations

Les animations "candy-eye" sont de plus en plus fréquentes dans les
pages web. Si elles sont gratuites, elles distraient l'utilisateur...
mais elles peuvent aussi améliorer l'ergonomie d'un site complexe en
mettant l'accent sur le comportement.

script.aculo.us fournit quelques effets de base, ainsi qu'une multitude
de combinaisons d'effets. Implémenter un effet est très simple ; voyons
par exemple comment ajouter un effet de fondu à un bloc quelconque :

    [xml]<p onclick="new Effect.Fade(this)">Cliquez moi</p>

C'est tout ! Tous les autres effets sont implémentables de la même
façon. Vous trouverez sur script.aculo.us une [page de démonstration de
tous les
effets](http://wiki.script.aculo.us/scriptaculous/show/CombinationEffectsDemo)
; pour voir comment cela peut améliorer l'ergonomie, rendez vous sur
[script.aculo.us](http://script.aculo.us), decsendez un peu, et tapez
moins de trois lettres dans le champ de recherche.

------------------------------------------------------------------------

#### Contrôles

##### Tri et Drag/drop

Le drag/drop est extrêment pratique lorsqu'il s'agit de trier ou de
réorganiser des éléments. Il peut aussi être utilisé sur les sites
d'achat en ligne, pour ajouter des éléments au panier de manière
intuitive. script.aculo.us propose différents contrôles pour implémenter
facilement des méthodes de tri. Imaginons par exemple que nous voulions
trier une liste :

    [xml]<ul id="list">    <li id="item_1">I'm number 1</li>    <li id="item_2">I'm number 2</li>    <li id="item_3">I'm number 3</li></ul><script type="text/javascript">    Sortable.create('list');</script>

Cela suffit à créer un effet *Sortable*. On peut également rajouter une
demi-ligne de Javascript pour envoyer automatiquement une requête Ajax
après avoir terminé le tri, pour par exemple sauvegarder le nouvel ordre
des éléments :

    [xml]<script type="text/javascript">Sortable.create(  'list',  {onUpdate:function(){    new Ajax.Request(      '/list.php',      {parameters: Sortable.serialize('list')}    )}  };</script>

A chaque fin de tri (lorque l'élément est relâché), une requête Ajax
sera envoyée à la page "list.php", contenant en paramètre l'ordre des
éléments. Il est alors facile de sauvegarder l'ordre des éléments, par
exemple dans une base de donnée. Voici un exemple de [Tri de liste avec
callback Ajax](http://demo.script.aculo.us/ajax/sortable_elements).

On peut également [trier des éléments entre deux listes
différentes](http://wiki.script.aculo.us/scriptaculous/show/SortableListsDemo),
en les mélangeant éventuellement.

##### Sliders, champs auto-éditables

Les sliders implémentent un contrôle absent de HTML. Cela permet de
modifier avec précisions des valeurs, dans un certain intervalle. De
même que pour le Tri, il est possible d'envoyer une requête Ajax
contenant la valeur du Slider lorsqu'il est modifié. La [démo
proposée](http://wiki.script.aculo.us/scriptaculous/show/SliderDemo) est
assez moche, mais tout est stylable à vonlonté avec un peu de CSS.

Un autre contrôle très sympathique est le champ auto-éditable.
Popularisés par le site [Flickr](http://www.flickr.com/), ils vous
permettent d'éditer un texte en cliquant simplement dessus. Le texte
devient alors éditable, et peut être sauvegardé sans même recharger la
page, grâce à Ajax. Un exemple sera sans doute plus clair, vous pouvez
en trouver un [au milieu de cette
page](http://wiki.script.aculo.us/scriptaculous/show/Ajax.InPlaceEditor).

------------------------------------------------------------------------

#### Généralités Ajax et auto-complétion

##### Généralités Ajax

Tout d'abord, si vous voulez avoir une définition précise de ce que
recouvre le terme *Ajax*, rendrez-vous sur
[Wikipedia:Ajax](http://fr.wikipedia.org/wiki/AJAX).

script.aculo.us est basé sur la librairie
[Prototype](http://www.sergiopereira.com/articles/prototype.js.html),
qui possède de multiples fonctions très pratiques. Elle permet entre
autre d'envoyer facilement des requêtes Ajax fonctionnant dans la
plupart des navigateurs. Un exemple type d'utilisation des
fonctionnalités Ajax de Prototype est :

    [xml]<script type="text/javascript">new Ajax.Request(  url,   {  method: 'get',      parameters: 'data=somestuff',      onComplete: showResponse  });</script>

On peut difficilement faire plus simple : ce code envoie une requête
Ajax de base, avec les paramètres spécifiés, et apelle la fonction
`showReponse` lorsque la réponse à la requête a été reçue. On peut
également utiliser l'objet `Ajax.Updater` pour spécifier en plus l'id
d'un élément, dont le contenu sera mis à jour avec le résultat de la
requête.

##### Auto-complétion

En ce basant sur ce système, script.aculo.us propose un système
d'auto-complétion facile à mettre en oeuvre. Cet outil permet par
exemple d'afficher un aperçu des résultats dans un champ de recherche,
lorsque l'utilisateur a déjà tapé quelques lignes. Cela s'implémente
d'une manière semblable à un Tri, c'est à dire en une seule ligne. Il
suffit de définir le champ que l'on veut auto-compléter, l'adresse de la
page du serveur renvoyant le contenu de l'aperçu, et cela suffit. Le
résultat est quelque chose de très proche d'une application logicielle
classique. Vous pouvez [essayer la
démonstration](http://demo.script.aculo.us/ajax/autocompleter_customized)
ici.

------------------------------------------------------------------------

#### Conclusion

Impossible en ce moment de parler d'Ajax et de Javascript sans
mentionner ajaxWrite, un traitement de texte entièrement en ligne
permettant d'éditer des documents Word avec une interface très
similaire.

Il existe d'autres librairies que script.aculo.us, bien sûr, proposant
des fonctionnalités semblables. On peut citer
[Rico](http://openrico.org/), qui comporte des effets très intéressants,
comme la LiveGrid. Je préfère néanmoins script.aculo.us, pour sa
simplicité d'utilisation et le nombre impressionnant de fonctionnalités
qu'elle comporte.

Il ne me reste plus qu'à vous souhaiter bonne chance dans votre
découverte de script.aculo.us et de l'Ajax !
