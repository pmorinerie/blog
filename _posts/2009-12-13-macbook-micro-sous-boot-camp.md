---
layout: post
title: "Macbook : micro sous Boot Camp"
date: 2009-12-13 23:55
---

Un problème récurrent des Macbook (Pro) sous Windows est l'impossibilité
de faire fonctionner correctement le microphone intégré. Le problème
dure depuis des années — mais récemment de [nouveaux pilotes
Cirrus](http://discussions.apple.com/message.jspa?messageID=10539210#10539210)
ont fait leur apparition, et ont corrigé le problème pour moi. Jetez-y
un œil si vous rencontrez le même souci.
