---
layout: post
title: "Presse-papier"
date: 2007-05-30 22:56
---

Si vous avez l'habitude de composer vos courriels dans des Webmails
modernes (Gmail, Yahoo Mail, et autres Live Mail), ou de poster sur des
forums à l'aide d'éditeurs WYSIWYG, vous avez peut-être constaté
l'absence de copier/coller dans tous ces éditeurs avancé : en effet,
pour des raisons de sécurité, le copier/coller est désactivé par défaut
dans les navigateurs Web. Il fut un temps où ce n'était pas le cas, mais
lorsque des données confidentielles ont commencé à circuler, certaines
personnes se sont mises à se demander s'il était vraiment souhaitable
que des pages Web puisse accéder à votre Presse-papier, et à y lire, par
exemple, le numéro de Carte Bleue que vous vous apprêtiez à coller sur
Amazon. Microsoft a longtemps nié le problème, mais maintenant,
l'opération est désactivée sous la plupart des navigateurs. Donc, point
de copier/coller.

Il est toutefois possible de réactiver le copier/coller, de manière
globale ou pour certains sites. Pour Internet Explorer 6, l'option est
globale (à ma connaissance) et se trouve dans les options avancées du
navigateur, dans le groupe "Active Scripting", où l'on trouve un
"Autoriser les opérations de copier/coller". Ceci réactive la
possibilité pour les pages Web d'accéder au presse-papier, mais aussi la
potentielle brèche de sécurité associée.  
Internet Explorer 7, plus respectueux, vous demandera d'approuver
l'opération à chaque fois d'un site tentera d'accéder au presse-papier.
Simple, mais cela peut devenir rapidement répétitif. On peut bien sûr,
comme sous IE6, activer l'option de manière permanente, mais la faille
de sécurité revient.

Firefox a le comportement le plus granulaire. Il permet pour chaque site
d'autoriser ou d'interdire l'accès au presse-papier, de façon
permanente. Il s'agit en gros d'une liste blanche contenant les sites
autorisés à effectuer des opérations de copier/coller. Cette liste est
éditable à la main, mais une sympathique extension, [AllowClipboard
Helper](https://addons.mozilla.org/en-US/firefox/addon/852), fournit une
interface simple à ce système. Cette extension rajoute une entrée dans
le menu "Outils", qui mène vers une petite interface permettant de gérer
les sites autorisés à accéder au presse-papier, et d'ajouter le site en
cours. Un petit onglet d'alerte est également ajouté en bas des champs
éditables pour lesquels le copier/coller n'est pas encore autorisé. En
quelques clics, gmail.com et yahoo.fr rejoignent la liste, et le
copier/coller fonctionne comme il devrait, tout en vous gardant à l'abri
d'éventuelles pages mal intentionnées.

**[AllowClipboard Helper sur Mozilla
Addons](https://addons.mozilla.org/en-US/firefox/addon/852)**
