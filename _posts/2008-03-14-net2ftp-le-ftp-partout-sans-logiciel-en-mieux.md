---
layout: post
title: "net2ftp, le FTP partout sans logiciel (en mieux)"
date: 2008-03-14 10:29
---

Trouvé sur Presse-citron, un
[billet](http://www.presse-citron.net/?2008/03/12/3159-anyclient-le-ftp-partout-sans-logiciel)
présentant *AnyClient*, un client FTP écrit en Java et accessible depuis
n'importe quelle page Web. Le concept est fort sympathique, mais
nécessite tout de même Java — et puis impossible de l'installer sur son
propre serveur, du moins pas sans payer.

Je voulais donc présenter ma préférence à moi, **net2ftp**. Le principe
est le même : pouvoir accéder à un [serveur FTP depuis un navigateur
Web](http://www.net2ftp.com/), très utile dans un cybercafé ou derrière
une connexion publique qui bloque les accès FTP. L'interface est
sympathique et légère (entièrement en Html), et les fonctionnalités
puissantes : upload en groupe, compression/décompression de fichiers
zip, éditions de fichier directement sur le serveur, etc. Aucune
inscription n'est nécessaire, et tout fonctionne simplement.

[![net2ftp screenshot - Cliquez pour afficher l'image
complète](/images/divers/net2ftp_screenshot_thumb.gif)](/images/divers/net2ftp_screenshot.jpg)

Avantages par rapport à AnyClient : pas besoin de Java, cela
fonctionnera n'importe où. Et surtout net2ftp est Open Source et
librement téléchargeable : on peut l'installer sur son propre serveur,
voire en hacker les sources. Et ça, c'est drôlement sympathique.

Bref, un client accessible de partout, très simple, léger et puissant,
qui vaudrait presque un client logiciel classique — good choice. A
garder sous le coude.
