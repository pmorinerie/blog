---
layout: post
title: "Nouvelle rubrique : le fond d'écran de la semaine"
date: 2006-07-01 10:10
---

En triant mes fonds d'écran, je me suis aperçu qu'il y en avait quand
même un certain nombre de sympatiques ; j'ouvre donc cette rubrique "[le
fond d'écran de la semaine](/?Fonds-d-ecran)". Vous verrez que beaucoup
proviennent de DeviantArt, mais j'essaierai de varier les styles et les
contenus autant que possible :)

Pour commencer, voici un très beau travail de
[KoL](http://-kol.deviantart.com/), l'aboutissement d'une longue série,
[Strange World VI](http://www.deviantart.com/deviation/22703076/).

[![Cliquez pour
agrandir](/images/wallpaper/Strange_World_VI_by__kol.jpg)](http://www.deviantart.com/view/22703076/)

Disponible, comme d'habitude chez KoL, en version bi-écran ou non, le
fichier téléchargeable contient différents formats et ratios, donc
forcément un qui convienne à votre écran.
