---
layout: post
title: "Sexisme chez les geeks : Pourquoi notre communauté est malade, et comment y remédier"
date: 2013-03-21 21:45
---

> Cet article a l’ambition impossible de proposer un panorama assez
> complet des différentes formes de sexisme sévissant dans les
> communautés geeks; le sujet étant extrêmement vaste et protéiforme,
> l’article est massif en conséquence. Il comprend beaucoup d’exemples
> tirés du milieu gamer (…), cependant il est essentiel de comprendre
> que les mécanismes à l’œuvre sont les mêmes dans les communautés
> voisines – comics, hacking, programmation, JdR, Logiciel Libre… –
> communément regroupées sous l’appellation « geek ». Installez-vous
> confortablement, on en a pour un moment.

Effectivement, il y en a pour un moment : un bien bel article, en
longueur, très illustré, truffé d'exemples, pertinent. Bonne lecture.

**[Sexisme chez les geeks : Pourquoi notre communauté est malade, et
comment y
remédier](http://cafaitgenre.org/2013/03/16/sexisme-chez-les-geeks-pourquoi-notre-communaute-est-malade-et-comment-y-remedier/)**,
par [@Mar\_Lard](https://twitter.com/Mar_Lard)
