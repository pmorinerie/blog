---
layout: post
title: "Afficher les raccourcis clavier de Firefox"
date: 2007-06-11 22:07
---

Firefox est doté de nombreux raccourcis claviers, dont certains très
utiles, mais ils sont parfois difficiles à dénicher, ou alors on ne se
rappelle que des plus connus. La plupart des utilisateurs avertis
connaissent sans doute *Ctrl+N* pour ouvrir une nouvelle fenêtre,
*Ctrl+F* pour afficher la barre de recherche, et peut-être *Ctrl+T* pour
ouvrir un nouvel onglet, mais quid des autres ?

![asdf en fonctionnement](/images/divers/firefox_asdf.png)Toute la
question est de se rappeler des raccourcis utiles – et d'oublier les
autres. Il existe pour cela **une extension pour Firefox,
[asdf-jlk](http://code.google.com/p/asdf-jkl/)**, nommée d'après
l'ordonnancement de la deuxième ligne de touches d'un clavier QWERTY.
Une fois installée, **les raccourcis clavier de chaque élément de
l'interface sont affichés dans une infobulle** lorsque la souris survole
l'élément en question. Par exemple, survoler la barre d'adresse
affichera une infobulle marquée *Ctrl+L* – de même pour le champ de
recherche, qui affichera le raccourci *Ctrl+K*. L'extension rajoute
aussi une entrée au menu Affichage de Firefox, menant à une page interne
au navigateur listant les [raccourcis clavier de
Firefox](http://www.mozilla.org/support/firefox/keyboard).

Bref, une fonctionnalité légère, non intrusive, et extrêmement pratique.
Have fun :)

**[asdf-jkl - Learn your browser shortcuts - Google
Code](http://code.google.com/p/asdf-jkl/)**
