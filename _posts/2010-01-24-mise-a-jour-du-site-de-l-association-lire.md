---
layout: post
title: "Mise à jour du site de l'association L.I.R.E."
date: 2010-01-24 21:36
---

![Animateurs et volontaires devant la bibliothèque municipale de
Kpalimé. En bleu au centre, Koffi Degboevi, président de
L.I.R.E.](http://www.asso-lire.org/wp-content/uploads/2009/12/animateurs2.jpg)Les
membres de [l'association L.I.R.E.](http://www.asso-lire.org/), après
une lutte épique avec l'Internet togolais, ont réussi à nous faire
parvenir des documents et des ressources pour mettre à jour le site web
de l'association. Nous avons donc pu poster trois nouveaux billets,
documentant les [activités de ces derniers
mois](http://www.asso-lire.org/dernieres-nouvelles/) - and there was
much rejoicing.

Oh, et une volontaire belge va partir les rejoindre en mars, ce qui me
réjouit beaucoup. D'ailleurs, si vous connaissez des gens qu'un
**[volontariat au
Togo](http://www.asso-lire.org/partir-comme-volontaire/)** serait
susceptible d'intéresser, faites circuler l'information !
