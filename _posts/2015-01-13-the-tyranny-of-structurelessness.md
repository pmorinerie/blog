---
layout: post
title: "The tyranny of structurelessness"
date: 2015-01-13 21:29
---

Je suis tombé par hasard (plus ou moins) sur [ce
texte](http://www.jofreeman.com/joreen/tyranny.htm), qui parle de
l'organisation des mouvements féministes – mais applicable bien sûr à
plein d'autres organisations.

Sa thèse : les mouvements sans structure n'existent pas – il n'y a que
des mouvements sans structure formelle. Au delà d'une certaine taille,
une élite invisible se forme, et exerce le pouvoir de manière informelle
mais réelle, et bien peu collective.

Il y a beaucoup de détails et d'analyses (comment apparaissent ces
élites informelles ? comment faire grandir un mouvement ?), mais surtout
une chouette partie sur le thème « Comment structurer un mouvement
démocratique ».

J'en colle l'extrait ici :

> 1\) **Delegation of specific authority to specific individuals for
> specific tasks by democratic procedures**. Letting people assume jobs or
> tasks only by default means they are not dependably done. If people are
> selected to do a task, preferably after expressing an interest or
> willingness to do it, they have made a commitment which cannot so easily
> be ignored.
>
> 2\) **Requiring all those to whom authority has been delegated to be
> responsible** to those who selected them. This is how the group has
> control over people in positions of authority. Individuals may exercise
> power, but it is the group that has ultimate say over how the power is
> exercised.
>
> 3\) **Distribution of authority** among as many people as is reasonably
> possible. This prevents monopoly of power and requires those in
> positions of authority to consult with many others in the process of
> exercising it. It also gives many people the opportunity to have
> responsibility for specific tasks and thereby to learn different skills.
>
> 4\) **Rotation of tasks among individuals**. Responsibilities which are
> held too long by one person, formally or informally, come to be seen as
> that person's "property" and are not easily relinquished or controlled
> by the group. Conversely, if tasks are rotated too frequently the
> individual does not have time to learn her job well and acquire the
> sense of satisfaction of doing a good job.
>
> 5\) **Allocation of tasks along rational criteria**. Selecting someone
> for a position because they are liked by the group or giving them hard
> work because they are disliked serves neither the group nor the person
> in the long run. Ability, interest, and responsibility have got to be
> the major concerns in such selection. People should be given an
> opportunity to learn skills they do not have, but this is best done
> through some sort of "apprenticeship" program rather than the "sink or
> swim" method. Having a responsibility one can't handle well is
> demoralizing. Conversely, being blacklisted from doing what one can do
> well does not encourage one to develop one's skills. Women have been
> punished for being competent throughout most of human history; the
> movement does not need to repeat this process.
>
> 6\) **Diffusion of information** to everyone as frequently as possible.
> **Information is power**. Access to information enhances one's power.
> When an informal network spreads new ideas and information among
> themselves outside the group, they are already engaged in the process of
> forming an opinion -- without the group participating. The more one
> knows about how things work and what is happening, the more politically
> effective one can be.
>
> 7\) **Equal access to resources** needed by the group. This is not always
> perfectly possible, but should be striven for. A member who maintains a
> monopoly over a needed resource (like a printing press owned by a
> husband, or a darkroom) can unduly influence the use of that resource.
> Skills and information are also resources. Members' skills can be
> equitably available only when members are willing to teach what they
> know to others.

[Jo Freeman – The tyranny of
structurelessness](http://www.jofreeman.com/joreen/tyranny.htm)
