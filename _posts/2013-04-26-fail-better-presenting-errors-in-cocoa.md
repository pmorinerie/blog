---
layout: post
title: "Fail better — Presenting errors in Cocoa"
date: 2013-04-26 13:33
---

Here are the slides of the talk I gave at CocoaHeads Paris in April.

> It's OK for software to fail — but it should display helpful errors.
> And the "helpful" part is not optional. "-presentError:" is a nice API
> to build powerful mechanisms for presenting helpful errors in Cocoa
> applications.
>
> The "Demo" part is [available on
> GitHub](https://github.com/kemenaran/ios-presentError). A good
> implementation of -presentError: on iOS can also be found there:
> [github.com/hectr/ErrorKit](https://github.com/hectr/ErrorKit)

<script async class="speakerdeck-embed" data-id="9e9c1790908501300c611e948e71a639" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>
<noscript>
https://speakerdeck.com/kemenaran/fail-better-presenting-errors-in-cocoa
</noscript>
