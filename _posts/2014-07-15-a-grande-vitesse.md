---
layout: post
title: "À Grande Vitesse"
date: 2014-07-15 10:56
---

Petite présentation de l'application iPhone de [Capitaine
Train](https://www.capitainetrain.com/welcome/6d7b20). À votre avis, si
vous êtes un nouvel utilisateur, combien de temps faut-il pour chercher
et réserver un billet prêt à être payé ?

<div style="text-align:center">
<iframe width="420" height="315" src="//www.youtube-nocookie.com/embed/ohNrsxXKFRo?rel=0" frameborder="0" allowfullscreen>
</iframe>
</div>
