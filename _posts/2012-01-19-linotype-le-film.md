---
layout: post
title: "Linotype: Le film"
date: 2012-01-19 10:29
---

Après un bref aperçu créé l'année dernière (que j'avais partagé sur le
défunt Google Reader), voici la bande-annonce complète du film
"Linotype", dédié aux machines d'imprimeries du même nom.

La mécanique est fascinante.

<div style="text-align:center">

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/avDuKuBNuCk" frameborder="0" allowfullscreen>
</iframe>

</div>
