---
layout: post
title: "Viens voir ce site !"
date: 2006-03-27 22:24
---

Après 6 longs mois de travail, "[Viens voir ce site
!](http://www.viensvoircesite.com)" est enfin lancé ! Il s'agit d'un
site destiné aux créatifs et professionnels de la publicité, réalisé
pour le compte de [Comkillers](http://www.comkillers.com). J'ai du
reprendre une ancienne version du site, qui avait été codée très
rapidement (et donc très mal), alors il y a beaucoup de choses moches...
Mais également des tas de fonctionnalités sympatiques : galerie, books,
Lightbox pour les images, Player Flash pour les sons, etc.

Il y a également pas mal d'Ajax, qui est utilisé un peu partout :

-   Autocomplétion dans le champ de Recherche
-   Chatterbox auto-rafraîchie
-   Glisser/déposer pour organiser la mise en page de sa fiche

Le tout réalisé dans les règles du *Hijax* : implémentation d'une
version standard fonctionnant sans script activé, avant d'insérer du
Javascript non obstrusif pour activer l'Ajax. Je vous reparlerai sans
doute des techniques et librairies utilisées dans un autre billet.

Bref, vous pouvez aller jeter un coup d'oeil, et éventuellement me dire
ce que vous en pensez. Pour information, le site a été testé sous IE
5.5/6/7b, Firefox 1.0\*/1.5\*, Opera 8, Konqueror, Safari 1.2/1.3 et
Netscape 7.
