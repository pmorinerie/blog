---
layout: post
title: "Tester Chrome sous Linux ou Mac OS X avec Wine"
date: 2008-09-17 09:12
---

![Logo de Chromium](/images/logos/chromium-logo.png)On a beaucoup
entendu parler de **[Chrome](http://www.google.com/chrome)**, le
navigateur de Google, depuis sa sortie en version bêta il y a deux
semaines. Interface sobre, moteur de rendu WebKit, mode incognito... et
la promesse d'une **future version multi-plateforme** (ou au moins
Windows/Mac/Linux).

Mais pour l'instant, niveau multi-plateforme, c'est le désert. Nada.
Chrome est Open-Source, et peut donc être compilé librement — la version
libre s'appelle [Chromium](http://code.google.com/chromium/). Chromium
compile et se lance sans problèmes sous Windows, mais ne compile que
quelques binaires de test sous Linux, et quasiment rien sous Mac : tout
le travail de portage reste encore à faire.

#### Un défi : "porter" Chromium en deux semaines

![Logo de Wine](/images/logos/wine-logo.png)Et c'est là que
[Wine](http://www.winehq.org/) rentre en scène. Wine, c'est ce programme
Open-Source permettant de **faire tourner des applications Windows sous
Unix** : il permet donc de lancer une application Windows en ".exe" sous
Linux ou Mac OS X. Ce n'est pas un émulateur : il exécute le code natif
du programme, mais en replaçant tous les appels à des fonctions de l'API
Windows par son implémentation propre. Wine est en développement depuis
plus de 15 ans, et fonctionne aujourd'hui assez bien : il fait tourner
entre autres Office, Photoshop, Picasa, et même de nombreux jeux comme
Oblivion ou Half-Life 2.

Dès la sortie de Chrome, pour Windows seulement, les développeurs de
CrossOver (la version commerciale de Wine) se sont demandé s'il serait
possible d'**utiliser Wine pour faire tourner Chromium sous Linux ou Mac
OS X**. Après quelques jours seulement, et quelques patchs rajoutés à
Wine, Chromium fonctionnait en grande partie, et permettait naviguer sur
Internet. Seul bémol, le support du HTTPS, le protocole sécurisé utilisé
par les sites marchants, les banques, et par de nombreux autres sites
Web. Pour cela, les développeurs de CrossOver ont dû implémenter une
bonne partie des fonctions du fichier "winhttp.dll", qui contient l'API
Windows relatif aux connections HTTP. Une semaine de plus pour hacker
vite fait une implémentation de base ; et voilà, depuis avant-hier,
[Chromium fonctionne totalement sous
Wine](http://www.codeweavers.com/about/people/blogs/jwhite/2008/9/15/fire-drills-and-proving-a-point).

[![Capture d'écran de Chromium tournant sous Mac avec
Wine](/images/screenshots/shot_chromium_mac_thumb.png)](http://www.codeweavers.com/images/products/shot_chromium_mac.png "Capture d'écran de Chromium tournant sous Mac avec Wine")

#### Tester Chromium sous Linux ou Mac OS X

CrossOver propose de beaux paquetages pour tester Chromium ; on
télécharge les fichiers, on lance tout ça, et, après un petit délai
d'initialisation, la fenêtre de Chromium apparaît. Tout est étonnamment
fluide et fonctionnel — on s'aperçoit à peine qu'en dessous, c'est Wine
qui travaille.

Pour télécharger ces paquetages, rendez vous sur la page **[CrossOver
Chromium](http://www.codeweavers.com/services/ports/chromium/)**, et
suivez les instructions :)

Le navigateur est décidément plutôt stable — sans doute pas de quoi en
faire son navigateur quotidien, mais cela fonctionne tout de même très
bien.

#### Une opération médiatique ?

![Logo CrossOver](/images/logos/crossover-logo.png)Beaucoup de
commentaires ont été émis sur Internet après cette annonce. Certains se
demandent notamment quel est le besoin d'avoir un navigateur Web de plus
sous Linux — à plus forte raison si les développeurs de Google n'ont pas
encore fait l'effort de faire **un vrai portage vers d'autres
systèmes**. D'autres enfin se demandent si tant d'efforts investis dans
le développement de Wine ne seraient pas mieux placés dans le portage
natif d'application vers Linux, voire vers le développement
d'applications *Linux-only* capables de mieux concurrencer en qualité
les applications Windows. Certains dénoncent enfin un **coup
médiatique**, qui au final n'apporte pas grand chose à Wine ou à la
communauté du Libre.

Il faut voir d'abord que ce "coup médiatique" est profitable sur le long
terme : **le travail réalisé par CrossOver pour faire tourner Chromium
va être [intégré dans
Wine](http://www.winehq.org/pipermail/wine-devel/2008-September/069138.html)**
prochainement, ce qui profitera bien sûr à Wine sur le long terme.
D'autre part, ce genre de *hackathlon* fortement publicisé est après
tout un bon moyen de motiver les développeurs, qui s'intéressent alors à
des domaines peu explorés auparavant, ou trouvent une vraie motivation à
déboguer des pans de code qui ne recevraient autrement pas beaucoup
d'attention.

D'autre part, c'est aussi une manière de faire savoir à un large public
que **oui, on peut faire tourner la plupart des programmes Windows sous
Linux** (ainsi que sous Mac OS X et beaucoup d'autres Unix). Cette
publicité profite bien sûr avant tout à CrossOver, qui édite une
solution commerciale basée sur Wine, mais retombe tout de même un peu
sur Wine lui-même, ainsi que sur Linux en général.

Enfin, je pense que Linux a somme toute besoin de cette compatibilité
avec Windows, même si elle apporte des inconvénients ; je supporte donc
plutôt les initiatives comme Wine ou Mono, qui visent à réaliser cette
compatibilité. Et en attendant, même si vous ne vous servez pas de Wine
au quotidien, c'est toujours l'occasion de **tester Chromium
facilement** si vous ne disposez pas d'un Windows sous la main :)
