---
layout: post
title: "Accélérer le démarrage de Thunderbird"
date: 2007-09-07 18:01
---

![Logo Thunderbird](/images/divers/thunderbird_logo.png)Après avoir
migré mon profil Thunderbird de Windows à Mac, Thunderbird démarrait
très lentement. Pour résoudre ce problème, divers blogs et un acticle de
la [MozillaZine Knowledge
Base](http://kb.mozillazine.org/Disappearing_mail) m'ont mis sur la
piste des fichiers d'index de Thunderbird, stockés dans le Profil sous
l'extension '**.msf**'.

Effacer ces fichiers '\*.msf' force Thunderbird à reconstruire l'index
des boîtes courriel au redémarrage, et, dans mon cas, a résolu mon
problème de démarrage lent. Avant de procéder, un
[compactage](http://kb.mozillazine.org/Thunderbird_:_Tips_:_Compacting_Folders)
peut être utile : il supprimera de vos boites courriel les messages
effacés, mais pas encore physiquement supprimés.
