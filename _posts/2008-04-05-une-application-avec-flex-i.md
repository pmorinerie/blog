---
layout: post
title: "Une application avec Flex - I"
date: 2008-04-05 12:37
---

Dans cette chronique, je vais détailler mes expériences de développement
avec Flex, la technologie d'Adobe.

J'ai été promu il y a quelques temps grand
grand-manitou-chef-programmeur d'une association, Antigone, visant à
encourager l'apprentissage du grec ancien — et développant pour se faire
un logiciel d'apprentissage du grec ancien. Je suis donc chargé de
concevoir et développer ce logiciel.

#### Choix d'une technologie

Je suis parti sans idées arrêtées quand à la technologie à employer. A
vrai dire, je cherchais quelque chose de moderne et sympa : rien de pire
que de s'enferrer dans une technologie trop contraignante qui fait sans
cesse obstacle au développement. J'ai donc un peu tâté du côté de .NET
et WPF, en ayant plutôt de bons résultats.

Quoi qu'il en soit, les prérequis étaient les suivants :

-   **Portabilité** : le logiciel doit fonctionner sous plusieurs OS,
    dont un système libre (typiquement Linux).
-   **Personalisation** : l'apparence du logiciel doit pouvoir être
    facilement et fortement modifiée - pas de widgets natifs non
    personnalisables, quoi.
-   **Multimédia** : le logiciel doit pouvoir rendre des textes en
    caractères grecs, enregistrer la voix, restituer du son et
    éventuellement de la vidéo.

![Logo Microsoft .NET](/images/logos/logo_microsoft_.NET.png)J'ai donc
commençé par quelques tests en utilisant
**[WPF](http://www.microsoft.com/net/wpf.aspx)** : une technologie
moderne, souple, et basée sur .NET que j'aime bien. L'interface et les
widgets sont personalisables à loisir, il y a plein de capacités
multimédias, et des bibliothèques annexes le cas échéant. Bref, la
panacée — mais pas portable. Le .NET pur peut tourner sous Linux et Mac,
grâce à Mono, mais WPF reste pour l'instant du pur Windows. A exclure
pour des raisons de portabilité, donc.

![Logo
Firefox](/images/logos/logo_firefox.png)**[XUL](http://developer.mozilla.org/en/docs/XUL)**
m'a bien fait de l'œil : c'est la technologie derrière Firefox. J'y
avais un peu touché en écrivant mon extension pour Thunderbird, et, il
faut dire ce qui est, c'est fort sympathique — ainsi que très portable
et hautement customisable par un peu de CSS. Plus la possibilité de
programmer en Javascript, qui est un langage que j'aime bien. Seul
souci : les capacités multimédias. Pour en intégrer, il aurait fallu me
servir d'un plugin du type Flash, ou en recoder en C++. Bref, pour un
projet multimédia, j'avais moyennement confiance : j'ai fini par
l'écarter.

![Logo Qt](/images/logos/logo_qt.png)J'ai alors pensé à **C++** : en
choisissant un bon système de widgets, cela devait être possible. Et
justement, Trolltech sortait en grande fanfare sont produit phare,
**[Qt](http://trolltech.com/products/qt)** 4.3. J'avais déjà jeté un
coup d'œil à Qt, qui est décidément une très chouette technologie,
vraiment agréable à manipuler ; c'est très portable, il y a quelques
capacités multimédias, et la personnalisation est hautement possible, à
l'aide de pseudo-CSS. Mais bon, le C++ n'est pas vraiment ma tasse de
thé — je préfère les langages d'un peu plus haut niveau, que ce soit du
genre Java/.NET ou plus genre script PHP/Javascript. Enfin pourquoi pas.

![Logo Java](/images/logos/logo_java.png)D'ailleurs, en y repensant,
il y avait **Java**. Proche de .NET, j'en avais déjà manipulé ;
portable, et relativement élégant. De plus, il y avait la possibilité
d'utiliser **[Jambi](http://trolltech.com/products/qt/jambi)**,
l'interface Java de Qt. Tous les avantages de Qt, plus du Java, donc :
pas mal. Cela dit, je connais plus .NET que Java ; cela donne souvent un
sentiment de plus grande propreté et modernité dans .NET que dans Java,
qui traîne quelques archaïsmes que j'avais moyennement envie de
retrouver. Rien de très concret, en tout cas ; c'était pour le moment
mon meilleur choix.

#### Flash, Flex et AIR

![Logo Flex](/images/logos/logo_flex.png)J'ai enfin regardé rapidement
du côté de **[Flex](http://flex.org/)**. Pour ce que j'en savais,
c'était une technologie similaire à XUL ou WPF, permettant de concevoir
des interfaces en XML et de les contrôler à l'aide d'un langage de
script. Basée sur Flash, je pouvais donc m'attendre à de la portabilité,
à une bonne personnalisation graphique, et à un très bon support
multimédia. En creusant un peu, j'ai confirmé ces impressions — et
surtout j'ai découvert AIR, à l'époque en version bêta.

![Logo Adobe
Air](/images/logos/logo_adobe_air.png)**[AIR](http://www.adobe.com/products/air/)**,
est principalement un enrobage d'intégrer du Flash dans des applications
de Bureau. L'emballage-desktop est très léger, il s'agit d'à peine plus
qu'un fichier de configuration détaillant le nom et la taille de la
fenêtre à employer ; AIR génère ensuite un exécutable, un installeur, et
permet de lancer son projet Flash dans une fenêtre. Du simple et du bon,
donc.

Concernant le développement concret en utilisant AIR, le développeur a
le choix entre trois technologies : XHTML/CSS, Flash ou Flex. Le XHTML
est ce qu'il y a de plus connu et de plus simple ; l'application est
alors rendue dans un navigateur embarqué. Le Flash permet d'héberger un
module Flash classique dans un conteneur AIR. Enfin, Flex permet de
créer une application riche en utilisant des fenêtres natives de l'OS.
La bibliothèque de contrôles utilisateurs disponibles avait l'air
importante, et les quelques exemples que j'ai téléchargé ont achevé de
me convaincre.

#### Conclusion

Finalement, AIR/Flex remplissait toutes mes contraintes, et me
permettait en plus d'utiliser une technologie moderne, des interfaces en
XML et du Javascript : tout pour plaire. Après beaucoup d'hésitations,
je suis donc parti là dessus.

Dans la prochaine chronique, je raconterai ma quête d'un framework MVC
pour Flex, qui soit à la fois léger et adapté à mes besoins.
