---
layout: post
title: "Christmas Night"
date: 2008-05-16 10:54
---

Mon fond d'écran actuel provient de l'excellent
[Vladstudio](http://www.vladstudio.com/) — méfiez vous des imitations.
Pleins de beaux fonds d'écrans, dont certains déjà postés dans ces
colonnes.

En ce moment, donc, une variante de [Christmas
Night](http://www.vladstudio.com/wallpaper/?438).

[![Vladstudio wallpaper — Christmas Night
2](/images/wallpaper/vladstudio_xmasnight2.jpg)](http://www.vladstudio.com/wallpaper/?438)

A noter la publication par Vlad d'un [tutoriel Photoshop basé sur ce
wallpaper](http://www.vladstudio.com/photoshoptutorials/?20), ce qui est
vraiment chouette — je vais y jeter un œil, ça a l'air tout bien.
