---
layout: post
title: "Un robot qui a du chien"
date: 2008-03-19 22:34
---

Les robots qui marchent, on en a tous vu — et généralement ce n'est pas
glorieux. Maladroite, déséquilibrée, lente et peu fluide, la démarche de
ces machines est rarement convaincante... Mais **BigDog** change la
donne. Développé par la société BostonDynamics, avec le soutien de la
branche Recherche de l'armée américaine, ce robot quadrupède à la
démarche fluide et relativement rapide peut transporter jusqu'à 150kg de
matériel. Et surtout, il fonctionne en dehors des laboratoires : les
côtes, les éboulis, la neige et même la glace, rien ne l'arrête. Les
séquences où l'on voit l'engin [rétablir son équilibre après une
glissade sur de la glace](http://www.youtube.com/watch?v=W1czBcnX1Ww)
(ou un violent coup de pied déstabilisateur !) sont époustouflantes.

[![BigDog sur une surface
gelée](/images/divers/bigdog.jpg)](http://www.youtube.com/watch?v=W1czBcnX1Ww "Voir une vidéo de BigDog sur de la glace")

Alors bien sûr, les questions et commentaires se succèdent. Beaucoup,
tout en saluant la prouesse technique, se demandent quelles pourraient
être les applications militaires concrètes (puisque le projet est
financé par l'armée), sans en trouver vraiment. A mon avis, c'est
surtout que nous sommes incapables pour l'instant de voir la portée et
d'imaginer les applications d'une telle technologie, à laquelle nous ne
sommes pas vraiment préparé. D'autres glosent sur les ressemblances de
cet engin avec les quadripodes de Star Wars, et prédisent la défaite de
ces robots par de petits drones qui voleraient autour en enroulant des
filins autour de leurs jambes.

Mais le sentiment commun semble être un ébahissement doublé d'une vague
gêne. Ce robot a vraiment des airs d'animal, et voir sauter comme un
cabri ou patiner comme un chien sur de la glace ce qui reste une machine
a un effet étrangement déstabilisant. Nous sommes sans doute en plein
dans l'[Uncanny Valley](http://en.wikipedia.org/wiki/Uncanny_Valley), la
Zone d'Etrangeté dans laquelle un robot ne ressemble pas totalement à un
être vivant, acceptable par notre esprit, mais déjà trop pour que nous
puissions simplement l'étiqueter comme "machine". *Weird times*...

PS : Je suis le seul à trouver que la démarche de BigDog ressemble
furieusement à celle des [Hunters de Half-Life
2](http://www.youtube.com/watch?v=R8NTEksHkjI) ?[![Half-Life 2
Hunter](/images/divers/250px-Hl2ep2_ministrider2.jpg)](http://en.wikipedia.org/wiki/List_of_humanoid_and_synthetic_Combine_in_Half-Life_2#Hunter)
