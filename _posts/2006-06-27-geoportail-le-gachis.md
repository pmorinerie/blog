---
layout: post
title: "Géoportail - le gâchis"
date: 2006-06-27 10:40
---

Quand même, quel dommage. On nous promettait monts et merveilles, une
technologie surpassant [Google Earth](http://earth.google.com/), une
précision de cartographie et de photographie inégalée, des randonnées en
3D... et au final personne n'a pu dépasser la page d'accueil de
[Géoportail](http://www.geoportail.fr/) - pour ceux qui sont parvenus à
la charger.

On pensait que l'affluence de la [télédéclaration d'impôt de l'année
dernière](http://www.pcinpact.com/actu/news/Les_contribuables_etranglent_le_site_impotsgouvfr.htm)
aurait échaudé les esprits, mais rien n'y fait : l'administration
française est incapable de répondre rapidement à des besoins élevés en
bande passante. Déboussolé, L'IGN a décidé de fermer l'accès à
Géoportail *(pourquoi pas)*, de mettre en place un système
d'invitations, de liste d'attente et d'indiquer les plages d'affluences
*(on eut aimé voir un tel système plus tôt)*, et... de lancer un bel
appel d'offre public pour de nouveaux serveurs. Le code des marchés
publics est strict, mais interdit-il les miroirs temporaires ? En tout
cas, pour répondre à la demande dans l'immédiat, niet, rien n'est
prévu ; en France, on temporise d'abord, on résoud les problèmes
ensuite. Enfin, au moins ils n'ont pas encore mis en place de
[taxe](http://www.pcinpact.com/actu/news/29045-Union-europeenne-et-si-on-taxait-les-mails-e.htm)...

A la décharge de l'équipe de Géoportail - et surtout de ses
administrateurs réseaux -, ils pensaient avoir [prévu
large](http://www.generation-nt.com/actualites/16245/geoportail-ign-excuses-serveurs-satures/),
en tablant sur un million de connexions par jour ; mais ce sont
finalement six millions d'utilisateurs par jour qui ont déboulé durant
tout ce week-end.

La [page d'excuses](http://www.geoportail.fr/excuses.htm) de Géoportail
est de nouveau accessible, on attend une remise en service progressive
en milieu de semaine. Mais fichtre, quelle gâchis...
