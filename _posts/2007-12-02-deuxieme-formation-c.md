---
layout: post
title: "Deuxième formation C#"
date: 2007-12-02 18:12
---

Voici les diaporamas de la deuxième formation C\# qui s'est déroulée ce
jeudi — accompagnés du fichier de TP et d'exemples mis à jour.

[Diaporama Formation C\# - Partie
2](http://www.slideshare.net/kemenaran/formation-c-cours-2-programmation-procdurale)  
[Diaporama Formation C\# - Partie
3](http://www.slideshare.net/kemenaran/formation-c)  
[Formation C\# - Exemples et
TPs](http://winosx.com/hosted_files/TP%20et%20Exercices%20CSharp.zip)
(mis à jour)
