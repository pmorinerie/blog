---
layout: post
title: "Passer des films à Athènes"
date: 2016-09-27 08:51
---

« Hé, on pourrait montrer des films et des dessins animés aux enfants le samedi soir, ça serait bien ! »

La riche idée que j’ai eue.

## Je peux avoir les sous-titres ?

À Athènes, dans les lieux d’accueil des réfugiés et exilés, on parle beaucoup de langues. Les personnes venues de Syrie ou d’Irak parlent arabe. Les personnes venues d’Iran ou d’Afghanistan parlent farsi. Les personnes venues d’encore ailleurs, du Soudan, de Libye… ça dépend. Certaines ont appris l’anglais à l’école ; d’autres sont en train de l’apprendre sur place. Nombreux sont celles et ceux qui prennent des cours de langue : anglais, grec, allemand, français, espagnol… On sent une vraie envie d’apprendre.

Et depuis la rentrée, les enfants vont à l’école grecque. Le gouvernement s’est arrangé pour que les enfants réfugiés puissent être accueillis dans des classes grecques, et aller à l’école. C’est impressionnant : le système scolaire a subi des coupes sévères, et ils se débrouillent encore pour accueillir tous ces enfants. Wow.

Mais c’est assez récent, donc pour l’instant les enfants parlent leur langue maternelle, peut-être aussi un peu d’anglais, et ils commencent tout juste le grec. Bon.

Comment montrer un film ou un dessin animé que tous les enfants pourront comprendre ? Il faudrait au minimum l’arabe _et_ le farsi.

Au passage, même si le farsi s’écrit avec des caractères arabes, ces deux langues n’ont rien à voir : elles ont à peu près autant en commun que le français et l’allemand. _(« – Ah mais pourtant le français et l’allemand ça s’écrit avec les mêmes lettres, vous devriez vous comprendre ! – Bin non, en fait. »)_

<h2 title="♫ « Ce rêve bleuuuuuu ! » ♪">!دى دنيا فوق</h2>

Après quelques recherches, il s’avère que les dessins animés doublés en arabe sont rares. Et en ce qui concerne le farsi, les doublages semblent inexistants. Il y a sûrement une production locale, mais faute de maîtriser la langue, j’ai du mal à tomber dessus. Finalement les seuls dessins animés correctement doublés en arabe sont les grosses productions : Pixar et Disney. Ça tombe bien, c’est plutôt des bons films.

Et autre bonne nouvelle : si les doublages en farsi n’existent pas, les sous-titres, eux, se trouvent très bien.

Il n’y a plus qu’à récupérer les dessins animés populaires de Disney et Pixar en arabe, et de trouver des sous-titres en farsi. Comme ça les enfants arabophones entendront l’arabe, les enfants persanophones liront le farsi (et les enfants persanophones ne sachant pas encore lire, heu… se feront traduire par les autres).

![Le Roi Lion avec des sous-titres en farsi](/images/lion-king-farsi.jpg "♫♩ « C’est l'histoire de la vie ! » ♪")

## Une aiguille dans une botte de langues

Les sites de téléchargement habituels ne sont pas vraiment taillés pour les langues non-occidentales : il m’a fallu plusieurs heures pour mettre la main sur la version arabe de quelques films. Finalement j’ai récupéré des rips bizarres de _Toy Story 2_, _Le Roi Lion_ et _Le monde de Nemo_, en arabe, en piochant sur différentes scènes de téléchargement arabophones. C’est tout ce que j’ai trouvé, mais ça allait bien.

Pour les sous-titres en farsi, beaucoup plus facile : un coup de [YIFY subtitles](https://www.yifysubtitles.com), et c’est plié.

Mais, première surprise : les sous-titres ne s’affichent pas correctement. Après quelques bricoles, il s’avère qu’ils sont encodés bizarrement, en `Window 1256` plutôt qu’en `Arabic ISO 8859-6`. Car oui, en 2016 il y a encore des sites réputés qui utilisent des encodages chelous plutôt que de l’UTF-8.

Ensuite, ces fameux rips bizarres n’ont pas la durée standard des films habituels : les sous-titres sont donc désynchronisés.

Il ne me reste donc plus qu’à re-synchroniser une piste de sous-titres dans une langue que je ne comprends pas, avec l’audio d’une langue que je ne comprends pas non plus. Facile.

Pour simplifier le tout, les sous-titres ne sont pas seulement décalés, mais aussi étirés dans le temps : ils se décalent au fur et à mesure. Il faut donc trouver deux points de synchronisation, faire coïncider l’affichage théorique des sous-titres avec l’affichage constaté, interpoler, utiliser un outil comme [Subshifter](http://subshifter.bitsnbites.eu/) pour étirer ou compresser les durées, vérifier que les sous-titres apparaissent enfin à peu près quand les personnages parlent, jusqu’à la fin du film, sans décalage…

C’est en faisant ces recherches que je suis tombé sur mon nouveau héros : un type qui collectionne les différents doublages des dessins animés de Disney, dans toutes les langues. Par exemple voici l’archive pour [Rapunzel](https://www.reddit.com/r/Frozen/comments/346wq2/tangleds_complete_dub_collection_40_languages/), qui comprend la même piste audio en 40 langues. Et voilà celle pour [La princesse de glace](https://www.reddit.com/r/Frozen/comments/2hfxar/complete_dub_collection_42_languages/), avec 43 langues. Et parmis ces langues, l’arabe !

Bien sûr ces archives ne comprennent que les pistes audio. Il faut donc :

- Télécharger la piste audio en arabe,
- Télécharger une version correcte du film _(pas de trop mauvaise qualité, c’est moche, mais pas trop haute non plus, sinon le vieil ordinateur portable sur lequel on projette ne sera pas assez puissant)_,
- Injecter dans la vidéo originale la piste audio doublée _(avec un outil comme [Subler](https://subler.org/), qui fait des merveilles)_,
- Ajouter les sous-titres en farsi _(et aussi en anglais pour faire bonne mesure, au cas où)_.

Au final, cinq dessins animés, sous-titrés en arabe et doublés en farsi.

## On vote ?

Avant que je ne retourne à Paris, on a pu faire une première projection à la _Orange House_. Il y a eu des questions sans fin pour savoir si _Le Roi Lion_ n’était pas trop triste, si _Le Monde de Nemo_ ne parlait pas trop de séparation, si _Toy Story 2_ ne faisait pas trop peur…

Au final on a simplement demandé leur avis aux enfants (c’est la moindre des choses), en leur montrant les affiches des films disponibles. _Rapunzel_ est arrivée en tête : ça sera donc le premier film, et _Le Roi Lion_ en second sera pour le lendemain.

Avec une clef USB, j’ai essayé de disséminer ces films autant que possible : avec un peu de chance d’autres squats pourront aussi organiser des projections, et ça sera bien.
