---
layout: post
title: "Le Conjugueur"
date: 2006-11-14 10:56
---

A force de buter sur la conjugaison de certains verbes courants, j'ai
fini par faire fréquemment appel aux services du
[Conjugueur](http://www.leconjugueur.com/), qui est une sorte de
Bescherelle en ligne.

Pour rendre son usage plus pratique encore, le Conjugeur propose un
petit plugin de recherche pour Firefox. Il suffit de sélectionner
l'icône du Conjugeur dans la Barre de Recherche de Firefox, de taper son
verbe, et ses conjugaisons apparaissent directement - pratique quand le
correcteur orthographique ne suffit plus.

**[Le Conjugueur - Plugin de recherche pour
Firefox](http://www.leconjugueur.com/frsearchplugin.php)**
