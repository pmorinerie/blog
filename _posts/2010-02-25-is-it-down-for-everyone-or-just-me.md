---
layout: post
title: "Is it down for everyone or just me ?"
date: 2010-02-25 17:14
---

Et hop, un site pratique au nom autoexplicatif dans mes marque-pages :
[downforeveryoneorjustme.com](http://downforeveryoneorjustme.com/). On
peut difficilement faire plus simple.
