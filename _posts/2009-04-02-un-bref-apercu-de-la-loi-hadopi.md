---
layout: post
title: "Un bref aperçu de la loi HADOPI"
date: 2009-04-02 16:34
---

Dans les milieux de geeks et d'informaticiens, on parle beaucoup en ce
moment de la "**loi HADOPI**" — pour s'y opposer, la plupart du temps.
Les informations et réactions commencent à atteindre le grand public,
mais encore trop peu. Je voulais donc prendre le temps de rédiger une
explication compréhensible de ce projet de loi, que vous sachiez en quoi
cela vous concerne.

### La loi HADOPI, qu'est-ce c'est ? {style="font-size:1.2em"}

C'est pour l'instant un projet de loi, en préparation et en discussion
devant l'Assemblée et le Sénat depuis plus d'un an. Il vise à créer une
"**Haute Autorité pour la Diffusion des Œuvres et la Protection des
droits sur Internet**" (HADOPI), chargée de sanctionner les infractions
au droit d'auteur (c'est à dire principalement le téléchargement
illégal) sur Internet. La sanction préconisée serait la "riposte
graduée".

### La riposte graduée ? {style="font-size:1.2em"}

La riposte graduée vise à sanctionner progressivement les infractions au
droit d'auteur, afin de dissuader le téléchargement illégal. En soi, ce
n'est pas une mauvaise idée <span class="s1">—</span>c'est dans les
modalités de mise en œuvre que cela se gâte.

Concrètement, L'HADOPI serait saisie par les représentants des ayants
droit sur présomption d'infractions à leurs droits d'auteurs. Après
enquête par recherche dans les données de connexion stockées par les
fournisseurs d'accès (FAI), l'HADOPI enverrait des courriers menaçant
les utilisateurs de sanctions. En cas de récidive, l'HADOPI ordonnerait
leur **déconnexion d'Internet** sans possibilité de souscrire à un
nouvel abonnement pour une durée allant jusqu'à 12 mois. Bien sûr, il
faudrait continuer à payer son abonnement à Internet pendant cette
période de suspension.

### Alors on pourrait m'identifier en train de télécharger illégalement sur Internet ? {style="font-size:1.2em"}

Pas vraiment — et c'est là un des problèmes. Sur Internet, on ne peut
identifier que des réseaux, pas des utilisateurs. Concrètement, cela
signifie que l'on peut identifier le titulaire d'un abonnement à
Internet — mais rien de plus précis.

Par exemple, si dans une famille plusieurs personnes se partagent un
abonnement à Internet, **impossible de savoir quel ordinateur est
incriminé** — et encore moins quel utilisateur. De la même façon, dans
une entreprise, l'HADOPI ne peut identifier que l'accès à Internet de
l'entreprise, mais pas l'ordinateur ou l'employé en cause.

### Pourtant, couper l'accès à Internet, c'est draconien. {style="font-size:1.2em"}

Bien sûr. Internet fait aujourd'hui partie de notre vie courante, au
même titre que le téléphone, et est devenu nécessaire pour maintenir le
lien social. Le travail collaboratif, les services administratifs par
Internet, la banque en ligne… tout cela devient impossible si l'on coupe
votre accès à Internet. À l'heure où l'on parle du "droit à l'Internet
haut-débit pour tous", et de réduction de la "fracture numérique", cette
menace de suspension de l'abonnement est pour le moins incohérente.

D'autre part, cette sanction est extra-judiciaire : à aucun moment la
justice n'est saisie, l'HADOPI a pouvoir sur la chaîne du début à la
fin. Il suffit que les sociétés d'ayants-droit fasse une simple demande
à l'HADOPI, et vos données personnelles de connexion à Internet seront
scrutées et examinées. Auparavant, il fallait un dépôt de plainte et
l'accord d'un juge pour que l'on puisse examiner ces données
personnelles — et seules les enquêtes anti-terroristes pouvaient
contourner la procédure. La protection du droit d'auteur serait placée
sur le même plan extra-juridique que le terrorisme.

Bref, à la fois la sanction et le caractère extra-judiciaire de cette
sanction sont disproportionnés.<span
class="Apple-converted-space"></span>

### Bon, mais si je le télécharge pas illégalement, je ne suis pas concerné ? {style="font-size:1.2em"}

En fait si — et par plusieurs biais.

D'une part, plusieurs personnes se partagent souvent un même abonnement
: il y a en gros un abonnement à Internet par foyer. Et **c'est tout le
foyer qui est pénalisé**. Si le petit frère télécharge illégalement,
adieu le dossier que la grande sœur doit rendre à l'école, adieu le
télétravail de maman, adieu la déclaration d'impôt en ligne de la
famille !

D'autre part, vous pouvez être pris pour un contrevenant par erreur. Car
la méthode d'identification employée (l'adresse IP) n'est **pas fiable**
: n'importe qui peut, par exemple, s'identifier sur Internet avec
l'adresse IP de votre abonnement à Internet. Et si cette personne
télécharge illégalement, c'est vous qui serez accusé. Des chercheurs
américains ont ainsi réussi à faire accuser de téléchargement illégal
deux imprimantes réseau.

Enfin, vous pouvez être victime de quelqu'un **utilisant votre réseau à
votre insu** pour télécharger illégalement. Vous avez protégé votre
réseau Wifi par un mot de passe facile à deviner ? Vous avez été infecté
par un virus ? Dommage : en tant que titulaire de l'abonnement, la loi
HADOPI stipule que vous êtres responsable de la sécurité de votre réseau
(tant pis pour vous si vous n'êtes pas informaticien), et l'abonnement
du foyer entier sera suspendu.

Donc la coupure est possible même si vous innocent. Cela dit, vous aurez
quand même deux courriels d'avertissement (assortis, si l'HADOPI le
décide, une lettre recommandée), avant que l'on ne suspende votre
abonnement à Internet. Mais il ne sera pas facile d'y réagir : jamais
ces messages ne comporteront le nom des fichiers ou œuvres incriminées,
et les recours ne sont possible qu'à partir du deuxième avertissement
(oui, vous avez bien lu : vous ne pouvez pas contester le premier
avertissement avant d'en recevoir un second). Bref, vous avez intérêt à
savoir quoi faire, et à le faire vite.

### Tout cela est bien bancal… Et personne n'a rien dit ? {style="font-size:1.2em"}

Bien sûr que si. Notamment, le **Parlement Européen** a voté à deux
reprises (en moins de six mois) des articles de loi affirmant que "la
suspension d'un abonnement à Internet dans un cadre extra-judiciaire est
contraire aux droits de l'homme" — et ceci à la demande d'euro-députés
français craignants les effets de l'HADOPI. Cela signifie que, portée
devant une instance de justice européenne, la valeur juridique de la loi
HADOPI serait nulle.

Après le vote de la première résolution, Christine Albanel, Ministre de
la Culture, a rétorqué que les résolutions du Parlement Européen ne
l'engageaient en rien : le gouvernement français allait passer outre, et
advienne que pourra.

Les réactions contre cette loi sont également nombreuses en France :
**Jacques Attali** ne décolère pas, **les jeunes UMP** lâchent leur
parti… Et même dans le **gouvernement**, on murmure que oui, bien sûr,
tout le monde sait qu'HADOPI est une clownerie monumentale — mais
impossible de s'y opposer, l'ordre vient de plus haut…

### Que va-t-il arriver si l'HADOPI est votée ? {style="font-size:1.2em"}

La question principale est tout d'abord : **quel sera l'efficacité sur
le piratage ?** Sans doute pas celle attendue.

Il est probable que seuls quelques lampistes et adolescents ignorants se
feront prendre. Tous les autres se mettront rapidement à utiliser des
**techniques de contournement** ou de **cryptage**, indécelables et
[<span class="s2">très faciles à
utiliser</span>](http://www.glazman.org/weblog/dotclear/index.php?post/2009/03/12/Contourner-HADOPI-en-27-secondes).
Ces techniques se généraliseront en un rien de temps, et l'HADOPI ne
pourra plus rien y faire.

Le problème, plus grave, est que les enquêtes sérieuses et nécessaires,
comme l'anti-terrorisme, ne pourront plus rien faire non plus : si la
majeure partie des communications devient fortement cryptée, les écoutes
et les surveillances de courriels deviennent impossibles.

**Prenons une analogie**. Imaginez que l'on décide de mettre en place un
système national de reconnaissance faciale : des caméras omniprésentes,
qui analysent le visage des passants et signalent les délinquants
recherchés. Mais le système est peu fiable, et envoie régulièrement en
garde à vue des innocents — et toute leur famille avec.  
Réaction : en quelques mois, tous les passants portent des cagoules. Le
système devient inutile — mais pire, il est devenu impossible de filer
quelqu'un dans la rue, ou d'exercer la plus élémentaire et légitime des
surveillances. Est-ce cela que l'Etat veut ?

Accessoirement, comme le
[soulignait](http://www.glazman.org/weblog/dotclear/index.php?post/2009/03/10/HADOPI-parlons-un-peu-klingon)
Daniel Glazman, "*si ce projet passe, cela sera la première fois
qu'officiellement [...] les communications privées des individus — et
pas de quelques individus mais tout le monde — sont observées, écoutées,
filtrées sans intervention judiciaire.*"

### Parce qu'en plus, ce n'est pas efficace ? {style="font-size:1.2em"}

Même pas ! Ce ne serait sans doute pas efficace pour diminuer le
piratage, vu la simplicité des solutions de cryptage ou de
contournement. Et cela ne ferait sans doute pas remonter les ventes de
disques ou de DVD, qui souffrent de problèmes structurels bien plus
importants (et puis il est difficile de vendre un produit en menaçant le
client).

Enfin, la mise en place de cette loi, qui devrait coûter environ [100
millions
d'euros](http://www.pcinpact.com/actu/news/49668-riposte-graduee-coutera-100-millions.htm)
au contribuable, ne rapportera pas un sou aux artistes — contrairement à
Deezer, par exemple, qui reverse une partie des recettes publicitaires à
la SACEM.

### Comment en savoir plus ? {style="font-size:1.2em"}

Les nombreux articles de Daniel Glazman [<span class="s2">à ce
sujet</span>](http://www.glazman.org/weblog/dotclear/index.php?q=hadopi)
sont variés et éclairants, et envisagent le problème [<span
class="s2">sous ses différents
angles</span>](http://www.glazman.org/weblog/dotclear/index.php?post/2009/03/14/HADOPI-mon-analyse-complete).
Sinon, le site de la [<span class="s2">Quadrature du
Net</span>](http://www.laquadrature.net/fr/olivennes-HADOPI-creation-et-internet),
qui défend les usagers d'Internet, est une bonne source d'information.

### Et si je veux faire quelque chose ? {style="font-size:1.2em"}

Vous pouvez tout d'abord [écrire à votre
député](http://www.laquadrature.net/fr/olivennes-HADOPI-creation-et-internet)
: la Quadrature du Net propose une lettre-type et une liste des députés.

Vous pouvez également envoyer un courriel à Christine Albanel, Ministre
de la Culture, exprimant votre point de vue : là encore, la Quadrature
peut vous aider.

L'important est aussi de faire circuler l'information ! Prenez le temps
d'expliquer les enjeux de cette loi à vos amis et à vos proches.
