---
layout: post
title: "Tir groupé"
date: 2006-11-05 21:28
---

Attaque massive de spam en ce moment - la file de modération de
Spamplemousse se remplit à vue d'oeil, et certains messages indésirables
réussissent quand même à passer. Je ferme les commentaires le temps que
ça se calme.

**Edit:** rétablissement des commentaires, pour voir.
