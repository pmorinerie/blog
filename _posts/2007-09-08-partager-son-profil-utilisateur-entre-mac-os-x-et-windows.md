---
layout: post
title: "Partager son Profil utilisateur entre Mac OS X et Windows"
date: 2007-09-08 14:56
---

![Profil utilisateur](/images/logos/home.png)(suite du tutoriel
précédent sur [l'installation de Windows sur un
Macbook](http://kemenaran.winosx.com/?2007/09/08/81-demarrer-windows-via-bootcamp-et-parallels))

Maintenant que les systèmes fonctionnent, nous voudrions bien que tous
nos fichiers Mac (musiques, images, documents) soient accessibles par
Windows, qu'il tourne à l'intérieur de Parallels ou en natif. Parallels
dispose d'outils très simples d'emploi pour activer le partage des
fichiers sous Windows, à l'aide de Disques réseaux montés
automatiquement, mais ceci ne fera pas l'affaire pour notre Windows
natif. Il faut donc s'y prendre autrement.

Nous allons donc créer une partition de données, sur laquelle nous
migrerons notre Profil d'utilisateur Mac ainsi que tous nos fichiers.
Cette partition sera accessible depuis Mac et Windows natif, ainsi que
par Parallels.

### Etape 1 : Partitionnement du disque dur

Créons donc une troisième partition pour nos données, sans effacer ce
qui existe déjà. Pour ce faire :

-   Démarrer sur le CD d'installation de Mac OS X, fourni avec
    l'ordinateur
-   Lancer l'Utilitaire de disque
-   Repérer le nom de votre partition Mac - sans doute disk0s2.
-   Lancer un Terminal
-   Taper `diskutil resizeVolume disk0s2 30G "MS-DOS FAT32" Data 60G `.
    Remplacer éventuellement disk0s2 par le nom de votre partition
    système Mac, et les tailles de partition par ce qui vous convient.
    Si les tailles sont mal choisies, diskutil vous préviendra.
-   Attendre que diskutil crée la nouvelle partition
-   Redémarrer sur Mac OS X (c'est à dire en maintenant la touche C
    appuyée au démarrage pour éjecter le CD)

Un nouveau disque de données, formaté en FAT32, et donc lisible par Mac
OS et Windows, devrait apparaître.

### Etape 2 : Migration du profil Mac

Il faut maintenant [déplacer notre profil
utilisateur](http://maczealots.com/articles/home/) et nos données sur la
nouvelle partition.

-   Ouvrir un Terminal
-   Taper `ditto -rsrcFork -V /Users /Volumes/Data/Users` pour copier
    les fichiers vers la nouvelle partition
-   Attendre la fin de la copie
-   Ouvrir l'utilitaire NetInfo
-   Cliquer sur le cadenas en bas pour s'identifier
-   Sélectionner les colonnes 'users/nom\_d'utilisateur'
-   Remplacer la valeur de la variable 'home' par
    '/Volumes/Data/Users/nom\_d'utilisateur'
-   Quitter NetInfo en sauvegardant les changements
-   Fermer puis rouvrir sa Session d'utilisateur. Si rien n'a changé, la
    migration de profil s'est bien passée. Sinon, vérifier que les
    fichiers ont bien étés copiés, et que la variable 'home' de NetInfo
    pointe au bon endroit.
-   Ouvrir un Terminal
-   Taper `sudo rm -dr /Users` pour effacer l'ancien Profil
    d'utilisateur
-   Puis taper ` sudo ln -s /Volumes/Data/Users /Users` pour créer un
    lien de l'ancien emplacement vers le nouveau

Le profil d'utilisateur se trouve maintenant sur la partition commune à
Mac OS et à Windows.

### Etape 3 : Restaurer le démarrage de Windows via BootCamp

La création de notre nouvelle partition a modifié l'identifiant de notre
partition Windows, qui n'est plus disk0r3 mais disk0r4. Lorsque l'on
essaie de démarrer Windows en natif, il crashe donc au milieu du
démarrage. Corrigeons cela.

Il faut pour cela que nous puissions éditer le fichier boot.ini de
Windows, et donc installer ntfs-3g sous Mac, pour pouvoir éditer des
fichiers sur la partition système de Windows.

-   Télécharger et installer [MACFuse
    Core](http://code.google.com/p/macfuse/downloads/list)
-   Télécharger et installer le [package
    ntfs-3g](http://www.daniel-johnson.org/) de Daniel Johnson (ou un
    autre, mais celui-ci a un installeur pratique)
-   Utiliser l'Utilitaire de disque pour démonter la partition système
    Windows (et vérifier éventuellement le nom de cette partition,
    normalement disk0s4)
-   Ouvrir un Terminal
-   Taper `mkdir /Volumes/ntfs` pour créer un point de montage
-   Taper `ntfs-3g /dev/disk0s4 /Volumes/ntfs -ovolname="WinSystem"`
    pour monter la partition Windows en écriture. Elle doit apparaître
    sur le Bureau
-   Ouvrir le fichier 'boot.ini' à la racine de la partition
-   Changer chaque occurence d'une ligne de type
    `multi(0)disk(0)rdisk(0)partition(3)` en
    `multi(0)disk(0)rdisk(0)partition(4)` (ou par le nombre ad-hoc, à
    partir du nom de votre partition)
-   Enregistrer le fichier 'boot.ini'
-   Redémarrer sous Windows

A ce stade il devrait être à nouveau possible de démarrer sous Windows
nativement. Au démarrage, nous constaterons la présence d'une nouvelle
partition, nommée Data, contenant notre profil utilisateur Mac.

### Etape 4 : Migrer le Profil Windows vers la partition de données

-   Démarrer Windows en natif
-   Afficher les propriétés du dossier 'Mes documents'
-   Déplacez le dossier vers votre partition de données (sans doute
    'E:om\_d'utilisateur')

Le dossier 'Mes documents' de Windows pointe maintenant vers le même
emplacement que le profil Mac : parfait.

### Etape 5 : Restaurer le démarrage de Parallels

Parallels aussi est un peu confus à la suite de notre changement de
partition : il refuse de démarrer la machine virtuelle BootCamp, tout en
laissant entendre que ce serait possible moyennement quelques
configurations.

-   Démarrer Mac OS X
-   Ouvrir le fichier de configuration de la machine virtuelle Boot Camp
    avec Textedit (sans doute à l'emplacement '\~/Documents/Parallels/My
    Boot Camp/My Boot Camp.pvs')
-   Chercher, dans la section 'IDE Devices', la ligne indiquant
    `Disk 0:0 image = Boot Camp`
-   Rajouter derrière le nom de votre disque système Windows,
    normalement `Disk 0:0 image = Boot Camp;disk0s4`
-   Enregistrer et fermer le fichier
-   Démarrer la machine virtuelle

La machine virtuelle Windows devrait désormais démarrer correctement.

### Etape 6 : Utiliser le profil partagé sous Parallels

Parallels ne peut pas monter notre partition de données, puisqu'elle est
utilisée par Mac OS. Contournons le problème en tirant parti de
l'excellent partage réseau de Parallels.

-   Démarrer la machine virtuelle Windows
-   Utiliser l'option 'File Sharing' pour activer le partage des disques
    Mac
-   Attendre que les disques réseaux se montent dans Windows
-   Connecter un nouveau disque réseau pointant vers la partition de
    données, mais avec la même lettre de lecteur que la partition native
    (normalement 'E:'). Bien activer l'option 'Connecter au
    redémarrage'.
-   Vérifier que 'Mes documents' pointe maintenant bien vers le dossier
    de profil

Windows virtualisé a donc accès aux mêmes fichiers que lorsque il est
exécuté en natif, via un disque réseau.

### Conclusion

Nous avons donc maintenant un profil partagé sous Mac OS X et Windows,
qui fonctionne de manière transparente que Windows soit virtualisé et
tourne nativement.

Pour améliorer les performances, on peut éventuellement convertir la
partition de données FAT32 en NTFS, maintenant que, grâce à ntfs-3g,
nous pouvons lire et écrire des partitions de ce type, mais ce sera
l'objet d'un prochain tutoriel.
