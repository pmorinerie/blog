---
layout: post
title: "Améliorer les barres de progression"
date: 2008-03-07 11:44
---

Trouvé sur OSNews, un projet de recherche discutant de l'[amélioration
du comportement des Barres de
progression](http://chrisharrison.net/projects/progressbars/), un
élément couramment utilisé dans la conception d'interfaces. L'article se
penche sur différents modes de progression d'une barre, analyse les
différents modes de perception, et fournit quelques pistes pour
améliorer le calcul de la progression dans une application.
Intéressant.![Exemple de Barre de
progression](/images/divers/mac_os_x_progressbar.png)
