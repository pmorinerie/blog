---
layout: post
title: "Débuter en C avec CodeBlocks"
date: 2006-09-07 15:07
---

Pour les élèves informaticiens qui commencent les cours de C (en
particulier dans mon école, l'[Isep](http://www.isep.fr/), mais cet
article peut à mon avis servir à tout le monde), voici un bref article
sur le thème "Comment réaliser son premier programme en C avec
CodeBlocks".

#### Pourquoi CodeBlocks ?

On vous a sans doute expliqué que, pour compiler un fichier de code
écrit en C, il fallait utiliser le
[compilateur](http://fr.wikipedia.org/wiki/Compilateur)
[gcc](http://fr.wikipedia.org/wiki/Gcc), en tapant une obscure ligne de
commande sous Linux, du style "*gcc -c source.c*". C'est sans doute la
meilleure manière de procéder, mais tout le monde n'est pas habitué à
Linux et à la ligne de commande. Heureusement, il existe des portages de
gcc sous Windows, comme par exemple
[MinGW](http://fr.wikipedia.org/wiki/MinGW). De plus, on peut s'épargner
l'emploi de la ligne de la ligne de commande en utilisant un
<acronym title="Integrated Development Environment">IDE</acronym>, c'est
à dire un éditeur de texte conçu pour vous faciliter la tâche, qui vous
permettra d'écrire votre code de manière plus performante et de le
compiler plus facilement.

Parmi ces IDE, sous Windows, deux se détachent : Dev-C++ et CodeBlocks.
[Dev-C++](http://www.bloodshed.net/devcpp.html) est très complet, mais
un peu vieux, et son développement est en pause depuis quelques années.
[CodeBlocks](http://www.codeblocks.org/) est plus récent, et se veut le
successeur de Dev-C++ ; il est plus léger, plus simple d'utilisation, et
à mon avis plus abordable pour le néophyte. Bref, un bon choix.

#### Télécharger et installer CodeBlocks

CodeBlocks est téléchargeable librement et gratuitement depuis
[codeblocks.org](http://www.codeblocks.org). Pensez à prendre
l'installeur pour Windows <ins>avec</ins> le compilateur MinGW intégré.
Pour ceux qui ont la flemme de chercher, voici un lien direct vers [le
fichier à
télécharger](http://belnet.dl.sourceforge.net/sourceforge/codeblocks/codeblocks-1.0rc2_mingw.exe).
Une fois CodeBlocks téléchargé, installez le dans le dossier de votre
choix. Lancez maintenant CodeBlocks à partir du Menu Démarrer.

#### Créer un projet C

Lors du premier lancement de CodeBlocks, une fenêtre apparaît, vous
demandant de choisir le compilateur par défaut. Normalement, seul le
compilateur tout juste installé (GNU GCC) est détecté - il vous suffit
donc de cliquer sur le bouton "Close".

![Choix du compilateur](/images/codeblocks/compiler_choice.png)

La page de démarrage est ensuite affichée. Nous allons créer notre
premier projet C. Cliquez sur "Create a new Project".

![Page d'accueil](/images/codeblocks/home_page.jpg)

Choississez l'objet "Console Application", et prenez garde à bien en
faire un projet de type "C", et non "C++". Cliquez ensuite sur le bouton
"Create" pour créer le projet, et enregistrez-le dans un dossier
quelconque.

![Créer un projet](/images/codeblocks/create_project.jpg)

Une fois le projet créé, observez l'arborescence des fichiers, dans la
barre de gauche. Cliquez successivement sur "Console Application",
"sources", "main.c" pour faire apparaître le fichier principal du
projet. Ce fichier est un [exemple
classique](http://fr.wikipedia.org/wiki/Hello_world) : il affiche juste
le message "Hello world" à l'écran.

Avant d'exécuter ce programme, il faut le compiler. Les boutons bleus de
la barre d'outils servent à cela. Cliquez sur le bouton "Build and run".
Cette action va tout d'abord faire appel à gcc pour compiler notre code,
puis va lancer l'exécutable ainsi créé.

![Compiler](/images/codeblocks/compile_run.png)

Normalement, le fruit de vos efforts s'affiche sur l'écran :

![Console Hello World](/images/codeblocks/console.png)

Voilà, vous avez exécuté votre premier programme en C ! Vous pouvez
maintenant vous entraîner avec les exemples donnés en cours, et
commencer à bricoler un peu - en vous aidant au besoin de l'excellent
[tutoriel de
C](http://www.siteduzero.com/tuto-3-8-0-apprenez-a-programmer-en-c.html)
du Site du Zéro. N'hésitez pas à vous aventurer un peu dans les options
de CodeBlocks et de ses plugins, pour voir ce qu'il est possible de
faire. Bonne chance !
