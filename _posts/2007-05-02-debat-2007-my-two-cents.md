---
layout: post
title: "Débat 2007 : my two cents"
date: 2007-05-02 23:22
---

Débat présidentiel en ce moment, et avis partagés, sur Internet.

[Daniel
Glazman](http://www.glazman.org/weblog/dotclear/index.php?2007/05/02/3503-22h06-mon-opinion-est-definitive)
voit une Royal se vautrant monumentalement : *« Depuis 1981, je n'ai
jamais vu un candidat de gauche à quelque élection que ce soit être
aussi minable à la télévision »* ; pour [Christophe
Carignano](http://carignano.blog.20minutes.fr/archive/2007/05/02/sarkozy-en-difficult%C3%A9-face-%C3%A0-royal.html),
Sarkozy est taclé par Royal sur tout les sujets ; [Le
Figaro](http://www.lefigaro.fr/election-presidentielle-2007/20070502.WWW000000412_h_les_etats_majors_retiennent_leur_souffle.html)
publie les petits phrases assassines du débat tous les quarts d'heure,
et ne sait plus quoi penser... Fichtre, diantre, foutre.

-   23h30 : Royal : *« Aucune loi votée sans application de la
    précédente. »* Quand on sait le temps que prend la publication d'un
    décret d'application, les réformes n'iraient pas de bon train...
-   23h39 : Deux déclarations apprises par cœur, Royal proposant un
    pacte pour tout, Sarkozy ressortant le plein emploi... et habituel
    décalage des temps de parole. Bon. Ce débat ne volait pas bien haut,
    finalement, et fait presque regretter la confrontation Royal-Bayrou
    de samedi dernier.

Je ne sais toujours guère comment me positionner : Sarkozy a évité ses
grands thèmes délicats (immigration, insécurité, etc), et ses sorties
sur les peines plancher pour les multi-récidivistes restent acceptables.
Royal plus à l'aise, mais agressive et imprécise. Je parie que les
journaux de demain concluront à un match nul, ou prendront parti suivant
leur tendance — les sondages d'après-débat seront peut-être plus
informatifs...
