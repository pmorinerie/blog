---
layout: post
title: "Vérifier les liens dans un mail"
date: 2012-01-18 13:28
---

Comme j'en avais assez d'envoyer des mails contenant des liens morts,
j'ai écrit un petit utilitaire pour résoudre ce problème.

Il installe un élément "Vérifier les liens" dans le menu "Services" de
Mac OS X.

Lorsqu'un texte est sélectionné dans Mail (ou dans n'importe quelle
application Cocoa), l'élément "Vérifier les liens" va extraire les liens
contenus dans le texte, vérifier qu'ils sont accessibles, et afficher le
résultat du test.

Pour l'instant, ça ne fonctionne qu'avec les liens qui sont seuls sur
une ligne de texte. Mais c'est déjà ça. Si ça peut vous être utile…

[Télécharger "Vérifier les
liens.workflow"](http://www.winosx.com/zoo/MacOSXServices/V%e9rifier%20les%20liens.workflow.zip)
