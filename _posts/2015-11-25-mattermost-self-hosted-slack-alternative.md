---
layout: post
title: A self-hosted Slack alternative? Asking for a friend.
date: 2015-11-25 16:46
---

I started using Slack at CaptainTrain a while ago – and couldn't go back. Having a nice
chat client is good, desktop notifications are great, email notification for missed messages
is awesome – plus searchable history, 1-to-1 discussions, images upload, and so on.

It's so nice that **I decided to setup a Slack team for chatting with my friends**.

But for one of my friend there was a issue: Slack runs on Slack servers, and all your messages
are stored elsewhere. He said he would use the chat only if it was open-source and self-hosted.

Well, fine. I trust Slack people, but having a large part of my private correspondance and
musing with my friends on a private server (rather than on Slack databases) sounds like a
good idea. So I started looking for open-source Slack alternatives. I found [Rocket.Chat](https://rocket.chat/), which looks nice, but was young at that time. I found
[Let’s Chat](https://sdelements.github.io/lets-chat/), and used it for a while (but
its development crawled down at some point). And recently I started to use [Mattermost](http://www.mattermost.org/).


Mattermost
----------

Mattermost is an "open source, self-hosted
Slack-alternative". Great, sounds exactly like what I want. It’s easy to install (especially the
Docker-based evaluation version), works pretty well, supports several teams and many integration
hooks – perfect.

[![Mattermost](/images/mattermost/mattermost.jpg)](http://www.mattermost.org/)

Thruth to be told, this is a young product (the 1.0 is only a two months old), and many
features, while present, are not as polished as their Slack counterparts. But its moving
rapidly, and I'm confident in the work made by the development team.

Plus in my book decentralizing services is always a good thing: it’s better for software
diversity, avoids single points of failure and global outages (like the Slack outage a few days
ago), and protects privacy.

So I wondered **how to make it easier to use Mattermost**. And I made two things.

A Mattermost package for Yunohost
---------------------------------

My server runs [YunoHost](https://yunohost.org/), a package to run a self-hosted servers without being a sysadmin. It let you install webapps in one click (Wordpress, Roundcube, etc.),
and frees you from tedious administration tasks. Try it, it's awesome.

As installing a new app on a YunoHost server is so simple, I made a package to install Matermost
in the same way! It allows you to **install a production-ready web-based chat in a minute**,
complete with email notifications and all.

To use it, open your YunoHost admin webpage, go to _"Install an application"_, and choose 
_"Install a custom application"_ at the bottom of the list. Then paste the package URL: [https://github.com/kemenaran/mattermost_ynh](https://github.com/kemenaran/mattermost_ynh). And that’s it!

![Installing Mattermost on YunoHost](/images/mattermost/install-mattermost-from-yunohost.png)

**[Try the Mattermost package for YunoHost - GitHub](https://github.com/kemenaran/mattermost_ynh)**

Binary builds for Matterfront
-----------------------------

Mattermost runs great in a web-browser tab – and yet it is sometime convenient to have a desktop
application that is separed from the browser. There is no official desktop client yet – but
[Loic Nageleisen](https://github.com/lloeki) made a nice cross-platform desktop client, named
[Matterfront](https://github.com/lloeki/matterfront).

![Matterfront desktop client for Mattermost](/images/mattermost/matterfront.jpg)

Internally, Matterfront uses Electron, the same web-app packaging technology used by Slack. And
it works great. Just one thing: there are no official builds yet, you have to build the app yourself from the command line.

So **I compiled some builds for the latest release of Matterfront**, and put them on GitHub. This should make it easier to use Matterfront for anyone – until there are official builds.

**[Download Matterfront desktop client for Windows, OS X and Linux](https://github.com/kemenaran/matterfront/releases)**
