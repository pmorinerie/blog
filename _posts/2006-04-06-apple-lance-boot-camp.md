---
layout: post
title: "Apple lance Boot Camp"
date: 2006-04-06 22:47
---

[Boot Camp](http://www.apple.com/macosx/bootcamp/) permet donc,
officiellement et facilement, d'installer Windows XP sur un *Macintel*.
D'aucuns pensent que c'est la ruine de Mac OS X, car les logiciels ne
seront plus portés sur Mac, d'autres y voient au contraire enfin une
possibilité de [switcher](http://www.apple.com/fr/switch/) en douceur.

La meilleure réaction que j'aie lu à ce sujet est sans doute celle de
[Daniel
Glazman](http://www.glazman.org/weblog/dotclear/index.php?2006/04/05/1694-converti),
qui se dit enfin « converti » ... Je pense que je vais faire comme lui ;
à moi le MacBook Pro !
