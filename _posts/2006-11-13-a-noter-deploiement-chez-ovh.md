---
layout: post
title: "A noter : déploiement chez OVH"
date: 2006-11-13 23:40
---

Si d'aventure vous devez déployer un site incluant du PHP sur un
[hébergement mutualisé
OVH](http://www.ovh.com/fr/produits/offres_mutualises.xml), vous risquez
d'être surpris par la configuration un peu atypique et par des
comportements bizarres Dans la liste des choses à vérifier :

-   CHMOD : la racine du site doit être en 705, les dossiers et les
    scripts en [755 au
    maximum](http://guides.ovh.com/InternalServerError). Pas de 777,
    sinon vous aurez droit à une "Internal Server Error" assez
    inexplicable.
-   Le mod\_rewrite ne fonctionne pas toujours parfaitement, notamment
    certaines options utilisées par [CakePHP](http://www.cakephp.org)
    (CakePHP reste réanmoins utilisable, mais en désactivant
    mod\_rewrite, donc avec des Url un peu moins jolies).
-   Un tilde (\~) dans les Url altère le fonctionnement de certains
    scripts, comme le système d'upload de fichiers de
    [FCKeditor](http://www.fckeditor.net) - ceci ne semble pas être
    spécifique à OVH. Préciser une adresse sur le système de fichier du
    serveur (donc sans tilde) aide bien, de même que la page de test
    d'upload de FCKeditor.
