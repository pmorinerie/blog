---
layout: post
title: "Éteindre l'écran de son Mac"
date: 2008-05-26 21:57
---

Si vous souhaitez éteindre l'écran de votre Mac, sans éteindre
l'ordinateur lui-même (par exemple pour écouter de la musique en
économisant la batterie), un simple raccourci clavier peut suffire. Il
s'agit de **Shift+Ctrl+Eject**, qui éteindra complètement l'écran — une
vrai extinction, pas juste une luminosité minimale laissant tout de même
fonctionner l'écran.
