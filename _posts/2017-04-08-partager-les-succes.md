---
layout: post
title: "🚀 Partager les succès"
date: 2017-04-08 11:08
---

Si vous travaillez sur un projet dans une petite équipe, il est sans doute facile de suivre ce qui se passe en ce moment. Qui fait quoi, quels sont les sujets du moment, quels sont les dernières modifications visibles et les dernières étapes effectués.

Mais au fur et à mesure que l’équipe grandit, ce sens de « qu’est-ce qui se passe en ce moment » devient plus difficile à appréhender. Et on découvre un jour qu’on n’était pas au courant de telle nouveauté, de tel changement dans l’équipe d’à côté, ou de tel recrutement. Ça a été annoncé, sûrement – mais peut-être pas aux bonnes personnes, ou au bon moment.

Chez Capitaine Train, on avait mis au point une petite convention simple pour rendre les succès plus visible.

## “There are things that can give you wings, but I’ll give you a rocket!”

Pour partager les succès, pas besoin d’en faire des caisses. Ce qu’on faisait, c'est simplement de poster un message dans un canal public sur le chat interne (Slack, en l’occurrence).

Une seule règle : mettre un emoji `:rocket:` 🚀 au début du message.

C’est tout.

Pas besoin d’être bien long ; il s’agit juste de communiquer aux autres une nouveauté dont vous êtes heureux ou fier :

- 🚀 _Les utilisateurs du site peuvent enfin modifier leur adresse email de contact._
- 🚀 _Le bogue pénible d’envoi des notifications a été corrigé_
- 🚀 _Nos affiches sont visibles dans le métro depuis ce matin !_
- 🚀 _C’est confirmé, Eva nous rejoindra en mars comme data-scientist_

Comme c’est très léger, ça ne prend pas beaucoup de temps à écrire.<br>
Comme c'est rapide, on hésite pas à annoncer la petite nouveauté dont on est fier, même si ce n’est qu’un petit détail.<br>
Comme c’est juste une convention, ça ne rajoute pas un processus de plus.<br>
Et comme c’est sur Slack, ça permet de recueillir les félicitations et les applaudissements (et c’est important, les applaudissements).

## Composer les outils pour aller plus loin

La beauté des conventions, c’est que ça active plein de possibilités. On peut s’en servir pour faire plein de belles choses en plus.

Par exemple, pour les changements qui s’y prêtent, on peut **ajouter une image** à son annonce : une capture d'écran, la photo de la personne recrutée, ou un gif animé qui présente une nouvelle fonctionnalité.

Pour être notifié de ces annonces spécifiquement, il est possible de rajouter `:rocket:` à la **liste des mots-clefs qui déclenchent une notification**. Ça peut permettre de mieux filtrer le bruit, et de n’être notifié que de ce qui est important.

Enfin, il est assez simple de **générer une newsletter interne**, mensuelle ou bimensuelle, à partir de ces annonces. Une simple recherche sur `🚀` renverra tous les messages à inclure dans la newsletter. Il est même possible d’automatiser le processus : coder un petit bot pour Slack qui recopie les `:rockets:` dans une planche Trello ; et un autre bot qui, une fois les annonces mises au propre, envoie automatiquement une newsletter avec le contenu de la planche.
