---
layout: post
title: "Leopard — contrôle au clavier"
date: 2008-03-06 11:16
---

Mac OS permet, entre autres éléments de navigation au clavier, d'avoir
dans les dialogues un bouton "alternatif". Par exemple, sur l'image
ci-dessous, le bouton bleu est le **bouton par défaut** (activé par la
touche "**Entrée**"), et le bouton entouré d'un halo bleu est le
**bouton alternatif** (activé par la **barre d'espace**).

![Mac OS X 10.5 Leopard Alternate dialog
button](/images/divers/Mac%20OS%20X%20Dialogue%20Halo%20bleu.png)

Ceci permet par exemple, dans un dialogue "*Souhaitez vous enregistrer ?
Oui/Non/Annuler*", de répondre "Non" par un simple appui sur la barre
d'espace.

Toutefois, ce comportement a disparu des réglages par défaut de Leopard.
Pour le réactiver, ouvrez les "Préférences Systèmes", panneau "Clavier
et Souris", onglet "Raccourcis clavier", et sélectionner l'option "Accès
au clavier complet : tous les réglages". Le halo bleu autour du bouton
alternatif et le contrôle total au clavier seront alors réactivés.
