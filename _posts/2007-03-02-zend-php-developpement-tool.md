---
layout: post
title: "Zend PHP Developpement Tool"
date: 2007-03-02 10:15
---

![Logo
Eclipse](/images/eclipse_tuto/eclipse_logo.png)[Zend](http://www.zend.com)
développe en partenariat avec [Eclipse](http://www.eclipse.ord) un
**environnement de développement complet pour PHP**, fonctionnant à
l'aide d'Eclipse. Eclipse est une plateforme de développement
initialement centrée sur Java, mais extensible à de multiples langages
et applications. **PHP Developpement Tool** (PDT, anciennement nommé
PHP-IDE) y ajoute les fonctionnalités suivantes :

-   Editeur PHP, HTML, CSS, Javascript
-   Gestion de projet
-   Coloration syntaxique
-   Auto-complétion
-   Affichage de l'aide des fonctions PHP native, et des fonctions
    personnelles documentées en JavaDoc
-   Exploration du code : recherche des déclarations, des
    instanciations, etc.
-   Débogage dynamique du code

PDT rend le développement PHP plus agréable et plus productif, en
particulier lorsque l'on travaille sur de gros projets, comprenant
beaucoup de fichiers.

Dans cet article, nous allons voir comment installer Eclipse et PDT, et
comment créer notre premier projet PHP avec Eclipse.

#### Installer PDT

La [zone de téléchargement de PDT](http://www.zend.com/pdt) se trouve
sur le site de Zend. La page détaille plusieurs manière d'installer PDT.

Une solution d'installation, si vous avez déjà Eclipse installé, est
d'effectuer une mise à jour via le système interne d'Eclipse. Suivez
pour cela [les procédures](http://www.zend.com/pdt#eclipse) décrites sur
le site de téléchargement.

Mais la manière la plus simple est d'utiliser le **paquetage
"All-in-One"**, qui comprend à la fois Eclipse, les différents plugins
nécessaire au fonctionnement de l'IDE, ainsi que PDT lui-même. Pour
cela, téléchargez l'archive "pdt-win32" disponible [sur cette
page](http://downloads.zend.com/pdt/all-in-one/) (notez que les fichiers
"pdt" et "php-ide" sont en fait les mêmes). À l'heure où j'écris, la
dernière version est la
[0.7.0](http://downloads.zend.com/pdt/all-in-one/pdt-0.7.0.I20070208_debugger-0.1.7-all-in-one-win32.zip).Une
fois le fichier téléchargé, décompressez-le dans le dossier de votre
choix (par exemple "C:\\Program Files\\eclipse"), puis lancez
"Eclipse.exe".

#### Configuration d'Eclipse

Au premier lancement, Eclipse vous demande de choisir l'emplacement du
Workspace, c'est à dire le dossier dans lequel l'environnement va
stocker ses fichiers de configuration et ses réglages.[![Configuration
du
workspace](/images/eclipse_tuto/eclipse_1_t.jpg)](/images/eclipse_tuto/eclipse_1.jpg)

Une page d'accueil pour les nouveaux arrivants apparaît : vous pourrez à
l'occasion lire les divers tutoriaux proposés, mais pour l'instant
ouvrons l'espace de travail (Workbench).[![Configuration du
workspace](/images/eclipse_tuto/eclipse_2_t.jpg)](/images/eclipse_tuto/eclipse_2.jpg)

L'espace de travail apparaît, mais il est encore vide : il nous faut
créer un projet. Utilisez pour cela le menu
"File/New.../Projet".[![Configuration du
workspace](/images/eclipse_tuto/eclipse_3_t.jpg)](/images/eclipse_tuto/eclipse_3.jpg)

L'assistant de création de projet vous guide dans la création de votre
projet. Commençons par choisir le type du projet : nous voulons
développer un projet PHP.[![Configuration du
workspace](/images/eclipse_tuto/eclipse_4_t.jpg)](/images/eclipse_tuto/eclipse_4.jpg)

Il faut ensuite définir l'emplacement du projet. Comme nous allons
développer un site Web, mettons le Projet dans un sous-répertoire du
dossier "www" d'EasyPHP (ou de votre Webroot si vous utilisez WAMP ou un
Apache natif). Cliquez ensuite sur le bouton "Finish".Notez que vous
pouvez importer dans Eclipse les fichiers d'un projet déjà existant : il
vous suffit de mettre le projet en cours de création dans le dossier où
vous avez déjà vos fichiers.[![Configuration du
workspace](/images/eclipse_tuto/eclipse_5_t.jpg)](/images/eclipse_tuto/eclipse_5.jpg)

Le projet est alors créé. Eclipse vous demande si vous voulez ouvrir la
perspective PHP. Dans l'environnement Eclipse, une **perspective** est
un ensemble de fenêtres et de barres d'outils correspondant à un usage
précis : il y a la perspective *Java*, la perspective *PHP*, la
perspective *Debug*, etc. On peut changer de perspective à tout moment
en utilisant les icônes en haut à droit de l'environnement.  
Une fois la perspective PHP ouverte, notre projet apparaît, mais il ne
contient pour l'instant aucun fichier. Pour ajouter un fichier au
projet, faites un clic-droit sur le nom du projet, puis sélectionnez
"New/PHP File".[![Configuration du
workspace](/images/eclipse_tuto/eclipse_6_t.jpg)](/images/eclipse_tuto/eclipse_6.jpg)

L'assistant de création de fichier apparaît : nommez votre fichier comme
vous le souhaitez (ici, "index.php"), puis cliquez sur le bouton
"Finish".[![Configuration du
workspace](/images/eclipse_tuto/eclipse_7_t.jpg)](/images/eclipse_tuto/eclipse_7.jpg)

Ouvrez ensuite le fichier nouvellement créé : vous avez accès à
l'éditeur PHP.[![Configuration du
workspace](/images/eclipse_tuto/eclipse_8_t.jpg)](/images/eclipse_tuto/eclipse_8.jpg)

#### Pour aller plus loin

Les fonctionnalités de PDT sont multiples : prenez le temps d'explorer
les différents panneaux. Certains vous listen toutes les fonctions de
votre code, d'autres vous permettent d'en parcourir les classes et les
objets...  
Vous pouvez également jouer avec le débogage dynamique : cliquez sur la
barre à gauche d'un éditeur de code PHP pour placer des Breakpoints,
puis lancez le débogueur pour suivre l'exécution de votre code ligne par
ligne.  
Nous n'avons vu que la création de fichiers PHP, mais PDT permet aussi
de créer et d'éditer des fichiers HTML, CSS, Javascript ou XML. Si vous
avez besoin de voir l'effet de petits changements ponctuels dans votre
code, vous pouvez visualiser le contenu généré par votre page
directement dans Eclipse, grâce au petit navigateur Web intégré.

N'hésitez pas à aller farfouiller dans les options : les possibilités de
configuration sont multiples, et certaines vous seront franchement
pratiques. Par exemple, si vous rencontrez des problèmes avec les
accents, sachez que l'encodage par défaut des fichiers PHP est en UTF8 -
vous préfèrerez sans doute les mettre en ISO-8859-1. Pour cela, ouvrez
les Options (menu "Window/Preferences"), puis dans "General/Content
Type", et changez l'encodage par défaut des fichiers de type "PHP".

Eclipse est un environnement complexe, qui vous demandera un peu
d'adaptation au début - mais il se révèle extrêmement pratique et
puissant. Bon courage !
