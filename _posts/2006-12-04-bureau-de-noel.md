---
layout: post
title: "Bureau de Noël"
date: 2006-12-04 19:59
---

Il faut bien que les décorations de Noël commencent quelque part -
puisque j'avais du temps ce soir, c'est le Bureau de mon PC qui s'est
retrouvé enneigé.

[![Cliquez pour agrandir
l'image](/images/wallpaper/noel_2006_mini.jpg)](http://lamorine.free.fr/MacOSX/img/noel_2006.jpg)

-   **Fond d'écran :**
    [Noel](http://www.vladstudio.com/wallpaper/?250/800x600/low), par
    Vlad Gerasimov
-   **Icônes :** [Christmas
    Icons](http://www.deviantart.com/deviation/44138151/), par
    gakuseisean
-   **Thème des fenêtres :** [Tiger
    V](http://www.deviantart.com/deviation/17919057/), par dobee

</p>

