---
layout: post
title: "Ubuntu : installer ntfs-3g"
date: 2007-01-08 01:44
---

Le pilote ntfs-3g, mentionné il y a près de 6 mois [dans ces
colonnes](http://kemenaran.winosx.com/?2006/07/15/30-ntfs-sous-linux),
est toujours en développement, et s'améliore progressivement. Pour
rappel, il permet de lire mais surtout d'écrire sur une partition
Windows au format NTFS lorsque l'on se trouve sous Linux.

Voici, trouvé sur OSNews, un tutorial expliquant comment installer
facilement ntfs-3g sous Linux Ubuntu. Le tutoriel comporte deux
parties : comment utiliser ntfs-3g pour lire un disque dur USB formaté
en NTFS, et comment lire une partition Windows d'un disque dur interne
classique. Simple et bien rédigé, il devrait vous permettre de tester ce
pilote facilement, et de découvrir les joies du NTFS sous Linux.

**[HowtoForge - How To Use NTFS Drives/Partitions Under Ubuntu Edgy
Eft](http://www.howtoforge.com/ubuntu_edgy_eft_ntfs_ntfs_3g)**
