---
layout: post
title: "Grammar fail"
date: 2009-08-03 11:50
---

Parfois, la correction grammaticale de Mac OS X a des ratés légèrement
sexistes :

![La correction grammaticale de Mac OS X a des
ratés](/images/screenshots/Grammar%20correction%20fail.png)

Bon, ça doit être parce que seul l'anglais est reconnu pour l'instant,
et qu'il connait l'expression "nouvelle cuisine" en anglais. N'empêche
\^\^
