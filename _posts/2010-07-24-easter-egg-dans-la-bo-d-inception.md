---
layout: post
title: "Easter-egg dans la BO d'Inception"
date: 2010-07-24 10:23
---

J'ai bien aimé la musique
d'[Inception](http://trailers.apple.com/trailers/wb/inception/). Il n'y
a pas vraiment de thème qui reste précisément en tête : on se souvient
d'une idée générale, de fragments — ce qui colle très bien avec les
rêves imbriqués du film.

Bref, [Hans Zimmer](http://en.wikipedia.org/wiki/Hans_Zimmer) a
mentionné ses sources d'inspiration et la manière dont il avait composé
ces musiques dans plusieurs interviews — donc on voit à peu près d'où
certaines choses viennent. N'empêche, l'entendre, c'est encore autre
chose. Enjoy.

<div style="text-align:center">

  
<object width="480" height="385">
<param name="movie" value="http://www.youtube.com/v/UVkQ0C4qDvM&amp;hl=en_US&amp;fs=1?rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param>
<embed src="http://www.youtube.com/v/UVkQ0C4qDvM&amp;hl=en_US&amp;fs=1?rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="480" height="385">
</embed>
</object>

</div>
