---
layout: post
title: "OVH détecte les intrusions"
date: 2010-04-18 16:06
---

Voilà ce que c'est que d'utiliser de vieilles versions de ses outils
Web : parfois, des scripts automatiques qui cherchent les failles
automatiquement arrivent à exploiter votre serveur. Ça m'apprendra à ne
pas utiliser la dernière version de dotclear.

Ces derniers jours, un script automatique a essayé (et réussi) à
installer des scripts de spam sur mon serveur. Mais ce n'est pas la
raison directe pour laquelle le site a été inaccessible ces derniers
jours. En réalité, c'est parce qu'OVH a détecté l'exécution de scripts
perl louches, et a automatiquement désactivé l'accès Web au serveur — ce
qui veut dire que plus personne ne pouvait accéder aux sites hébergés,
mais qu'au moins le serveur n'envoyait pas 15 000 spams à la minute.

Je ne savais pas qu'OVH avait un système de détection automatique comme
ça — mais franchement, c'est pas mal. J'ai été averti par courriel, et
en regardant les logs, j'ai pu voir exactement d'où venait le problème,
et prendre les mesures adéquates (en l'occurrence, mettre à jour vers la
dernière version de dotclear). La classe.

Mais n'empêche, mort aux hackbots qui n'ont que ça à faire de me faire
perdre deux heures en résolution de problèmes…
