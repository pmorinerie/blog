---
layout: post
title: "Synchronisation iCal / Google Calendar"
date: 2008-05-07 10:56
---

![Logo iCal](/images/logos/ical.png)Si vous gérez votre emploi du
temps avec Google Calendar, vous seriez peut-être content de pouvoir
utiliser également l'excellent iCal sous Mac, en synchronisant les deux.
Pour récupérer les agendas Google dans iCal, pas de soucis : une simple
URL à copier dans iCal suffit. Mais que faire quand on veut également
envoyer les événements iCal vers Google Calendar ?

Deux solutions existent : premièrement, l'excellent [Spanning
Sync](http://spanningsync.com/), qui est simple, bien fait, fonctionne
comme il faut... et cher, 65\$ environ. Mais si vous geekez un peu, il
existe un petit utilitaire en Java qui peut faire le même travail,
gratuitement : [GCALDaemon](http://www.gcalsync.com/). L'installation
est un peu délicate, mais un [bon tutoriel décrit la marche à
suivre](http://blog.kno.at/tools/finally-full-google-calendar-sync-with-gcaldaemon/),
bien mieux que ne le fait le site officiel. Cela fonction parfaitement
chez moi — le pied :)

**[Installer GCALDaemon sous Mac OS
X](http://blog.kno.at/tools/finally-full-google-calendar-sync-with-gcaldaemon/)**

**Edit:** A noter toutefois que suite à des changements dans la gestion
des calendriers par iCal, ce tutoriel n'est pas compatible avec Leopard.
Mais ce n'est pas très grave, puisque l'on peut désormais se passer de
GCALDaemon en utilisant directement la [synchronisation
CalDAV](http://kemenaran.winosx.com/?2008/07/30/134-synchronisation-ical-google-calendar-avec-caldav)
mise en place par Google :)
