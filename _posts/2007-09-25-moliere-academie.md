---
layout: post
title: "Molière académie"
date: 2007-09-25 15:58
---

Un nouveau concept de Novarina, lancé depuis une semaine : la Molière
Académie. Un site pro, Benoît Solers, plein de gens, de vidéos et de
contenu... Tout cela monte en puissance, et on sent la qualité du
partenariat avec Dailymotion.

<div style="text-align:center">

[![moliere-academie](http://www.moliere-academie.com/site/banniere/bannierMA.gif)](http://www.moliere-academie.com/site/ "moliere-academie")

</div>
