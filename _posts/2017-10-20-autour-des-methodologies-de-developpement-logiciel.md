---
layout: post
title: "Autour des méthodologies de développement logiciel"
date: 2017-10-20 17:45
---

Un article posté sur Hacker News m’a entraîné dans une suite de développement sur les méthodologies de développement logiciel.

La problématique est la suivante : **comment peut-on industrialiser le développement logiciel** – c’est à dire faire en sorte d'obtenir des résultats prédictibles en terme de temps et de qualité ?

Et son corollaire : parmi les multiples méthodes de développement logiciel qui n’ont pas manqué d'être proposées depuis quarante ans, pourquoi est-ce qu’aucune ne semble atteindre ce but ?

Le **point de départ** est un article de Ian Miell, [My 20-Year Experience of Software Development Methodologies](https://zwischenzugs.wordpress.com/2017/10/15/my-20-year-experience-of-software-development-methodologies/).

Sa thèse est que toutes les méthodes de développement qu’il a rencontré dans sa vie professionnelle (Waterfall, Flow, Agile, ou n'importe quoi de vaguement indéfini) sont des fictions collectives. Ces méthodes ne sont pas bonnes ou mauvaises en soi – mais permettent de s'organiser en grands groupes suivant différents principes partagés par tous.

> Lean, Agile, Waterfall, whatever, the fact is we need some kind of common ideology to co-operate in large numbers. None of them are evil, so it’s not like you’re picking racism over socialism or something. Whichever one you pick is not going to reflect the reality, but if you expect perfection you will be disappointed.

Au passage, un lien pointe vers **cet autre article** d’il y a quelques années, [Why don’t software development methodologies work?](http://typicalprogrammer.com/why-dont-software-development-methodologies-work). En résumé, l’article dit ceci :

> Once a programming team has adopted a methodology it’s almost inevitable that a few members of the team, or maybe just one bully, will demand strict adherence and turn it into a religion. The resulting passive-aggression kills productivity faster than any methodology or technology decision.

Si pour lui par essence aucune méthodologie de développement logiciel ne fonctionne, par sédimentation des rituels, il voit quand même un critère qui permet de prédire le degré de succès d’une équipe de développement :

> My own experience, validated by Cockburn’s thesis and Frederick Brooks in _No Silver Bullet_, is that software development projects succeed **when the key people on the team share a common vision**, what Brooks calls “conceptual integrity.” This doesn’t arise from any particular methodology, and can happen in the absence of anything resembling a process. I know the feeling working on a team where everyone clicks and things just get done.

Ce qui me mène vers des recherches effectuées par des employés de Google, sur **[d'autres critères qui influencent la performance d'une équipe de développement](https://rework.withgoogle.com/blog/five-keys-to-a-successful-google-team/)**.

Leur prémisse est que le succès d’un projet dépend plus de l’équipe que de la méthodologie employée. Et d’après l’expérience des auteurs, cinq critères semblent être prédominants :

> 1. **Psychological safety**: Can we take risks on this team without feeling insecure or embarrassed?
> 2. **Dependability**: Can we count on each other to do high quality work on time?
> 3. **Structure & clarity**: Are goals, roles, and execution plans on our team clear?
> 4. **Meaning of work**: Are we working on something that is personally important for each of us?
> 5. **Impact of work**: Do we fundamentally believe that the work we’re doing matters?


Je ne peux parler que pour moi, mais **j’ai effectivement retrouvé tous ces critères dans les boulots où je me suis vraiment plu**. Et inversement, dans les endroits ou les moments plus disfonctionnels, je retrouve bien ce qui manquait dans les éléments de cette liste. Comme quoi…
