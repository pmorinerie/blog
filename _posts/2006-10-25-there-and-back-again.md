---
layout: post
title: "There and back again"
date: 2006-10-25 09:35
---

Lu hier un billet de Scott Adams, l'auteur des comics Dilbert. Il était
devenu quasiment muet il y a 18 mois... et vient de retrouver la voix.
Et la manière dont il raconte ça est vraiment chouette, en fait.

**[The Dilbert Blog - Good news
day](http://dilbertblog.typepad.com/the_dilbert_blog/2006/10/good_news_day.html)**

**Edit:** L'article a été
[slashdotté](http://science.slashdot.org/science/06/10/24/2044220.shtml) -
comme quoi je ne suis pas le seul à bien avoir aimé ce billet :)
