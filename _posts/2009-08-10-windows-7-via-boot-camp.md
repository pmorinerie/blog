---
layout: post
title: "Windows 7 via Boot Camp"
date: 2009-08-10 16:14
---

Hier soir, séance d'installation de Windows 7 sur mon Mac — qui faisait
déjà tourner XP. Merci aux téléchargements de la MSDN !

Voici quelques conseils d'installation si vous mettez à jour depuis un
Windows déjà installé sur Boot Camp :

-   Pas besoin de graver l'ISO de Windows 7 : copier les fichiers sur le
    disque dur suffit.
-   Après l'installation, pour intégrer les pilotes Boot Camp :
    1.  Si vous avez un CD d'installation de **Snow Leopard** (10.6),
        vous êtes bon : insérez le CD sous Windows, et lancez
        l'installeur Boot Camp. Tout devrait fonctionner.
    2.  Par contre, si vous avez un CD de **Leopard** (10.5) et que vous
        lancez l'installeur Boot Camp directement, vous aurez droit à
        une "erreur 2229" : il faut donc hacker un petit peu
        l'installeur. Pour ce faire, téléchargez un [éditeur de fichiers
        MSI](http://www.brothersoft.com/easy-msi-editor-70589.html),
        puis ouvez le fichier "BootCamp.msi" (ou "BootCamp64.msi") avec
        l'éditeur, et supprimez la table "LaunchConditions". Lancez
        ensuite "BootCamp.msi" **en mode Administrateur** — sinon vous
        aurez des problèmes lors du lancement des services installés.

<!-- -->

-   Si vous avez des **problèmes de son**, tout le monde recommande
    d'installer les pilotes RealTek. Mais si vous avez un MacBook Pro
    récent (Unibody 2009), tentez plutôt les **pilotes Cirrus** qui sont
    sur le DVD Boot Camp. J'ai mis du temps à trouver l'information.
    **Edit:** Et si votre micro intégré ne fonctionne pas sous Windows,
    [voir
    ici](http://kemenaran.winosx.com/?2009/12/13/164-macbook-micro-sous-boot-camp).

Bref, l'installation se passe quand même bien, et ça fonctionne à
merveille. Quelques remarques en vrac sur Windows 7 :

-   L'installation est relativement rapide : environ 30mn, et quelques
    redémarrages.
-   Le système est très réactif, plus que Windows XP. Certaines tâches
    sont peut être plus lentes, mais dans l'ensemble les fenêtres
    usuelles s'ouvrent bien plus vite.
-   Les thèmes (et l'apparence graphique en générale) sont vraiment
    chiadés. C'est beau, configurable, et les séries de fonds d'écrans
    par défaut sont sympa et pas conventionnelles. Pareil pour l'écran
    de démarrage et de login.
-   Je ne suis toujours pas fan du système de "Bibliothèques", c'est
    confondant. Conduisez moi à la racine de "Mes documents", bon sang !
-   IE8 n'est pas mal (meurs, IE6, meurs !), mais Firefox avec [(New)
    Glasser](https://addons.mozilla.org/fr/firefox/addon/12951), ça
    poutre.
-   La nouvelle Barre des Tâches fonctionne bien, c'est assez agréable.
    Quand on vient de Mac OS X, par contre, on a tendance à croire
    qu'une application dont on ferme toutes les fenêtres va rester
    ouverte (genre Firefox avec un téléchargement en cours) : il faut en
    fait minimiser la dernière fenêtre pour laisser l'application
    tourner.
-   Le nouveau système de préférences est assez clair, les réglages se
    trouvent sans trop de difficultés.

Dans l'ensemble, on a envie de découvrir le bestiau : chouette système.
Alez, adopté comme Windows courant !

**Edit:** Si le volume sonore sous BootCamp vous semble trop faible,
c'est de la faute des maudits pilotes Cirrus pas à jour. Quelques
utilisateurs ont [modifié les fichiers de configuration du
pilote](http://discussions.apple.com/message.jspa?messageID=9966565#9966565),
pour obtenir un volume normal — jetez un œil.
