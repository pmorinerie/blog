---
layout: post
title: "Quelles langues enseigner à l'école ?"
date: 2014-11-24 08:23
---

Tiens, au passage : en 2005, au HCE (Haut Conseil de l’Éducation), on se
demandait quelle langue(s) enseigner à l'école et utiliser pour la
construction européenne. Tout-anglais ? Plurilinguisme ? Langue
commune ?

Alors F. Grin a étudié les différentes options ; notamment en terme
d'impact économique. [Le résumé en une
page](http://esperanto-france.org/presse/communiqueGrin.pdf).

<http://esperanto-france.org/presse/communiqueGrin.pdf>
