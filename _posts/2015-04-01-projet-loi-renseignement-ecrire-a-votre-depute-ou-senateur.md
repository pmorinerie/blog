---
layout: post
title: "Projet de loi sur la Surveillance : écrivez à vos représentants"
date: 2015-04-01 12:18
---

Le Projet de loi sur la Surveillance est aujourd’hui examiné par la commission des lois,
et par l’Assemblée Nationale dans deux semaines. C’est donc pile le moment de dire à votre député·e ou votre sénateur·rice tout le bien que vous pensez de ce projet de loi.

Vous pouvez trouver le contact de vos représentants parlementaires à partir de votre code
postal sur [nosdeputes.fr](http://www.nosdeputes.fr/) et
[nossenateurs.fr](http://www.nossenateurs.fr/). Et je vous met aussi en dessous une copie de ce que j'ai envoyé à ma députée. N’hésitez pas à reprendre ce texte si ça peut vous être utile.

> Madame la députée,
> 
> Le projet de Loi relatif au Renseignement, aujourd’hui en cours d’examen en commission des lois, est inscrit à l’ordre du jour de l’Assemblée Nationale à partir du 13 avril.
> 
> Comme vous le savez, l'objectif de projet de loi est de légaliser un certain nombre de pratiques des services de Renseignement. Ces services effectuent depuis plusieurs années des opérations de surveillance, collectes d’informations, mises sur écoute, etc. illégalement et sans contrôle de l’autorité judiciaire.
> 
> **Ce projet de loi est dénoncé par les membres de la société civile**. Citons par exemple  :
> 
> - l’ARCEP (Autorité de Régulation des Communications Électroniques et des Postes)
> - le Conseil National du Numérique
> - la CNIL
> - le Conseil de l’Europe
> - Amnesty International
> - la Ligue des Droits de l’Homme
> - Reporters sans Frontières
> - l’Union Syndicate des Magistrats
> 
> Parmi les personnes ayant exprimé une opposition, on trouve également :
> 
> - Marc Trédicic, juge anti-terroriste
>
>    (qui parle d’un projet de loi donnant « des pouvoirs exorbitants » aux services de renseignement)
> - Alain Marsaud, ancien juge anti-terroriste
> 
>   (qui considère qu’« **avec un tel texte, toutes les oppositions, même politiques, peuvent être surveillées**. »)
>
> Ce projet de loi contient également des dispositions illégale au regard du droit international — comme le souligne le Haut-Commissariat aux Droits de l’Homme des Nations Unies.
> 
> En tant que citoyen, **ce qui m’inquiète le plus est l’aspect généralisé de cette surveillance**. Il ne s’agit pas de surveiller quelques personnes suspectes. Ce projet de loi permettrait de mettre « sur écoute » non seulement l’ensemble d’un groupe (parti politique, association, syndicat) mais également l’ensemble de la population française (via l’analyse des données transitant sur Internet).
> 
> Cette surveillance pourrait se faire **sans contrôle de l’autorité judiciaire**. Seule la justice est garante du droit — et dans le cadre de ce projet de loi aucun magistrat n’aurait de contrôle sur la légitimité des écoutes.
> 
> Voulons-nous d’une société où, chaque fois que nous composons un numéro de téléphone ou que nous ouvrons un navigateur web, une petite voix nous rappelle que ce que nous allons faire est surveillé et pourra être retenu contre nous ?
> 
> Aujourd’hui, ce projet loi n’offre pas les garanties nécessaires au maintien d’un état de droit plutôt que d’une société de surveillance. Je vous demande, Madame la députée, de **soutenir les amendements assurant le contrôle des services de renseignement par l’autorité judiciaire** et de rejeter les dispositions visant à une surveillance généralisée (plutôt que ciblée sur les personnes suspectées).
> 
> Pour en savoir plus, je vous invite à consulter les analyses rédigées par l’association La Quadrature du Net, disponibles ici : [https://wiki.laquadrature.net/PJL_relatif_au_renseignement](https://wiki.laquadrature.net/PJL_relatif_au_renseignement)
> 
> Je vous priée d’agréer, Madame la députée, l’expression de ma considération distinguée,
> 
> \<Signature\>
>
