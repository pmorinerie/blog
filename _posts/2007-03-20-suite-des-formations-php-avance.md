---
layout: post
title: "Suite des formations PHP Avancé"
date: 2007-03-20 09:15
---

Pour les isépiens : une séance de formation supplémentaire aura bien
lieu le **jeudi 22 mars à 13h**. Faisant suite à la [formation PHP
Avancé /
CakePHP](http://kemenaran.winosx.com/ecrire/poster.php?post_id=60), mais
s'orientant sur des domaines un peu différents, elle aura pour
programme :

-   Les tests unitaires : méthode, environnements, les tests avec Cake,
    SeleniumIDE
-   L'eXtreme Programming : présentation, méthodes, retours d'expérience
-   Les méthodes de développement collaboratif : Trac, SVN, intégration
    de SVN dans différents IDE

Le diaporama de cette formation sera disponible prochainement.

**Edit :** Le diaporama est disponible sur SlideShare :

**[Formation Extreme programming / Tests unitaires / Travail
collaboratif](http://www.slideshare.net/kemenaran/formation-extreme-programming-tests-unitaires-travail-collaboratif)**  
[![](/images/Formation%20PHP/formation-extreme-programming-tests-unitaires-travail-collaboratif_t.jpg)](http://www.slideshare.net/kemenaran/formation-extreme-programming-tests-unitaires-travail-collaboratif)
