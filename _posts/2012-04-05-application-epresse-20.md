---
layout: post
title: "Application ePresse 2.0"
date: 2012-04-05 12:39
---

![Logo ePresse v2](/images/logos/logo-epresse-v2.png)La version 2 de
l'application ePresse est [sortie ce
matin](http://www.ebouquin.fr/2012/04/05/epresse-une-premiere-offre-web-et-une-nouvelle-application-ios-android/).
Ce kiosque de la presse française propose onze titres (quotidiens
nationaux et magazines), et de nombreux autres sont à venir.

Je suis assez fier des moyens techniques mis en œuvre sur cette
application ; j'espère que vous apprécierez la navigation dans le
catalogue et la bibliothèque, ainsi que la lecture haute-résolution sur
un écran Retina.

À titre d'offre de bienvenue, l'application vous offre tous les titres
pendant une semaine.

**[Télécharger gratuitement ePresse sur
l'AppStore](http://itunes.apple.com/us/app/epresse/id430832375?mt=8)**
