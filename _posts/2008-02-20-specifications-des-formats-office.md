---
layout: post
title: "Spécifications des formats Office"
date: 2008-02-20 15:36
---

Microsoft a récemment publié les spécifications des fichiers binaires
d'Office — c'est à dire nos bons vieux .doc, .xls, .ppt, et consorts.
Ces spécifications étaient auparavant disponibles sur simple courrier,
mais à l'heure de la standardisation, Microsoft a préféré s'orienter
vers une voie plus ouverte. L'approche du vote de l'ISO sur le format
OOXML [y est certainement pour quelque
chose](http://www.pcinpact.com/actu/news/41880-microsoft-office-binaires-open-specification.htm).

On peut espérer que tout cela permettra une meilleure compatibilité des
logiciels de bureautique comme OpenOffice, mais ce n'est même pas
certain. En effet, ces spécifications sont monstrueusement obscures et
compliquées, et surtout démesurément longues. Joel Spolsky explique,
dans un article extrêmement bien fait, [les pourquoi de cette
complexité](http://www.joelonsoftware.com/items/2008/02/19.html), et
quelques solutions pour y remédier.

Bien que ses explications soient très pertinentes, il est dommage que la
plupart des solutions qu'il propose tournent autour de "scriptez Office
avec VBA", ce qui nécessite toujours l'achat de coûteuses licence
Windows et Office. A quand un vrai projet d'interfaçage Open Source avec
les documents Office ?

**[Joel Spolsky - Why are the Microsoft Office formats so complicated
?](http://www.joelonsoftware.com/items/2008/02/19.html)**
