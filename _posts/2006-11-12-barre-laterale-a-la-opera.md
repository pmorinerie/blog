---
layout: post
title: "Barre latérale à-la Opéra"
date: 2006-11-12 12:31
---

![Opera logo](/images/divers/Opera_logo.png)Une des fonctionnalités
sympathiques du navigateur Web [Opéra](http://www.opera-fr.com/), c'est
sa manière de gérer les barres latérales. Elles sont toutes regroupées
dans un seul volet, et ce volet s'affiche, entre autres, à l'aide d'une
petite barre-interrupteur verticale à droite ou à gauche de l'écran.
Comme ça, quand on veut par exemple ouvrir ses Marques-pages, on n'a
qu'à caler le pointeur de la souris tout à gauche et à cliquer, plutôt
que d'aller chercher le petit bouton idoine au milieu de la Barre
d'outils.

![Firefox logo](/images/divers/firefox-logo.png)Regrettant que ce
comportement n'existe pas sous Firefox, j''envisageais de créer une
extension qui permette cela... Et coup de chance, elle existe déjà. Il
s'agit de l'extension [All-In-One
Sidebar](https://addons.mozilla.org/firefox/1027/), qui adapte le
comportement des panneaux latéraux de Firefox pour le rendre similaire à
celui d'Opéra. Par exemple, en plus de la petite barre-interrupteur
susdécrite, elle permet d'afficher la Liste des téléchargements dans un
panneau latéral, plutôt que dans une fenêtre externe - on trouve
rapidement cela bien plus pratique.

Jetez-y donc un coup d'oeil, c'est pratique et chouette :)

**[Extension "All-In-One Sidebar" pour
Firefox](https://addons.mozilla.org/firefox/1027/)**
