---
layout: post
title: "Do you sprach français ?"
date: 2006-11-12 01:00
---

Il est bien, ce site de [réservations](http://www.hostels.com/fr/) pour
les hôtels et auberges de jeunesses allemandes. Très bien. Mais alors
les [traductions](/images/divers/auberges.jpg)... "Charactère" et
"propret", ça vous dit quelque chose ?
