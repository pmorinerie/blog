---
layout: post
title: "Zelda Majora's Mask retexturé"
date: 2010-05-10 08:59
---

**[Zelda: Majora's
Mask](http://fr.wikipedia.org/wiki/The_Legend_of_Zelda:_Majora%27s_Mask)**,
sur Nintendo 64, était un excellent jeu. Si si. Il a réussi à proposer
un [ton](http://www.youtube.com/watch?v=p9zvIJyF4M8&feature=related),
une atmosphère et un style différent à la série après le succès
planétaire d'Ocarina of Time : ce n'était pas évident, les équipes de
Nintendo auraient aussi bien pu sortir un fade "Ocarina 2".

À la place, nous avons eu droit à ce mélange de [Un jour sans
fin](http://fr.wikipedia.org/wiki/Un_jour_sans_fin) et des côtés
bizarres et décalés des réalités parallèles, où le monde touche à sa
fin, les mêmes journées recommencent encore et encore sans espoir de
sortie du cercle… À la fin de chaque cycle, tous les problème résolus
sont effacé par le temps qui remonte, et il faut recommencer, encore —
et pourtant, au fur et à mesure, l'espoir fini par poindre, on s'attache
aux personnages et à cette vie dont on finit par connaitre les rouages.

Bref, tout ça pour vous dire que Toon-Link, un allemand, a sorti hier
une nouvelle version plus complète de son **pack de textures pour le
jeu**, qui vise à **augmenter la qualité des textures** souvent floues
de la Nintendo 64, mais aussi, autant que possible, à **tirer légèrement
le style graphique du jeu vers celui de Twilight Princess**.

<div style="margin: auto; text-align:center">

<object width="480" height="385">
<param name="movie" value="http://www.youtube-nocookie.com/v/UCQXIXiFVo0&amp;hl=fr_FR&amp;fs=1&amp;rel=0&amp;color1=0x2b405b&amp;color2=0x6b8ab6"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param>
<embed src="http://www.youtube-nocookie.com/v/UCQXIXiFVo0&amp;hl=fr_FR&amp;fs=1&amp;rel=0&amp;color1=0x2b405b&amp;color2=0x6b8ab6" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="480" height="385">
</embed>
</object>

</div>

C'est somptueux, ça redonne une nouvelle jeunesse à cette merveille, et
c'est gratuit. Pour voir ça en action, téléchargez le pack (le lien est
en dessous de la vidéo YouTube), ouvrez votre émulateur N64 préféré avec
le plugin [1964 Vidéo](http://code.google.com/p/emu-1964/), activez
l'option "Load Hi-Res textures", et profitez du spectacle.
