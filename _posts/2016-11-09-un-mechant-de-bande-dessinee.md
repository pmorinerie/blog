---
layout: post
title: "Un méchant de bande dessinée"
date: 2016-11-09 10:27
---

Le film [Speed Racer](https://fr.wikipedia.org/wiki/Speed_Racer_(film)) (Les Wachowski, 2008) a une certaine ingénuité que j’aime bien, au fond. [FilmCritHulk en parle](http://birthmoviesdeath.com/2015/03/27/film-crit-hulk-smash-speed-racer-as-artist) mieux que moi.

Royalton, le méchant de l’histoire, est un vrai méchant, au premier degré. Pas de subtilité : c’est une caricature du type le plus faux qu’on puisse imaginer. Tiens, je vous ai même fait un petit montage pour vous présenter le bonhomme.

Mais évidemment cette situation caricaturale n’existe pas, ne peut pas exister en vrai. Bien sûr. C’est trop gros.

Quoique…

<iframe width="560" height="366" style="width: 100%" src="https://www.youtube.com/embed/uptS9vQ-i5M?rel=0&amp;controls=2&amp;hl=en&amp;cc_lang_pref=en&amp;cc_load_policy=1&amp;cc=1" frameborder="0" allowfullscreen></iframe>

_<br>Une version avec des sous-titres est disponible [directement sur Youtube](https://www.youtube.com/edit?video_id=uptS9vQ-i5M)._
