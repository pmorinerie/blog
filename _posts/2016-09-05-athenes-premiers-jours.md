---
layout: post
title: Athènes, premiers jours
date: 2016-09-05 18:50
---

![Ménage à Notara](/images/athens/menage.jpg "Avec une belle thématique « Motifs à fleurs »")

Cette scène de ménage se passe à [Notara 26](http://www.notara26.info/blog/?lang=en), un centre d'accueil à Athènes qui héberge gratuitement entre 100 et 200 exilés. Un beau projet qui tourne depuis un an, avec uniquement avec des volontaires bénévoles. _[(D’autres photos ici)](http://blogyy.net/2016/05/08/film-solidaire-nouvelle-action-a-exarcheia/)_

Il y a une semaine, pendant la nuit, un groupe d’assaillants a cassé une vitre extérieure, avant de jeter à l’intérieur des cocktails Molotov puis des bonbonnes de gaz, provoquant des explosions entendues dans tout le quartier.

Heureusement l’incendie a pu être éteint ; il n’y a pas de blessés, mais beaucoup de dégâts.

![Incendie à Notara 26](/images/athens/incendie.jpg "Ça a quand même salement brûlé. Et je ne vous raconte pas la suie partout.")

Les attaques incendiaires contre des squats ne sont hélas pas rares à Athènes ; elles sont souvent liées à de membres du parti néo-nazi « Aube dorée » ([comme cela semble avoir été le cas cette fois ci](http://blogyy.net/2016/08/24/attaque-incendiaire-du-squat-de-refugies-notara26-a-qui-profite-le-crime/)).

Comme je suis à Athènes en ce moment, je suis passé voir.

Depuis une semaine des volontaires se relaient pour tout remettre en état : réparer les murs, les portes, les vitres, nettoyer, ranger, repeindre. En espérant que les occupants, répartis dans d’autres lieux, puissent retrouver rapidement un hébergement.

![Une salle après beaucoup de travaux et de rangement](/images/athens/rangement.jpg "Il a fallu vider toutes les pièces, réparer, repeindre, nettoyer, et remettre en place le matériel.")

