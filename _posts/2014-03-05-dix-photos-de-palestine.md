---
layout: post
title: "Dix photos de Palestine"
date: 2014-03-05 08:25
---

Noël dernier, j’étais à Bethlehem. Chaque année, il y a ce camp-chantier
organisé par une association palestinienne, avec une vingtaine de
volontaires internationaux : pendant dix jours, on creuse le matin, on
déplace des pierres, on plante des arbres — et l’après-midi on va se
promener, on visite les alentour, des palestiniens viennent nous parler
de leur vie, ou de l’histoire du pays.

J'ai enfin mis en forme une dizaine de photos de mon passage là bas,
avec quelques notes — si ça vous intéresse, ça se trouve ici :

**[Dix photos de Palestine — un aperçu des territoires
occupés](http://kemenaran.winosx.com/10%20photos%20de%20Palestine/)**

Bonne lecture !
