---
layout: post
title: "Clic central pour Macbook"
date: 2009-09-03 21:09
---

Si vous possédez un Macbook, et êtes fatigué d'avoir à maintenir la
touche "Cmd" pour ouvrir un **nouvel onglet** dans Firefox (en utilisant
"Cmd+Click"), je vous comprends. Après tout, il suffirait d'avoir une
combinaison de doigts ou un geste sur le **touchpad** qui corresponde à
un **clic-central**.

Réjouissez-vous, la fin de vos souffrances est proche :
**[MiddleClick](http://clement.beffa.org/labs/projects/middleclick/)**
permet d'émuler un clic-central en tapotant le touchpad avec trois
doigts. Ce qui vous permet d'ouvrir un lien dans un nouvel onglet, de
fermer un onglet sans avoir à viser le bouton de fermeture, et toute
autre sortes de merveilles.

And there was much rejoicing.
