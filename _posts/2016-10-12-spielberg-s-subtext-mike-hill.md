---
layout: post
title: "Spielberg’s Subtext – Mike Hill"
date: 2016-10-12 21:19
---

_This is the transcript of a talk given by [Mike Hill](http://www.mikehill.design/), “[Spielberg’s Subtext](http://www.mikehill.design/spielberg-subtext/)”, in which he explores the allegories of the movie “Jurassic Park”._

_All the video and content are by Mike Hill – this is only a transcript of his talk._

<iframe src="https://player.vimeo.com/video/165693758" width="100%" height="370" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

## Transcript

Andres asked me to come and do a talk–and I decided the talk would be about Jurassic Park. Now the reason for that is I presume most of us have seen Jurassic Park, and also maybe Jurassic World.

What I wanted to talk about is the difference between them. On the surface they are pretty much identical: they have the same cinematography, they have the same subject, they have the same material, they have the same pretty much everything. Every surface attribute has been lifted from Jurassic Park.

But somehow, when I finished watching Jurassic World, I feel a big vague emptiness where there should be a feeling of positivity. And I wanted to actually look deep down the surface of Jurassic Park, and really get to grip why we like it.

So for me a story is a lot like a bridge. If you spend an entire life looking at a bridge from the perspective of the road-side, and someone asks you to make a bridge, you might look at what’s here and say: “_Well, it’s just a roadside, there is some paint, and there is handrails on the side, and that’s what a bridge is composed of.”_ But obviously, we’re all made of common-sense, so we know that from the perspective of anywhere but the road-side a bridge is made of foundations, supports–it’s an entire engineering project.

And that’s the same with a story. It isn’t just composed of itself: it’s composed of lots and lots of foundations. And if you don’t have those foundations, then your bridge collapses, and your surface, regardless of these materials, looks broken, and no one can travel across it.

In the case of Jurassic Park and Jurassic World, one story holds up for 25 years and we still love it. The other was released a year ago; does anyone even remembers what the characters names were?

So, if we look at Jurassic Park and Jurassic World, they’re both blockbusters, they’re both summer releases, and they’re bases on a high concept format. In the case of Jurassic Park, it’s “_What if we could clone dinosaurs?_” Now today’s blockbusters, Jurassic World included, they take that high concept, and they take it literally. They say : “_Well, how many dinosaurs can we have on screen?_”

Now the problem with this is it doesn’t take into account that people are watching this movie, and people don’t respond to just lots and lots of dinosaurs.

What Spielberg does instead is he takes the high concept, and he makes it secondary to a human story, and he uses the dinosaurs as a way to support that story. The dinosaurs in Jurassic Park are symbolic tools. They are literally there as a narrative function.

Now Spielberg does this by using allegories. An allegory is a narrative that metaphorically represents an abstract idea. The whole point of Jurassic Park is that it is actually an allegory for becoming parents.

I’m going to quickly show you a clip from Jurassic Park. I just want you to pay close attention to what is happening in this sequence to what is being said, and generally the production design of the setting.

> _Alan Grant’s team is assembled in from of an old CRT monitor._
>
> ELLIE: Post-mortem contraction in the posterior neck ligaments… Velociraptor?  
> ALAN: Yeah. That’s good shape too. Loot at the half moon shaped bones. No surprise that these guys learned how to fly.
> 
> _Laughs in the crowd_
> 
> ALAN: Well, maybe dinosaurs have more in common with present-day birds than they do with reptiles. And even the word "raptor" means "bird of prey".
> 
> _We cut to Alan explaining Velociraptors to a kid._
> 
> ALAN: Because velociraptor is a pack hunter, you see. He uses coordinated attack patterns and he’s out in force today.Then he slashes you with this: six-inch retractible claw. Like a razor.
> 
> _Alan and Ellie climb up to their mobile-home._
> 
> ELLIE: Hey Alan, if you just wanted to scare the kid you could have pulled a gun on it.  
> ALAN: Yeah, I know… Kids… You want to have one of those?  
> ELLIE: I don’t want *that* kid, but a breed of child, Dr. Grant, could be intriguing…

Now Jurassic World simply has characters that do stuff randomly around VFX dinosaurs. In Jurassic Park the VFX dinosaurs are specifically doing things to support the narrative of the allegory, which is “becoming parents”.

So, if you look at the high concept of "cloning dinosaurs", what kind of responsibility is involved into creating that life?
And the second question that is being played in the allegorical thread, which is “creating children”, you’re asking  the question “_Can we be responsible for life?_”

The allegory and the high concept are exchanging the same questions, and that creates subtext. Now, the powerful thing about subtext is that when it comes to say one thing about something, it can mean something else on an entirely different level. Which means that dinosaurs, and people talking about chaos theory can actually resonate emotionally.

So, one of the things that Spielberg uses to get these ideas across is archetypes. When you got a complex idea, something like Chaos Theory, and the idea of responsibility for life, and you need to display in a two-hours movie for kids, and adults, and grandchildren, and grandparents–you need to show basically everything through a _filter_ that we automatically understand, culturally and socially.

> Ian Malcom, dressed in black, and John Hammond, dressed in white, are seated side-by-side.

Now, the archetypes in Jurassic Park are Chaos and Control, which is Devil and God. It may seem that the idea of Chaos and Control is not a big deal, but that’s the factors in all of our lives. We have weather forecast, because we love the idea of controlling what we’re going to wear, what we’re going to do.
And when it comes to kids, that element of Control and Chaos is absolutely fundamental. The whole concept of Chaos Theory in Jurassic Park is there to be a sounding board for these two characters preparing to have children.

I’m going to show you a quick supercut of how these archetypes show up in regards to the Devil and to God.

> _In Alan and Ellie’s mobile-home_
> 
> GRANT: Who in God’s name do you think you are?  
> HAMMOND: John Hammond. (…) I own an island. (…) Really spectacular, spared no expense.

> _In the helicopter leading to the island_
> 
> MALCOM: John doesn’t subscribe to chaos, particularly what it has to say about his little science project.  
> HAMMOND: Codswallop, Ian.

> _In the genetic facility_
> 
> HAMMOND: I insist on being here when they’re born.

>  _In Alan and Ellie’s mobile-home_
> 
> HAMMOND: I can tell instantly about people; it’s a gift.

> _In the park Control’s Room_
> 
> HAMMOND: I don’t blame people for their mistakes. But I do ask that they pay for them.  
> NEDRY: Thanks, dad.

> _In the genetic facility_
> 
> HAMMOND: I’ve been present for the birth of every creature of this island.

> _In the helicopter leading to the island_
> 
> MALCOM: Dr. Sattler, Dr. Grant, you’ve heard of Chaos?

> _In the park’s Control Room_
> 
> MALCOM: Hello? Hello? Yes?  
> HAMMOND: I really hate that man…

So, it seems like an obvious point, but the entire idea of the Chaos Theory and God Complex is to reflect something that is socially relevant to these people creating life. This becomes a lens to see things through.

I’m going to stay away from that element; I’m going to stick to the main allegory thread, which is about becoming parents, and to show how that ends up resonating in every single scene in the film.

So, the idea for the allegory “_Preparing for parenthood_”. The best way to get the audience primed for that allegory without knowing it is to start putting an idea in the fabric of the film that they’re not necessarily aware of.

The first act of the film is about the idea of symbolic pregnancy. It’s the _idea_ of becoming parents.

The second act then leads into “What would be just the end result of becoming parents”: that’s parental responsibility.

And the third part in the film is about pregnancy, and how the parental responsibility leads to a family unit. It’s a very human threat.

### Symbolic pregnancy

So we start with the symbolic pregnancy and think about how this resonates through the first act. The first thing we see these guys doing: Alan is looking at a dead dinosaur; and he’s obsessed with Velociraptors. And they’re looking at on an ultra-sounds screen. It’s a screen that’s normal people would be looking at when preparing to have a baby.

Then they arrive at the park, go through this “marvel at life” sequence with the Brachiosaurus; then they’re going through a facility tour, where they get around the process sex-education wise about how to create life, in this slightly odd and slightly unusual way compared to normal life–which allow people to reflect on a very common idea, “having children”, but in an unusual context.

And then they experience simulation of childbirth through the birth of the Raptor. And the entire time, we’ve got these two everyday people with very common problems, who are basically being shot around this guided tour of life by archetypal God and archetypal Devil. The reason we resonate with this is because these are human issues, being dressed up as very exotic ideas. 

### Parental responsability

So, then we move in to the next, where these two people have to take responsibility of the product of pregnancy, which is children.

So, our children arrive, they’re the grandchildren of God. And if you’ve paid attention to the very beginning of the film, there is a very very small line that states that Hammond daughter is getting divorced, which means sub-textually that these children are orphans, and these two people are potential parents.

They then go, and visit the Triceratops. Now, this is our first major dinosaur beyond the Brachiosaurus. And the function of this dinosaur is not to look like a cool dinosaur, that’s not the reason it’s there. The reason the Triceratops is there is to confirm to us than Ellie is ready to have kids.

If you watch this scene, pay attention to the dialog that Spielberg has given to Ellie to use, and look at the way she reacts to this Triceratops.

> _Ellie is kneeling at the triceratops_
> 
> ELLIE: Hey, baby girl…  
> ALAN: This one was always my favorite when I was a kid. And now I’ve seen one, it’s the most beautiful thing I ever saw. 

What Spielberg is doing is he’s masking the very understandable story that we all have as adults, and he’s hiding it under a layer of dinosaurs and theme park and allegories. 

So if we look at this Triceratops, and we listen to what she says, “_Hey baby girl_”, she cries, he says it’s the most beautiful thing he’s ever seen: this is all what people say when a baby is born.

And then, she then continues to look after the Triceratops, and sticks her hands in poo. The point logically of the poo is that she’s looking for the toxins that makes this triceratops sick. But you could look further the sickness in any number of ways. You could take blood samples, you could do any number of things. But the allegory dictates, in the case of Spielberg, that you have to have a pile of dung, because that way the audience starts to resonate, we start to resonate, with what she’s actually doing: she’s taking maternal care of a sick animal and changing his nappy. It seems really obvious when you think about it, and it is obvious, but unless we’re aware of it, we don’t necessarily plug it consciously.

So at this point of the story, we need to logically understand that Ellie is going to stay with the Triceratops, and Alan is going to go with the children. They need to be separated, because at this point, it’s all about _his_ trial. He has not shown any sign yet of being willing to become a father figure, so now he needs to be put through a metaphor of control, to show he’s ready to be a parent.

What Spielberg uses here is a couple of metaphors or symbols, which all tie into the allegory. And there is Eggs, Birth, Tree-houses and Fire.

And now, way to pay attention to is that in this scene the function–think about the function–of the Tyrannosaurus Rex is not to be a scaring monster, he’s there to represent the paternal instinct of Dr. Grant.

> _The T-Rex escapes over the fence, and roars loudly_
> 
> LEX: Dr. Grant?
>
> _The T-Rex walks in between the two cars_

I’m showing that short mainly because it’s awesome, and secondly because I honestly do not think that any shot in Jurassic World, despite 25 years of VFX progress, looks as good as that.

And the reason it doesn’t look as good as that is that we resonate with this; because somehow we know what’s going on with the story, because it is working on an archetypal level.

Maybe look at the framing of this: we’ve got the Devil and Alan in one car, and we’ve got two isolated children who have just been deserted by a lawyer, and between them is a giant monster that has just become encaged from his realm.

This in a Jungian archetype: this is the idea of his psychological fear of commitment, escaping the cage, and now he has to face up to real commitment to the children.

So the next shot, I want to show you how Spielberg now illustrates the idea of Dr.Grant becoming ready to take control of these kids.

> Alan finds a light, exits the car, and waves it to lure the T-Rex away from the kids car.

Notice how personal this situation is. This isn’t 60 frames a second cameras rotating ten miles an hour through every single possible point of access. This is one man, who’s just re-discovered his primal instinct, and he has rediscovered fire, and he’s now taking control of the situation with a wild animal. It’s not a massive VFX live fest. This is him, next to this broken shell of a car, with two children who are on the brink of metaphorical extinction. That’s what this scene is about. The reason that scenes like this are powerful is because they’re operating with archetypal scenarios.

> Alan helps Lex getting out the crushed car.

The car in this scene is an egg. So at this point the broken car is a broken shell, is being compressed, these two children are being buried in the mud, and then he births Lex. So this is the birth of their new relationship together. It’s not a birth physically, it’s them forming a new relationship to each other that’s not about her mating kids.

Then the T-Rex, which is a symbol for him not being committed to kids, forces all three of them back into the primal realm. He forces all of them into the park, where society doesn’t function, intellectual ideas don’t function, archeology doesn’t function.

The whole point of this–and if you think about it this doesn’t even logically make sense because the T-Rex just stepped out of that giant abyss–Spielberg has intentionally got that dropped there, even though it doesn’t make logical sense, in the same way that Kubrick moves doors into illogical places in The Shining. it’s there to unnerve and make it mythological.

In the next stage, the car is sent down into the tree. I just need you to pay attention to couple of things here: the situation that puts all three of them into, and what the subtext of what they say means.

> ALAN: Listen, Lex, right here, I’m going to look after you, but I have to go help your brother. So I want you to stay right here and wait for me.  
> LEX: He left us! He left us!  
> ALAN: But that’s not what I’m going to do. Okay? Stay here.

I’ll pause there for a second. When she says: “_He left us, he left us_”, and she say’s that twice before, when she’s in the car, “_He left us he left us he left us he left us_”, that’s a hell of a lot of screenplay space to be using up for things like an absolutely incidental throwaway line.

But it’s not incidental.

Because in the opening shots Spielberg sets up, these two children are currently going through a divorcing family. So when she says “_He left us_”, she’s not talking about the literal event of the lawyer leaving around the car, because that wouldn’t leave much of a scar, she’s talking about the idea that the father figure left us.

So when he says “_But that’s not what I’m going to do_”, the subtext is what is making us react. It’s not the text, it’s not the literal event, it’s the subtext.

And now he goes to get Tim.

> _Alan climbs in the tree_
> 
> ALAN: Okay so Tim, it’s not too bad.  
> TIM: Yes it is.  
> ALAN: It’s just like coming off a treehouse. Did your Dad ever build you a treehouse, Tim?  
> TIM: No.  
> ALAN: Me too.
> 
> _The car bends and starts to fall down_
> 
> ALAN: Go Tim!
> 
> _The car falls down the tree as Alan and Tim are descending it, then falls over them_
> 
> TIM: Well, we’re back in the car again.  
> ALAN: Hu… At least you’re out of the tree.

Now there are two elements to that car falling out of the tree which are really important. The car has already been an egg on the top side, a symbolic egg, and now it becomes a symbolic tree-house.

The whole reason that chain of events happens, that particular sequence of action sequences of the car coming down the tree, isn’t because of some arbitrary need for an action sequence. The logical chain of events is that the allegory dictates that Spielberg needs these symbols. Then, now, they’ve got those symbols, that dictates what situation needs to happen in order for this to unfold. The allegory droves the creative process.

So when the car comes down and then ends on top of them, that isn’t a throw-away idea. And you’ll see in the next scene that it’s being used, because now the car is an egg again. I’m going to show you how that plays out.

> _From the base of a tree, Alan, Tim and Lex look at an egg shell_
> 
> ALAN: it’s a dinosaur egg. The dinosaurs are breeding. Look! Life found a way.
> 
> _The camera pans on tiny dinosaur footsteps_

> _Ellie finds the broken car at the base of the tree_
> 
> ELLIE: Alan!  
> ROBERT: They’re not here.
>
> _The camera pans on human footsteps_

Now, what Spielberg is doing is he’s setting up a symmetry, so that you unconsciously create a relationship between the two events.

On the biological level, we have Malcom, the Chaos theory God talks about in the beginning of the film, “_Life has found a way_”. That manifests in the animals, the females of Jurassic Park breeding and creating life. And this shells are evidence of that.

But they are at the base of a tree, in exactly the same way that the broken shell of the car is at the base of the tree. And the idea is that there is a parallel between “_Life finds a way_” socially and “_Life finds a way_” biologically. That’s there to show us that what’s actually happening there: that a new life is being found in the relationship between Alan and the two kids.

Now we’re going further into the allegory of this guy on a very warped and twisted day with the kids at a theme park. And now they go up in another tree, and they start to interact with the Brachiosaurus. The function of the scene–we’ve saw the Brachiosaurus before. It was used in the opening segment they arrive at Jurassic Park, and it was what he and Ellie used to marvel at life. It was the miraculous opening shock to see a dinosaur.

And that’s now used as a symmetry with him creating a family unit with this two children. The positivity of the opening scene and the positivity of this scene are both mirroring each other.

> Alan finds the velociraptor claw in his pocket, looks at it, then throws it away

In the opening scene we saw that he’s scaring the shit out that kid. Spielberg is making us associate Alan with a Raptor. He openly talks about his respect for raptors, about how they’re pack hunters, about how they’re vicious, brutal animals–and he uses that knowledge and a prop to scare the living shit out of kids.

And in this scene, as he forges this new relationship, Spielberg uses that same prop to make sure that we understand his internal shift. These props, these sets, these production designs, these scenarios are always to externalize an internal shift in the characters. That didn’t exist in Jurassic World. None of that happens.

What is happening with Alan at the moment is that he’s going from an intellectual that obsesses over dead animals, and he’s now becoming somebody that interested  socially in living creatures. This is the shift he’s taking, and this is why we’re leading for the props into the scenario.

The next morning they wake up, and now it’s the birth of a new situation. Now, the entire thing is framed: he has developed into a father figure. He’s now going to the petting zoo, he’s interacting with the kids, almost if there is no reason, but that’s not the end of his growth. We need to escalate things.

As they make their way back to the Visitor Center, the allegory requires that we keep strengthening the bonds between the children. And what better way to strengthen the bond, and to remind us of the function of the T-Rex. So the T-Rex comes back for the second time. He only features three times with these characters, because he has a function that represents something.

They then get redirected to the fence, where Tim is electrocuted and effectively dies. We’ve already established the strong bond between Lex and Tim and Alan by having him save both of them individually in different scenarios. But now to really up the ante, we need to make something happen very symbolic. And that is that Alan needs to give life, literal life to Tim. Now this is a close as it gets to being an actual, literal father figure.

### Unified family

So now the third act is where we have to make them unified as a family. We’re sticking to the allegory here, remember? We’re not detouring just for sake of showing a sixty-foot T-Rex running out on a park.

Now we’re bringing the big guns, which is the Raptor. We’ve already established throughout the entire film that Alan was at the beginning a metaphorical Raptor. And now he’s throwing away his claw, and now we face up to the biggest threat that all of them have got, which is these guys basically attacking the children when they’re by themselves.

> Tim and Lex escape the Raptors in the kitchen.

The children now use the responsibility and the things they’ve learned from the role model in the previous scenes, and they look after each other. So this is a socially responsible film (Jurassic World isn’t).

Once the kids go through their personal journey looking after each other, the whole family unit is returned to work together.

> Alan looks at the Raptor through the reinforced door window.

Alan then faces up to the Raptor, the first time he has seen what he was admiring at the beginning of the film, sees it face-to-face, that’s the function of this shot, he gets to see it in the eye and see what it really is. And what is he seeing? He’s seeing himself in the eye. That’s the whole point of the raptor: he’s a symbol.

They then work as a team, and you’ll notice, as cheesy as it, Lex using that computer in a completely illogical way. The whole point of that is to show that–at the beginning of the film, if you remember, when Alan touches the monitor for the ultrasound, he breaks it. And the whole thread is our machines hate him.

Now the whole point of that thing, the whole point of that incidental moment is to make sure that when the family unit becomes unified, that it shows that there’s been a progress. So what we’re learning, what Alan is learning, is that he’s better off in the family unit. Things that he can’t do or things that he’s not capable of, the next generation, his girlfriend, his wife, his children can help him out with. It’s a whole progression of him as an individual.

So then the big trial is–and this is the point where he comes to a absolute point of being real father figure–is when he has to put his own life on the line.

This is the shot when the Raptors finally go to attack him, and he puts everything down, and he’s ready to give his life and make sure as best you can to save this family, his metaphorical family. Then, what happens in a moment where there’s absolutely no chance of winning? Our T-Rex comes back.

Now the T-Rex is the one that came out the cage and force this journey to start. It’s the one that forced them to the electric fence and got Tim killed, and forced Alan to resurrect him. This T-Rex is a _symbolic_ T-Rex. He’s not there to be a biological threat, he’s there to be a symbolic motivator for Alan.

So at this point they’re a family unit; they head back to the helicopter. And at this point you notice the whole theme: just pay attention to the theme of Alan’s theory “_the dinosaurs evolved into birds_”.

> Ellie looks at Alan with the children falling asleep on him.
> Alan looks at the birds flying outside of the helicopter.

### Conclusion

So the general idea–I’m going to wrap this up now–the whole thing about Jurassic Park is it’s symbolic evolution. And that’s why we resonate with it, because it’s coming at us from every single level.

The idea of the Brachiosaurus: it represents Family. Triceratops represents the idea of maternal instinct. The T-Rex symbolizes paternal instinct as well, in terms of father figure. The Velociraptor represents just brutal nature, and the bird represents the evolution of the characters into something bigger than themselves.

This is what we’re responding to when we watched Jurassic Park, this is what’s happening here [in the heart] not up here [in the head]. In summary, if you look at Jurassic Park, the reason that it stands strong 23 years later is because its foundations are strong. It’s foundations are built on allegory, archetypes, metaphor, subtext, motifs and symbolism. And Jurassic World…

> Chris Pratt<br>
> Dinosaurs<br>
> Dinosaurs<br>
> Dinosaurs<br>
> Dinosaurs<br>
> Dinosaurs

The reason I talk about today is because I would love to go and see a summer blockbuster that revolves around this, and not this, and maybe if a conversation started we might start seeing something like that.

Thank you.
