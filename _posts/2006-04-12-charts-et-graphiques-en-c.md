---
layout: post
title: "Charts et Graphiques en C#"
date: 2006-04-12 17:47
---

Dans le cadre d'un projet en C\#, j'ai eu à représenter des courbes dans
un repère - un peu comme sur l'écran d'un oscilloscope. J'ai donc dû me
documenter sur les différentes possibilités d'afficher des graphiques,
des courbes et des statistiques diverses en C\#. Cela m'a conduit à
plusieurs librairies et tutoriaux permettant d'atteindre ce but plus ou
moins facilement.

#### Les librairies

##### NPlot

[NPlot](http://netcontrols.org/nplot/wiki/) est une librairie de charts
légère, mais semble-t'il assez complète. Sa courte [page
d'exemples](http://netcontrols.org/nplot/wiki/index.php?n=Main.Examples)
est assez impressionante par le nombre d'options et de customisations
possibles. Les graphiques produits peuvent être affichés dans un
contrôle WindowsForm ou dans une page web ASP.NET. De plus, sa license
personnalisée est compatible avec une utilisation commerciale et non
contaminante - à l'inverse de la GPL. Malheureusement, il n'y a
quasiment aucune documentation, ni, ce qui est bien pire, aucun exemple
de code. Bref, par paresse, je n'ai même pas essayé. Mais l'auteur
semble être en train de rédiger une documentation et des exemples
complets ; NPlot peut donc être à surveiller.

##### ZedGraph

[ZedGraph](http://zedgraph.org/wiki/index.php?title=Main_Page) est une
autre librairie OpenSource, plus puissante et plus complète que NPlot.
Comme NPlot, elle est compatible WinForms et WebForms. Licencié sous
LGPL, qui permet une inclusion à du code propriétaire sans problèmes
particuliers, elle est très bien
[documentée](http://zedgraph.sourceforge.net/documentation/) (à l'aide
de l'exellent [NDoc](http://ndoc.sourceforge.net/)), et surtout possède
de nombreux
[exemples](http://zedgraph.org/wiki/index.php?title=Sample_Graphs),
ainsi qu'un [tutoriel très
complet](http://codeproject.com/csharp/zedgraph.asp) sur Code Project.
Bref, très complet, simple à utiliser... mais évidemment pas super
légère, et ne convenant probablement pas à un usage PocketPC ou
SmartPhone.

#### Tutoriaux

##### Création d'un contrôle utilisateur GDI+

Ce tutoriel, qui nous vient de [Supinfo](http://www.supinfo.com/),
explique comment créer un contrôle utilisateur utilisant GDI+. Cela
permet en gros de créer un composant qui instanciera une surface et
dessinera des primitives dessus. Le contrôle est ensuite exportable dans
Visual Studio, et peut se configurer entièrement avec le Form Designer.
Bref, un très bon pas-à-pas pour établir la structure d'un composant
GDI+ générique.  
[Supinfo - Créer un contrôle utilisateur d'affichage des
données](http://supinfo-projects.com/en/2004/cshap_user_control__fr/)

##### Charts minimalistes en C\#

Ce second tutoriel, hébergé sur [DevX](http://wwwdevx.com/), explique
comme afficher des graphiques et des courbes statistiques en utilisant
GDI+. Il détaille le système de changement de repère, la création
d'échelles et de graduations, et bien plus encore. Le texte de l'article
est assez avare en code, il faut penser à suivre les liens pour
récupérer le code correspondant. C'est ce tutorial qui m'a été le plus
utile pour arriver à mon propre composant.  
[Build a Reusable Graphical Charting Engine with
C\#](http://www.devx.com/dotnet/Article/20077)

#### Conclusion

On trouve beaucoup de librairies .NET de Charts sur le web, mais
beaucoup sont payantes, certaines reposent sur l'infâme controle ActiveX
d'Office, difficilement réutilisable... J'espère avoir pu montrer toutes
les possibilités offertes par les librairies gratuites, ainsi que la
possibilité de construire sa propre librairie facilement.
