---
layout: post
title: "WideGB now available on Windows"
date: 2019-05-18 12:04
---

Good news: the WideGB emulator is now available on Windows. See below for download links.

![The WideGB Game Boy emulator running on Windows](/images/widegb/widegb-on-windows.png)

## What took so long?

When [the first WideGB release was published](/posts/widegb-playing-game-boy-games-on-wide-screens/) a few weeks ago, only macOS builds were available.

Theoretically, the emulator WideGB is based on (SameBoy) is also available on Windows—so it should have been a simple matter of booting a Windows machine, and compiling the code.

So what took so long? Well, several small things.

First, setting up a Windows development environment in a Virtual machine is somehow painful. Downloading a Windows VM, installing Visual Studio, configuring the build environment, setting up dependencies, getting the vanilla SameBoy to build, and so on.

Second, I wanted to share the same source folders between my macOS host and the Windows guest. Which means having shared folders, which are mounted on Windows as an external network drive. And it turns out that on Windows, relative paths in Makefiles work by accident… except when mixing different drives, where they stop working entirely. More scripting and fixing required.

Last thing, I thought C was portable, and recompiling wouldn’t require major changes. Ha. Of course many POSIX features are not available on Windows (except when using a POSIX compatibility layer—but the SameBoy build system doesn’t). So remplacements or compatibility stubs were found for creating, enumerating and deleting directories, generating random numbers, etc.

## Download links

As always, this is very much a work in progress. Glitches are to be expected.

- **[SameBoy + WideGB for Windows](https://github.com/kemenaran/SameBoy/releases)**
- [SameBoy + WideGB for macOS](https://github.com/kemenaran/SameBoy/releases)
- [Source code on GitHub](https://github.com/kemenaran/SameBoy/tree/wide_gb/Misc)

## What’s next?

Now that the Windows version compiles, integrating WideGB in the **libretro port** should be interesting. SameBoy already features a working libretro core—so maybe WideGB could be integrated into this.

The **heuristic used to detect a new scene** is still finicky: sometimes it detects too little, and sometimes too much. A better perceptual hashing implementation could be less sensitive to scrolling (which hardly ever signals a scene change), and more sensitive to hard-cuts and fades-to-white.

To fine-tune the algorithm, a good tool would be to dump some typical frames of different games into PNG files—and then use a scripting language to test different algorithms. New algorithms could be applied to the dumped frames, to compare the results and check for regressions.

Also, **my C-fu is still not as good as I’d like to**: there are some inefficiencies, crashes, and low-hanging performance optimisations that would definitely improve the stability of WideGB.

And of course, [contributions are welcome](https://github.com/kemenaran/SameBoy/tree/wide_gb/Misc)!
