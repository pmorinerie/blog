---
layout: post
title: "L'association LIRE recherche des volontaires !"
date: 2009-06-13 13:00
---

Si certains d'entre vous ont suivi le blog que j'ai tenu pendant mon
[volontariat au Togo](http://www.lavaliseencarton.fr/), vous vous
souvenez sans doute de **l'association LIRE**, avec laquelle j'ai
beaucoup travaillé, et qui vise à [promouvoir la lecture au
Togo](http://www.asso-lire.org/). Avec cette association de professeurs
togolais, nous avions mis en place toute sorte d'activité pour inciter
les collégiens à la lecture : les emmener dans les bibliothèques de
Kpalimé, organiser des recherches documentaires et des concours,
apporter les livres de la bibliothèque ambulante dans les collèges trop
éloignés de la ville…

En sept mois, nous avons eu le temps de faire beaucoup de choses, et de
nous rendre compte que le projet est motivant, sérieux, tient la route,
et surtout donne des résultats concrets : les élèves, progressivement,
prennent l'habitude de lire — et croyez moi, il y en a besoin.

Pour continuer toutes ces activités, nous nous sommes dit, en quittant
le Togo, que ces gens là auraient bien besoin d'autres volontaires, qui
pourraient leur donner un coup de main. On en a beaucoup discuté avec
les gens sur place, qui sont très demandeurs — et surtout motivés pour
s'impliquer vraiment dans l'accueil de volontaires étrangers.

Bref, **vous qui cherchez un projet de [volontariat en
Afrique](http://www.asso-lire.org/partir-comme-volontaire/), nous avons
quelque chose pour vous !** Des gens sympathiques, un hébergement dans
des familles, une belle occasion de découvrir l'Afrique et le Togo et de
donner un coup de main. Et c'est supervisé par nous, volontaires qui en
revenons tout juste, qui avons aidé à monter le projet là bas — enfin
qui savons comment se passent les choses là bas.

Si vous n'êtes pas vraiment dans l'optique de partir en Afrique, là,
comme ça (je sais, ça arrive), vous pouvez tout de même [soutenir les
activités de l'association
LIRE](http://www.asso-lire.org/soutenir-nos-activites/), notamment dans
le cadre du **projet de
[parrainage-lecture](http://www.asso-lire.org/soutenir-nos-activites/#parrainage-lecture)** :
vous achetez quelques livres pour jeunes enfants (entre 3 et 8 ans) dans
une libraire, et nous les transmettons à des volontaires européens au
Togo qui les liront aux enfants de leur famille d'accueil. Nous avons
déjà expérimenté ce projet dans notre famille-hôte, et c'est vraiment
fantastique : là-bas, personne ne songe à faire la lecture aux enfants —
et pourtant ils adorent ça, en redemandent, se mettent à faire semblant
de lire eux-même… Ce contact avec les livres qui se développe très jeune
leur manque vraiment, parce que ce n'est pas dans les habitudes des
parents ; alors en plus de la joie immédiate des enfants lorsqu'on leur
fait la lecture, c'est un bel investissement pour l'avenir.

N'hésitez pas à parler de cette association autour de vous —
franchement, c'est un beau projet, et je vais travailler dessus encore
un certain temps.

**[Association LIRE — Promotion de la lecture au
Togo](http://www.asso-lire.org/)**
