---
layout: post
title: "Configurer son DNS avec BIND"
date: 2006-04-09 18:43
---

Ceux qui font un peu d'administration serveur ont du se perdre assez
fréquemment dans les méandres de **BIND** et des **configurations DNS**.
Ce long tutorial, trouvé sur [Howtoforge](http://www.howtoforge.com),
vise à tout reprendre de zéro, et à faire réellement comprendre la
manière dont le système DNS fonctionne. Après cette lecture, au moins,
on sait ce que l'on fait quand on configure BIND, ce qui est très
agréable.

[Traditional DNS Howto](http://www.howtoforge.com)
