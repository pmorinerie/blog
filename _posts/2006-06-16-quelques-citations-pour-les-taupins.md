---
layout: post
title: "Quelques citations pour les taupins"
date: 2006-06-16 14:57
---

Les mordus de physique ou ceux ayant longuement souffert en classe
préparatoire devraient s'amuser à lire ces quelques [citations d'un
professeur de physique en
MP](http://www.eleves.ens.fr/home/sray/dd.html).

Quelques extraits :

“Y'a un théorème d'optique qui dit que les rayons vont toujours de la
gauche vers la droite.”

“Ceci est un interféromètre de Michelson. Il y a un super-beau miroir
bien poli et tout ; celui qui met le doigt dessus, il va mourir très
vite ; celui qui, ayant mis les doigts dessus, essaie d'essuyer les
traces avec un chiffon, il va mourir dans d'atroces souffrances.”

“Théorème : toute fonction est intégrable”
