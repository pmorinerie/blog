---
layout: post
title: "Paris mon camarade – Bernard Dimey"
date: 2022-12-09 11:08
---

Un poème de [Bernard Dimey](https://fr.wikipedia.org/wiki/Bernard_Dimey), que j'ai eu du mal à trouver facilement en ligne :

> Paris, mon camarade, pour causer, faut connaître,<br>
> Faut s’y prom’ner la nuit, faut s’y fair’ des copains, <br>
> Faut s’offrir du bitume, en faire des kilomètres, <br>
> Y’aura toujours un pote pour t’offrir un bout de pain.<br>
> Paris, si tu connais c’est comme un’ cour d’école,<br>
> T’es tout partout chez toi si t’as l’coeur bien placé,<br>
> Si jamais t’as l’bourdon, va voir ceux qui rigolent<br>
> Et tu verras, l’soleil y en a toujours assez.
>
> Paris, mon camarade, c’est pas tout c’qu’on raconte,<br>
> C’est pas les bulldozers, c’est pas la Tour Machin,<br> 
> C’est un coeur qui s’allume au hasard des rencontres,<br> 
> C’est le petit bistrot où vont tous les copains ;<br> 
> Paris, si tu connais, c’est le vent dans les voiles,<br> 
> Romeo et Juliette en blue-jeans à midi,<br> 
> C’est le clodo Marcel qui dort sous les étoiles ;<br> 
> Y a de l’Enfer, c’est sûr, mais il y du Paradis.
> 
> Paris, mon camarade, si tu connais, c’est chouette,<br> 
> C’est toujours aussi bon, quand j’fous l’camp, quand j’reviens,<br> 
> C’est le sourire en coin quand le cafard me guette,<br> 
> C’est l’Opéra d’quat’ sous qu’est pas fait pour les chiens,<br> 
> C’est le seul cinéma où y a jamais d’entracte,<br> 
> Où j’ai tous mes amours et j’espère vraiment<br> 
> M’offrir un soir la joie d’y jouer mon dernier acte<br> 
> Et d’être parisien jusqu’au dernier moment.

_[Bernard Dimey](https://fr.wikipedia.org/wiki/Bernard_Dimey), récité par [Bernard Beaufrère](https://www.facebook.com/BernardBeaufrereGardeChampetreRepubliqueMontmartre/)_
