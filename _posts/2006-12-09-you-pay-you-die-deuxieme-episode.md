---
layout: post
title: "You pay You die - Deuxième épisode"
date: 2006-12-09 15:50
---

Le deuxième épisode de **[You pay You
die](http://kemenaran.winosx.com/?2006/12/01/51-you-pay-you-die)** est
sorti. Désormais, le site propose également un flux RSS et un podcast,
pour accéder aux nouveaux épisodes plus facilement.

**[www.youpayyoudie.com](http://www.youpayyoudie.com)**
