---
title: "Category: Disassembling Link’s Awakening"
lang: en
date: 2016-08-13 11:05
layout: redirect
redirect:  https://zladx.github.io/archives
---

These articles are part of a “Disassembling Link’s Awakening” series, where I attempt to gain some understanding on how special effects were implemented in this game.

1. [Introduction: Special Effects in Link’s Awakening](/posts/special-effects-in-zelda-links-awakening)
2. [Link’s Awakening: Rendering the Opening Cutscene](/posts/links-awakening-rendering-the-opening-cutscene)
3. [Link’s Awakening: Progress Report – week 1](/posts/links-awakening-disassembly-progress-report)
4. [Link’s Awakening: Progress Report – week 2](/posts/links-awakening-disassembly-progress-report-week-2)
5. [Link’s Awakening: Progress Report – week 3](/posts/links-awakening-disassembly-progress-report-week-3)
6. [Link’s Awakening: Progress Report – week 4](/posts/links-awakening-disassembly-progress-report-week-4)
7. [An in-depth look at Link’s Awakening render loop](/posts/links-awakening-render-loop)
8. [Link’s Awakening: Progress Report – week 5](/posts/links-awakening-disassembly-progress-report-week-5)
9. [Link’s Awakening: Progress Report – week 6](/posts/links-awakening-disassembly-progress-report-week-6)
10. [Link’s Awakening: Progress Report – part 7](/posts/links-awakening-disassembly-progress-report-part-7)
11. [Link’s Awakening: Progress Report – part 8](/posts/links-awakening-disassembly-progress-report-part-8)
12. [Link’s Awakening: Progress Report – part 9](/posts/links-awakening-disassembly-progress-report-part-9)
13. [Link’s Awakening: Progress Report – part 10](/posts/links-awakening-disassembly-progress-report-part-10)
14. [Link’s Awakening: Progress Report – part 11](/posts/links-awakening-disassembly-progress-report-part-11)
15. [The hidden structure of Link's Awakening Overworld map](/posts/links-awakening-overworld-map)
16. [Achieving partial translucency on the Game Boy Color](/posts/links-awakening-partial-translucency)
17. [Link’s Awakening: Progress Report – part 12](/posts/links-awakening-disassembly-progress-report-part-12)

