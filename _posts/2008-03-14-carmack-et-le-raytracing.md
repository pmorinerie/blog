---
layout: post
title: "Carmack et le raytracing"
date: 2008-03-14 11:54
---

![John Carmack](/images/divers/John_Carmack.jpg)En réponse à l'article
d'Intel sur le raytracing que je
[mentionnais](http://kemenaran.winosx.com/?2008/01/24/99-raytracing) il
y a quelques semaines, John Carmack (créateur de Doom et Quake) [nous
livre ses
pensées](http://www.pcper.com/article.php?aid=532&type=overview) sur
cette technique et sur le futur des techniques 3D en général.

“I saw the quote from Intel about making no sense for a hybrid approach,
and I disagree with that. (...) I have my own personal hobby horse in
this race and have some fairly firm opinions on the way things are going
right now. I think that ray tracing in the classical sense, of
analytically intersecting rays with conventionally defined geometry,
whether they be triangle meshes or higher order primitives, I’m not
really bullish on that taking over for primary rendering tasks which is
essentially what Intel is pushing.”

En résumé, Carmack ne croit pas au seul raytracing comme la prochaine
innovation, et pense à d'autres techniques qui permettront d'avoir un
rendu plus réaliste et impressionnant à moindre coût. Ce type a tendance
à mener l'innovation dans ce domaine, son avis est donc instructif, et
sans doute éclairant sur ce que l'on pourra voir sur nos machines d'ici
quelques années.

**[John Carmack on id Tech 6, Ray Tracing, Consoles, Physics and
more](http://www.pcper.com/article.php?aid=532&type=overview)**
