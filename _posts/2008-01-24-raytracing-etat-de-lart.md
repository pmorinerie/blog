---
layout: post
title: "RayTracing : état de l'art"
date: 2008-01-24 12:13
---

[![Raytracing
refections](http://upload.wikimedia.org/wikipedia/en/thumb/9/93/Raytracing_reflection.png/225px-Raytracing_reflection.png)](http://en.wikipedia.org/wiki/Image:Raytracing_reflection.png)On
n'a jamais vraiment cessé de parler du raytracing. Cette méthode de
rendu d'images 3D consiste, plutôt que de dessiner des polygones et des
textures, à tracer des rayons depuis la caméra, et à voir sur quels
objets ils rebondissent. Avantages : une excellente gestion des reflets
et des ombres, pour un rendu magnifique.

L'idée est simple, mais demande une puissance monstrueuse — jusqu'à
présent, c'était principalement un truc de geeks cherchant à tester les
limites de leurs cartes graphiques. Cela dit, avec l'arrivée des
processeurs à 4 ou 8 cœurs, massivement parallèles, la technique
commence à devenir intéressante du point de vue temps de calcul. **[Un
article d'un chercheur chez
Intel](http://www.pcper.com/article.php?aid=506&type=expert&pid=1)**
montre en effet qu'au delà d'un certain nombre de polygones, le
raytracing devient beaucoup plus efficace que la rasterisation.

![Raytracing performances](/images/divers/logvslin.png)

Cet article fait donc le point sur l'état de l'art, et se demande si
nous aurons vraiment une période de cohabitation
raytracing/rasterisation, qui n'aurait aucun sens d'un point de vue
technique. Plus concrètement, il montre aussi comment le raytracing
permettrait d'améliorer et de simplifier significativement des jeux
comme [Portal](http://www.youtube.com/watch?v=GWzmL05OlYA).
