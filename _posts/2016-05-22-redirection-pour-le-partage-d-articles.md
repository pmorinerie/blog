---
layout: post
title: Nouvelle URL de partage d'articles
date: 2016-05-22 12:05
draft: true
---

Bonjour à tous !

La liste d'articles que je partage utilisait jusqu'alors Delicious — mais ce site fonctionne de moins en moins bien. Apparemment une nouvelle équipe a pris les rennes, et a essayé de refaire le site web depuis zéro. Résultat : le site ne fonctionne plus, ils ont dû revenir à l'ancienne version deux ou trois fois, ils injectent de la publicité dans les articles que je vous partage, et rien de tout ça ne fonctionne plus sous Firefox.

Bref, ça y est, j'ai finalement changé de système pour générer ce flux RSS d'articles partagés. Pour l'instant c'est basé sur Pocket (mais je migrerai peut-être vers Diaspora à un moment).

Dans tous les cas, **si vous voulez continuer à lire cette liste, rajoutez cette adresse à votre
lecteur RSS :**

<a href="https://pmorinerie.eu/rss/shared.php">https://pmorinerie.eu/rss/shared.php</a>

Ce nouveau lien fonctionne dès maintenant, il sera mis à jour vers le nouveau
système sans pub automatiquement, et si jamais je change de système pour publier ces articles il continuera à fonctionner quoi qu'il arrive.

Plein de bises à tous !
Pierre
