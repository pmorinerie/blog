---
layout: post
title: "Javascript - un problème de poids"
date: 2006-04-09 17:54
---

Comme vous l'avez remarqué, j'utilise beaucoup Javascript en ce
moment... et comme tous les utilisateurs de librairies complexes, je me
suis trouvé confronté au problème du poids des scripts. Une bonne
librairie Javascript peut faire parfois jusqu'à 200 Ko, ce qui prend
plus d'une minute à charger pour une connexion 56K.

Ce billet présente quelques méthodes et conseils pour minimiser ce
problème.

#### Le cache

La première astuce, évidemment, est d'utiliser au maximum le cache du
navigateur. Après tout, un gros CSS peut aller jusqu'à 20-30 Ko, mais il
n'est chargé que lors de l'affichage de la première page du site - pour
les pages suivantes, la version en cache est utilisée.

Il peut donc être au final rentable d'utiliser une librairie lourde,
mais complète. On la charge ainsi en première page, cela prend un peu de
temps, mais ensuite elle sera réutilisée tout au long du site. Deux
inconvénients cependant : le Javascript doit être parsé par le
navigateur à chaque page, même s'il n'est plus téléchargé, ce qui prend
du temps ; de plus, si la première page ne s'affiche pas rapidement, le
visiteur peut être tenté d'aller voir ailleurs.

#### La compression

Une autre idée est de compresser les fichiers Javascript. Il ne s'agit
pas ici d'une compression ZLIB ou autre, mais d'une réduction de la
taille du code, tout en le conservant sous forme de texte. La plupart
des compresseurs Javascript enlèvent les commentaires, les espaces
inutiles et les sauts de ligne ; certains, plus avancés, renomment les
variables internes, pour gagner encore un peu de place. Un bon
compresseur peut parfois diviser le poids d'un gros fichier Javascript
par deux.

Le meilleur compresseur Javascript est sans doute
[ShrinkSafe](http://alex.dojotoolkit.org/shrinksafe/), par l'équipe de
[Dojo](http://dojotoolkit.org/). Il est basé sur un interpréteur
Javascript, et non pas sur des expressions régulières plus ou moins
hasardeuses, comme beaucoup d'autres compresseurs. En utilisant
ShrinkSafe, j'ai par exemple fait passer la totalité du code de
[script.aculo.us](http://script.aculo.us) de 171 Ko à 99 Ko, ce qui est
extrêmement appréciable, en particulier pour les 56K.

#### Utiliser des librairies légères

Identifier précisément ses besoins permet d'utiliser la bonne librairie,
et donc d'adapter le poids du Javascript. Si vous envisagez de bâtir un
site faisant appel à des effets visuels riches et à des requêtes Ajax
fréquentes, l'utilisation du couple
[script.aculo.us](http://script.aculo.us)/[prototype](http://prototype.conio.net/)
est tout à fait envisageable, pour les raisons de cache exposées
ci-dessus. En revanche, si vous ne voulez intégrer que quelques effets
simples à un endroit précis, ou seulement une requête Ajax particulière,
il est inutile de forcer le téléchargement d'une librairie complète.

[Moo.fx](http://moofx.mad4milk.net/) est une librairie d'effets
graphiques de 3 Ko seulement. Elle permet d'utiliser des effets de
redimensionnement et de fondu, de les combiner, et de les implémenter
facilement. Il devient donc tout à fait enviseagable d'intégrer des
effets visuels à une page seulement : moo.fx se charge aussi rapidement
qu'une petite image. Elle n'est de plus pas compressée, on peut donc
réduire encore un peu sa taille en utilisant ShrinkSafe. Son seul point
faible est le manque d'exemples, la documentation est en revanche assez
complète.

#### Conclusion

En résumé, l'utilisation d'un compresseur Javascript permet de diminuer
le temps de chargement des librairies Javascript. Après, si l'on compte
utiliser intensivement effets et Ajax, mettre en cache une librairie
complète est une bonne idée ; pour des effets poncutels, il vaut en
revanche mieux adopter une librairie légère, comme
[Moo.fx](http://moofx.mad4milk.net/).
