---
layout: post
title: "Installer Windows via BootCamp et Parallels"
date: 2007-09-08 15:33
---

Développant toujours parfois avec des outils Microsoft, j'ai besoin
d'avoir une installation propre de Windows sur mon Macbook. De plus, je
ne suis pas contre quelques jeux de temps en temps. Pour les programmes
simples, une solution de virtualisation comme Parallels fait l'affaire —
pour les jeux, c'est autre chose, et démarrer Windows en natif serait
une bonne chose.

En résumé, j'ai besoin de :

-   Programmer sous Windows : peu de puissance requise, bonne
    intégration à Mac OS si possible : virtualisation
-   Jouer : puissance maximale nécessaire, intégration pas nécessaire :
    système natif

La solution : combiner Parallels et Boot Camp, et ainsi profiter du
meilleur des deux mondes. Trouver une bonne manière d'organiser ses
partitions et ses fichiers pour arriver à quelque chose de pratique
n'est pas évident ; voici en tout cas comment je m'y suis pris.

![Boot Camp logo](/images/logos/bootcamp-logo.png)

### Etape 1 : installer Windows via BootCamp

-   Télécharger [Boot Camp](http://www.apple.com/macosx/bootcamp/)
    depuis le site d'Apple
-   L'installer, lancer l'Assistant
-   Graver le CD de pilotes
-   Lancer l'installation de Windows depuis l'Assistant
-   Insérer le CD de Windows et suivre la procédure d'installation

A la fin de cette étape, Windows est installé correctement, et l'on peut
sélectionner son système d'exploitation en maintenant la touche Option
appuyée au démarrage de l'ordinateur.

![Parallels Desktop logo](/images/logos/parallels-logo.png)

### Etape 2 : installation de Parallels Desktop

-   Télécharger la version de test de [Parallels
    Desktop](http://www.parallels.com/products/desktop/) (limitée à 15
    jours) ou mieux, l'acheter
-   Installer Parallels
-   Exécuter l'application — une machine virtuelle basée sur BootCamp
    est créée automatiquement
-   Lancer la machine virtuelle, attendre que Parallels installe ses
    propres composants

A la fin de cette étape, Windows tourne dans une fenêtre sur votre
Bureau Mac OS, la souris devrait sortir et entrer automatiquement dans
la fenêtre Windows, et les options graphiques être toutes activées.
