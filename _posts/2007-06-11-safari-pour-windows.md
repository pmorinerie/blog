---
layout: post
title: "Safari pour Windows"
date: 2007-06-11 22:16
---

![Logo de Safari](/images/logos/safari.jpg)Enfin ! Steve Jobs a
annoncé aujourd'hui un portage du navigateur Web Safari pour Windows (XP
et Vista). L'idée avait été avancée par [certains
blogueurs](http://firsttube.com/read/A-Suggestion-for-Apple-in-2007)
depuis quelques temps, et même envisagée sur [certaines pages du wiki de
Mozilla](http://wiki.mozilla.org/Firefox3/Firefox_Requirements#Observations_.26_Assumptions) -
de plus, un portage de WebKit, le moteur de rendu Open-source de Safari,
existait déjà ; mais c'est désormais officiel, le navigateur complet
sera porté.Une bêta est déjà disponible, même si elle ne fonctionne pas
encore de manière fantastique.

**[Bêta publique de Safari pour Windows](http://www.apple.com/safari/)**

Source :
[MacRumors](http://www.macrumors.com/2007/06/11/wwdc-safari-for-windows/)

**Edit :** Les réactions sont plus que mitigées après les [premiers
tests](http://arstechnica.com/news.ars/post/20070612-afirst-look-safari-3-on-windows.html)
de cette version bêta, le manque de stabilité, les
[bugs](http://erratasec.blogspot.com/2007/06/niiiice.html) et les
plantages décourageant les testeurs – même la sécurité n'est pas au
rendez-vous, avec une faille
[0-day](http://fr.wikipedia.org/wiki/Zero_day) découverte [deux heures
après la mise à disposition de Safari
3](http://larholm.com/2007/06/12/safari-for-windows-0day-exploit-in-2-hours/).

PCInpact propose quelques solutions pour [corriger les erreurs et
plantages les plus
courants](http://www.pcinpact.com/actu/news/36974-apple-safari-windows-polices-instabilite-dav.htm),
liés, semble-t-il à des problèmes de police et aux systèmes non
anglophones. Il s'agit de renommer ou de remplacer quelques fichiers de
Safari, ce qui donne un résultat plutôt bon : moins de textes illisibles
et un panneau des marque-pages enfin accessible sans plantages.
