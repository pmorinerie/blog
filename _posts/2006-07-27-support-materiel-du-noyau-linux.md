---
layout: post
title: "Support matériel du noyau Linux"
date: 2006-07-27 09:42
---

“Le Plug & Play sous Linux n'est pas encore au niveau de Windows” :
voici une des principales idées reçues sur le [noyau
Linux](http://fr.wikipedia.org/wiki/Noyau_Linux). Pour répondre à ces
affirmations sur le support matériel de Linux, Greg Kroah-Hartman a mis
en ligne les transparents d'une conférence donnée lors de l'Ottawa Linux
Symposium, intitulée "[Myths, Lies, and Truths about the Linux
kernel](http://www.kroah.com/log/linux/ols_2006_keynote.html)".

Intéressant, peu technique mais plutôt explicatif, ce document explique
les choix de développement du noyau Linux en ce qui concerne les pilotes
matériels. Evidemment, c'est un peu biaisé : Greg travaille pour Novell.
Mais cela montre que le
[bazar](http://catb.org/jargon/html/B/bazaar.html), s'il reste très
différent des modèles de développement traditionnels, a encore de beaux
jours devant lui.
