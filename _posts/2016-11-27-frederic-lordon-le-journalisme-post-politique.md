---
layout: post
title: "Frédéric Lordon : le journalisme post-politique"
date: 2016-11-27 18:55
---

De Frédéric Lordon, _[Politique post-vérité ou journalisme post-politique ?](http://blog.mondediplo.net/2016-11-22-Politique-post-verite-ou-journalisme-post)_ :

> Ce que le journalisme « de combat » contre la post-vérité semble donc radicalement incapable de voir, c’est qu’il est lui-même bien pire : un journalisme de la post-politique — ou plutôt son fantasme. Le journalisme de la congélation définitive des choix fondamentaux, de la délimitation catégorique de l’épure, et forcément in fine du gardiennage du cadre. La frénésie du fact-checking est elle-même le produit dérivé tardif, mais au plus haut point représentatif, du journalisme post-politique, qui règne en fait depuis très longtemps, et dans lequel il n’y a plus rien à discuter, hormis des vérités factuelles. **La philosophie spontanée du fact-checking, c’est que le monde n’est qu’une collection de faits et que, non seulement, comme la terre, les faits ne mentent pas, mais qu’ils épuisent tout ce qu’il y a à dire du monde**.

Derrière les phrases un peu longues et le ton agacé, l'article entier est une réflexion intéressant sur les rapports du « _journalisme post-vérité_ » et de la fin des idéologies.

De ce que j'en comprends, l'argument est que si tant d'énergie est mise à nous convaincre que rien ne peut plus changer, qu'il faut se plier à la réalité, celle qui reconduit le monde tel qu'il est – n'est-il pas inévitable que les votes finissent par se tourner vers les candidats qui proclament malgré tout la possibilité de faire changer les choses ?


