---
layout: post
title: "Fix &quot;duplicate symbols&quot; when linking a universal static library with iOS SDK 4"
date: 2010-06-30 16:18
---

If you updated to the recently released iOS SDK 4, you may have
encountered a rather annoying issue. All projects that link again a
static library which is the combination of multiple sub-libraries now
fail to link, producing only a "duplicate symbols" error. It is the
case, for instance, with the widely used
[Three20](http://github.com/facebook/three20) library for iPhone
development.

This is because of a bug in libtool : when building an universal library
(understand "with multiple architectures", like the "Standard" build
option that includes both armv6 and armv7), if you are merging multiple
libs together, libtool will fail to strip properly some parts of the
libs, then try to merge them, adding the same symbols several times. It
will only be noticed on a linker invocation, such as when building the
application that uses the library. This is perfectly explained with much
details in [this blog post of James
Briant](http://blog.binaryfinery.com/?p=225).

He suggested a script to fix this issue with Three20. I generalized it,
so it can be used for any library that has the same issue.

To use it, open your library Target in Xcode, add a new "Shell Script
build phase", and copy the content of this script inside :

    #!/bin/bash# (c) 2010 James Briant, binaryfinery.com# Edited by Pierre de La Morinerieif [[ $TARGET_BUILD_DIR == *iphoneos* ]] && [[ $ARCHS == *\ * ]]thenecho "Rebuilding library as proper multiarch file"LIB_ARM6=$TEMP_FILES_DIR/Objects-$BUILD_VARIANTS/armv6/$EXECUTABLE_NAMELIB_ARM7=$TEMP_FILES_DIR/Objects-$BUILD_VARIANTS/armv7/$EXECUTABLE_NAME# Libtool skrewed up, and built fat binaries in place of the arch-specific ones : strip them.lipo $LIB_ARM6 -remove armv7 -o $LIB_ARM6lipo $LIB_ARM7 -remove armv6 -o $LIB_ARM7# Now recombine the stripped lib to the final productlibtool -static $LIB_ARM6 $LIB_ARM7 -o $BUILT_PRODUCTS_DIR/$EXECUTABLE_NAMEelseecho "Skipping arm multi-architecture rebuild"fi

Now, each time your project is built, the architectures in the output
binary will be correctly merged. This should be as unobtrusive as
possible — but hopefully Apple will fix the bug soon.
