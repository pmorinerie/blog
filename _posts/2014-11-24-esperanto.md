---
layout: post
title: "Espéranto"
date: 2014-11-24 08:21
---

Et donc j'ai passé le week-end à discuter en espéranto.

Après seulement deux mois d'apprentissage tranquille (genre lire une
méthode un soir sur deux, ou trouver des sous-titres en espéranto pour
"Mon voisin Totoro"). Et venant de quelqu'un fâché avec les langues, et
qui a toujours eu du mal avec ça.

La vitesse à laquelle on peut arriver à un niveau de conversation, c'est
fou ; c'est *vraiment* facile.
