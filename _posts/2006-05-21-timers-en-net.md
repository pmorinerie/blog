---
layout: post
title: "Timers en .NET"
date: 2006-05-21 15:53
---

Dans le cadre d'un projet en C\#, j'avais à lire des données dans un
buffer *n* fois par secondes - rien de plus classique. Je me suis
cependant rendu compte que cela nécessitait des Timers assez précis : si
l'on perd ne serait-ce que 10 millisecondes par seconde à cause
d'imprécisions du timer, le système devient rapidement impossible à
gérer.

Or les différents types de Timers du .NET Framework ne fournissent
qu'une précision maximale d'environ 20ms - et encore, cela dépend
fortement du système hôte. C'est largement insuffisant pour de
nombreuses applications où la gestion du temps est critique... Comment
alors peut-on faire pour obtenir des ticks parfaitement réguliers ?

Il faut pour cela utiliser des Timers Multimédia. On désigne
généralement sous ce nom les Timers faisant appel à des fonctions Win32
particulière, produisant des ticks très précis, mais assez couteux en
temps processeur. Il n'existe hélas pas de Timer Multimédia dans le .NET
Framework... On peut cependant faire une surcouche .NET à ces APIs, et
ainsi utiliser des timers précis dans .NET. C'est ce que Leslie Sanford
a fait, et cela fonctionne parfaitement.

L'inconvénient de ces Timers est bien sûr la non-portabilité, car ils
marshallent directement les APIs Win32. Il est donc exclu de les faire
fontionner sous PocketPC, SmartPhone, bref sous tout système
non-Windows. Il doit y avoir moyen de les porter sous Linux, mais ce
serait sans doute compliqué.

En tout cas cet article de Leslie Sanford est très instructif, et permet
enfin d'avoir une synchronisation du temps correcte en C\#.

[The Multimedia Timer for the .NET Framework - The Code
Project](http://www.codeproject.com/csharp/lescsmultimediatimer.asp)
