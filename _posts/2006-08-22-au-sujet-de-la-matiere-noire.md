---
layout: post
title: "Au sujet de la matière noire"
date: 2006-08-22 09:41
---

Voici un article intéressant sur la *[matière
noire](http://fr.wikipedia.org/wiki/Mati%C3%A8re_noire)* - cette matière
indétectable qui semble composer la majeure partie de l'univers, par
opposition à la matière dire *lumineuse*. Jusqu'à présent, son existence
n'était qu'une théorie pour expliquer certaines variations très étranges
du champ gravitationnel, mais son existence semble être confirmée par de
récentes analyses autour de deux amas de galaxies, qui se sont
interpénétrées il y a 150 millions d'années.

Je n'ai pas assez de connaissances pour juger de la solidité de
l'article, mais il est simple, bien expliqué, illustré de façon
pertinente... Bref, une lecture agréable si on s'intéresse un tant soit
peu à l'astrophysique.

[Dark Matter Exists - Cosmic
Variance](http://cosmicvariance.com/2006/08/21/dark-matter-exists/)
