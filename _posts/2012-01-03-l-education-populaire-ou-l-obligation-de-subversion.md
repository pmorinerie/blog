---
layout: post
title: "L'éducation populaire ou l'obligation de subversion"
date: 2012-01-03 11:08
---

Transmis via
[Solidarités-Jeunesses](http://www.solidaritesjeunesses.org), un message
qui me tient à cœur :

> Par quelque biais qu'on pose la question, la conclusion s'impose:
> **l'éducation populaire ne peut échapper à sa vocation profonde: la
> subversion**. le mot peut faire peur.
>
> On le voit la loi Sapin annonce l'arrivée du privé dans le loisir et
> la culture: si un maire est obligé (ou a le droit) de soumettre
> l'attribution d'un équipement d'éducation populaire à un appel
> d'offre, ou un marché concernant le loisir, ou les vacances, ou le
> théâtre... comme pour n'importe quel marché public, on ne voit pas ce
> qui empêcher le privé de se mettre sur les rangs et de dire: je fais
> mieux et moins cher! La seule réponse à ce défi est dans ce qui fait
> depuis toujours la légitimité de l'éducation populaire et de
> l'associatif: la faculté à subvertir. Cela, le secteur marchand ne le
> fera jamais. *(...)*
>
> Mais qu'entendons par "subversion"? Seraient-ce les restes du Grand
> Soir? Non, bien sûr. Cela peut aussi s'appeler citoyenneté, ou
> politisation ou tout simplement...idéal. Après des décennies où le
> terrain de la politisation fut occupé par des groupes extrémistes, il
> peut être utile de rappeler qu'on peut être politisé sans vouloir
> pendre tous les bourgeois.
>
> *L'auteur rappelle alors la déclaration de Villeurbane, rédigée en
> 1968 par les patrons du théâtre public d'alors avec cette définition
> de la "subversion" :*
>
> "Tout effort d'ordre culturel ne pourra plus que nous apparaître vain
> aussi longtemps qu'il ne se proposera pas expressément d'être une
> entreprise de politisation: c'est-à-dire d'inventer sans relâche, à
> l'intention du non-public, des occasions de se politiser, de se
> choisir librement, par delà le sentiment d'impuissance et d'absurdité
> que ne cesse de susciter en lui un système social où les hommes ne
> sont pratiquement jamais en mesure d'inventer leur propre humanité."
> (...)
>
> Tant il est vrai qu'à la fin, la seule question est bien d'humaniser
> la société.
>
> *Jacques Bertin,* in *Politis, Le retour de l'utopie, hors série,
> février-mars 2000*
