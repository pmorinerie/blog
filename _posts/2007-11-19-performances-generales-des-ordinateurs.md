---
layout: post
title: "Performances générales des ordinateurs"
date: 2007-11-19 11:18
---

*"Pourquoi les ordinateurs ne sont-ils pas plus rapides, depuis le temps
?"* C'est une des questions que les utilisateurs lambda peuvent finir
par se poser, après avoir vu quelques années d'évolution de la
technique. Des processeurs toujours plus rapides, toujours plus de
mémoire et d'espace disque, mais globalement, on a l'impression que le
Logiciel reste lent. Qu'en penser ? Deux articles permettent de répondre
en partie à cette question.

Deux articles pointant du doigt les performances des PC sous Windows
sont récemment parus.

![Mac Vs Amd logo](/images/logos/MacVsAmd.png)Premièrement, un
comparatif d'un [Mac Plus](http://en.wikipedia.org/wiki/Mac_Plus) datant
de 1986 avec une configuration AMD 64 Dual Core. Il s'agit de tester les
mêmes fonctionnalités, voire les mêmes logiciels, et d'effectuer des
tests de vitesse. Word, Excel, temps de démarrage et répondant de
l'interface, tout y passe — en essayant de conserver une vraie
équivalence entre les tests effectués sur chaque machine. Bilan : d'une
courte marge, un Mac Plus est plus rapide qu'un PC moderne.  
**[86 Mac Plus Vs. 07 AMD
DualCore](http://hubpages.com/hub/_86_Mac_Plus_Vs_07_AMD_DualCore_You_Wont_Believe_Who_Wins)**

![Wintel logo](/images/logos/Wintel%20logo.png)Un deuxième article
accuse plus spécifiquement Microsoft d'être à l'origine de ces
ralentissements. L'équipe de recherche de
[xpnet.com](http://www.xpnet.com/), un site spécialisé dans la mesure de
performance informatique, a comparé la rapidité de différentes versions
d'Office sous Windows 2000, XP et Vista. Ils essaient principalement
d'établir le facteur de ralentissement logiciel en fonction du gain de
performance du matériel. Là encore, conclusion prévisible mais un peu
effarante : en utilisant la configuration matérielle-type de l'époque
dans chaque cas, Office est plus lent sous Vista que sous Windows 2000,
et utilise 12 fois plus de mémoire.  
**[What Intel Giveth, Microsoft Taketh
Away](http://exo-blog.blogspot.com/2007/09/what-intel-giveth-microsoft-taketh-away.html)**

Bien sûr, on ne saurait en conclure que les logiciels sont de plus en
plus lent juste pour le plaisir : on a tout de même gagné en
fonctionnalité et en stabilité, depuis le temps. Et je suis le premier à
plaider pour les outils simplifiant la tâche des développeurs, même
s'ils induisent une certaine perte de répondant. Il n'empêche que
l'industrie du logiciel semble avoir de vrais soucis de performance — il
serait bon que nous puissions un jour voir les résultats concrets de
l'augmentation des performances matérielles de nos ordinateurs.
