---
layout: post
title: "Windows Vista: The &quot;choose your own adventure&quot; UI"
date: 2006-09-27 20:43
---

L'interface de Windows Vista, Aero Glass, demandera des ressources
machine importantes. Elle est donc déclinée en plusieurs versions, qui
vous permettent d'adapter l'interface à la puissance de votre
ordinateur.

Voici un aperçu des 4 différents styles d'interface de Vista : Aero
Glass, Vista Standard, Vista Basic, et Windows Classic. A l'aide d'une
série de captures d'écran, vous pourrez comparer les versions, et
observer l'apparence des nouvelles applications Windows (Internet
Explorer 7, Windows Media Player 11, etc) sous chacun des différents
thèmes.

**[Windows Vista: The "choose your own adventure"
UI](http://www.istartedsomething.com/20060919/vista-choose-own-adventure-ui)**
