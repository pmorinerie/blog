---
layout: post
title: "Nouveaux thèmes"
date: 2006-06-27 17:11
---

J'ai mis à jour les thèmes de ce blog, et rajouté un [sélecteur de
styles](#themes). J'aime particulièrement le thème
[NeSpring](http://kemenaran.winosx.com/?type_url=query_string&theme=NeSpring) ;
ça change du bleu cobalt.
