---
layout: post
title: "Présentation de ma pomme"
date: 2009-06-11 16:16
---

**Qui suis-je ?**

Je suis **Pierre de La Morinerie**, un passionné d'informatique et de
tout un tas d'autres choses. Je travaille en ce moment chez miLibris, où
je m'occupe de développement de liseuses numériques sur les appareils
iOS. Je suis un peu touche-à-tout, et adore expérimenter de nouvelles
technologies.

**Que sais-je ?**

Du côté logiciel, après avoir fait mes premières armes avec RapidQ
(souvenirs souvenirs), j'ai expérimenté le C++ et le Java, avant de
découvrir C\#, avec lequel j'ai pas mal travaillé, et qui reste mon
langage de prédilection. Après mon retour sur Mac, tout en continuant à
développer en .NET (merci les machines virtuelles), j'ai commencé à
faire quelques bricoles en Python et en LISP ; j'ai également passé un
certain temps à tester les fonctionnalités de Cocoa (et à apprendre
l'Objective-C, qui est vraiment un chouette langage). En ce moment, je
m'intéresse à Flex, et développe un logiciel d'apprentissage des langues
anciennes avec une combinaison Flex/AIR.

Côté Web, j'ai fourbi mes armes avec les technologies standard : HTML,
CSS, PHP, MySQL. J'ai passé beaucoup de temps à explorer les Frameworks
PHP, puis à développer le mien — mais c'était avant de découvrir
[CakePHP](http://cakephp.org/), qui est une vraie merveille. J'ai aussi
développé quelques bricoles en Javascript, en utilisant Prototype et
script.aculo.us pour faire de jolies choses en AJAX. Enfin j'ai eu
l'occasion de développer une grosse application en ASP.NET — mais c'est
une technologie qui ne m'a décidément pas convaincu. Alors que
[Cappuccino](http://cappuccino.org/), ça c'est quelque chose…

**Qu'ai-je réalisé ?**

Dans les grandes lignes : des formations, quelques gros projets, et
quelques petits utilitaires.

En ce qui concerne les **formations**, j'ai donné pendant deux ans des
cours internes pour les étudiants de la Junior Entreprise de l'ISEP.
J'ai formé une centaine d'étudiants à
[PHP](http://www.slideshare.net/kemenaran/formation-php) et à
[C\#](http://www.slideshare.net/kemenaran/formation-c-cours-1-introduction-premiers-pas-concepts-186982),
en leur apprenant les bases et les éléments nécessaires pour qu'ils
puissent aller plus loin seuls. J'ai également réalisé une [introduction
à la technologie
Silverlight](http://juniorisep-silverlight.blogspot.com/) (qui, pour
archive, a été
[filmée](http://juniorisep-silverlight.blogspot.com/2008/02/participation-de-microsoft-lvnement.html)).

Dans les gros projets, il y a eu :

-   [WinOSX](http://www.winosx.com/) : un pack permettant de customiser
    Windows XP en Mac OS X, programmé avec NSIS
-   [PictbaseWeb](http://www.daxium.com/solutions/index.html), pour
    Daxium : Un logiciel de Data Asset Managment en ligne, développé en
    ASP.NET
-   [ArghosReader](http://www.arghos-diffusion.com), pour Arghos
    Diffusion : Une visionneuse complète de documents PDF, en C\# et C++
    managé
-   [Antigone](http://antigone.googlecode.com/) : Un logiciel-support à
    l'apprentissage du grec ancien, développé en Flex/AIR

Et du côté des **petits utilitaires**, dont je ne suis pas moins fier :

-   [iColorFolder](http://icolorfolder.sourceforge.net/?page_id=2) :
    permet d'organiser les dossiers de Windows par couleur, et de
    changer les icônes des dossiers (en C\#, puis C++/MFC)
-   [SearchBar](http://kemenaran.winosx.com/?2007/04/17/66-searchbar-extension-for-thunderbird-20) :
    une extension pour Thunderbird réorganisant les barres d'outils
-   [WallpaperSeasonsSwitcher](http://kemenaran.winosx.com/?2007/04/09/65-wallpaperseasonsswitcher) :
    un utilitaire en ligne de commande pour changer une série de fonds
    d'écrans en fonction de la saison (en C)
-   [NewDocumentPlugIn](http://kemenaran.winosx.com/?2009/07/03/146-newdocumentplugin-for-mac) :
    un plugin pour Mac OS X qui rajoute un menu "Nouveau document" au
    Finder
-   [CastingStage](http://castingstage.googlecode.com/) : une
    application de raytracing réalisée avec Cocoa

J'ai aussi contribué sporadiquement à quelques **projets Open-Source**,
en envoyant des patchs pour [CakePHP](http://cakephp.org/),
[Adium](http://adium.im/),
[Wp-Sentry](http://wordpress.org/extend/plugins/wp-sentry/)…

**Et aussi…**

Comme il n'y a pas que l'informatique dans la vie, voici ce à quoi j'ai
pu également participer :

-   [La valise en carton](http://www.lavaliseencarton.fr/) : le blog de
    mon volontariat au Togo
-   [Association LIRE](http://www.asso-lire.org/) : une association
    togolaise promouvant la lecture dans les collèges, à laquelle je
    participe

**That's all folks !**

Enfin, c'est tout pour ce que je mets ici — il y a tout le reste,
évidemment… Mais c'est déjà pas mal.
