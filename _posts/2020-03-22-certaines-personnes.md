---
layout: post
title: "Certaines personnes"
description: "Certaines personnes se sentent angoissées et impuissantes. Certaines personnes s’ennuient. Certaines personnes…"
date: 2020-03-22 19:00
---

_Ce texte est une traduction de « [Some people](https://kottke.org/20/03/some-people) », de Jason Kottke._

Certaines personnes se sentent angoissées et impuissantes.

Certaines personnes s’ennuient.

Certaines personnes sont isolées en confinement et se sentent seules.

Certaines personnes se rendent comptent que l’Après sera très différent de l’Avant.

Certaines personnes profitent du temps supplémentaire avec leurs enfants et quand ça sera fini, ce temps leur manquera.

Certaines personnes viennent de sortir de leur douzième créneau d’affilé à l’hôpital et ne peuvent pas embrasser leur famille.

Certaines personnes ont mangé à leur restaurant préféré pour la dernière fois et ne le savent pas encore.

Certaines personnes sont mortes du coronavirus.

Certaines personnes ne peuvent pas s’empêcher de lire les nouvelles.

Certaines personnes n’ont pas les moyens d’acheter du savon.

Certaines personnes apprennent à faire du pain.

Certaines personnes travaillent de chez elles tout en essayant de faire l’école à la maison.

Certaines personnes sont des parents seuls qui essayent de travailler de chez eux tout en essayant de faire l’école à la maison.

Certaines personnes ont du mal à boucler les fins de mois et la prochaine paye ne viendra pas.

Certaines personnes sont inaptes à la fonction de président.

Certaines personnes ont quitté la ville pour leurs résidences secondaires.

Certaines personnes ne peuvent pas faire les courses parce qu’elles sont des personnes à risque.

Certaines personnes ont perdu leur travail.

Certaines personnes n’arrivent pas à dormir.

Certaines personnes regardent gratuitement des opéras en ligne.

Certaines personnes sont en quarantaine depuis plusieurs semaines.

Certaines personnes ne peuvent pas télé-travailler.

Certaines personnes ont attrapé le coronavirus et ne le savent pas encore.

Certaines personnes sont trop angoissées pour se concentrer sur leur travail.

Certaines personnes n’ont pas les moyens de payer leur loyer du mois prochain.

Certaines personnes continuent à se réunir en grands groupes.

Certaines personnes prennent de vrais risques pour sauver nos vies.

Certaines personnes n’ont pas acheté assez de solution hydro-alcoolique.

Certaines personnes ont acheté trop de solution hydro-alcoolique.

Certaines personnes n’ont plus accès à leur thérapeute.

Certaines personnes ne peuvent pas aller travailler mais sont toujours payées par leur employeur. Pour l’instant.

Pour certaines personnes, le principal souci est de décider ce qu’elles vont regarder ensuite sur Netflix.

Certaines personnes se portent volontaires.

Certaines personnes ont une entreprise qui va faire faillite.

Certaines personnes se rendent comptent que les enseignants sont formidables.

Certaines personnes commandent à emporter aux restaurants du coin.

Certaines personnes voudraient vraiment juste un câlin.

Certaines personnes n’arrivent pas à convaincre leurs parents âgés de prendre tout ça au sérieux.

Certaines personnes s’inquiètent de leurs investissements en bourse pour leurs vieux jours.

Et certaines personnes n’ont jamais eu d'investissements.

Certaines personnes vont être confrontées à plus de violences domestiques.

Certaines personnes vont tomber malade ou se blesser et auront plus de mal à accéder à des soins médicaux.

Certaines personnes ne peuvent pas acheter la nourriture dont elles ont besoin parce que les produits acceptés par les aides sociales alimentaires ne sont plus en rayon.

Certaines personnes ne veulent pas arrêter de faire la fête.

Certaines personnes ont perdu leur solution de garde d’enfant.

Certaines personnes font tout ce qu’elles peuvent pour rester calmes et garder l’espoir et ça ne marche pas.

Certaines personnes regardent Contagion et jouent à Pandémie.

Certaines personnes ne savent pas ce qu’elles vont faire.

Certaines personnes sont surchargées de conseils sur comment travailler depuis chez soi.

Certaines personnes mangent ou boivent trop.

Certaines personnes pensent à l’après.

Certaines personnes sont contrariées de ne pas pouvoir voyager.

Certaines personnes sont  en manque de sexe.

Certaines personnes prévoient de jardiner plus cette année.

Certaines personnes ne vont pas voir leur famille pendant des mois.

Certaines personnes se déconnectent pour garder la tête froide.

Certaines personnes n’arrivent pas à voir le bout du tunnel.

Certaines personnes vont se rendre compte qu’il faut qu’elles rompent avec leur conjoint.

Certaines personnes chantent _Imagine all the people_.

Certaines personnes ne sont pas dans cette liste.

Ces expériences sont toutes celles de vraies personnes, tirées de journaux, des réseaux sociaux, et d’amis. Courage : vous n’êtes pas la seule personne à passer par ce que vous vivez en ce moment. Mais soyez attentionné : tout le monde ne passe pas par la même chose que vous. Même si en dernier lieu, tous nous sommes affectés par ces mêmes évènements.

_Jason Kottke_
