---
layout: post
title: "Quelles grèves pour l’informatique ?"
date: 2019-12-12 09:39
---

Depuis le 5 décembre, je suis en grève contre la retraite individualiste dite « à points », et pour la mise en place d'un système de retraite solidaire.

Faire grève, dans l'informatique, c’est pas courant. Les informaticien·nes sont peu syndiqué·es, et ne participent que rarement à des mouvements sociaux. Moi-même, ce n’est que la troisième fois que je fais grève de toute ma carrière – et la première grève reconductible.

J’imagine qu’il y a des spécificités liées à l'informatique, qui font qu’on envisage moins la grève comme moyen d’action :

- **L’habitude** : culturellement on s’est rarement posé la question de la grève, on se dit que ça ne nous concerne pas.
- **Beaucoup d'indépendant·es** dans le métier : des indépendant·es qui font grève, ça ne bloque pas la production d’un patron.
- **L’automatisation** : l’objectif de notre travail est que les systèmes tournent avec le minimum d’intervention humaine. Donc si on fait grève, ça se verra à peine.

Dans ces conditions, quel sens peut avoir une grève dans l'informatique ?

## La grève pour discuter

Depuis le début de la grève, et même un peu avant, on a beaucoup discuté avec des collègues. De la réforme à venir, de la grève, des moyens d'actions. Qui fera grève, quand, comment ? On partage des liens, on s'informe. On se retrouve en manifestation. Beaucoup de collègues ont écrit des articles pour parler de la réforme, et de la grève dans l'informatique.

De ce temps disponible sont nés par exemple :

- 😎 [greve.cool](https://greve.cool), le site qui informe sur le droit de grève ;
- 📝 [l’appel des travailleur·ses du numérique](https://onestla.tech) pour une autre réforme des retraites, qui a déjà réuni plus de 1 000 signatures.

Bref, la grève a libéré du temps pour s’informer, discuter, réfléchir. C'est déjà une belle réussite.

## Visibiliser la grève

Comment rendre visible l’arrêt de travail alors que les systèmes que nous gérons sont largement automatisés ? Même si on est nombreux·ses à cesser le travail, les systèmes continueront à tourner pendant un moment : ça se remarquera à peine. Et l'enjeu d'une grève, c'est quand même que ça soit visible.

Avant la grève, on a joué avec quelques idées pour rendre notre arrêt de travail plus visible. Coder un fonctionnement où les systèmes se désactivent si on ne pointe pas le matin ? Faire la grève de résolution des soucis en production ?

Finalement on est un certain nombre à être partis sur une idée pas nouvelle, mais simple : **mettre un bandeau en haut des sites web qu'on gère**, qui explique qu'une partie de l'équipe est en grève. Le bandeau ne bloque pas l'accès au site, ni son fonctionnement.

Selon les sites et les contextes, la formulation peut être plus ou moins forte. Il y a par exemple le bandeau de grève de mon blog personnel, qui mentionne explicitement les revendications :

![Bannière de grève sur mon blog personnel](/images/quelles-greves-pour-l-informatique/banniere-greve-perso.png)

En revanche, le site sur lequel je travaille comme indépendant fait partie du service public, et demande donc une formulation plus neutre :

![Bannière de grève sur un site du service public](/images/quelles-greves-pour-l-informatique/banniere-greve-ds.png)

**Si l'idée d'un bandeau n'est pas bien radicale, elle a tout de même fait débat**. Certaines personnes s'interrogent sur la légitimité de ces bannières : n'est-ce pas utiliser une resource professionnelle pour faire passer un message personnel ?

Alors on en discute, on essaie de trouver des arguments. Une bannière sur un site, ce n'est sans doute pas différent d'une banderole devant une usine, ou une école. Et d'ailleurs, une banderole, ça ne signifie pas que toute l'équipe ou l'établissement derrière est d'accord avec le message – mais que personne ne va y opposer un veto fort, ou aller la décrocher.

Par ailleurs, ce genre de bannière existe sur de nombreux sites, y compris du service public : Radio-France, la RATP, la SNCF… L’enjeu, c'est sans doute plus la formulation du message, plus ou moins militante. Mais ce genre de question montre que la culture du mouvement social en informatique part de loin, et ne commence que tout doucement à se construire.

## Et le blocage ?

Au bout de quelques jours de grève, on se rend compte que malgré la visibilité apportée, **une bannière, ça ne dérange pas grand monde**. Que le mouvement dure, et que la grève se fait longue.

Alors les discussions continuent. On se dit que probablement, la grève n'est efficace que lorsqu'elle dérange, qu'elle perturbe. Si la vie continue, si l'usine tourne malgré tout, si les systèmes ronronnent, rien ne se passe. 

Et de fait, dans les mouvements sociaux, on a l'impression que des concessions sont obtenues **par les actions qui ont un impact sur la vie quotidienne**, moins que par le nombre de manifestants dans la rue. Blocages de la production (grève massive dans l'usine, piquets de grève, etc.) ou de la circulation (arrêt des transports, blocage des dépôts de bus ou de raffineries, opérations escargot sur les routes), tracteurs d'agriculteur·trices devant la préfecture, ordures qui s'accumulent.

Pour certaines professions, l'arrêt de travail est rapidement visible (chauffeurs, éboueurs, enseignants) ; d'autres où c'est moins le cas (argiculteurs, avocats, étudiants). Dans ce cas, les piquets de grève et les blocages sont régulièrement utilisés pour renforcer l'impact de la grève :

- Des étudiant·es bloquent leur université ;
- Des routier·ères bloquent les raffineries ;
- Des ouvrier·ères organisent un piquet de grève pour bloquer la production de toute l'usine ;
- Des agriculteur·rices ralentissent la circulation avec des tracteurs ;
- Des gilets jaunes bloquent les entrées des autoroutes et des supermarchés ;
- Des agent·es de la RATP, rejoints par des étudiant·es, bloquent des dépôts de bus ;
- Des agent·es d'EDF coupent le courant de la préfecture, ou passent tout le monde en heures creuses.

Dans tous ces cas, les blocages et les piquets de grève, s'ils sont techniquement illégaux, sont des outils très efficaces utilisés régulièrement dans le rapport de force.

Et justement, ces professions ont lancé il y a quelques jours un [appel à leurs collègues informaticien·nes](https://lundi.am/Appel-a-nos-collegues-informaticiens-et-informaticiennes), les enjoignant à rejoindre la lutte avec leurs moyens.

## Bloquer légitimement en informatique

Alors, comment transposer ces moyens d'action à l'informatique ?

Il faudrait sans doute déjà **ne pas être seul·e**. Bloquer une ressource sans subir de répression, ça fonctionne si on est nombreux·ses à le faire – que le cadre soit une équipe, un collectif informel ou un syndicat.

La **légitimité du blocage** se pose aussi. Idéalement, des blocages ciblés concerneraient au maximum les entreprises et le gouvernement – et au minimum les usagers ordinaires. Ou en tout cas aurait un effet insupportable mais ciblé, qui pousse au maximum à ce que les revendications de la grève soient satisfaite.

Par exemple, si des informaticien·nes décident de bloquer un service, il peut être intéressant de ne le rendre indisponible que la section utilisée par les administrateurs du site (le _backoffice_), et de laisser la partie utilisée par des utilisateurs externes disponible.

Avec tout ça, des idées émergent :

- Et si la partie "Administration" de service-public.fr, celle qui traite les formulaires, était en grève ?
- Et si des informaticien·nes d'OVH bloquaient les nouvelles commandes, ou l'accès au Manager ?
- Et si des prestataires de paiement en ligne bloquaient les paiements, ce qui affecterait tous les sites de commerce en ligne ?

J'ai l'impression que ces réflexions n'en sont qu'au tout début. Et vous, qu'en pensez vous ?<br>Discutons-en sur [Mastodon](https://mastodon.xyz/@pmorinerie/103300229767593855), ou sur le [chat de onestla.tech](https://discord.gg/se3PnEr).
