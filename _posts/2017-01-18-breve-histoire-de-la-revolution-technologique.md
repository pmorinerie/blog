---
layout: post
title: "Brève histoire de la révolution technologique"
date: 2017-01-18 20:59
---

Lu dans [Espejos](http://resistir.info/livros/galeano_espejos.pdf), d’Eduardo Galeano (2008) :

> Croissez et multipliez-vous, avons nous dit, et les machines crûrent et se multiplièrent.<br>
> Elles nous avaient promis qu’elles travailleraient pour nous.<br>
> Aujourd’hui nous travaillons pour elles.<br>
> Les machines que nous avons inventées pour multiplier la nourriture multiplient la faim.<br>
> Les armes que nous avons inventées pour nous défendre nous tuent.<br>
> Les autos que nous avons inventées pour nous déplacer nous paralysent.<br>
> Les villes que nous avons inventées pour nous rencontrer nous isolent.<br>
> Les grands médias, que nous avons inventés pour communiquer entre nous, ne nous écoutent ni ne nous voient.<br>
> Nous sommes machines de nos machines.<br>
> Elles plaident leur innocence.<br>
> Elles ont raison.

L’ajout d’une phrase sur Internet et la surveillance est laissé à l’exercice du lecteur.
