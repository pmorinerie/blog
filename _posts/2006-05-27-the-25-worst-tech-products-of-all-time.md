---
layout: post
title: "The 25 Worst Tech Products of All Time"
date: 2006-05-27 14:49
---

PCWorld nous présente sa liste des pires logiciels/matériels de tous les
temps, c'est à dire depuis le début de sa parution, il y a près de 25
ans. Et devinez qui est [en tête](http://www.aol.fr/) ;) Beaucoup de
classiques, mais aussi quelques produits oubliés ; amusant.

[PCWorld.com - The 25 Worst Tech Products of All
Time](http://www.pcworld.com/news/article/0,aid,125772,00.asp)
