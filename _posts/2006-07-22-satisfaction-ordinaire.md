---
layout: post
title: "Satisfaction ordinaire"
date: 2006-07-22 10:12
---

1.  Télécharger distraitement le dernier
    [OCRemix](http://www.ocremix.org/) en date, sans même regarder
    l'auteur.
2.  Le mettre dans sa playlist tout aussi distraitement.
3.  10 minutes plus tard, se dire "Je ne connais pas cette piste, mais
    elle ressemble fichtrement à du
    [zircon](http://www.ocremix.org/remixer/zircon/)."
4.  [Confirmer](http://www.ocremix.org/remix/OCR01497/).

C''est idiot, mais ça fait plaisir de commencer à connaître un peu
quelques artistes, au point de reconnaître leur style musical dès les
premières notes. Sans compter que retrouver un style particulier remet
en mémoire toutes les autres pistes du même auteur, et Dieu sait si
elles poutrent.

Je regrette une fois de plus de ne pas avoir appris la musique... Penser
à rajouter en bas de ma Todo-list : “Se mettre au piano”.
