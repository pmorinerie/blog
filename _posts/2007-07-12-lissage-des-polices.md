---
layout: post
title: "Lissage des polices"
date: 2007-07-12 11:11
---

La récente sortie de [Safari pour
Windows](http://kemenaran.winosx.com/?2007/06/11/73-safari-pour-windows)
a remis sur le devant de la scene un débat qui dure depuis quelques
années : les différentes manières de lisser les polices de caractère sur
un écran.

Tout part d'une impression générale ressentie par certains testeurs de
Safari pour Windows : "le texte est flou". En effet, Safari pour Windows
utilise le moteur de lissage des polices de Mac OS X, et non pas le
système ClearType de Windows : le texte lissé apparait donc légèrement
différent dans Safari que dans les autres applications Windows.

![Lissage des polices Apple
Windows](/images/divers/apple_windows_fonts.png)L'explication donnée
par les typographes est venue rapidement : Windows lisse le texte en
essayant de faire coincider la forme des lettres et des pixels, pour
obtenir plus de netteté quite a déformer les caractères ; en revanche,
Mac OS X tente de respecter au mieux l'apparence de la police et la
forme des caractères, au prix d'un piqué un peu moindre dans les petites
tailles.

Il s'agit donc de deux philosophies différentes, et de savoir si l'on
privilégie la netteté au détriment de la forme ou l'inverse. Les
testeurs de Safari sous Windows étaient en réalité plus désorientés par
l'apparence *inhabituelle* du texte que par un problème fondamental de
flou. [Joel
Spolsky](http://www.joelonsoftware.com/items/2007/06/12.html) explique
mieux que moi ces differences (c'est de lui que provient l'image
d'exemple de cet article).

Un autre article va plus en profondeur, et se demande ce qui arrivera
lorsque la résolution de nos écrans commencera (enfin !) a augmenter.
Selon lui, les lissages de police qui "collent aux pixels" hypothèquent
l'avenir, car ils seront illisibles sur les écrans de haute résolution.
L'article est en tout cas détaillé et intéressant, et mérite le coup
d'oeil.

**[Anti-Grain Geometry - Texts Rasterization
Exposures](http://antigrain.com/research/font_rasterization/index.html)**
