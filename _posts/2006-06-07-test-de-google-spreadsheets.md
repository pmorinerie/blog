---
layout: post
title: "Test de Google SpreadSheets"
date: 2006-06-07 09:37
---

![](/images/google-spreadsheets/google-spreadsheets-logo.png)

Google lance un nouveau service, Google SpreadSheets, un tableur en
ligne. Il vise le grand public et les personnes ayant besoin des
fonctionnalités basiques, mais se focalise particulièrement sur l'aspect
"échange". Les accès à la bêta sont limités, pour ne pas saturer les
serveurs ; voici donc un **test complet** de cette nouvelle application
Google.

#### Accès à l'application

Pour ne pas saturer ses serveurs, Google a mis en place une liste
d'attente : on inscrit son adresse courriel, et une invitation est
envoyée quelques temps après. Lors de ce test le processus a été
relativement rapide, il m'a fallu environ 3 heures pour recevoir mon
invitation.Une fois l'accès activé, on peut se rendre sur
<http://spreadsheets.google.com>, et accéder au tableur.

#### Premières impressions

Comme dans beaucoup de produits Google, la première impression produite
est « *propre et net* ». L'interface est sobre, avec les tons bleu
pastel caractéristiques de Google ; on sent un effort de simplicité.

![](/images/google-spreadsheets/google-spreadsheets.png)

Le comportement n'est pas dépaysant : ce tableur fonctionne
intuitivement, comme on s'y attend. Les cellules s'éditent en cliquant
dessus ; on peut redimensionner les lignes et les colonnes, ou en
ajouter, ainsi que modifier la mise en forme du texte de manière assez
complète. La fonction "Surligner", qui rappelle celle de la barre
d'outils Google, se révèle assez pratique pour mettre en valeur
certaines données. Les fonctions couper/copier/coller fonctionnent elles
aussi remarquablement bien à l'aide des raccourcis claviers. On regrette
en revanche de ne pas pouvoir déplacer des cellules par glisser/déposer.

#### Fonctionnalités

Un autre onglet permet d'accéder aux fonctions de tri. Elles sont
efficaces mais succintes : on peut trier le tableau par ordre
alphabétique ou inverse, en gelant certaines lignes pour qu'elles ne
soient pas prises en compte. Le tri ne fonctionne pour l'instant pas
avec des chiffres, ce qui est assez dommage.

![](/images/google-spreadsheets/google-spreadsheets-3.png)

Le dernier onglet est celui des formules, indispensables dans tout bon
tableur qui se respecte. Ici, pas d'assistant de créations de formules
comme dans Office, il faut déjà connaître la syntaxe à employer. Les
formules les plus courantes sont accessibles depuis la barre d'outils,
et un pop-up permet de choisir une formule dans la liste complète, dont
la taille est assez impressionante. Une fois la formule entrée, elle
fonctionne comme on s'y attend, et se copie/colle dans d'autres cellules
sans problèmes.

![](/images/google-spreadsheets/google-spreadsheets-2.png)

#### Echange

Un des aspects importants de Google SpreadSheets est la notion
d'échange, qui mêle le travail en commun et l'échange à proprement
parler. Chaque feuille peut être éditée par 10 personnes simultanément,
en conservant une cohérence dans les modifications. Un bouton permet de
faire apparaître une fenêtre de chat sur la droite de la page, on peut
donc communiquer facilement avec les personnes en train d'éditer le même
document que nous.

On imagine assez bien l'utilité de ce système lors de travail
collaboratif à distance ; Office tente déjà de s'imposer sur ce créneau,
mais le système proposé par Google semble plus simple et plus intuitif
que les outils proposés par Office - tout en étant sans doute plus
limité.

#### Sauvegarde et téléchargement

Les feuilles sont automatiquement sauvegardées sur les serveurs de
Google ; on peut également choisir d'exporter son travail au format
Exel, CSV ou encore Html. Les feuilles téléchargées s'ouvrent
impeccablement dans Exel ou OpenOffice, sans problème de compatibilité.

On peut également importer des feuilles Excel dans Google SpreadSheets,
et cela fonctionne plutôt bien. Cela permet donc de commencer un travail
collaboratif sur un document déjà existant.

![](/images/google-spreadsheets/google-spreadsheets-4.png)

#### Conclusion

Google SpreadSheets est très récent, et ne dispose pour l'instant que
des fonctionnalités de base d'un tableur. Elles sont en revanche
disposées dans une interface claire et intuitive, qui les rendent
agréables à utiliser. L'aspect collaboratif est innovant et efficace ;
et la compatibilité avec Excel, à l'importation comme à la sauvegarde,
est très appréciée.

Bref, un bon produit de Google, qui va sans doute s'enrichir au cours
des semaines à venir. Google SpreadSheets vise clairement à concurencer
Office sur le terrain du grand public, en proposant des outils
innovants... La bataille Google/Microsoft est loin d'être terminée.
