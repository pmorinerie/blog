---
layout: post
title: "Searchbar extension for Thunderbird 2.0"
date: 2007-04-17 12:27
---

<div class="langs_tabbox">

<div class="langs_tabs">

[English](#dummy) [Français](#dummy)

</div>

<div class="langs_tabpanel selected" lang="en">

![Thunderbird logo](/images/divers/thunderbird_logo.png)[Thunderbird
2.0 RC1](http://www.mozilla.org/projects/thunderbird/roadmap.html) is
available since a few days. The early testers surely noticed the new
features, but maybe also the removal of the Searchbar, the one above the
mails list. Search tools are now integrated in the main toolbar.

For those who regret the Searchbar, here is my first Thunderbird
extension, simply named **SearchBar**. It just show the Searchbar of the
1.\* versions, and it does it well. The Searchbar is displayed by
default, but can be hidden as any other toolbar. The extension is for
now localized in English and French.

Any comment about the features or the code of the extension is welcome.
Have fun !

**[SearchBar 0.1 for Thunderbird
2.0](http://www.winosx.com/zoo/SearchBar/searchbar.xpi)**

**Edit :** [Thunderbird 2.0](http://www.mozilla.com/thunderbird/) final
is out.

**Edit 2:** I just discovered a similar extension,
[xSearchBar](http://xsidebar.mozdev.org/tsearchbar/index.html), written
by Philip Chee two months after mine - and maybe a bit more polished.

</div>

<div class="langs_tabpanel" lang="fr">

![Thunderbird logo](/images/divers/thunderbird_logo.png)[Thunderbird
2.0](http://www.mozilla.org/projects/thunderbird/roadmap.html) est
disponible en Release Candidate 1. Ceux d'entre vous qui l'ont testé ont
certainement remarqué ses nombreuses nouvelles fonctionnalités ; mais il
faut également noter la disparition de la barre de recherche, celle
situé juste au dessus de la liste des courriels. Les outils de recherche
sont maintenant intégrés à la barre d'outils principale.

Pour ceux qui regretteraient la barre de recherche, je leur propose ma
première extension pour Thunderbird, **SearchBar**. Elle ne fait rien
d'autre que rétablir la barre de recherche des versions 1.5 et
antérieures, mais elle le fait bien. La barre de recherche est alors
affichée par défaut, mais on peut bien sûr la masquer, comme n'importe
quelle autre barre d'outils. L'extension est pour l'instant localisée en
français et en anglais.

Tout commentaire sur le fonctionnement ou le code de l'extension est le
bienvenu. Have fun !

**[SearchBar 0.1 pour Thunderbird
2.0](http://www.winosx.com/zoo/SearchBar/searchbar.xpi)**

**Edit :** [Thunderbird 2.0](http://www.mozilla.com/thunderbird/) est
sorti en version finale.

**Edit 2:** Je viens de decouvrir une extension comparable,
[xSearchBar](http://xsidebar.mozdev.org/tsearchbar/index.html), realisee
par Philip Chee un mois et demi apres la mienne - et peut-etre un peu
plus peaufinee.

</div>

</div>
