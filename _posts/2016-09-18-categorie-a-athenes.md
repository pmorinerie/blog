---
layout: post
title: "Catégorie : À Athènes"
date: 2016-09-22 16:21
---

En septembre 2016 j’ai passé deux semaines autour de différentes structures d’accueil pour les exilés, à Athènes.

Voici les articles écrits pour l’occasion.

1. [Athènes, premiers jours](/posts/athenes-premiers-jours)
2. [Athènes : à Ellinikó](/posts/a-elliniko)
3. [Lieux d’Athènes](/posts/lieux-d-athenes)
4. [Passer des films à Athènes](/posts/passer-des-films-a-athenes)
