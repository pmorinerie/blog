---
layout: post
title: "Final Fantasy - Voices of the Lifestream"
date: 2007-09-16 16:45
---

![Final Fantasy Voices of the Lifestream - CD 1 :
Crisis](/images/divers/disc-1-crisis.png)Hier est sorti un des plus
grands projets collaboratifs qu'[OCRemix](http://www.ocremix.org/) ait
jamais mené à bien : un album de remix de Final Fantasy VII, intitulé
[Final Fantasy - Voices of the Lifestream](http://ff7.ocremix.org/). Les
plus grands remixeurs de jeux vidéos, 4 CD, 45 pistes, de somptueux
artworks, le tout téléchargeable gratuitement et sans limitations. On y
trouve d'excellents morceaux de djpretzel, zircon, Dshu — une vraie
réussite. Y'a bon.

*:: retourne siffler le thème d'Aeris ::*
