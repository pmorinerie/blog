---
layout: post
title: Préparer du Yogi Tea maison
date: 2021-02-19T22:11:02.049Z
lang: fr
---
Le Yogi Tea à la cannelle et aux épices, c’est bien bon. Pour en faire une version maison, je mélange (pour 4 tasses)  :

- Quelques copeaux ou bâtons de cannelle,
- Un cm3 de gingembre frais coupé en petits morceaux,
- Trois gousses cardamone fendues,
- Deux clous de girofle,
- Un peu de poivre.

Faire bouillir le tout pendant 20mn - 1/2h fera un excellent thé de cannelle.

## Variante rapide et économique

Si on manque de temps, ou qu’on préfère utiliser des ingrédients plus simples, on peut utiliser des ingrédients en poudre. Pour cela, je mélange (pour une tasse) :

- Deux cuillères à café de cannelle en poudre,
- Une cuillère à café de gingembre en poudre,
- Une pincée de cardamome en poudre,
- Un clou de girofle,
- Un peu de poivre.

