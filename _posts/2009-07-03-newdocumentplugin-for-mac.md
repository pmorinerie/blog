---
layout: post
title: "NewDocumentPlugIn for Mac"
date: 2009-07-03 16:18
---

![NewDocumentPlugIn Icon](/images/logos/pluginIcon.png)Some times ago,
using Leopard, I got tired of having to open an application, create a
new file, select where I want it to go, and close the application — just
to create a damn new file. I mean, can't we just have a "New document"
contextual menu, just as in Windows, Gnome, KDE, or any decent
environment ? Mac users say that eventually, the need for it disappears
— just as the need to have "folders first" in the Finder goes off with
time and practice. Well, I must agree I can live without "folder first".
But a "New document" menu is a requirement for me.

I didn't find anything on the Web : forum members are quicker to answer
"that's the way OS X is made, get used to it" than suggesting a
solution. I wasn't aware of
[NuFile](http://www.growlichat.com/NuFile.php) at this time, and I
wanted to learn how to create a plugin with CoreFoundation. So I made it
myself.

**[NewDocumentPlugIn](http://newdocumentplugin.googlecode.com/ "Project page on GoogleCode")**
adds a "New document" contextual menu to the Finder. You can thus create
easily new documents without opening the associated application. It is
meant to be simple yet customizable : you can easily **add your own
templates**, just drop an empty file of the wanted type in the
"Templates" folder of the plugin. And it is **localized in English and
French**: you can add localizations for the contextual menu labels and
for the document names if wanted.

![Filename highlighting
example](/images/screenshots/newdocumentplugin-example.png)The neat
feature that I didn't see anywhere else is **filename highlighting** :
when you create a new document, the document name is highlighted, in
order to customize the name without superfluous keystroke (ensure that
AppleScript GUI Scripting is enabled for this).

On the technical side, the plugin is written in C, using the
CoreFoundation API — and some bits of Carbon for file manipulation. I
could have integrated Cocoa code in there (easier to write and to
maintain), but keeping it C-only reduce overheight. Guaranteed without
memory leaks. Oh, and it uses AppleScript and GUI Scripting for the
"filename highlighting" feature : there is some nice code to execute an
AppleScript file from a Carbon program.

The plugin is hosted on Google Code and is Open-Source (MIT Licence).
Please read it, recompile it, fork it, whatever you want.

**[Download NewDocumentPlugin (Mac OS 10.4 to 10.5, 700
Ko)](http://newdocumentplugin.googlecode.com/files/NewDocumentPlugIn.dmg)**  
[See the project page on
GoogleCode](http://newdocumentplugin.googlecode.com/)
