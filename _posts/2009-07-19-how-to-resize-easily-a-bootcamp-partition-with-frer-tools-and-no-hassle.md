---
layout: post
title: "How to resize easily a BootCamp partition, with free tools and no hassle"
date: 2009-07-19 21:59
---

This is a common scenario for Mac users having a BootCamp setup : the
Windows partition runs out of space, and you would like to extend it. A
lot of tutorials explains that you need to clone the partition, format
it and restore the data, or use expensive partition-resizing tools.
There is much simpler.

First, this is what the method exposed here can do :

-   No need of expensive partition-resizing software
-   Works with multiple partitions setups (if you have more than two
    partitions on you hard-drive)
