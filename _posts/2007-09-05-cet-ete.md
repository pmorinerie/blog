---
layout: post
title: "Cet été"
date: 2007-09-05 15:20
---

Qu'ai-je donc bien fait de mon été, vous demanderez vous ? (ou pas)

Hé bien, pour commencer, un passage d'un mois chez

![](/images/logos/alcatel_lucent_logo.png)

...puis deux semaines de vacances à

[![Budapest](/images/vacances/Budapest_small.jpg)](/images/vacances/Budapest.jpg)

...quelques jours avec la familia en

[![Irlande](/images/vacances/Irlande_small.jpg)](/images/vacances/Irlande.jpg)

...et enfin un passage par

[![Irlande](/images/vacances/Venise_small.jpg)](/images/vacances/Venise.jpg)

Brefle, une période bien remplie :)
