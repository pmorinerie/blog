---
layout: post
title: "Tout le monde en parle : aujourd'hui, 22 octobre, c'est…"
date: 2009-10-22 18:29
---

… l'anniversaire de Brassens. Le moment de réécouter quelques chansons
mythiques ou poétiques (et parfois même les deux) au lieu de gloser sur
un nouvel OS.
