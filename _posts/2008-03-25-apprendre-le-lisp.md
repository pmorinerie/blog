---
layout: post
title: "Apprendre le Lisp"
date: 2008-03-25 10:38
---

Ca fait un certain temps que je fantasme [sur](http://xkcd.com/224/)
[le](http://xkcd.com/297/)
**[Lisp](http://en.wikipedia.org/wiki/Lisp_programming_language)**. Mais
cette fois ci c'est décidé, je m'y met : j'ai commencé à apprendre le
Lisp depuis quelques jours. Expériences jusqu'ici :

#### Le langage Lisp

A première vue, le Lisp est effectivement un chouette langage. C'est
encore très vivant, il y a plein d'implémentations et de bibliothèques
différentes, des bindings avec des tas d'autres langages — j'ai même
découvert des sites web dynamiques écrits en Lisp !

C'est un style de programmation assez différent (déclaratif au lieu
d'être fonctionnel), que l'on découvre assez agréable, élégant, et tout.
Il n'y a que quelques principes fondateurs, desquels tout découle, ce
qui rend le langage finalement assez facile à apprendre : rapidement, on
se retrouve à penser en Lisp plutôt qu'en fonctionnel.

D'autre part, tout le monde insiste beaucoup sur la grande flexibilité
de Lisp, et c'est vrai : on peut programmer autant en style fonctionnel
(boucles, déclarations de variables, etc.) qu'en style déclaratif — il
est également possible de faire de l'orienté objet ou aspect.

#### Logiciels et tutoriels : comment démarrer

Les tutoriels sur le Lisp sont légion — mais un peu trop légion,
justement : on ne sait pas par quoi commencer. Voici une rapide
sélection de bons documents, pour débuter ou approfondir :

-   [Casting Spell in Lisp](http://www.lisperati.com/casting.html) :
    celui qui m'a fait démarrer. Un rapide tutoriel de quelques pages
    sous forme de Comic book, qui présente le Lisp à travers la création
    d'un petit jeu de rôle en mode texte, à base de magiciens, de
    grenouilles et de bouteilles de whisky. Amusant et bien fait, il
    présenter les concepts fondamentaux du Lisp par la pratique.
    Vivement recommandé :)
-   [Learning Lisp
    fast](http://www.cs.gmu.edu/~sean/lisp/LispTutorial.html) : une
    bonne référence pour apprendre un peu plus en profondeur les
    structures du Lisp, à l'aide de nombreux petits exemples.
    Intéressant, quoique un peu plus proche de la collection d'exemples
    que du cours organisé.
-   [Welcome to
    Lisp](http://lib.store.yahoo.net/lib/paulgraham/acl2.txt) : un livre
    dont le premier chapitre, déjà très complet, est disponible en
    ligne. Que le texte brut ne vous déroute pas, ce cours explique
    vraiment bien les concepts fondamentaux du Lisp, et est une
    excellente lecture une fois que l'on a tapé quelques exemples.

Quand au choix d'un interpréteur Lisp, j'utilise pour l'instant [Clozure
CL](http://trac.clozure.com/openmcl), qui fonctionne sur tous les bons
Macs — mais il y a certainement plein d'autres bonnes choses.

#### Conclusion

Le Lisp mérite vraiment qu'on y jette un œil, même sans l'apprendre à
fond. C'est souple, puissant, et surtout sa réputation légendaire
d'élégance n'est pas usurpée. Et puis apprendre de nouveaux langages,
saybien :)
