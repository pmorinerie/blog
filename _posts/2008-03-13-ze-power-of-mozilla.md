---
layout: post
title: "Ze power of Mozilla"
date: 2008-03-13 18:32
---

Donc, il y a une semaine, une première bêta publique d'Internet Explorer
8. Tant mieux : à la base, cette bêta ne devait être réservée qu'à une
poignée de testeurs.

Et donc, tout le monde s'empare des nouveautés : compatibilité accrue
(en même temps, il y avait de la marge...), amélioration du Javascript —
plus deux nouvelles fonctionnalités, les *Activities* et les
*WebSlices*. Les *Activities* permettent d'ajouter des services au menu
contextuel d'une page, et les *WebSlices* de mettre en marque-page des
portions d'une page web spécialement conçue.

Un an et demi entre la sortie d'IE7 et cette bêta ; mais pour que ces
deux fonctionnalités soient implémentées dans Firefox, il a fallu
[moins](http://www.kaply.com/weblog/2008/03/07/microsoft-activities-for-firefox-new-version/)
[d'une
semaine](http://www.glazman.org/weblog/dotclear/index.php?post/2008/03/10/WebSlices-in-Firefox-who-wants-to-try-).
Parlez moi des avantages d'avoir une plate-forme ouverte pour laquelle
développer des extensions est facile et sympa...
