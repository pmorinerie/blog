---
layout: post
title: "Musique légale : Spotify, Deezer, Jiwa, et consors"
date: 2009-11-23 18:20
---

*Attention : aujourd'hui, billet d'humeur. Amis du recul et des
réflexions nuancées, passez votre chemin.*

Ma culture musicale est pleine de trous. Ce matin, j'ai décidé d'y
remédier, en commençant par Led Zeppelin. Et comme je suis un bon
garçon, j'ai voulu utiliser les sites de musique en ligne légaux :
Spotify, Deezer, etc.

-   **Spotify** : "The author of this album decided not to make it
    available online. You can buy it though." Trop bien.
-   **Deezer** : Que des remixs pourris. Les albums authentiques ? Pas
    disponibles. Sans doute pour les mêmes raisons légales.
-   **Jiwa** : "Nous sommes désolés, mais pour des raisons légales nous
    ne pouvons plus diffuser dans votre pays." Non, ce n'est pas la
    censure chinoise : juste les Etats-Unis, où je suis en ce moment.
    Deezer m'a déjà fait le même coup.
-   **The Pirate Bay** : "À votre service, tout ce que vous voulez,
    voilà voilà !" Et toc.

Et là j'ai envie de pousser une gueulante, un petit peu, quand même. Bon
sang, ayants-droits de mes deux, qu'est-ce que ça peut vous faire que
mes oreilles soient d'un côté ou de l'autre de l'Atlantique ? Je ne sais
pas qui bloque ça, Led Zeppelin ou Big Content, mais vous croyez une
seule seconde que vous allez vendre plus de disques parce que vous
bloquez la diffusion en ligne, en streaming même pas téléchargeable ?
Ces systèmes bourrés de liens "Achetez ce morceau sur iTunes" et de
boutons "Achetez cet album sur Amazon", le pain bénit de l'achat
impulsif ?

Morale de l'histoire : si je veux découvrir Led Zeppelin, juste comme ça
(et éventuellement acheter par la suite quelques singles ou albums si
j'aime bien), Bittorrent est mon ami — et je rejoins la foule des odieux
pirates qui menacent toute notre société.
