---
layout: post
title: "Réparations"
date: 2008-04-08 16:56
---

> Three things are certain:  
> Death, taxes, and lost data.  
> Guess which has occurred.

-   S'étonner d'un freeze bizarre de son MacBook
-   L'observer bloqué à l'écran gris de démarrage
-   Essayer sans succès de le démarrer depuis un DVD d'installation
-   Effectuer toutes les [manipulations
    standard](http://docs.info.apple.com/article.html?artnum=303234-fr)
-   Finir par réussir à démarrer sur le DVD
-   Entendre des râclements et des cliquetis venant du disque dur
-   Observer la disparition de son disque dur de la liste des
    périphériques
-   Vérifier la date de sa dernière sauvegarde (1 mois)
-   Blêmir
-   Obtenir d'excellents conseils de l'Assistance Apple
-   Apporter la bête à un centre de maintenance
-   Attendre...

Bonne nouvelle : la garantie joue. Mauvaise nouvelle : quelques
documents perdus, une sauvegarde plus toute jeune, et pas d'ordinateur
pendant quelques jours…
