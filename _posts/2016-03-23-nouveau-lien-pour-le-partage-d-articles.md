---
layout: post
title: Nouvelle URL de partage d'articles
date: 2016-03-23 17:05
draft: true
---

Bonjour à tous,

Comme vous l'avez vu, Delicious injecte depuis peu des publicités dans la
liste d'articles que je partage (et à laquelle vous êtes abonnés si vous lisez
ce message). C'est la honte. Et aussi la dernière refonte de leur site web est toute
cassée.

Je vais donc rapidement trouver un autre système pour partager des articles
(sans doute quelque chose à base de Diaspora* ou de The Old Reader). Ce qui
veut dire que l'adresse de ce flux RSS va changer.

**Si vous voulez continuer à lire cette liste, rajoutez cette adresse à votre
lecteur RSS :**

<a href="https://pmorinerie.eu/rss/shared.php">https://pmorinerie.eu/rss/shared.php</a>

Ce nouveau lien fonctionne dès maintenant, et sera mis à jour vers le nouveau
système sans pub automatiquement.

Plein de bises à tous :)
Pierre
