---
layout: post
title: "Sudo pour Windows"
date: 2006-06-16 13:27
---

Sous Windows XP, les comptes à privilèges limités sont rarement
utilisés... parce que justement les privilèges sont bien trop limités,
rendant nécessaire l'utilisation quasi-perpétuelle d'un compte
administrateur. [SudoWn](http://sudown.mine.nu/) permet de résoudre
élégamment ce problème, en équipant Windows d'un équivalent de la
commande Unix [sudo](http://www.gratisoft.us/sudo/man/sudo.html). On
peut donc temporairement élever ses privilèges, afin par exemple
d'installer un jeu, puis utiliser le jeu installé sous privilèges
limités.

Des vidéos de démonstration des différentes interfaces de SudoWn
(console, GUI, menu contextuel) sont présentes sur le site web, ainsi
que un manuel et le code source. Pour l'instant, la technique pêche un
peu : SudoWn est composée principalement d'un service en C\#, tournant
donc continuellement en arrière plan - mais l'auteur semble travailler à
une version C++, plus légère.

[SudoWn - Superuser do for limited accounts on Windows
XP](http://sudown.mine.nu/)
