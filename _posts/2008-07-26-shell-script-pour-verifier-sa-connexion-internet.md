---
layout: post
title: "Shell script pour vérifier sa connexion Internet"
date: 2008-07-26 10:44
---

Les serveurs DNS principaux de Free étaient down hier soir (peut-être un
problème lié à la récente [faille
DNS](http://www.pcinpact.com/actu/news/44694-serveurs-dns-dan-kaminski-correctifs.htm?vc=1)
?) ; les sites étaient accessible par IP, mais pas par nom de domaine.
Et sans Internet, pas moyen de récupérer l'adresse de serveurs DNS
alternatifs, du genre [OpenDNS](http://www.opendns.com/)...

Bref, au lieu de rester devant mon ordinateur à attendre que cela
revienne, j'ai tapé dans un Terminal (bash) :

    until (ping -a google.com) ; do true ; done

Ce script essaie de pinger google.fr tant qu'il n'y arrive pas, et émet
un bip par ping lorsque le site redevient accessible. Lorsque le réseau
se remet à fonctionner, on est donc averti par une **série de bips**.

On peut également limiter le nombre de pings, pour limiter le nombre de
beeps lorsque la connexion revient — tout cela en une ligne de commande.
Décidément, vive le shell script !
