---
layout: post
title: "Windows 7 install-party"
date: 2009-09-14 17:25
---

Windows 7 install-party drinking game :

-   One shot every thirty minutes the install or upgrade process takes.
-   Two shot if you have to start over.
-   Drain the bottle if it ATE YOUR GODDAMN DATA.

*(from [David
Gerard](http://tech.slashdot.org/comments.pl?sid=1367735&cid=29413793))*

As the upgrade process from Vista to 7 might take as long as [20
hours](http://tech.slashdot.org/story/09/09/14/1338207/Windows-7-Upgrade-Can-Take-Nearly-a-Day?art_pos=1),
it might be a fun game.

Besides this, if you install from XP, you have to perform a fresh
install (no upgrade), which is actually quite fast.
