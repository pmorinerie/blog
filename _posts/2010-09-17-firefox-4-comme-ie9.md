---
layout: post
title: "Firefox 4 comme IE9"
date: 2010-09-17 14:00
---

La nouvelle interface d'Internet Explorer 9 a été dévoilée hier. Elle a
ses défauts, mais son côté minimaliste est vraiment agréable.

D'ailleurs, avec Firefox 4 bêta, il y a moyen d'obtenir la même
interface — sans extension, simplement en réorganisant les barres
d'outils.

[![Firefox avec l'interface
d'IE9](/images/screenshots/firefox-ie9-thumb.png)](/images/screenshots/firefox-ie9.png)

Il suffit de glisser la barre d'adresse et les quelques boutons
nécessaires dans la barre d'onglets, et voilà : une interface simple à
la IE9.
