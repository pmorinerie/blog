---
layout: post
title: "Menu déroulant en CSS uniquement"
date: 2006-05-07 17:21
---

Si vous avez déjà essayé de faire un menu déroulant dans une page Web,
vous avez sans doute constaté que c'est tout à fait réalisable en pur
CSS (sans Javascript), mais qu'à cause des limitations d'Internet
Explorer il fallait rajouter une surcouche de Javascript - ce qui
empêche les visiteurs ayant désactivé Javascript d'utiliser le menu. Il
est communément admis qu'un menu CSS-only n'est pas réalisable sous
IE...

Hé bien [Stu Nicholls](http://www.cssplay.co.uk/) a trouvé un moyen, et
développé un menu déroulant sans Javascript, passant sous les principaux
navigateurs, dont IE6 et IE7b. Il utilise pour cela une bizzarerie du
rendu d'IE6, qui lui permet d'imbriquer des liens si ils sont contenus
dans des tables. Grâce aux [commentaires
conditionels](http://msdn.microsoft.com/workshop/author/dhtml/overview/ccomment_ovw.asp),
le code général et la feuille CSS restent valides.

J'ai implémenté cette merveille du côté de [Viens voir ce site
!](http://www.viensvoircesite.com), ce sera en ligne d'ici quelques
jours. Vous pouvez en attendant consulter la page de démonstration et
d'explications de Stu Nicholls, [The Ultimate CSS only Dropdown
Menu](http://www.cssplay.co.uk/menus/final_drop.html). Enjoy !
