---
layout: post
title: "Joel Spolsky et les standards du Web"
date: 2008-03-17 21:44
---

Ah, les trolls sur les standards du Web... "Mon navigateur est mieux que
le tien", "IE6 est bogué jusqu'au trognon", "IE8 ne va pas respecter les
standards"... Joel Spolsky, faisant écho au récent débat sur le mode
standard par défaut d'Internet Explorer 8, [analyse longuement la
situation](http://www.joelonsoftware.com/items/2008/03/17.html), et
explique surtout pourquoi il n'y a pas de bonne solution. Rapide résumé
des idées présentées :

#### Les idéalistes et les pragmatiques

![Logo du W3C](/images/logos/w3c_logo_andrei_herasimchuk.gif)Le débat
a lieu entre deux clans, les idéalistes et les pragmatiques ; ceux
prônant un respect strict des standards quoi qu'il en coûte et ceux qui
pensent aux applications réelles et à la rétrocompatibilité. En
substance, Spolsky pense que le modèle standard prôné par les idéalistes
n'existe pas : contrairemement à de nombreux standards, il n'existe pas
d'implémentation de référence des standards Web. Il est donc difficile
de tester la conformité à ces standards, qui n'existent que sur le
papier.

Selon Spolsky, les idéalistes (le "clan MSDN") ont déjà gagné par le
passé, par exemple en demandant [le respect par défaut des standards
dans
IE8](http://blogs.msdn.com/ie/archive/2008/03/03/microsoft-s-interoperability-principles-and-ie8.aspx),
ou de nouvelles API pour Vista. Ceci a minimisé la rétrocompatibilité,
et contribué aux critiques de ces produits.

#### Le poid des acquis

![Logo Internet Explorer 7](/images/logos/ie7_logo.png)Que faire,
alors ? Et comment assurer la compatibilité des sites existants tout en
continuant à améliorer le support des standards du Web dans les
navigateurs ?

La solution pour préserver la compatibilité, d'après Spolsky, est de la
ré-établir comme valeur cardinale — et de créer de nouveaux modes isolés
des anciens plutôt que d'améliorer (et de briser) sans cesse l'existant.
Concrètement, cela revient à préserver le mode par défaut d'IE7 dans
IE8, et à conserver l'idée d'un sélecteur optionnel dans les pages Web
pour activer le mode le plus standard. Même si, sous la pression de la
communauté, IE8 bêta respecte finalement les standards par défaut,
Spolsky est convaincu que ce choix est irréaliste, et que Microsoft
reviendra sur cette décision lors de la sortie de la version finale.

#### Aller plus loin

L'article est intéressant — mais oublie de mentionner tous les détails
de la solution finalement retenue par Microsoft pour IE8. Bien sûr, le
mode le plus standard (et donc brisant certains sites) est activé par
défaut, mais un sélecteur simple à intégrer dans une page Web permet de
l'afficher en utilisant le mode IE7. Alors certes, certaines pages ne
sont pas éditables (trop anciennes, sur CD-ROM, etc.), et cette solution
demande un léger travail d'édition. Mais elle permet tout de même, en
une ligne de HTML, de rendre un site "compatible" avec IE8. Bref,
l'alternative "compatible" ou "brise-tout" n'est pas aussi marqué que le
présente Spolsky.

D'autre part, l'article exhibe des images de sites complètement brisés
sous IE8, comme Google Maps — preuves qu'un mode respectueux des
standards conduirait irrémédiablement à une absence de compatibilité.
Mais il y a bien d'autres navigateurs qui respectent bien mieux les
standards, et sous lesquels ces problèmes n'apparaissent pas. Google
Maps fonctionne impeccablement sous Firefox 3 bêta ou sous Safari, qui
sont connus pour bien mieux respecter les standards qu'Internet
Explorer. Il doit donc être possible de respecter les standards *et* de
maintenir la compatibilité, puisque d'autres l'on fait — et les
problèmes d'IE8 sont sans doute plus liées à son statut de bêta et aux
nombreux changement dans le code qu'à une incompatibilité intrinsèque
liée aux respect des standards.
