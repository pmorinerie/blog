---
layout: post
title: "InterfaceLift"
date: 2008-03-06 11:44
---

Mon Bureau actuel, prétexte pour partager un [site de fonds
d'écrans](http://interfacelift.com/wallpaper/) que j'aime bien...

[![Bureau Kemenaran - mars
2008](/images/wallpaper/Bureau_InterfaceLift_06032008_mini.jpg)](http://winosx.com/hosted_img/Bureau_InterfaceLift_06032008.jpg)

-   **Système:** Mac OS 10.5 Leopard
-   **Fond d'écran:** [Pacific
    City](http://interfacelift.com/wallpaper/details.php?id=572) par
    Chris Fenison
