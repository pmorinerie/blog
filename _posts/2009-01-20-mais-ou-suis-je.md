---
layout: post
title: "Mais où suis-je ?"
date: 2009-01-20 11:57
---

Si vous vous demandez où je suis, ce que je fais, et pourquoi ce blog
n'est pas beaucoup mis à jour depuis trois mois, je vous invite sur
[www.lavaliseencarton.fr](http://www.lavaliseencarton.fr) :j'y raconte
la vie quotidienne d'un volontaire européen au Togo.

KZone reprendra son fonctionnement normal à mon retour du Togo, dans
quatre mois environ. A bientôt !
