---
layout: post
title: "Google Wave"
date: 2009-05-29 11:12
---

Bon, tout le monde va sans doute en parler, mais jeter donc un œil à la
présentation d'un nouveau service développé par Google : [Google
Wave](http://wave.google.com/).

L'idée est que le modèle du courriel, bien que très utilisé, est
vieilissant : l'e-mail a bientôt 40 ans — et il se base sur le modèle du
courrier papier, qui est encore un peu plus vieux :) Et donc les joyeux
ingénieurs de Mountain View ont décidé de réinventer le courriel, en
prenant en compte les technologies d'aujourd'hui. C'est assez
impressionnant.

<div>

<object width="425" height="265">
<param name="movie" value="http://www.youtube.com/v/v_UyVmITiYQ&amp;hl=en&amp;fs=1&amp;rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param>
<embed src="http://www.youtube.com/v/v_UyVmITiYQ&amp;hl=en&amp;fs=1&amp;rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="265">
</embed>
</object>

</div>

La technologie est Open-source, les API seront publics, et tout cela
utilise HTML5 (déjà supporté par Firefox 3.5, téléchargez-le quand il
sortira !). Bref, que du bonheur.
