---
layout: post
title: "L'innovation chez Blizzard"
date: 2008-04-07 11:01
---

Le [blog sur l'innovation](http://innovation.freedomblogging.com/) tenu
par Colin Stewart a récemment posté un billet qui me rappelle fort
certains enseignements tirés des cours d'innovation que j'ai suivi à
l'Isep (dont il faudra que je vous parle un jour). Il analyse le succès
de Blizzard et du jeu World of Warcraft, et en tire [11 leçons sur
l'innovation](http://innovation.freedomblogging.com/2008/04/04/11-innovation-lessons-from-creators-of-world-of-warcraft/)
— qui sont en fait applicables à quasiment tout développement
informatique, voire industriel, voire quoi que ce soit. On trouve par
exemple des leçons comme "servez-vous des critiques", "utilisez votre
propre produit", "demandez la perfection", etc., le tout détaillé et
expliqué. Mérite un coup d'œil, c'est très instructif.

**[11 innovation lessons from creators of World of
Warcraft](http://innovation.freedomblogging.com/2008/04/04/11-innovation-lessons-from-creators-of-world-of-warcraft/)**
