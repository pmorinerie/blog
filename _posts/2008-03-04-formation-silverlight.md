---
layout: post
title: "Formation Silverlight"
date: 2008-03-04 19:57
---

Voici le screencast de la **formation
[Silverlight](http://www.silverlight.net)** que j'ai donnée le [20
février à l'ISEP](http://juniorisep-silverlight.blogspot.com/). Il est
divisé en sept vidéos, chacune couvrant une partie de la formation :
aperçu de la technologie, démos, outils de développement nécessaires,
création en live d'exemples simples, pistes de formation plus complète,
etc.

Pour ceux qui le souhaitent, une [version non montée en haute
résolution](http://winosx.com/hosted_files/Formation%20Silverlight/Silverlight%20screencast.mov)
est disponible, ainsi que le [diaporama de la
formation](http://www.slideshare.net/kemenaran/introduction-silverlight/)
et les [fichiers d'exemples
utilisés](http://winosx.com/hosted_files/Formation%20Silverlight/Silverlight%20-%20D%e9mos%20et%20exemples.zip).

<div style="text-align:center">

<object width="420" height="357">
<param value="http://www.dailymotion.com/swf/x4hhgk&amp;v3=1&amp;related=1" name="movie"></param><param value="true" name="allowFullScreen"></param><param value="always" name="allowScriptAccess"></param>
<embed allowscriptaccess="always" width="420" src="http://www.dailymotion.com/swf/x4hhgk&amp;v3=1&amp;related=1" height="357" allowfullscreen="true" type="application/x-shockwave-flash">
</embed>
</object>
  
**[Présentation Silverlight
1](http://www.dailymotion.com/video/x4hhgk_presentation-silverlight1_school)**

</div>

<div style="text-align: center; margin-top:30px;">

<object width="420" height="357">
<param value="http://www.dailymotion.com/swf/x4hi1z&amp;v3=1&amp;related=1" name="movie"></param><param value="true" name="allowFullScreen"></param><param value="always" name="allowScriptAccess"></param>
<embed allowscriptaccess="always" width="420" src="http://www.dailymotion.com/swf/x4hi1z&amp;v3=1&amp;related=1" height="357" allowfullscreen="true" type="application/x-shockwave-flash">
</embed>
</object>
  
**[Présentation Silverlight
2](http://www.dailymotion.com/video/x4hi1z_presentation-silverlight-2_school)**

</div>

<div style="text-align: center; margin-top:30px;">

<object width="420" height="357">
<param value="http://www.dailymotion.com/swf/x4hibe&amp;v3=1&amp;related=1" name="movie"></param><param value="true" name="allowFullScreen"></param><param value="always" name="allowScriptAccess"></param>
<embed allowscriptaccess="always" width="420" src="http://www.dailymotion.com/swf/x4hibe&amp;v3=1&amp;related=1" height="357" allowfullscreen="true" type="application/x-shockwave-flash">
</embed>
</object>
  
**[Démo Silverlight partie
1](http://www.dailymotion.com/video/x4hibe_demo-silverlight-partie-1_school)**

</div>

<div style="text-align: center; margin-top:30px;">

<object width="420" height="357">
<param value="http://www.dailymotion.com/swf/x4hkqs&amp;v3=1&amp;related=1" name="movie"></param><param value="true" name="allowFullScreen"></param><param value="always" name="allowScriptAccess"></param>
<embed allowscriptaccess="always" width="420" src="http://www.dailymotion.com/swf/x4hkqs&amp;v3=1&amp;related=1" height="357" allowfullscreen="true" type="application/x-shockwave-flash">
</embed>
</object>
  
**[Démo Silverlight partie
2](http://www.dailymotion.com/video/x4hkqs_demo-silverlight-partie-2_school)**

</div>

<div style="text-align: center; margin-top:30px;">

<object width="420" height="357">
<param value="http://www.dailymotion.com/swf/x4hjsh&amp;v3=1&amp;related=1" name="movie"></param><param value="true" name="allowFullScreen"></param><param value="always" name="allowScriptAccess"></param>
<embed allowscriptaccess="always" width="420" src="http://www.dailymotion.com/swf/x4hjsh&amp;v3=1&amp;related=1" height="357" allowfullscreen="true" type="application/x-shockwave-flash">
</embed>
</object>
  
**[Démo Silverlight partie
3](http://www.dailymotion.com/video/x4hjsh_demo-silverlight-partie-3_school)**

</div>

<div style="text-align: center; margin-top:30px;">

<object width="420" height="357">
<param value="http://www.dailymotion.com/swf/x4hjwy&amp;v3=1&amp;related=1" name="movie"></param><param value="true" name="allowFullScreen"></param><param value="always" name="allowScriptAccess"></param>
<embed allowscriptaccess="always" width="420" src="http://www.dailymotion.com/swf/x4hjwy&amp;v3=1&amp;related=1" height="357" allowfullscreen="true" type="application/x-shockwave-flash">
</embed>
</object>
  
**[Silverlight concours et
conclusion](http://www.dailymotion.com/video/x4hjwy_silverlight-concours-et-conclusion_school)**

</div>
