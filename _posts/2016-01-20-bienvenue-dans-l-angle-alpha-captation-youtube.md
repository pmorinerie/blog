---
layout: post
title: "Bienvenue dans l’angle Alpha : la captation"
date: 2016-01-20 07:11
---

Le meilleur spectacle que j’ai vu en 2015 est certainement « Bienvenue dans l’Angle Alpha »

<img src="/images/Bienvenue dans l'angle alpha.jpg" style="max-width:560px" alt="L'échelle rouge au centre du spectacle" />

À la base, il s'agit d'une adaptation au théâtre d'un livre de Frédéric Lordon, « [Capitalisme, désir et servitude](http://www.lalibrairie.com/nos-rayons/livres-en-francais/economie-industrie-technique/economie-et-entreprises/economie/capitalisme-desir-et-servitude--marx-et-spinoza-frederic-lordon-9782358720137.html) ». Judith Bernard l’a adapté, mis en scène, et en a fait un spectacle passionnant, drôle et profond sur le monde du travail d’hier et d’aujourd’hui.

C'est un spectacle de théâtre, à la durée de vie forcément éphémère. Mais miracle : la compagnie vient de mettre en ligne une [captation sur Youtube](https://www.youtube.com/watch?v=cw3Yn0B77Zo). Et franchement je vous le recommande :

* Parce qu’il m’a passionné sur le monde du travail,
* Parce qu'il m’a demandé à quoi je voulais consacrer mon temps,
* Parce qu’il a cristallisé ma vague sensation de mal-être face au « management à la cool »,
* Parce qu'il développe une idée, tire les fils, arrive à un autre problème, une autre idée, et sans cesse rebondit et continue d'explorer.

Est-ce que ça rendra la même chose que joué devant vous au théâtre ? Non, clairement. Mais maintenant que la pièce est dite, c’est la meilleure approximation disponible, et c’est quand même pas mal.

Prenez une heure, un grand écran, et laissez vous plonger vous dans le monde du travail, du désir-maître et du conatus.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/cw3Yn0B77Zo?rel=0" frameborder="0" allowfullscreen></iframe>
