---
layout: post
title: "WideGB: playing Game Boy games on wide screens"
date: 2019-04-21 22:02
---

A few months ago, Daniel Prilik released [WideNES](http://prilik.com/blog/2018/08/24/wideNES.html). This clever emulator hack allows to peek beyond the screen boundaries of NES games, to see more of the game, and to play in wide screen formats.

To celebrate the 30th anniversary of the Game Boy release, today I'm releasing a WideNES-inspired emulator, **WideGB**.

<video width="470" controls loop>
    <source src="/videos/widegb/WideGB - Super Mario Land 2 DX.mp4" type="video/mp4"/>
</video>

## What is this thing?

WideGB is an emulator. Well, it is actually a modified version of [SameBoy](https://sameboy.github.io/), an excellent and very accurate Game Boy emulator made by Lior Halphon.

When running a build of SameBoy compiled with WideGB, an extra option appears: “Use Widescreen”. Enabling this option extends the screen boundaries of the game: you can then resize the window, and use the aspect ratio you wish.

<video width="500" controls>
    <source src="/videos/widegb/WideGB - Pokemon Gold.mp4" type="video/mp4"/>
</video>

## How does it work?

WideGB is very similar to WideNES. It basically records the screen as it moves, and keeps the parts of the screen previously drawn in place.

When starting a game for the first time, or when reaching a new area of the game, WideGB doesn't know yet about any parts of the screen. But as soon as you move, it starts recording the area graphics, and you can see the places you've been to appearing gradually.

When the player moves to a new location (such as entering inside a house), WideGB detects that the picture changed suddenly: it saves the previous scene, and starts a new one. It even looks for previously encountered locations, so that if a scene was already visited, it can be restored automatically.

Additionally, WideGB attempts to draw the HUD of the game (with timers, lifes count, etc.) with a translucency effect, so that the part of the game under the HUD are still visible.

<video width="470" controls loop>
    <source src="/videos/widegb/WideGB - Zelda Link's Awakening.mp4" type="video/mp4"/>
</video>

<br>A limitation of this method is that sprites are not recorded. Sprites are often used for NPC and ennemies: recording them causes still characters to appears at the edge of the screen, and doesn't look very good.

However, this method has the benefit of being (in theory) compatible with every game. Truth to be told, some heuristics needs to be tuned on a per-game basis (such as "how much does a picture need to change to signal a new scene?"). But should work with most games of the Game Boy and Game Boy Color library. It has been tested with Pokémon Red, Gold, Super Mario Land 2, and Zelda: Link's Awakening.

_For more informations on the inner workings of this emulating method, see the [original article describing the internals of WideNES](http://prilik.com/blog/2018/08/24/wideNES.html)._

## Download it

Want to try this at home?

- [SameBoy + WideGB for macOS](https://github.com/kemenaran/SameBoy/releases)
- [SameBoy + WideGB for Windows](https://github.com/kemenaran/SameBoy/releases)
- [Source code on GitHub](https://github.com/kemenaran/SameBoy/tree/wide_gb)

## What needs to be improved

This is very much a work in progress.

As you can see, the **Windows build** is not out yet. Some Windows-specific code has to be adjusted (like enumerating directories), and a build should be available soon. _**EDIT**: the Windows port is now available._

A limitation of the current engine is the **heuristic used to detect a new scene**. A perceptual hash of the current and next frame is used–but sometimes it detects too little, and sometimes too much. A better perceptual hashing implementation could be less sensitive to scrolling (which harding ever signals a scene change), and more sensitive to hard-cuts and fades-to-white.

On the long run, I'd like to merge WideGB into the officiel SameBoy tree. However, it could also be easily integrated into other emulators. **WideGB is designed  as a platform-agnostic library**. This means it can be used with any kind of emulator, regardless of the rendering method: give it frames, and it will split out the data required to render the extended screen. It has already been used as a backend for two different rendering methods (Cocoa and SDL), and should be quite flexible.

If you are interested, have a look at the [source code](https://github.com/kemenaran/SameBoy/tree/wide_gb). And, of course, contributions are welcome.

Happy 30th birthday, Game Boy!
