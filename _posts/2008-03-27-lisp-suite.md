---
layout: post
title: "Lisp (suite)"
date: 2008-03-27 12:19
---

Je suis finalement passé à **CarbonEmacs+Slime+OpenMCL** comme
environnement de développement
[Lisp](http://kemenaran.winosx.com/?2008/03/25/113-comment-apprendre-le-lisp),
et c'est tout chouette — bien plus que le petit éditeur de démo
d'OpenMCL. Editeur de code, interpréteur, animations et petites phrases
amusantes dans Emacs... Le pied. Merci à David Steuber pour son
excellent tutoriel sur l'installation d'un [environnement Lisp sous Mac
OS X](http://www.david-steuber.com/Lisp/OSX/) !

[![Carbon Emacs + Slime +
OpenMCL](/images/screenshots/emacs-lisp-screenshot_thumb.gif)](/images/screenshots/emacs-lisp-screenshot.png "Mon environnement de développement Lisp")
