---
layout: post
title: "Fiche de lecture : « Au commencement était… – Une nouvelle histoire de l’humanité »"
date: 2024-09-23 11:57
---

Je viens de finir « _Au commencement était… – Une nouvelle histoire de l’humanité_ », le dernier bouquin co-écrit par [David Graeber](https://fr.wikipedia.org/wiki/David_Graeber). Un beau pavé, fascinant.

La thèse du bouquin en très court : historiquement, l’être humain est bien plus doué que ce qu’on pense pour inventer toutes sortes de systèmes politiques très différents les uns des autres. Les conditions matérielles ne déterminent _pas_ les formes politiques. Alors retrouvons de l’inventivité !

<a href="https://www.lalibrairie.com/livres/au-commencement-etait-----une-nouvelle-histoire-de-l-humanite_0-7595777_9791020910301.html">
<img width="200" style="float:right; margin-top: 2.8em; margin-left: 0.7em" src="/images/graeber_au_commencement_etait_couverture.webp" />
</a>

## Quelques notes

Déjà, c’est un livre épais. Je l'ai lu en plusieurs mois, tranquillou, chapitre par chapitre.

Ça vaut le coup : le bouquin explore plusieurs facettes des mêmes idées centrales, en les développant dans différents chapitres, époques et cultures.

Au début je ne voyais pas trop où il voulait en venir, mais ça s'est clarifié à force d'évoquer plusieurs la même chose de manière différente.

(Et aussi, pour les impatient·es, le [résumé sur Wikipédia](https://fr.m.wikipedia.org/wiki/Au_commencement_%C3%A9tait%E2%80%A6) est très bien.)

## La politique, déterminée par les conditions matérielles ?

L'idée de base, c'est que l'être humain a une grande inventivité pour créer des systèmes politiques et des manières de s'organiser.

Pas seulement à l'époque moderne : sans doute aussi loin que les preuves archéologiques remontent.

Plus on fouille les sites, plus on découvres les cultures humaines, plus on voit que y'a mille façons d'organiser des petits groupes, des villes, des aires culturelles entières.

Et le point important : tout ça sans déterminisme matériel, lié par exemple à l'économie ou à la taille du groupe.

- Concernant l'économie/la production de nourriture, le bouquin cite des sites où l'agriculture se pratiquait non pas en continu, mais de manière intermittente, ou semi-annuelle. Pas parce que les gens ne savaient pas faire mieux : mais parce qu'ils préféraient comme ça.

    Ou encore différents peuples de Californie du Nord, presque sur le même territoire, avec les mêmes ressources de poisson – mais ayant des organisations politiques radicalement différentes (par exemple libertaires ou esclavagistes).

- Et sur le déterminisme de taille du groupe, le livre cite des fouilles archéologiques d'une ville de centaines ou milliers de familles. Cette ville a d'abord été sous des régimes très hiérarchiques, puis après un basculement très égalitaires (faute d'un meilleur mot).

    Ou encore le cas une ville égalitaire, mais devenue très hiérarchique, puis qui a été abandonnée – entrainant un refus culturel des chefs trop puissants dans toute la région pendant les centaines d'années qui ont suivi.
    
## L'agriculture, cause de l'État moderne ?

Mais pourquoi insister autant sur le refus du déterminisme matériel ? Pourquoi chercher à prouver que l'avènement de l'agriculture ou des villes n'entraine pas un mode politique unique (typiquement celui de l'État moderne) ?

Parce que c'est généralement le présupposé qu'on a tous en tête, depuis les Lumières.

C'est l'histoire classique de Rousseau : au début des petits groupes de chasseurs cueilleurs pouvaient bien être égalitaires. Et puis bim, on découvre l'agriculture, et sans même qu'on s'en rende compte, cette découverte entraine la propriété, les excédents, l'administration centrale, les riches, l'État, l'inégalité. C'est mathématique. L'État et les inégalités, c'est l'aboutissement du progrès, c'est comme ça.

Sauf que, dit le bouquin, c'est historiquement faux.

La marche vers l'État moderne n'est pas linéaire, elle n'est pas déterminée. En tout cas c'est ce que le livre s'attache à prouver, à travers plein d'exemples.

Par exemple, que les peuples n'ont pas été « pris au piège de l'agriculture » : ils l'ont pratiqué de manière non-permanente pendant des millénaires, sans doute justement pour ne pas y adjoindre les désavantages de la sédentarisation. On trouve beaucoup de traces d'agriculture pratiquée seulement quelques mois dans l'année, ou quelques temps avant un déplacement. Et ce n'est pas que les gens ne savaient pas s'y prendre : ils choisissaient consciemment de ne pas pratiquer une agriculture permanente.

Ou encore, qu'on peut avoir des villes de taille importante qui soient auto-organisées – ou pas. Mais il n'y a pas de déterminisme là dessus : une ville n'est pas forcément hiérarchique, c'est un choix.

## Caractériser plus finement la notion d'État

Autre sujet abordé par le bouquin : la question de l'émergence de l'État.

Essentiellement, c'est pour dire que la question n'a de sens que si on considère l'État comme quelque chose qui *doit* émerger, dans un processus historique. Et qui reste là une fois qu'il a émergé (sauf effondrement de civilisation).

À la place, les auteurs préfèrent un découpage plus fin de ce que c'est qu'un « état ». Ils voient trois points :

1. La souveraineté : la tête a-t-elle le pouvoir de faire respecter ses décisions là où elle n'est pas ? Typiquement via la violence légitime

2. La bureaucratie : il y a-t-il une administration centrale et un contrôle de l'information efficace ?

3. La concurrence charismatique : les potentiels leaders politiques mettent-ils en scène des luttes/jeux entre eux ?

À partir de là, ils utilisent plein d'exemples historiques pour montrer que des systèmes politiques peuvent comporter un, deux, ou trois de ces éléments de domination de l'État. Et parfois avancer sur la voie d'un État, mais parfois aussi revenir à quelque chose de plus vivable.

## Les Lumières, inspirées par les réflexions indigènes

Il y a aussi des passages passionnants sur la manière dont certaines idées des Lumières sur l'égalité se sont formées à partir de la critique contemporaine des indiens d'Amérique. Ceux-ci, dans les décennies après la conquête, découvraient le fonctionnement européen – et avaient parfois un avis très clair dessus.

Si ce point vous intéresse, je vous recommande Wikipédia pour un résumé – ou une pré-publication de ce chapitre disponible en accès libre : [La sagesse de Kandiaronk : la critique indigène, le mythe du progrès et la naissance de la Gauche](https://web.archive.org/web/20240618054529/https://www.journaldumauss.net/?La-sagesse-de-Kandiaronk-la-critique-indigene-le-mythe-du-progres-et-la).

## En bref

Je ne donne que ce que j'ai compris des principaux arguments – mais en vrai, l'ensemble des descriptions d'exemples historiques sont très chouette à lire, et argumentent bien le propos.

C'est vraiment quelque chose que j'apprécie bien dans l'archéologie et l'anthropologie : lire des exemples d'organisations radicalement différentes de la nôtre – et se dire qu'on pourrait bien en faire tout autant.

Bref, conclusion de tout ça : ne restons pas bloqués dans la forme de l'État moderne, recommençons à inventer plein de choses ; mais si, c'est possible.

