---
layout: post
title: "Zelda: Link's Awakening progress reports moved to a new location"
date: 2023-07-11 16:31
---

After two years of waiting, a new progress report for the Zelda: Link's Awakening disassembly is [finally published](https://zladx.github.io/posts/links-awakening-disassembly-progress-report-part-13)!

To celebrate this, I took the time to move this series of articles to its own dedicated website: the [Link's Awakening disassembly blog](https://zladx.github.io/). Of course, the former URLs now redirect to these new pages.

This move makes subscribing to new disassembly-related articles easier, since only relevant Link’s Awakening content will be published.

And meanwhile my own blog will resume to more random and personal stuff.
