---
layout: post
title: "Le côté obscur du cerveau"
date: 2018-01-09 15:33
---

J’aime beaucoup la capacité des sciences, naturelles et sociales, à proposer des modèles mentaux qui rendent intuitif un comportement complexe. C’est particulièrement frappant en biologie, où le bon modèle mental peut permettre de donner du sens aux réactions de son propre corps.

Je sors justement d’une intervention de [Mani Ramaswami](https://www.tcd.ie/Neuroscience/partners/PI%20Profiles/Mani_Ramaswami2.php), neurobiologiste, qui est venu à l’[IISER Pune](http://www.iiserpune.ac.in/) parler de ses recherches sur un  mécanisme particulier du cerveau : l’inhibition des engrammes. Sous le titre accrocheur (« Le côté obscur du cerveau »), et le jargon scientifique, il s’agit en fait d’un modèle mental de « Comment ça se fait qu’on s’habitue à des stimulus de toute sorte ». Et ce modèle mental permet de donner une intuition de plein de choses : le filtrage des bruits de fond, la mémoire à long-terme, la fonction du sommeil, et certaines maladies mentales.

Ces recherches se basent sur un certain nombre d’[expériences et de publications](http://www.gen.tcd.ie/ramaswami/) – mais pour être honnête, je n’ai pas tout retenu. Je vous laisse voir les expérimentations par vous même – et à la place je vais plutôt vous raconter le modèle mental avec les mains.

Comment le cerveau s’habitue-t-il ?
---

Le cerveau est une machine à filtrer. Plus précisément une des fonctions principales du cerveau est de séparer les informations pertinentes des détails pas importants : les bruits de fond, ou les bruits répétitifs, ou les mouvements perturbants, etc.

Ce filtrage se fait en bonne partie par la diminution de l’importance accordée aux évènements répétitifs et peu pertinents. Par exemple on rentre dans une pièce, et on remarque qu’il y a un bruit de fond, ou une odeur persistante – mais passé dix minutes on ne s’en rend plus vraiment compte.

Comment est-ce que ça fonctionne ? Eh bien on peut faire des expériences qui montrent que les stimulus complexes sont encodés comme des groupes de neurones qui, à force d'être stimulés, se déclenchent simultanément. Les neurobiologistes appellent ça des [engrammes](https://fr.wikipedia.org/wiki/Engramme).

L’histoire, c’est qu’au fur et à mesure qu’un stimulus se répète, une réaction en miroir se construit : des neurotransmetteurs inhibent cet engramme. Ce n’est pas qu’il disparait : il est toujours activé, mais il y a également des inhibiteurs sur ce groupe de neurones. La réaction transmise _in fine_ au cerveau est alors bien moindre : on s’est habitué.

![Diagramme d’une exposition à un stimuli initial, puis une fois l’habituation déclenchée](/images/le-cote-obscur-du-cerveau/negative-inhibition.jpg)

C’est également ce qui explique (à très gros traits) comment un stimulus, même inhibé à force de répétition, peut revenir rapidement, dès qu’il y a un léger changement des conditions : les inhibiteurs disparaissent, l’engramme est actif à nouveau.


La mémoire à long-terme
---

Ce mécanisme de _stimulus → répétition → habituation_ permet de modéliser plein de comportements différents.

C’est le cas par exemple du passage de la mémoire à court-terme à la mémoire à long-terme. On peut imaginer le processus de cette manière :

1. On a une expérience ;<br>_(→ stockée comme un engramme)_
2. Le souvenir de cette expérience tourne dans la tête ;<br>_(→ l’engramme réagit très facilement, et est répété par le cerveau)_
3. Après un moment le souvenir s’atténue, et cesse d'être présent au quotidien ;<br>_(→ à force de répétition, l’engramme reçoit des inhibiteurs)_
4. Mais on peut toujours convoquer ce souvenir explicitement dans la mémoire à long-terme.<br>_(→ les inhibiteurs sont supprimés, et l’engramme redevient actif)_

Schématiquement, ce passage d’une mémoire qui s’impose (court terme) à une mémoire convocable à la demande (long terme) se représenterait de cette façon :

![Un souvenir nouveau est présent dans la mémoire à court terme ; l’inhibition par la répétition le fait passer dans la mémoire à court terme.](/images/le-cote-obscur-du-cerveau/engrams-memories.jpg)

C’est donc l’inhibition par la répétition qui ferait passer un souvenir ou un stimulus de la mémoire court-terme à la mémoire long-terme. Et de la même manière, la dé-inhibition ré-activerait le souvenir ou le stimulus.

« Je vais dormir dessus »
---

Ce modèle mental donne également une intuition sur une des fonction du sommeil.

On dit souvent qu’on mémorise en dormant – et les expériences montrent effectivement des liens entre le sommeil et la mémoire. Mais que se passe-t-il concrètement ?

On peut utiliser une analogie. Quand on dort, on sait que le système moteur est déconnecté ; c’est pour cela qu’on peut rêver de jouer au foot sans bouger les jambes. Une hypothèse est que le système émotionnel serait également déconnecté de la même manière. Le sommeil serait alors le moment où on peut rejouer des souvenirs en boucle, sans pour autant susciter de réaction émotionnelle. Et la répétition permet justement de construire des inhibiteurs, et donc de faire passer le souvenir dans la mémoire à long terme.

Pour Ramaswami, ça permet aussi d’avoir une intuition sur le fonctionnement des rêves : ils seraient provoqués par des inhibiteurs qui lâchent aléatoirement pendant le sommeil. Des souvenirs disparates sont donc ramenés à la conscience — et ensuite le cerveau essaie de donner du sens avec ça (parce qu’il parait qu’une des zone du cerveau a pour fonction spécifique de donner du sens aux choses, ce qui n’a pas fini pas de m’étonner.)

En cas de pépin
---

Enfin, cette manière de représenter la mémoire ouvre une porte sur les mécanismes à l’œuvre dans certaines maladies mentales.

Par exemple, si la fonction d’inhibition ne marche plus correctement, on se retrouve avec des souvenirs qui tournent en boucle (parce que la répétition est nécessaire à la construction de l’inhibition), mais qui ne quittent pas la mémoire à court-terme. Cela peut donner une idée de ce qui se passe avec certaines formes de stress post-traumatique.

Et dans l’autre sens, si le mécanisme suppresseur de l’inhibition dysfonctionne, ça donnerait des formes d’amnésie. Ce qui peut aussi expliquer comment il est possible de se souvenir de choses qu’on croyait avoir oubliées : le souvenir était là, mais inhibé.

Je crois qu’une partie de ces intuitions sont encore des théories – même si le mécanisme fondamental de stimulus-inhibition a l’air bien documenté. Mais je trouve que ça donne de chouettes résultats, et une manière intéressante de penser le sommeil et les souvenirs.
