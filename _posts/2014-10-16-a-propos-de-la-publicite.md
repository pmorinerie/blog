---
layout: post
title: "À propos de la publicité"
date: 2014-10-16 21:41
---

*Une amie, qui travaille sur un mémoire au sujet de la publicité en
ligne, m'a transmis un sondage de quelques questions. Est-ce que vous
trouvez la publicité en ligne intrusive ? Que faire pour l'améliorer ?
Qu'est-ce qui peut rendre la publicité en ligne plus pertinente ?*

*Je crois que je me suis un peu lâché sur la question de la publicité en
ligne d'une manière générale. Attention, roman. N.B. : ces opinions
n'engagent que moi.*

**Selon vous, que faut-il changer dans le mode de publicité en ligne ?**

Les incitations ; le modèle.

Pour moi le problème est double. Il y a d'une part, au 1er degré, le
côté intrusif de la publicité en ligne. Les bannières qui masquent
l'écran, ce qui scintille, le fait que seule une petite partie de la
page soit dévolue au contenu, et que la pub occupe la majeure partie de
l'espace.

C'est un problème, mais ça se règle assez facilement avec AdBlock. Je
l'installe sur mon ordinateur, sur ceux de toute ma famille, et la pub
disparaît, on n'en parle plus. Du coup j'aimerais bien voir le problème
des publicités intrusives résolu pour tout le monde (et pas seulement
pour les gens qui ont installé AdBlock), mais c'est un problème mineur.

En revanche, le gros problème pour moi, c'est que le mode principal de
financement sur Internet soit la publicité. Ça entraine des conséquences
moins visibles, mais beaucoup plus pénibles :

- les sites sont encouragés à faire des pages pour la pub, plutôt que
pour le contenu. Par exemple les journaux qui font des articles
alléchants mais vide de contenu, juste pour que les gens cliquent
dessus. Les articles coupés en trois pages, juste pour afficher trois
fois de la pub. Je préfèrerais que les journaux aient plus d'intérêt à
écrire de bons articles plutôt que des articles courts et publicitaires.

- pour maximiser les revenus, les régies publicitaires sont incitées à
pister les utilisateurs en ligne, à travers des sites web différents.
Facebook, Google ou Criteo savent quels sites je visite, et ceux qui
m'intéressent. Partout où une pub est affichée, la régie enregistre que
j'ai visité ce site. Les régies savent donc que je m'intéresse aux jeux
vidéos, à l'actualité (pas grave), et aussi au dépistage du cancer
(mmm), aux sites de rencontres extra-conjugales (oups) ou aux mouvements
de soutien des sans-papiers (tiens tiens). J'imagine sans peine les
mauvaises utilisations qui peuvent être faites d'un tel fichier. Et je
préfèrerais que personne n'ait intérêt à collecter et à croiser ce genre
d'informations sur les gens.

- les gens sont encouragés à donner eux-même ce contenu personnel, et
sont pisté avec. Par exemple, si sur Facebook j'ai toujours caché mon
homosexualité (parce que ce ne sont pas les oignons de mes parents),
Facebook sait malgré tout que j'ai des amis homosexuels, va en déduire
que j'ai de fortes chances de l'être, et va m'afficher des publicités
pour des sites de rencontre gay. Je préfèrerais que Facebook ait intérêt
à me mettre en contact avec mes amis, plutôt que d'inciter mes amis à
lâcher le maximum d'informations possibles sur eux.

- une quantité phénoménale de gens très créatifs et très intelligents
travaillent dans la publicité : pour créer des campagnes, pour pister
les gens, pour optimiser les algorithmes qui décident d'afficher telle
pub à tel ou tel moment. Je préfèrerais que ces gens très créatifs aient
plus intérêt à faire de l'art plutôt que de la publicité, et que les
gens très intelligents aient plus intérêt à chercher comment améliorer
le monde plutôt que de décider s'il faut afficher une publicité, à qui
et quand.

Bref, je préfèrerais que la publicité ne soit pas le modèle économique
dominant sur Internet, parce que je pense qu'elle produit beaucoup
d'incitations à faire de mauvaises choses. Et je préfère des systèmes où
la manière de gagner de l'argent n'est pas la publicité.

À titre d'exemple, je travaille sur un site de [réservation de billets
de trains](https://www.capitainetrain.com). Contrairement à
Voyages-SNCF, ce site n'affiche pas de publicités.

Pourquoi ? Parce que sa source de revenu est uniquement la vente de
billets. Plus le site vend de billets, plus il gagne d'argent, et c'est
tout. Il n'a dont pas intérêt à mettre de la publicité : ça embêterait
les gens, et ça rapporterait moins d'argent.

Après il s'agit aussi d'une entreprise qui place de la publicité pour
elle-même sur Internet. Comme quoi je ne suis pas à l'abri des
contradictions. Mais malgré tout ça me semble un pas dans la bonne
direction.
