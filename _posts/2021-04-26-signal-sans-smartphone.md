---
layout: post
title: Utiliser Signal sans smartphone
date: 2021-04-26T18:12:19.500Z
lang: fr
---
Si vous utilisez la messagerie Signal, vous savez sans doute qu'il est possible d'envoyer et de recevoir des messages sur Signal depuis votre ordinateur, en utilisant l'application officielle.

Mais sur ordinateur, il n'est pas encore possible de *créer* un compte Signal : on ne peut officiellement que relier un compte qui existe déjà sur un smartphone.

J'ai cherché un peu, et en fait il existe un moyen d'utiliser Signal sans smartphone. On peut utiliser un outil en ligne de commande pour créer un compte sans installer l'application mobile, et ensuite y relier l'application Signal pour ordinateur.

J'ai testé ça l'autre jour, et ça a bien fonctionné. La création du compte, est un peu technique ; mais après plus besoin d'y toucher : une fois que l'appli Signal est connectée sur l'ordinateur, elle fonctionne normalement, sans commandes particulières.

![Une capture d'écran de l'application Signal pour ordinateur.](/images/uploads/signal-desktop.png "L'application Signal pour ordinateur a moins de fonctionnalités que la version mobile, mais l'essentiel est là.")

## Mode d'emploi

Pour ma part j'ai suivi ces instruction :

1. **Installez l'utilitaire [`signal-cli`](https://github.com/AsamK/signal-cli)**, qui permet de créer un compte Signal sans smartphone.

    ```shell
    # Sous mac
    brew install signal-cli
    # Sous Linux, suivre les instruction ici :
    # https://github.com/AsamK/signal-cli/wiki/Quickstart
    ```
2. **Demandez la création du compte**, en remplaçant `+33600000000` par votre propre numéro de téléphone :

   ```
   signal-cli -u +33600000000 register
   ```

   (Au besoin, ajoutez l'option `--voice` à la fin de la ligne pour avoir un appel vocal à la place ; par exemple si vous utilisez une ligne fixe.)
3. (optionnel) **Si vous voyez un message *"Captcha invalid or required for verification"*** :

   * Ouvrez la page [https://signalcaptchas.org/registration/generate.html](https://signalcaptchas.org/registration/generate.html),
   * Résolvez le test demandé ; une page blanche apparaît,
   * Ouvrez la Console du navigateur web, et cherchez un message parlant de `signalcaptcha://<plein de chiffres et de lettres>`,
   * Copiez tous les chiffres et les lettres juste après `signalcaptcha://`,
   * Demandez à nouveau la création du compte, mais cette fois-ci en rajoutant le captcha que vous venez de copier :

     ```
     signal-cli -u +33600000000 register --captcha 'les chiffres et lettres que vous avez copié'
     ```
4. **Vérifiez le compte**. Vous allez recevoir un SMS avec un code de vérification. Notez-le (avec ou sans le tiret, ça n'a pas d'importance), puis entrez cette commande :

   ```
   signal-cli -u +33600000000 verify 000000
   ```

   (en remplaçant `000000` par votre code de vérification).
5. **Créez un profil Signal**. Cela vous permettra de rejoindre les groupes de discussion.

   ```
   signal-cli -u +33600000000 updateProfile --name 'Votre nom ou pseudo'
   ```
6. **Téléchargez [l'appli Signal pour votre ordinateur](https://signal.org/fr/download/)**, et ouvrez-la.
7. L'application va vous demander de **scanner un QR-code**. Prenez une capture d'écran du code, et envoyez-le sur [https://zxing.org](https://zxing.org) (par exemple) pour le décoder. Cela vous donnera un texte qui commence par `sgnl://` ; copiez-le.
8. **Associez l'appli pour ordinateur** à votre compte Signal, en collant le texte décodé depuis le QR-code.

   ```
   signal-cli -u +33600000000 addDevice --uri "sgnl:/…"
   ```

   (N’oubliez pas les guillemets autour du lien.)

Pour moi c'était suffisant : l'appli Signal sur mon ordinateur s'est automatiquement connectée au bout de quelques instants, et m'a permis d'envoyer et de recevoir des messages.

ℹ️ **Pensez à lancer l'appli Signal régulièrement** (ou à la garder ouverte) : au bout de 30 jours sans lancement, l'application sera déconnectée du compte, et ne ne recevra plus les nouveaux messages. Si cela arrive, il parait qu'il faut refaire la procédure avec le QR-code de l'application (l'étape 7).

## Résumé

Si vous cherchez juste les instructions à taper rapidement, voici un résumé des commandes expliquées plus haut :

```shell
# Remplacez par votre n° de téléphone
PHONE='+33600000000'
# Mac
brew install signal-cli
# Linux
# Voir https://github.com/AsamK/signal-cli/wiki/Quickstart
# Allez sur https://signalcaptchas.org/registration/generate.html pour récupérer un captcha
signal-cli -u $PHONE register --captcha <les lettres et chiffres après signalcaptcha://>
signal-cli -u $PHONE verify <code reçu par SMS>
signal-cli -u $PHONE updateProfile --name <votre nom>
signal-cli -u $PHONE addDevice --uri "<lien sgnl:/ dans le qr-code de l’appli pour ordinateur>"
```

*Signal évolue régulièrement : si vous voyez une rectification à apporter à ces instructions, [envoyez-moi un message sur Mastodon](https://mastodon.xyz/@pmorinerie).*
