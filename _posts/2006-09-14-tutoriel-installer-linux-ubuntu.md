---
layout: post
title: "Tutoriel : installer Linux Ubuntu"
date: 2006-09-14 14:11
---

Pour ceux d'entre vous qui, en parallèle des [cours de
C](http://kemenaran.winosx.com/?2006/09/07/37-debuter-en-c-avec-codeblocks),
voudraient installer un système
[Linux](http://fr.wikipedia.org/wiki/Linux) chez eux, voici comment
procéder. Il faut d'abord savoir que Linux est disponible sous la forme
de plusieurs
[distributions](http://fr.wikipedia.org/wiki/Distribution_Linux). Une
distribution contient à la fois le noyau Linux, c'est à dire le coeur du
système d'exploitation, et un ensemble de logiciels sélectionnés pour un
usage précis. Par exemple, certaines distributions, comme [Red
Hat](http://www.redhat.fr/), sont orientées serveur ; d'autres, comme
[Gentoo](http://www.gentoofr.org/), sont d'effrayants systèmes de geek,
où on est supposé construire son système soi-même en partant de rien.

![Logo d'Ubuntu](/images/UbuntuLogoSmall.png)

[Ubuntu](http://ubuntu-fr.org/) est une distribution Linux à la mode, en
développement continu, et relativement facile d'accès pour les
débutants. Elle est basée sur le système
[Debian](http://www.debian.org/index.fr.html), qui est utilisé à l'Isep,
et sur l'environnement graphique [Gnome](http://www.gnomefr.org/). Son
interface polie et sa facilité d'utilisation en font "*ze*" distribution
dans le vent.

Ubuntu est téléchargeable sous la forme d'un
[LiveCD](http://fr.wikipedia.org/wiki/LiveCD). Il s'agit d'un CD-ROM à
insérer au démarrage de votre ordinateur ; un superbe système Linux sera
alors lancé directement depuis le CD, sans rien modifier sur votre
disque dur. Si vous décidez d'installer Ubuntu sur votre disque dur, une
icône sur le Bureau vous permettra de lancer l'assistant
d'installation ; celui-ci vous aidera à accorder un peu de place à
Ubuntu sur votre disque dur, pour le faire cohabiter avec Windows.

Je comptais rédiger un tutoriel complet sur l'installation d'un système
Ubuntu, mais il se trouve que quelqu'un a déjà rédigé un article
extrêmement bien fait à ce sujet. Il explique toutes les étapes de
l'installation point par point, avec nombre de captures d'écran ; le
tout est vraiment orienté vers les débutants, qui à priori n'y
connaissent rien. Voici le lien vers le tutoriel - maintenant amusez
vous \^\^

**[HowTo : Installer Ubuntu pour les
débutants](http://wiki.cerkinfo.be/howto:ubuntu_dapper)**

------------------------------------------------------------------------

*Note :* ce tutoriel est conçu pour l'environnement graphique Gnome, qui
est l'environnement par défaut sélectionné par Ubuntu. Si vous souhaitez
utiliser l'environnement [KDE](http://fr.wikipedia.org/wiki/KDE) (comme
à l'Isep) à la place, vous pouvez soit installer
[Kubuntu](http://www.kubuntu-fr.org/), une distribution dérivée d'Ubuntu
mais basée sur KDE, soit vous renseigner sur la marche à suivre pour
remplacer Gnome par KDE sur une Ubuntu déjà installée.
