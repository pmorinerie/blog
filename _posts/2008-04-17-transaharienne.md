---
layout: post
title: "Transaharienne"
date: 2008-04-17 09:01
---

Je pars ce soir pour une semaine dans le
[Sahara](http://jsaadani.free.fr/) - je posterai sûrement [quelques
photos](http://picasaweb.google.fr/kemenaran/Transaharienne2008Extrait)
au retour.

*PS: Les commentaires sont désactivés pendant la semaine, pour cause de
vague de spam difficile à contenir.*
