---
layout: post
title: "Athènes : à Ellinikó"
date: 2016-09-16 09:24
---

Hier, on est allé trier une demi-journée à l’entrepôt d'Ellinikó.

Ellinikó, c'est l’ancien aéroport d'Athènes – converti en stade il y a quelques années. En ce moment, un grand bâtiment a été de nouveau re-converti, cette fois en entrepôt pour les dons à redistribuer aux réfugiés. D’autres bâtiments sont devenus des camps pour les exilés.

Pour aider la Grèce à faire face à l’afflux de réfugiés, beaucoup de dons sont arrivés, depuis l’Europe entière. Des salles entières de cartons, contenant vêtements, livres, produits d'hygiène…

![Cartons de donations à trier s’empilant à Ellinikó](/images/athens/elliniko.jpg "Où est-ce qu’ils ont rangé l’Arche d’Alliance, déjà…")
Les camps, squats, lieux d’accueil et de soutien aux exilés peuvent alors venir demander, par exemple :

- deux cartons de vêtements pour garçon 5-12 ans,
- trois cartons de vêtements d’hiver femme,
- un carton de pyjamas homme,
- un carton de livres scolaires en grec,
- deux cartons de rasoirs et mousse à raser.

Pratique.

Pour que tout cela fonctionne, il faut trier les dons, et regrouper les affaires similaires dans des cartons clairement étiquetés, à un endroit où on pourra les retrouver.

![Cartons triés par contenu, catégorie, âge…](/images/athens/ellikino-sorted.jpg "Parfois la différence entre « Children tops » et « Boy t-shirts 0-5 » n’est pas évidente – mais au final on s’y retrouve quand même.")

Et donc des volontaires se succèdent tous les jours, pour venir ouvrir, inventorier, trier les donations arrivées en vrac, et ranger des cartons à Ellinikó. Hier, il y avait (entre autres) un groupe d’espagnols, des gens de Grèce, et puis nous, les français du jour ; et aussi deux personnes des États-Unis, en vacance en Grèce et qui ont décidé de venir aider pendant une journée, parce que voilà.

Ça tourne avec trois bouts de ficelles, mais une remarquable organisation. Un bel endroit, finalement.
