---
layout: post
title: "WallpaperSeasonsSwitcher 1.1"
date: 2007-06-22 19:14
---

<div class="langs_tabbox">

<div class="langs_tabs">

[English](#dummy) [Français](#dummy)

</div>

<div class="langs_tabpanel selected" lang="en">

An update for my
[WallpaperSeasonsSwitcher](http://kemenaran.winosx.com/?2007/04/09/65-wallpaperseasonsswitcher).
Here is the changelog :

-   Fixed offset bugs in seasons calculation
-   Fixed startup registration
-   Better console handling

In short, the main new feature is that the program should now work as
intended \^\^ The startup launch is fixed – it came from the handling of
the current directory.  
I also added a new command-line switch, `/console`. By default, nothing
is displayed when WSS is launched, in order to run it on startup without
having a nag black console poping-up for one second. But if you want to
display error messages, the `/console` switch will create a separate
console, where the help text or the errors will be displayed.
Consequently, `wallpaperseasonswitcher_console.exe` is not used anymore,
and has been removed. One program, one executable, let's keep it simple.

**[WallpaperSeasonsSwitcher
1.1](http://www.winosx.com/zoo/WallpaperSeasonsSwitcher/WallpaperSeasonsSwitcher_Release.zip)**  
[Sources](http://www.winosx.com/zoo/WallpaperSeasonsSwitcher/WallpaperSeasonsSwitcher_Sources.zip "WallpaperSeasonsSwitcher 1.0 Sources")

</div>

<div class="langs_tabpanel" lang="fr">

Mise à jour de
[WallpaperSeasonsSwitcher](http://kemenaran.winosx.com/?2007/04/09/65-wallpaperseasonsswitcher)
; voici la liste des changements :

-   Corrigé des erreurs de décalage dans le calcul des saisons
-   Corrigé l'enregistrement au démarrage
-   Meilleure gestion de la console

Concrètement, le principal changement est que, grâce aux corrections de
bogue, le programme devrait maintenant fonctionner correctement \^\^ Le
lancement au démarrage est réparé – il s'agissait d'un problème dans la
gestion du dossier courant.  
J'ai également ajouté une option à la ligne de commande, `/console`. Par
défaut, WSS n'affiche rien sur la sortie standard, ce qui permet de
l'exécuter à chaque démarrage sans écran noir parasite. Toutefois,
lorsque l'on veut afficher les erreurs, l'option `/console` crée une
console au lancement du programme, dans laquelle peuvent s'afficher les
messages d'erreur ou l'aide. En conséquence, le fichier
`wallpaperseasonsswitcher_console.exe` n'est plus nécessaire, et n'est
plus inclus dans la distribution. Un programme, un exécutable, les
choses sont plus propres.

**[WallpaperSeasonsSwitcher
1.1](http://www.winosx.com/zoo/WallpaperSeasonsSwitcher/WallpaperSeasonsSwitcher_Release.zip)**  
[Sources](http://www.winosx.com/zoo/WallpaperSeasonsSwitcher/WallpaperSeasonsSwitcher_Sources.zip "WallpaperSeasonsSwitcher 1.0 Sources")

</div>

</div>