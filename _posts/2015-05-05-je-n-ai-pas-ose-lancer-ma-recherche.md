---
layout: post
title: "Alors j’ai pas osé lancer ma recherche"
date: 2015-05-05 07:42
---

Les boîtes noires du gouvernement, qui surveillent en permanence ce que vous faites sur Internet et signalent
les « comportements suspects », posent de beaucoup de problèmes.

Pour moi le plus grave est que **quand on se sent regardé, on ne fait plus la même chose**. Quand on me regarde,
je ne danse plus de la même façon, je ne fais pas les mêmes grimaces, et je me dis moins par curiosité « au fait,
ça ressemble à quoi un site djihadiste ? »

L’excellente [Klaire](http://www.klaire.fr) l’a parfaitement illustré dans cette vidéo de 3 minutes,
qui parle du danger des boîtes noires pour votre vie privée, et donc vos libertés :

<iframe width="560" height="315" style="margin-bottom:20px" src="https://www.youtube-nocookie.com/embed/gbSpokOYhqI?rel=0" frameborder="0" allowfullscreen></iframe>

Juste après avoir regardé cette vidéo, je me suis souvenu « au fait, je me demandais hier soir les boîtes noires
pouvaient casser le traffic chiffré HTTPS ? » J’ai tapé « boîtes noires ssl »… et je n’ai pas osé lancer
ma recherche.
