---
layout: post
title: "WallpaperSeasonsSwitcher 1.0"
date: 2007-04-09 12:32
---

<div class="langs_tabbox">

<div class="langs_tabs">

[English](#dummy) [Français](#dummy)

</div>

<div class="langs_tabpanel selected" lang="en">

I recently had to set up a new box : install Windows XP, configure it,
and... select a good wallpaper. I chose the [Seasons
wallpapers](http://www.deviantart.com/deviation/24476482/) of KoL, and
started to wonder how I could change the wallpaper automatically,
according to the current season.

Finally I came up with this little tool, **WallpaperSeasonsSwitcher**.
It is a small exe that can check the current season and browse through a
set of season-named wallpapers, to switch them automatically. You can
register the tool to be run at startup - as it is coded in plain pure C,
the startup time overhead will be very small, around 15ms. A small
[readme](http://www.winosx.com/zoo/WallpaperSeasonsSwitcher/ReadMe.txt)
file explains all this, and gives more details about the usage of the
tool.

The soft is Windows-only, but should be easily portable : the sources
are provided, and the OS-specific functions are isolated in a separate
file. It was designer with KoL's wallpapers in mind, but you can use it
for any set of wallpapers.

<p>
**[WallpaperSeasonsSwitcher
1.0](http://www.winosx.com/zoo/WallpaperSeasonsSwitcher/WallpaperSeasonsSwitcher_Release.zip)**  
[Sources](http://www.winosx.com/zoo/WallpaperSeasonsSwitcher/WallpaperSeasonsSwitcher_Sources.zip "WallpaperSeasonsSwitcher 1.0 Sources")

</div>

<div class="langs_tabpanel" lang="fr">

J'ai du récemment configurer un nouvel ordinateur portable : installer
Windows XP, le bricoler un peu, et choisir un fond d'écran correct. Je
suis retombé sur la série de fond d'écran
[Seasons](http://www.deviantart.com/deviation/24476482/), de KoL, et me
suis demandé comment il serait possible de changer le fond d'écran
automatiquement, en fonction de la saison actuelle.

J'ai finalement bricolé un petit utilitaire,
**WallpaperSeasonsSwitcher**. Il s'agit d'un petit exécutable qui peut
détecter la saison courante, et placer automatiquement en fond d'écran
la bonne image parmi un ensemble de fonds d'écran. L'utilitaire peut
être configuré pour se lancer à tous les démarrages - étant codé en pur
C, il ne grève pas trop le temps de boot (environ 15ms). Un petit
fichier
[LisezMoi](http://www.winosx.com/zoo/WallpaperSeasonsSwitcher/ReadMe.txt)
explique tout cela plus en détails, ainsi que la marche à suivre pour
configurer et utiliser le programme.

Cet outil ne fonctionne pour l'instant que sous Windows, mais il devrait
être facilement portable : les sources sont fournies, et les fonctions
spécifiques à l'OS sont isolées dans un fichier spécifique. Il a été
conçu pour les fonds d'écran de KoL, mais est utilisable pour n'importe
quel jeu d'images.

**[WallpaperSeasonsSwitcher
1.0](http://www.winosx.com/zoo/WallpaperSeasonsSwitcher/WallpaperSeasonsSwitcher_Release.zip)**  
[Sources](http://www.winosx.com/zoo/WallpaperSeasonsSwitcher/WallpaperSeasonsSwitcher_Sources.zip "WallpaperSeasonsSwitcher 1.0 Sources")

</div>

</div>
