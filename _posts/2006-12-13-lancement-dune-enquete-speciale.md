---
layout: post
title: "Lancement d'une Enquête spéciale"
date: 2006-12-13 15:52
---

Avez-vous déjà vu le personnage ci-contre ? Si oui, apportez votre
témoignage sur
**[www.enquete-speciale.com](http://www.enquete-speciale.com)** : les
meilleurs seront publiés sur le site.

[![Enquête
spéciale](/images/serialbuzzer/TOC_small.jpg)](http://www.enquete-speciale.com)
