---
layout: post
title: "Pour des prises de décisions ouvertes"
date: 2016-12-16 10:27
---

Je découvre ce bel article de [John Ousterhout](https://web.stanford.edu/~ouster/cgi-bin/home.php) au sujet de son [processus de prise de décisions ouvertes](https://web.stanford.edu/~ouster/cgi-bin/decisions.php) (en anglais) :

> The open decision-making approach includes elements that many people find counterintuitive or contradictory to their "upbringing":
> 
> - Consensus is easier to achieve than you might think.
> - You don't have to control decisions as much as you think.
> - Encouraging controversy early in the process results in better decisions and less controversy later.

C'est précisément le genre d'articles que je cherchais quand, il y a quelques années, chez Capitaine Train, je me demandais comment formaliser notre processus de prise de décisions. Ça ressemblait à quelque chose de beaucoup plus léger – essentiellement basé sur du consensus, parce qu'on est de grandes personnes, qu'on sait discuter avec passion, et malgré tout s'incliner devant une décision qu'on aurait prise différemment.

En comparaison, John Ousterhout propose aussi un moment de vote formel en plus du consensus. Ça ne me semble pas forcément nécessaire pour des petits groupes – mais en même temps ça peut aider à ne pas avoir seulement l'avis des plus grandes gueules.

Il décrit aussi des étapes de décisions sous la forme « _resseré - élargi - reserré_ » : dégrossir le problème avec un petit nombre de personnes, ensuite le plus vite possible récolter plein d'avis très largement, et finalement décider à nouveau en comité restreint.

Au final, l'enjeu est toujours de prendre une bonne décision, rapidement – mais surtout de la faire accepter par ceux qui vont l'implémenter. Tout ira tellement plus vite si les personnes concernées comprennent et acceptent le choix qui est finalement fait ! Et tellement plus efficace que laisser les gens traîner des pieds pendant des mois.
