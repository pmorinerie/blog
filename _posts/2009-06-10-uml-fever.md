---
layout: post
title: "UML Fever"
date: 2009-06-10 21:42
---

En ce moment, je baigne dans les frameworks. J'ai commencé depuis
quelques semaines à coder [Antigone](http://antigone.googlecode.com/)
(souvenez-vous, [je vous en avais déjà
parlé](http://kemenaran.winosx.com/?2008/04/05/117-une-application-avec-flex-i)) ;
le projet devenant trop complexe, j'ai finalement dû partir à la
recherche d'un framework, et de bonnes techniques d'architecture
logicielle. D'où grande piscine plein de design patterns, de découplage
et de diagrammes.

**Bref, je souffre de [fièvre
UML](http://queue.acm.org/detail.cfm?id=984495).** Les cas graves,
quand, en somnolant avant de s'endormir, on se surprend à comater :
« non, je ne dois pas invoquer directement sleep(), il faut que je passe
par mon contrôleur, où est-ce que j'ai mis mon contrôleur, bon sang… Ah,
voilà, controller.sleep(), c'est mieux… »

[![UML Fever
diagram](http://deliveryimages.acm.org/10.1145/990000/984495/bellfig1.jpg)](http://queue.acm.org/detail.cfm?id=984495)

C'est dur. Quand tout cela sera passé, je songerais à vous parler de
[Mate](http://mate.asfusion.com/), et du [délicieux accent
argentin](http://link.brightcove.com/services/player/bcpid1596744118?bctid=1738801386)
de la développeuse qui fait les screencasts. En attendant, renseignez
vous : il paraît que la fièvre UML pandémise parmi les développeurs.
Méfiance.
