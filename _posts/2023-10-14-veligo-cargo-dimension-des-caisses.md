---
layout: post
title: "Véligo cargo : dimensions des caisses"
date: 2023-10-14 22:21
---

En cherchant un siège-enfant qui rentre dans un [Véligo](https://www.veligo-location.fr/) cargo, je me suis rendu compte qu'il était difficile de trouver en ligne les dimensions précises de la caisse de chaque vélo (biporteur ou triporteur).

Voici donc les relevés des dimensions de chaque caisse.

## Véligo biporteur

![Vélo biporteur Véligo](/images/veligo/veligo-biporteur.webp)
![Dimensions de la caisse du vélo biporteur Véligo](/images/veligo/veligo-biporteur-dimensions.png)

## Véligo triporteur

![Vélo triporteur Véligo](/images/veligo/veligo-triporteur.webp)
![Dimensions de la caisse du vélo triporteur Véligo](/images/veligo/veligo-triporteur-dimensions.png)
