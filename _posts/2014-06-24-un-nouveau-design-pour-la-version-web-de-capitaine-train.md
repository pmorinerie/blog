---
layout: post
title: "Un nouveau design pour la version web de Capitaine Train"
date: 2014-06-24 19:33
---

[Capitaine Train](https://www.capitainetrain.com) vient de mettre à jour
son site Web, avec [un nouveau design pour rafraîchir l'apparence du
site](https://blog.capitainetrain.com/6868-nouveau-design-pour-capitaine-train)
qui n'avait pas bougée depuis les premières versions publiques, il y a
trois ans.

[![](/images/capitainetrain/new-frontend.png)](https://blog.capitainetrain.com/6868-nouveau-design-pour-capitaine-train "Plus de détails sur le nouveau design")

Nous avons passé beaucoup de temps avec [Cédric
Raud](https://twitter.com/CedricRaud) à implémenter les détails de cette
nouvelle version. Ce n'est pas qu'un coup de peinture : partout où nous
pouvons, nous avons simplifié, ajusté, rendu l'interface plus claire et
plus évidente.

Au fil du temps nous avions identifié beaucoup d'endroits où un texte
explicatif était nécessaire : la création d'un passager, l'ajout d'une
carte de réduction, l'ajout d'un billet dans le panier… Et nous avons
essayé de rendre plus évidente ces opérations. Parce qu'un utilisateur
ne devrait pas avoir à apprendre à se servir d'un site Web : chaque
utilisation, y compris la première, doit être évidente.

Par exemple, quand vous réservez un billet, une animation permet
d'identifier le billet qui vient juste d'être ajouté au Panier.

![Capitaine Train - Ajouter un billet au
Panier](/images/capitainetrain/adding-to-cart.gif)

Et lorsque vous créez un passager, nous vous proposons en même temps
d'ajouter une carte de réduction ou de fidélité — plutôt que de faire
apparaître un dialogue séparé.

![Capitaine Train - Créer un
passager](/images/capitainetrain/creating-passenger.gif)

Ici et là, des animations indiquent quels sont les éléments affichés, et
d'où ils proviennent. Nous n'avons pas voulu saturer l'interface
d'animations inutiles, qui ralentissent le site pour un simple effet
visuel, mais donner du contexte là où c'est nécessaire.

Cette sortie en grande pompe n'est que le début : maintenant que les
bases sont là, nous allons encore simplifier et améliorer au fur et à
mesure. On a déjà plein d'idées. Merci encore à [Cédric
Raud](https://twitter.com/CedricRaud) et
[Mikael](https://twitter.com/meidenberg), c'est un plaisir de travailler
avec vous.
