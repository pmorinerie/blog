---
layout: post
title: "PCSX2 settings for Macbooks"
date: 2009-09-26 00:11
---

I discovered recently that my **Macbook Pro Unibody** (i.e. mid-2009
models, but anything with a 9600M graphic chip should do) is good enough
to **run PS2 games at decent speed**, provided you use a recent version
of [PCSX2](http://pcsx2.net/). I spent a few days tweaking the
settings - so here are my advices if you want to give it a try.

These settings are tested on a Macbook Pro Unibody 13" - but most of
them should be valid for other architectures. First, some general
configuration settings:

-   Graphics:
    -   GsDX works well - go for it
    -   Use "Native resolution"
    -   Use DirectX 10 to prevent a few minor glitches (like a faint
        grid around the characters). Plus it allows switching
        windowed/fullscreen on the fly.
    -   Use F5 in-game to see which (de-)Interlacing mode works best for
        you
-   Sound:
    -   I didn't played a lot with the settings, standard config will do
-   CPU:
    -   Don't forget to activate the Multithreaded GS : you have a Core
        2 Duo, right ?
-   Speedhacks:
    -   Generaly, you can check all the recommanded speed hacks - and
        disable just the most dangerous ones
    -   "VU cycle stealing" as "Slight" is a good bet - try "Moderate"
        if everything runs well

And now some **Shadow of the Colossus**-specific tricks:

-   Graphics:
    -   You don't have to use any (de-)interlacing mode : the game takes
        care of this itself. There is an in-game setting for Progressive
        Scan, which works well - but I personally thinks that the
        picture looks crisper in Non-progressive (i.e. interlaced mode),
        plus the game will produce a neat motion-blur instead of
        unmatched horizontal lines.
    -   The game will be (very) slightly faster in 4/3 screen ratio
        (this is an in-game setting, btw)
-   CPU:
    -   MicroVU works fine - not really faster, but cleaner, they say.
        Here we go.
    -   "Limit frames" if you want, but don't use frameskip: the display
        flickers, and SotC already skips frames if needed
-   Speedhacks:
    -   SotC loves "x2 cycle rate", and it is a big speedup
    -   You can set "VU cycle stealing" to "Moderate" without major
        glitchs
-   Advanced:
    -   A neat trick for SotC is to set all "Clamp mode"s to "None" :
        big speedup expected

You should be able to play Shadow of the Colossus at somehow-full speed,
that is without slowdowns. The framerate itself is often not very high,
but I think the original game wasn't much better : SotC is beautifull,
but resource-intensive, even on a real PS2.

I also tried a CD of Final Fantasy XII, which runs even better : it is
probably less resources-demanding. PCSX2 compatibility is very good
these days, so give a try to every PS2 game you own...
