---
layout: post
title: "Plugin OpenDocument pour Office"
date: 2007-03-02 09:35
---

[Sun](http://fr.sun.com/) a développé un plugin pour Word permettant de
lire les documents OpenOffice au format
<acronym title="Open Document Format">ODF</acronym>. Ce plugin
fonctionne pour Word XP/2003/2007, et est disponible gratuitement sur
Sourceforge.

**[Plugin ODF pour Word](http://odf-converter.sourceforge.net/)**Pour
l'instant, seuls les documents "Writer" sont gérés, mais le support des
diaporamas et des tableurs est prévu pour la sortie de la version finale
du plugin, en avril.
