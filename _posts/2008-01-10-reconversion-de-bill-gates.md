---
layout: post
title: "Reconversion de Bill Gates"
date: 2008-01-10 11:42
---

Bill Gates a récemment annoncé son départ de Microsoft, pour se
consacrer uniquement à la fondation humanitaire Bill & Melissa Gates.
Lors de son dernier Keynote, il y a deux jours, il a présenté une vidéo
de 6 minute évoquant sa reconversion — et les pistes envisagées sont
plutôt audacieuses... Featuring Bill Gates, Steve Ballmer, Bono, Georges
Clooney, Al Gore, et plein de gens.

<div style="text-align: center">

<object width="425" height="355">
<param name="movie" value="http://www.youtube.com/v/1lE21kpE3M0&amp;rel=1"></param><param name="wmode" value="transparent"></param>
<embed src="http://www.youtube.com/v/1lE21kpE3M0&amp;rel=1" type="application/x-shockwave-flash" wmode="transparent" width="425" height="355">
</embed>
</object>

</div>

[Vidéo sous-titrée en français sur
Koreus](http://www.koreus.com/video/dernier-jour-bill-gates.html)
