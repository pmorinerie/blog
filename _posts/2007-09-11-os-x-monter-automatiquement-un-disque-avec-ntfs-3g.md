---
layout: post
title: "OS X : monter automatiquement un disque avec ntfs-3g"
date: 2007-09-11 17:32
---

![Boot Camp drive
icon](/images/divers/boot_camp_drive_icon.jpg)L'installation de
ntfs-3g sous Mac OS X est relativement simple (et succinctement décrite
dans un tutoriel précédent), mais il reste un hic : au démarrage de Mac
OS X, les disques NTFS sont montés avec le pilote par défaut, qui ne
permet que la lecture - il faut alors démonter chaque disque NTFS, puis
le remonter avec ntfs-3g. C'est un peu pénible, et certainement pas
optimal.

Il existe cependant un petit hack permettant de monter automatiquement
les disques ntfs-3g au démarrage : il s'agit en résumé de remplacer le
binaire de montage par défaut de Mac OS X par un petit script effectuant
la même opération, mais en utilisant ntfs-3g. Notez toutefois que cette
manœuvre est risquée et potentiellement dangereuse - soyez avertis et
prudents !

Assurez-vous avant de commencer de disposer de l'utilitaire `ntfsprog`,
en plus de ntfs-3g, qui est normalement inclus dans le package de Daniel
Johnson. Ensuite, ouvrez un éditeur de texte, et copiez-y le script
ci-dessous :


> `#!/bin/sh`  
> `VOLNAME=${2%/}`  
> `VOLNAME=${VOLNAME##*/}`  
> `OPTS="-o default_permissions,locale=fr_FR,volname=$VOLNAME"`  
> `/usr/local/bin/ntfs-3g $@ $OPTS`

Enregistrez ce fichier sur le Bureau, en texte brut, sous le nom
"mount\_ntfs", sans extension. Ouvrez ensuite un Terminal, et entrez les
commandes suivantes :

> `sudo mv /sbin/mount_ntfs /sbin/mount_ntfs.old`  
> `sudo cp mount_ntfs /sbin`  
> `sudo chown root:wheel /sbin/mount_ntfs`  
> `sudo chmod 555 /sbin/mount_ntfs`

Redémarrez ensuite, et vérifiez que vos disques NTFS sont maintenant
bien accessibles en écriture. En cas de problèmes, renommez le fichier
`/sbin/mount_ntfs.old` en `/sbin/mount_ntfs` pour restaurer le montage
original des disques.

Cette astuce provient directement d'[un sujet du forum de support
ntfs-3g](http://forum.ntfs-3g.org/viewtopic.php?t=572), n'hésitez pas à
aller y faire un tour - notemment pour trouver une version du script
plus évoluée, qui supporte les espaces dans les noms des volumes NTFS.
