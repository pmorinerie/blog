---
layout: post
title: "Le single de la semaine sur iTunes"
date: 2010-04-18 20:14
---

En écoutant la radio, je suis tombé sur un extrait d'une **reprise de
"Le vent nous portera"** par **Sophie Hunger**, une chanteuse zurichoise
que je ne connaissais pas. Puissance de la musique en ligne : j'ai
cherché les différentes versions de cette chanson sur Deezer (pour être
certain du nom de l'artiste), puis j'ai trouvé [une excellente version
live](http://www.youtube.com/watch?v=AyUp1rnv7rY) sur Youtube. Allez
hop, on lance emule pour voir s'il n'y a pas moyen de la télécharger…

Et puis je me suis dit que si j'aimais bien ce qu'elle chantait, je
pouvais aussi bien acheter le morceau sur iTunes — ça ne coûte pas si
cher, après tout. Recherche sur l'iTunes Store, c'est bien elle, c'est
son album, je clique sur la piste… [Une page
s'affiche](http://itunes.apple.com/fr/album/le-vent-nous-portera-single/id366900453) :
"Le vent nous portera - Single de la semaine sur iTunes, à télécharger
gratuitement !". Hébin nickel !

Coïncidence marrante — et puis comme quoi ça vaut le coup de chercher à
acheter, parfois.
