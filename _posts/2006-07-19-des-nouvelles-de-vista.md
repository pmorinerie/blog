---
layout: post
title: "Des nouvelles de Vista"
date: 2006-07-19 09:29
---

Beaucoup de personnes se sont plaint de la lenteur de la [Bêta 2 de
Windows
Vista](http://www.pcinpact.com/actu/news/29296-Windows-Vista-est-disponible-en-Beta-2-publi.htm) ;
le système n'était en effet vraiment pas réactif. Curieusement, [la
build suivante](http://www.osnews.com/story.php?news_id=15010) était
nettement plus rapide, comme si Microsoft passait enfin à la phase
"optimisations".Avant-hier, Microsoft a publié la [permière build pré-RC
de Windows Vista](http://neosmart.net/blog/archives/223), centrée elle
aussi sur l'optimisation. D'après les testeurs, les performances se sont
encore améliorées, tout en gardant le volume de code compilé à un niveau
raisonnable (environ 100 Mo de plus). Il semble donc que la Bêta 2, que
beaucoup de testeurs ont - à juste titre - décrié pour sa lenteur et son
poids, ne soit bien qu'une étape de la conception du produit et non un
aperçu des performances finales. Ouf.

Pendant ce temps là, Bill Gates annonce que “il y a 80 pour cent de
chances pour que Vista sorte à la date prévue”, c'est à dire en janvier
2007. Il ajoute également qu'il reportera sans hésitations cette date si
le système n'est pas parfaitement prêt.

Bref, des nouvelles plutôt bonnes pour Windows Vista... sauf une :
[Symantec](http://www.symantec.com/region/fr/) note que la pile réseau
ayant été entièrement réécrite pour Vista, “de grandes quantités de code
testé et certifié ont été remplacées par du code frais”, comportant
encore de nombreuses failles de sécurités (potentielles). Il faut
espérer qu'un effort sera fait sur ce point particulièrement critique
d'un système d'exploitation.
