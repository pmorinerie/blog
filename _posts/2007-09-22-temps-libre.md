---
layout: post
title: "Temps libre"
date: 2007-09-22 10:33
---

Ajouté depuis quelques jours à mon agrégateur RSS, le [blog de
Boulet](http://www.bouletcorp.com/blog/). J'ai lu une bonne partie des
archives hier, et décidément, certaines planches sont d'anthologie (Cela
dit je devrais arrêter : se mordre les lèvres de rire derrière mon
écran, ça ne sied pas au cours de comptabilité de gestion. Du tout.)
