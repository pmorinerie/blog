---
layout: post
title: "L'histoire de la chaîne &quot;User-agent&quot;"
date: 2008-09-10 21:10
---

L'User-agent, c'est ce bout de texte qu'envoient les navigateurs Web
lors que chaque requête HTTP, pour s'identifier. C'est généralement
assez embrouillé, mais heureusement, WebAIM est là pour nous raconter
pourquoi en rigolant un peu :)

**[WebAIM Blog - History of the browser user-agent
string](http://www.webaim.org/blog/user-agent-string-history/)**
