---
layout: post
title: "Minimalist searchbox for Firefox"
date: 2010-12-23 14:36
---

As you know, I love how the Firefox UI can be customized with only a few
lines of CSS. This time, I wanted to simplify the Search box of Firefox
4 — because I don't think we need a bright search-engine icon, and the
useless magnifying glass button. Here is the result:  
  
  
![Firefox 4 minimalist search bar — with a few lines in
userchrome.css](/images/screenshots/firefox-searchbar-minimalist.png)  
  
Compare with the original theme:  
  
  
![Firefox 4 searchbar using the default
theme](/images/screenshots/firefox-searchbar-vanilla.png)  
  
And here are the few lines you can add to your
[userChrome.css](http://lifehacker.com/197715/customize-firefox-with-userchromecss)
to achieve this result:

``` css
/* Remove the magnifying glass icon on the right of the searchbox */
.search-go-container { display: none; }
.searchbar-textbox .textbox-input-box { padding-right: 7px !important; }

/* Replace the active search engine icon by a magnifying glass */
.searchbar-engine-image { display: none; }
.searchbar-engine-button {
  padding-left: 16px !important;
  background: url("chrome://browser/skin/Search.png") 4px 4px no-repeat;
}
```

Tested with Firefox 4.0b8 — but should work with Firefox 3.6 too.
