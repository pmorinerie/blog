---
layout: post
title: "Réformes et technoscience"
date: 2009-07-30 18:07
---

Vous savez sans doute qu'on parle beaucoup en ce moment de réformer la
recherche française. À la clef, un débat pas vraiment posé sur la
recherche fondamentale et la recherche appliquée.

Un [bel article de Jean-Marc
l'Hermite](http://www.sauvonslarecherche.fr/spip.php?article2860) pose
le problème, et rappelle les fondements historiques et actuels des
différentes conceptions de la recherche.

“Deux versions s’opposent ici clairement : les uns soutiennent qu’en
recherchant des solutions à des problèmes pratiques, tels que construire
un bateau rapide, produire un yaourt à longue conservation ou aller sur
la lune, on découvrira forcément, de façon fortuite, certains secrets de
la nature. Les autres prétendent au contraire que l’on doit mener une
recherche fondamentale sans autre but que d‘explorer l‘inconnu, et que,
fortuitement mais inévitablement, de ces découvertes découleront plus ou
moins directement le moyen de construire un bateau rapide, produire un
yaourt à longue conservation ou aller sur la lune. Ces deux points de
vue se sont affrontés lors des débats qui ont précédé la création du
CNRS en 1939.”

Si vous avez un peu de temps, ça vaut le coup d'y jeter un œil.

**[SLR - Le Technoprimat - Jean-Marc
l'Hermite](http://www.sauvonslarecherche.fr/spip.php?article2860)**
