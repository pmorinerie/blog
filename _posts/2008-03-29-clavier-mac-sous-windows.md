---
layout: post
title: "Clavier Mac sous Windows"
date: 2008-03-29 13:44
---

En essayant de corriger le mapping de mon clavier sous Parallels, j'ai
dû chercher comment faire fonctionner toutes les touches d'un clavier
Mac sous Windows — que le clavier Mac soit intégré, USB ou virtualisé.

J'ai finalement retrouvé la configuration à installer pour parvenir à un
résultat parfait. Il s'agit d'un mapping créé par [Florent
Pillet](http://www.florentpillet.com/), qui s'installe facilement, et
s'active en suivant les instructions fournies. Et joie, Windows
reconnaît maintenant correctement toutes les touches du clavier :
l'arobase, les accolades, tout fonctionne. Si vous avez des pépins
d'interaction Mac/Windows, vous savez donc comment vous y prendre :)
