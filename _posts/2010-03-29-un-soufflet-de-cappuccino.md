---
layout: post
title: "Un soufflet de Cappuccino"
date: 2010-03-29 21:49
---

En ce moment, [Luc](http://twitter.com/brindavoine) et moi nous amusons
avec [Cappuccino](http://cappuccino.org/) (enfin je m'amuse — lui bosse
vraiment dessus). Ceux qui me connaissent le savent bien : Cappuccino
est un de mes dada, une vraie chouette technologie qui permet de faire
des applications Web bluffantes.

Dernière démo en date qui m'est tombé dessus :
**[almost.at](http://almost.at/)**. Un service encore en bêta,
apparemment, qui agrège différentes sources de données sur les
événements récents (Twitters, flux vidéos, sites d'actualités, etc.)

Techniquement, c'est la grosse claque. Le layout est robuste malgré sa
complexité. La timeline en bas est probablement dessinée à la main avec
les API smili-Quartz de Cappuccino. Les données se filtrent et se
mettent à jour en temps réel.

Maintenant, rappelez-vous : ceci est une appli Web ; à l'intérieur, très
profond, il n'y a rien du Javascript et du CSS. Et pourtant, on fait du
tracé custom, du relayout à la volée — oh, et aussi, normalement c'est
compatible IE6. Bref, la tuerie. Cappuccino, c'est le bien.
