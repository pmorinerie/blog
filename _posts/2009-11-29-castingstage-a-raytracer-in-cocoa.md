---
layout: post
title: "CastingStage: a raytracer in Cocoa"
date: 2009-11-29 16:13
---

Here is the first release of
**[CastingStage](http://code.google.com/p/castingstage/)**, that was
first [previewed
here](http://kemenaran.winosx.com/?2009/09/28/156-teaser). It is a
simple **raytracing engine** made using Cocoa. It intends to be lean and
mean, easy to use, and to showcase several Mac OS X technologies, such
as bindings, CoreData, Grand Central Dispatch, and so on.

[![CastingStage cocoa raytracer
preview](/images/screenshots/castingstage_raytracer_preview_small.jpg)](/images/screenshots/castingstage_raytracer_preview.png)

Here are the currently supported features :

-   Basic primitives handling (spheres and planes)
-   Reflection and refraction
-   Point and area lights
-   Depth-of-field effect
-   Quick preview of scenes
-   Anti-aliasing
-   Document-based application: save and open scenes
-   Export rendering in various formats
-   Demonstration scenes included

You can view some
[screenshots](http://code.google.com/p/castingstage/wiki/Screenshots) or
[download the
application](http://code.google.com/p/castingstage/downloads/list)
(requires Mac OS 10.6 for now, Leopard support coming soon). And the
[code](http://code.google.com/p/castingstage/source/browse/#svn/trunk/CastingStage)
is under the MIT license — so feel free to play with it !
