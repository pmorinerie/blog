---
layout: post
title: "Début des formations PHP"
date: 2007-02-28 22:20
---

Avis aux isépiens : la formation "PHP avancé" commencera demain **jeudi
1 mars**, à 13h, dans la salle 14 du bâtiment Branly. Les thèmes abordés
seront :

-   PHP Avancé : Programmation Orientée Objet, couches d'accès aux
    données
-   Introduction aux frameworks, présentation du modèle
    [MVC](http://fr.wikipedia.org/wiki/Mod%C3%A8le-Vue-Contr%C3%B4leur),
    conception d'architectures
-   Découverte et utilisation du framework
    [CakePHP](http://www.cakephp.org/), utilisation du scaffolding,
-   Si possible : introduction à l'eXtreme Programming et aux tests
    unitaires

La formation durera trois fois deux heures, mais contrairement à ce que
peut laisser suggérer le plan, la majeure partie du temps sera consacrée
à CakePHP : nous verrons comment créer un blog basique, puis parcourront
les différentes fonctionnalités de Cake en recréant un site Mobitroc à
partir de zéro.

La première partie du support de cours est déjà disponible sur
SlideShare - ainsi que celui de la formation "PHP débutant".  
**[Formation PHP avancée -
CakePHP](http://www.slideshare.net/kemenaran/formation-php-avanc-cake-php/)**  
[![Formation PHP
SlideShare](/images/Formation%20PHP/formation-php_t.jpg)](http://www.slideshare.net/kemenaran/formation-php-avanc-cake-php/)

**[Formation PHP
débutant](http://www.slideshare.net/kemenaran/formation-php/)**  
[![Formation PHP Avancé CakePHP
SlideShare](/images/Formation%20PHP/formation-php-avance_t.jpg)](http://www.slideshare.net/kemenaran/formation-php/)
