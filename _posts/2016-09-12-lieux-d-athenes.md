---
layout: post
title: "Lieux d’Athènes"
date: 2016-09-18 12:47
---

Vous avez 16 ou 18 ans – ou peut-être 40, ou peut-être 5. Vous vous retrouvez en Grèce – parce que c’est la guerre, que votre ville a été détruite, ou parce que les talibans ont menacé de vous tuer comme ils ont tués votre père.

## Que faire ? Où aller ?

Si vous êtes en Grèce, il de fortes chances que vous soyez arrivé dans un bateau précaire, depuis la Turquie. Vous avez eu de la chance, vous ne faites pas partie des noyés.

Vous avez probablement débarqué sur l’île grecque de Lesbos. Jusqu’il y a quelques mois, vous pouviez rejoindre le continent. Mais depuis l’accord avec la Turquie, c’est terminé : à votre arrivée sur le rivage grec, avec plus ou moins d’eau dans les poumons, vous serez enfermé dans un camp, derrière des barbelés – le temps que votre demande d’asile soit examinée. À terme vous serez sans doute renvoyé vers la Turquie. Perdu.

Mais vous êtes arrivé avant l’entrée en vigueur de l’accord, ou vous êtes passé par une autre voie. Encadré par une association, le <acronym title="Haut Commissariat aux Réfugiés">HCR</acronym>, _Médecins sans Frontières_ ou autre, vous prenez un bateau pour le continent, et débarquez dans un camp de réfugiés autour d’Athènes.

Vous n’avez pas vraiment envie de rester ici. Votre but est sans doute de rejoindre votre mari en Autriche, ou votre compagnon en Suède, ou des membres de votre famille au Royaume-Uni. Et d’y refaire votre vie quelques années, en attendant des jours meilleurs au pays.

Mais voilà, depuis février la frontière est fermée. À Idomeni, au point de passage avec le reste de l’Europe, [on ne passe plus](http://www.leparisien.fr/international/grece-une-centaine-de-migrants-gazes-par-la-police-a-idomeni-13-04-2016-5711425.php "Le Parisien : Au moins 260 migrants gazés et blessés par des policiers à Idomeni"). Autant pour la [libre circulation des personnes](https://fr.wikipedia.org/wiki/Quatre_libertés_(Union_européenne) "Wikipédia : Les quatre libertés de l’Union Européenne") (celle des capitaux se porte bien, rassurez-vous).

Alors vous êtes coincé en Grèce, avec 60 000 autres personnes.

[![Street-art: Fences can’t hold giants](/images/athens/fences-cant-hold-giants-thumbnail.jpg "Street-art à proximité du squat de la Fifth School.")](/images/athens/fences-cant-hold-giants.jpg)

## Des camps

Au début, vous ne savez pas trop. Mais au fur et à mesure, vous découvrez qu’il existe plusieurs sortes de camps. Certains sont gérés par une ONG spécifique, ou par plusieurs. Ici on dort sous tente, là dans des conteneurs reconvertis. Ici il des distributions de repas quotidiens, probablement des pâtes ou du riz, tous les jours. Là-bas on fournit de nourriture à cuisiner, et là-bas encore il n’y a rien. Vous êtes peut-être [à Ellinikó](/posts/a-elliniko "Kzone : Athènes : à Ellinikó"), dans un des grands stades reconverti en camp ; vous dormez peut-être dans l’ancien aérogare, ou simplement dehors dans les tribunes. Dans un camp officiel, ou dans un camp officieux.

Il y a les camps gérés par l'armée. C’est plutôt le génie civil, celui qui d’habitude transporte des chars. Maintenant ils se retrouvent à gérer un camp de plusieurs milliers de personnes, sans grand enthousiasme. Alors ils laissent les choses se passer – et tant pis si des durs cherchent à prendre la main sur le camp, les militaires regardent ailleurs.

## Des squats

Au bout d’un moment, vous apprenez que, dans le centre d’Athènes, des squats accueillent aussi des réfugiés. Il y a _City Plaza_, un ancien hotel, abandonné puis transformé en squat. Ils vous accueillent chez eux. Un comité d’athéniens organise la logistique et la vie collective. Ils coordonnent les volontaires de passage qui filent un coup de main, et les représentants des résidents désignés pour faire le dialogue avec les familles hébergées. Un hôtel, c’est pratique pour héberger, et les gens là-bas communiquent bien : ils reçoivent généralement assez de dons pour faire tourner la boutique.

Sinon il y a _Notara 26_, un squat ouvert dans l’ancien Ministère du travail. À Notara vous n'êtes pas chez eux, vous êtes chez vous. C’est votre bâtiment – et il sera ce que vous en faite, du ménage aux activités. C’est ce squat qui a été [incendié par des néo-nazis](/posts/athenes-premiers-jours/ "Kzone : Athènes, premiers jours") il y a quelques semaines – et qui a été remis en état par des volontaires en un temps record.

C’est fou le nombre de bâtiments fermés qu’on peut voir en se promenant dans le centre d’Athènes. Des hôtels, des maisons, des magasins, des écoles… C'est assez impressionnant. La crise et les « réformes » imposées par la troïka sont passées par là. Alors certaines anciennes écoles ont été récupérées. La _Fifth School_ héberge aujourd’hui près de 400 personnes. La _Second School_, près de 200, des familles syriennes pour la plupart. Les enfants y jouent dans la cour ; des volontaires se relaient pour filer un coup de main. Ce soir on va y projeter un film, doublé en arabe et sous-titré en farsi.

## Ailleurs encore

Peut-être avez-vous dû quitter votre pays parce que vous êtes gay, dans un pays où on emprisonne pour ça. Ou peut-être êtes-vous seule avec vos enfants. Dans ce cas il y a à Athènes des structures pour personnes vulnérables, comme la _Orange House_. Plus petite, plus confortable qu’un camp, plus stricte qu’un squat, soutenues par des associations dans une maison louée.

Bien sûr il faut être au courant de l’existence de tous ces endroits, et pouvoir s’y rendre, et accepter la promiscuité dans ces lieux. Sinon, un an sous la tente, ça vous dit ? La solidarité des grecs est incroyable, et les projets sont nombreux. Mais malgré tout cette solidarité ne couvre que les besoins primaires – et encore, il n’y a pas de place pour tout le monde. Pendant ce temps, les rêves et les espoirs des exilés sont en attente.

Tout ça parce que la frontière est fermée.
