---
layout: post
title: "CSS avancé dans un site de production"
date: 2006-05-11 18:22
---

En utilisant un excellent tutoriel sur les [Image
Map](http://www.cssplay.co.uk/articles/imagemap/index.html) de [Stu
Nicholls](http://www.cssplay.co.uk), dont j'ai déjà parlé [dans ces
colonnes](http://kemenaran.winosx.com/?2006/05/07/9-menu-deroulant-en-css-uniquement),
j'ai implémenté un site composé uniquement d'images et de zones
réactives sur celles-ci, avec apparition de texte au survol, le tout en
pur XHTML/CSS. Le tutoriel permet de ne pas se perdre et d'être sûr de
fonctionner sous Internet Explorer.

Mais un autre point du cahier des charges était l'apparition de blocs de
texte lors du **clic**, en plus du survol. Une solution est de faire un
lien vers une page quasiment identique, mais comportant le bloc de
texte ; ce n'est pas très élégant. J'ai donc utilisé les pseudo-classes
CSS *:active* et *:focus* sur les zones réactives pour faire apparaître
les zones de texte lorsque que les liens ont le focus - c'est à dire
qu'ils ont été cliqués ou activé. La contrainte sur le XHTML est que
tout le contenu texte du bloc à faire apparaître doit être contenu dans
le lien réactif, il faut donc n'utiliser que des éléments inline. Mais
au final, l'effet obtenu est assez réussi, le code élégant et surtout
léger.

Quelques problèmes rencontrés néanmoins : les utilisateurs d'Opéra
doivent maintenir le bouton de la souris appuyé pour voir le texte, à
cause d'une bizarrerie de comportement de ce navigateur qui prend très
mal en compte les liens actifs ; à noter également que les navigateurs
Gecko 1.7 (comme Firefox 1.0\*) n'affichent pas le texte lorsque l'on
clique sur le texte de la zone réactive, il faut cliquer dans la zone
mais pas sur le texte - ceci a été résolu dans Gecko 1.8 (et donc
Firefox 1.5\*). Tous ces comportements étranges peuvent sans doute être
corrigés à l'aide d'un brin de Javascript non-obtrusif. **Edit:** C'est
même un peu plus qu'un brin, mais le Javascript a été rajouté ; il
dégrade sur la version "full CSS" si JS n'est pas activé.

Vous pouvez regarder le produit fini sur
[pourquoitucours.fr](http://www.pourquoitucours.fr). Je requiers
néanmoins votre indulgence car le site a été réalisé en très, *très* peu
de temps ;)

**Edit:** Le site a depuis été remplacé par une autre version, réalisée
par un autre développeur. Le lien donné n'a plus rien à voir avec moi,
donc.
