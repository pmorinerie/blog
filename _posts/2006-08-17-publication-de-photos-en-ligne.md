---
layout: post
title: "Publication de photos en ligne"
date: 2006-08-17 19:54
---

Rentrant de vacances, j'ai cherché un site web où mettre en ligne
quelques photos. Comme tout le monde, en googlant un peu, je suis
rapidement tombé sur ce guide de l'Internaute, qui est un
[comparatif](http://www.linternaute.com/guides/categorie/101/index.shtml)
des différentes solutions et services existants ; cela permet déjà de se
faire une idée.

Suite à la lecture de ce guide, j'ai tenté ma chance du côté de
[Wistiti.fr](http://www.wistiti.fr/), qui fait à la fois de
l'hébergement de photos et du tirage d'albums. L'inscription est rapide,
l'interface jolie, la mise en ligne des photos simple (à l'aide d'une
applet Java bien fichue), la gestion des albums et des privilèges de
publication agréable, et on n'a pas besoin de s'inscrire pour consulter
les photos mises en ligne. Seul bémol : la qualité des photos n'est pas
géniale ; même en taille maximum des artefacts de compression restent
très visibles.

Peu après, je suis tombé par hasard sur la bêta de [Picasa Web
Album](http://picasaweb.google.com/). J'ai toujours entendu dire
beaucoup de bien de [Picasa](http://picasa.google.com/), donc pourquoi
ne pas y jeter un oeil. Comme souvent avec les services Google en bêta,
une inscription au programme est nécessaire, mais j'ai reçu mon
invitation à tester Picasa Web Album instantanément - il ne doit pas y
avoir foule. Bref, Picasa s'installe tranquillement (ils utilisent
[NSIS](http://nsis.sourceforge.net/), les braves gens !), m'importe mes
photos, et me propose en un clic de publier mes photos sur le Web. Après
un bref temps d'upload, on me demande si l'Album est public ou privé,
puis on me fournit le lien vers la galerie créée.

Cette galerie en ligne est ma foi fort sympa ; l'interface est sobre,
mais plus dans le style de Picasa que des interfaces Google ; la
navigation parmis les photos est aisée. Au niveau fonctionnalités, c'est
comparable à Wistiti, mais avec un coté plus poli. Et bien sûr, la
qualité des photos est au top, aucun problème là dessus.

Conclusion : [Wistiti.fr](http://www.wistiti.fr/) est agréable, mais
pêche un peu par la trop faible qualité de ses images ; [Picasa Web
Album](http://picasaweb.google.com/) lui est égal dans tous les
domaines, et conserve une bonne qualité d'image. Le point à Picasa,
balle au centre.
