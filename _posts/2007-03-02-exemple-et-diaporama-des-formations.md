---
layout: post
title: "Exemple et diaporama des formations"
date: 2007-03-02 09:31
---

Voici les liens vers les exemples de la première partie de la formation
"PHP avancé".

Certains d'entre vous m'ont également demandé les sources originales des
diaporamas des supports de cours, en plus de la version [disponible en
ligne](http://kemenaran.winosx.com/?2007/02/28/60-debut-des-formations-php)
sur SlideShare. Je les ai donc également mis en ligne. Ils sont au
format OpenDocument, lisible par OpenOffice (ou bientôt par Office,
lorsque la version finale du [plugin ODF de
Sun](http://kemenaran.winosx.com/?2007/03/02/62-plugin-opendocument-pour-office)
sera sortie).

**[Formation PHP débutant - diaporama
ODF](http://www.winosx.com/hosted_files/Formation%20PHP.odp)**  
[![Formation
PHP](/images/Formation%20PHP/formation-php_t.jpg)](http://www.winosx.com/hosted_files/Formation%20PHP.odp)

**[Formation PHP avancé / CakePHP - diaporama
ODF](http://www.winosx.com/hosted_files/Formation%20PHP%20avancé.odp)**  
[![Formation PHP Avancé
CakePHP](/images/Formation%20PHP/formation-php-avance_t.jpg)](http://www.winosx.com/hosted_files/Formation%20PHP%20avancé.odp)

  
**[Formation PHP avancé / Cake PHP - exemples, TD et TP - partie
1](http://www.winosx.com/hosted_files/PHP%20Avance%20-%20Exercices%20td%20et%20tp%20POO.zip)**

**Edit:** Lien vers le fichier d'exemples réparé.
