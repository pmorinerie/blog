---
layout: post
title: "La Zone du dehors et le management par objectifs"
date: 2020-10-22 10:40
---

Il y a quelques mois, j'ai fini _La zone du dehors_ d'Alain Damasio. J'en suis ressorti avec des impressions mitigées, qui ont fini par se décanter. Pour une bonne partie, j'ai eu l'impression que la narration avait du mal à savoir où elle allait, que les séquences se juxtaposent dans une certaine confusion thématique – et que même si j'ai du mal avec les arcs narratifs lisses et stéréotypés, un peu de cohérence ne fait pas de mal non plus.

Mais y'a quelques points qui m'ont bien intéressés. En vrac :

## Le vitalisme

Une des choses que La zone du dehors a clarifié pour moi, c'est que Damasio me semble attaché moins à l'anarchisme qu'à un genre de vitalisme. "Oui, tout foisonne, tout bouillonne, ça pose plein de problèmes mais c'est génial".

J'ai l'impression que cette appréciation du vitalisme se trouve aussi dans _Les furtifs_, et dans une moindre mesure dans _La horde du Contrevent_ – mais aussi beaucoup dans ce que Damasio a pu écrire sur la zad de Notre-Dame des Landes il y a quelques années.

Et ça m'aide à comprendre pourquoi tout ne me parle pas dans les récits de Damasio : le vitalisme c'est sa came à lui, et pas trop la mienne. Et que y'a aussi d'autres choses que ça dans l'anarchisme (genre des courants qui sont plus "l'ordre moins le pouvoir"). Et que c'est bien aussi.

## Le patriarcat

Ouch. La zone du dehors, en terme de féminisme, c'est tendu. Et j'ai l'impression que c'est un point aveugle des écrits de Damasio depuis longtemps.

Je renvoie ici à la série d'articles (encore en cours) de Mélissa et [Lunar](https://mastodon.potager.org/@lunar), qui ont fait une analyse des Furtifs sous ce prisme, avec plein de dataviz : [https://dérivation.fr/furtifs/](https://dérivation.fr/furtifs/).

## La société de l'évaluation

Les passages sur la société de l'évaluation m'ont bien parlé. En bonne partie parce que pour moi ça donnait du sens à ce qui se passe dans le néo-management qui se retrouve fréquemment dans la startup-nation.

Vous savez, c'est cette forme de management à la cool où l'évaluation est omni-présente. Toutes les deux semaines, un one-to-one avec un manager. Toutes les six semaines, faire une liste d'objectifs hyper-précis à atteindre – et définissez des cibles chiffrées. Tous les six mois, une "review à 360°" où vous êtes évaluées par des managers et des pairs – dans l'espoir d'obtenir une augmentation qui ne viendra jamais. Et en permanence, devoir participer aux nombreuses évaluations de vos collègues. Mais c'est cool, parce que c'est vous-même qui définissez vos objectifs, tout ça.

Évidemment, tout ça ressemble fortement au Clastre, le grand système de classement social général de La zone du dehors. Et ce qu'en dit Damasio dans la grande tirade deleuzo-foucaldienne, c'est que l'objectif de tout ça est de re-former la personnalité des individus.

Il s'agit bien sûr d'une part les individualiser (chacun essaie de suivre ses objectifs le nez dans son guidon, ça atomise), mais même au sein de l'individu, de découper chaque individu en compétences, en traits de caractères désirables, de diviser l'individu lui-même. Et ensuite l'évaluation des traits permet de dissoudre en partie l'individu, et à la place d'encourager les traits désirables par l'entreprise / l'État / etc.

Au même moment, dans ma vie, j'avais eu vent de discussion dans une startup de la fintech française où des managers commentaient des traits de personnalité. Par exemple « Finalement, toi tu sais défendre tes idées, mais tu respectes aussi l'autorité quand il faut », des choses de ce genre.

Mais en fait c'est des discours complètement performatifs : ça sert pas à remarquer ce qui est bien, mais à dégager un trait, et à dire aux individus « Ça serait bien que tu cultives ça, c'est valorisé par le management, ça t'aidera à avoir une augmentation aux prochains 6 mois. »

Bref, cette idée de "Le micro-management (même auto-organisé) sert à non seulement individualiser, mais séparer les individus eux mêmes en plusieurs traits, et à les modeler" ça m'a semblé éclairant.
