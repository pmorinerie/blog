---
layout: post
title: "Le futur de Gecko"
date: 2006-12-15 20:04
---

Ceux d'entre vous intéressés par le développement de Gecko 1.9 (le
moteur de rendu qui sera utilisé dans Firefox 3), ne manqueront pas
cette brève de Stuart Parmenter, qui a mis en ligne les diapos d'une
récente conférence au Japon. Un excellent résumé de ce que permettra le
moteur de Firefox 3.

**[Pavlov.net : Return from
Japan](http://www.pavlov.net/blog/archives/2006/12/return_from_jap.html)**
