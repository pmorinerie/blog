---
layout: post
title: "Ondes électromagnétiques"
date: 2010-01-21 10:41
---

Lu aujourd'hui dans 20 minutes, un entretien avec Chantal Jouanno, chef
de file UMP à Paris pour les élections régionales :

> Je veux créer des « ambulances vertes », des conseillers en
> environnement intérieur qui se déplaceraient chez les particuliers
> pour les conseiller. Lorsque vous avez un enfant allergique **ou que
> vous êtes sensible aux ondes électromagnétiques**, par exemple.

Ah oui. Un élu qui nous parle de ça. Alors rappelons deux faits en ce
qui concerne la sensibilité aux ondes électromagnétiques.

1.  Les symptômes (nausées, maux de têtes, etc) sont attestés comme
    réels. Ce n'est pas inventé : les gens les ressentent vraiment, et
    c'est pénible.
2.  Aucune personne se prétendant sensible aux ondes électromagnétiques
    n'a jamais pu détecter à l'aveugle si un appareil ou une antenne
    était en train d'émettre des ondes ou pas. Malgré études, tests
    médicaux, et tout : personne, jamais.

Comme le rappelle Wikipedia, “L’Organisation mondiale de la santé (OMS)
considère qu’il n’y a pas d’éléments scientifiques qui permettraient
d'appuyer l’affirmation que la sensibilité électromagnétique soit
réellement causée par les champs électromagnétiques, et non par des
troubles psychologiques.”

À rapporter à [cette histoire
récente](http://idle.slashdot.org/story/10/01/15/1516245/Tower-Switch-Off-Embarrasses-Electrosensitives) :
les habitants d'une ville d'Afrique du Sud se plaignant d'une grande
antenne à côté de chez eux (migraines, exéma, troubles digestif et du
sommeil), qui ont demandé à ce qu'on en interrompe le fonctionnement.
Surprise : l'antenne était déjà coupée depuis six semaines.

Je ne dis pas qu'on ne prouvera jamais que certaines personnes sont
*réellement* sensibles aux ondes — juste, pour l'instant, on n'en a
trouvé aucune. Et je veux bien qu'on parle des symptômes, qui sont
réels. Mais simplement, on ne peut pas continuer à parler de “personnes
sensibles aux champs électromagnétiques” à tort et à travers, et
prétendre résoudre le problème en coupant ou en éloignant les antennes.
Le problème n'est manifestement pas les ondes en elles-même.
