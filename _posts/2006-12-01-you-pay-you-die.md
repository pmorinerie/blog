---
layout: post
title: "You pay You die"
date: 2006-12-01 17:57
---

Hier soir, soirée de lancement de la série web **You pay You die** - et,
ce qui me concerne plus, lancement du site Web de l'opération :
[www.youpayyoudie.com](http://www.youpayyoudie.com/), réalisé par mes
soins.

Pour sa réalisation, j'ai utilisé le framework
[CakePHP](http://www.cakephp.org/), les bibliothèques
[Prototype](http://prototype.conio.net/) et
[script.aculo.us](http://script.aculo.us), la classe
[PHPMailer](http://phpmailer.sourceforge.net/), le lecteur [Flash Video
Player](http://www.jeroenwijering.com/?item=Flash_Video_Player), le
script [Flash UFO](http://osflash.org/ufo), l'outil de statistiques
[Google Analytics](http://www.google.com/analytics/) - ainsi que
d'autres éléments. Merci aux créateurs de tous ces outils, sans lequel
le développement serait encore plus ardu qu'il ne l'ai déjà.

Have fun on *You pay You die* :)
