---
layout: post
title: "Rsizr - Content-Aware Image Resizing"
date: 2007-10-03 18:36
---

Une vidéo circulant depuis quelques semaines sur le Web fait grand bruit
: elle fait en effet la démonstration d'un étonnant algorithme de
redimentionnement d'images, qui préserve le contenu et les proportions
des objets de celle-ci (voir vidéo [ci-dessous](#dynamic_resizing)).

[![Rsizr
logo](http://rsizr.com/logo/rsizr_small.png)](http://www.rsizr.com/)[Rsizr](http://www.rsizr.com),
une application Flash au nom typiquement 2.0, propose aujourd'hui de
tester cette technologie en ligne. Envoyez une image, et prenez les
commandes du logiciel pour réduire ou agrandir sans perdre les
proportions ou le contenu. L'application dispose également de deux
outils présentés dans la vidéo, l'un pour préserver une portion de
l'image de tout redimentionnement excessif, l'autre pour effacer une
portion de l'image — le résultat est surprenant.

<div id="dynamic_resizing" style="text-align: center; margin-top: 20px">

<object width="425" height="299">
<param name="movie" value="http://www.dailymotion.com/swf/3efI6YvAcF0gfjKGF"></param><param name="allowfullscreen" value="true"></param>
<embed src="http://www.dailymotion.com/swf/3efI6YvAcF0gfjKGF" type="application/x-shockwave-flash" width="425" height="299" allowfullscreen="true">
</embed>
</object>

</div>

</p>

