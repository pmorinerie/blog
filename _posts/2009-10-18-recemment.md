---
layout: post
title: "Récemment"
date: 2009-10-18 20:59
---

[![Gnome Nature -
Storm](http://www.winosx.com/hosted_img/Storm_small.jpg)](http://www.winosx.com/hosted_img/Storm.jpg)

J'ai utilisé cette première image en fond d'écran pendant plusieurs
mois : ces teintes gris-bleu que le flou transforme en doux applats, je
pourrais les regarder des heures.

[![Tiger staring into
you](http://www.winosx.com/hosted_img/47_Tiger_staring_into_you_by_Chunga_Stock_small.jpg)](http://chunga-stock.deviantart.com/art/47-Tiger-staring-into-you-138141883)

Et puis aujourd'hui j'ai trouvé ce tigre sur DeviantArt, et il redonne
des couleurs chaudes à mon bureau. C'est l'automne.
