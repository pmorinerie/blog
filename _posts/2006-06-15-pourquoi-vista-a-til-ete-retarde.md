---
layout: post
title: "Pourquoi Vista a-t'il été retardé ?"
date: 2006-06-15 16:56
---

Phillip, un membre d'une équipe de management chez Microsoft, se demande
pourquoi Vista a été autant retardé - pourquoi la date de sortie d'un
projet imposant, certes, mais pas au point d'être incontrôlable, a été
décalée de plusieurs années au fil du temps.

En évitant les clichés du genre "Vista sapu et c'est bloaté", il essaie
de relier les méthodes de management en vigueur chez Microsoft et le
retard de Vista, en décrivant le quotidien des développeurs et le
processus long et aléatoire de remontée des décisions.Très intéressant,
notamment pour en apprendre plus sur le travail des développeurs chez
Microsoft.

[The World As Best As I Remember It : Broken Windows
Theory](http://blogs.msdn.com/philipsu/archive/2006/06/14/631438.aspx)
