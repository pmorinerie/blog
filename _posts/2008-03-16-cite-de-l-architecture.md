---
layout: post
title: "Cité de l'architecture"
date: 2008-03-16 20:08
---

Hier, c'était dimanche : j'ai passé la journée à la [Cité de
l'architecture](http://www.citechaillot.fr/), à Paris.

Nouvellement réouverte, ce musée fait partie du Palais de Chaillot, et
présente tout un tas de choses en rapport avec (surprise) l'architecture
et le patrimoine. Un des principaux atraits, ce sont les moulages de
tympans, chapiteaux et autres splendeurs de l'art roman et gothique.
Avec de bons commentaires (et ils étaient excellents), on s'instruit,
découvre et s'émerveille.

[![Cité de
l'architecture](/images/photos/Cité%20de%20l\'architecture_thumb.jpeg)](/images/photos/Cité%20de%20l\'architecture.jpeg)

Oh, et aussi, les jardins du Trocadéro par le temps doux de Mars (et
sous les averses qui vont avec), c'est sympa :)
