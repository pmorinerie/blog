---
layout: post
title: "Women are made of water"
date: 2006-09-21 18:14
---

Juste en passant, voici l'apparence actuelle de mon Bureau Windows. Il
est construit autour du fond d'écran, trouvé aujourd'hui dans les Daily
Deviations de DeviantArt, et intitulé "*Women are made of water*".

[![Blog de KemenAran - Bureau
septembre](http://lamorine.free.fr/MacOSX/img/glass_desktop_mini.jpg)](http://lamorine.free.fr/MacOSX/img/glass_desktop.jpg)

-   **Fond d'écran :** [Women are made of
    water](http://www.deviantart.com/deviation/38197450/), par
    dark-zephyr (retaillé au format 16:10 par mes soins)
-   **Thème visuel :** [Tiger
    V](http://www.deviantart.com/deviation/17919057/) Graphite, par
    dobee
-   **Icônes du Bureau :**
    [MilkMx](http://www.customize.org/details/39739), par Faneramx
-   **Dock :**
    [ObjectDock](http://www.stardock.com/products/objectdock/)
-   **Post-it :** [Avedesk](http://www.avedesk.org/), avec StickyNotes
-   **Lecteur audio :** [Winamp](http://www.winampfr.com), avec la skin
    [AeroTunes](http://www.deviantart.com/deviation/10666951/)
