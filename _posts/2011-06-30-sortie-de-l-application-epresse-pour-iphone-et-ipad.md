---
layout: post
title: "Sortie de l'application ePresse pour iPhone et iPad"
date: 2011-06-30 14:08
permalink: posts/sortie-de-l-application-epresse-pour-iphone-et-ipad
---

![Logo de l'application ePresse](/images/logos/epresse-small.png)

L'application ePresse est sortie ce matin !

Il s'agit d'un kiosque numérique, qui rassemble neuf titres de presse
française dans une seule application iPhone et iPad. On y trouve pour
l'instant les éditions quotidiennes de *Libération*, du *Figaro*, du
*Parisien* et *Aujourd'hui en France*, de *L'Équipe* et des *Échos*,
mais aussi les éditions hebdomadaires du *Nouvel Observateur*, du
*Point* et de *L'Express*. Une [présentation plus
complète](http://www.igeneration.fr/app-store/epresse-le-kiosque-alternatif-pour-la-presse-francaise-51712)
est disponible sur iGeneration.

Cette sortie marque pour moi la fin de plusieurs mois de développement
et de peaufinage, pour pouvoir au final présenter quelque chose de
qualité. Il y a des raccourcis et des imprécisions, bien sûr : c'est une
1.0. Mais n'hésitez pas à me dire ce que vous en pensez !

<div style="text-align:center">

<iframe src="http://player.vimeo.com/video/25756924?title=0&amp;byline=0&amp;portrait=0" width="400" height="225" frameborder="0">
</iframe>

</div>

[![ePresse -
ePresse](http://ax.phobos.apple.com.edgesuite.net/images/web/linkmaker/badge_appstore-lrg.gif)](http://itunes.apple.com/fr/app/epresse/id430832375?mt=8&uo=4)
