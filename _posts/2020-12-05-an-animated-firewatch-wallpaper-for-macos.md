---
layout: post
title: "An animated Firewatch wallpaper for macOS"
date: 2020-12-05 11:08
lang: en
---

A while ago [Daka](https://www.artstation.com/dakadibuja) made a series of [great Firewatch wallpapers](https://www.artstation.com/artwork/1VBvq). Then dynamicwallpaper.clup user Jagoba turned them into a nice macOS dynamic wallpaper for macOS.

However the previous dynamic wallpaper was timed to the clock – and so didn't account for the current sunrise and sunset time at your location. Which means that during winter, the wallpaper would still be a bright blue while outside the night was already dark.

So I edited the wallpaper metadata, to make them dependant on the sun position rather than the time of the day.

## Finding out sun coordinates

On macOS, making the [wallpaper dependant on the sun position](https://nshipster.com/macos-dynamic-desktop/) means adding fictive sun altitude and azimuth to the images.

Rather than computing the sun angle and elevation ourselves, an easiest way is to set the time and GPS position of the pictures. Wallpaper-makers can then automatically convert this time+location data into the sun angle and elevation.

First, the location. I chose the wallpapers to be in Shoshone National Forest, for obvious reasons.

Then, the time. The game takes place during summer 1989, so it seems like a good fit. I've arbitrarily chosen the 3rd of July 1989, which roughly matches the first days of the game.

Now we need to time the different wallpapers. For the middle of the night, this is easy enough: jut set the time to midnight. But for the various sunrise and sunset pictures, this is harder to define.

Which leaves us with interesting questions. For instance: **at what time was the first light of dawn visible in Shoshone Forest on the 3rd of July 1989?**

Turns out there are softwares that can tell the light of the day at a given time and location: planetariums. For instance, by using the atmosphere simulation of [stellarium-web.org](https://stellarium-web.org), we can get a rough idea of the time of each picture.

Here are the exact timing used for the wallpapers:

- Firewatch 1 : 1989:07:03 22:00:00
- Firewatch 2 : 1989:07:03 23:00:00
- Firewatch 3 : 1989:07:03 04:50:00
- Firewatch 4 : 1989:07:03 05:20:00
- Firewatch 5 : 1989:07:03 07:00:00
- Firewatch 6 : 1989:07:03 08:00:00
- Firewatch 7 : 1989:07:03 19:30:00
- Firewatch 8 : 1989:07:03 20:45:00
- Firewatch 9 : 1989:07:03 21:10:00

macOS wallpaper engine will then use these informations to display the correct picture to you, depending on the season at your place. A little trickery though: if the middle-of-the-day picture is timed at 12:00, it's sun elevation will be very high. Which means that during winter, when the sun is low, the wallpaper engine will simply skip this picture. To make sure the middle-of-the-day will be displayed even during winter, I inserted instead two pictures, timed early in the morning and late in the afternoon.

## End result

So here you are! A nice [Firewatch dynamic wallpaper for macOS](https://dynamicwallpaper.club/wallpaper/9qzi6vu8hvo), adjusted to the sun's position. Enjoy!

<a href="https://dynamicwallpaper.club/wallpaper/9qzi6vu8hvo"><video width="480" autoplay loop><source src="/videos/firewatch/firewatch-animated-wallpaper.mp4" type="video/mp4"/>
</video></a>

_**EDIT:** I fixed some timing issues, and re-uploaded a new version under the name "Firewatch Sun". It makes sure the 'Sunset' variant doesn’t start displaying too soon in the afternoon, even during winter._
