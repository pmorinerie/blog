---
layout: post
title: "Bouletcorp"
date: 2008-04-09 14:04
---

Boulet a posté aujourd'hui ses planches réalisées lors des "24h de la
BD" du Festival d'Angoulême. Une histoire en 24 planches, donc, avec une
contrainte : la planche douze doit comporter une réunion de famille. Le
résultat est excellent, très bien dessiné et assez
poétique/drôle/déprimant, selon ce que l'on veut y voir. Jetez y un
oeil, et profitez-en éventuellement pour découvrir le reste :)

[![Boulet - Un dimanche en
famille](/images/divers/boulet%20-%20une%20histoire%20de%20famille.jpg)](http://www.bouletcorp.com/blog/index.php?date=20080404 "Cliquez pour lire l'histoire complète")
