---
layout: post
title: "IE7, c'est du gâteau"
date: 2006-10-25 22:18
---

L'équipe d'[Internet Explorer
7](http://www.microsoft.com/france/windows/ie/default.mspx) a décidément
bon esprit - on irait *presque* jusqu'à leur pardonner les nombreux
bogues dont leur navigateur a souffert pendant des années. Voici ce
qu'ils ont envoyé à l'équipe de Firefox pour marquer la sortie de
[Firefox 2](http://www.mozilla-europe.org/fr/).

**[Firefox 2 - From Redmond with
Love](http://fredericiana.com/2006/10/24/from-redmond-with-love/)**

Peut-être est-ce enfin le retour de la concurrence saine et loyale entre
les navigateurs ? En tout cas ce bon esprit est vraiment rafraîchissant.
