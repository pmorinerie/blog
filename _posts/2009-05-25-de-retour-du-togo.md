---
layout: post
title: "De retour"
date: 2009-05-25 22:22
---

Je suis de retour [du Togo](http://www.lavaliseencarton.fr/) ! Le fil de
ce blog va donc reprendre son déroulement normal.

Pour fêter cela, une petite chanson mignonne et sympathique, découverte
dans l'avion de retour. Enjoy !

<div style="width:180px;height:236px;">

<object width="180" height="236">
<param name="movie" value="http://www.deezer.com/embedded/widget.swf?path=26006423⟨=FR&amp;colorBack=0x525252&amp;colorVolume=0x00CCFF&amp;colorScrollbar=0x666666&amp;colorText=0xFFFFFF&amp;autoplay=0&amp;autoShuffle=0&amp;id=8855189"></param>
<embed src="http://www.deezer.com/embedded/widget.swf?path=26006423⟨=FR&amp;colorBack=0x525252&amp;colorVolume=0x00CCFF&amp;colorScrollbar=0x666666&amp;colorText=0xFFFFFF&amp;autoplay=0&amp;autoShuffle=0&amp;id=8855189" type="application/x-shockwave-flash" width="180" height="236">
</embed>
</object>
  
<font size="1" color="#000000" face="Arial">Découvrez
[Aldebert](http://www.deezer.com/fr/aldebert.html)!</font>

</div>
