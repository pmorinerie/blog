---
layout: post
title: "Synchronisation iCal / Google Calendar avec CalDAV"
date: 2008-07-30 14:33
---

![Logo iCal](/images/logos/ical.png)La synchronisation de calendriers
entre Google Calendar et iCal a toujours été un sujet épineux — j'en
avais [touché un
mot](http://kemenaran.winosx.com/?2008/05/07/125-synchronisation-ical-google-calendar)
dans un billet il y a quelques mois.

Mais joie, depuis quelques jours, Google Calendar propose **la
synchronisation des agendas par le protocole
[CalDAV](http://en.wikipedia.org/wiki/CalDAV)**, protocole supporté par
iCal ! On peut donc désormais synchoniser ses agendas sans avoir besoin
de logiciel tierce-partie. Une belle page d'explications a été mise en
ligne sur le site de support de Google Calendar, et explique la [marche
à suivre pour rajouter des comptes CalDAV à
iChat](http://www.google.com/support/calendar/bin/answer.py?answer=99358).

![Ajouter un compte CalDAV à
iCal](http://www.google.com/help/hc/images/calendar_99358a_en.gif)

A noter toutefois **quelques limitations** : du côté d'iCal, on ne peut
accéder qu'à un agenda par compte CalDAV — il faut donc rajouter un
compte par agenda à synchroniser ; de plus, un nombre réduit
d'événements de l'agenda Google peuvent ne pas s'afficher correctement
dans iCal. Et du côté de Google, certaines fonctionnalités sont encore
manquantes, comme la synchronisation des tâches, des notifications, ou
la création de nouveaux agendas depuis iCal. Paraît-il que l'on y
travaille, chez Google comme chez Apple.

Cela dit, même si la simplicité et les fonctionnalités ne sont pas
encore au rendez-vous, ce système est une excellent alternative gratuite
aux logiciels du type [SpanningSync](http://spanningsync.com/). And
there was much rejoicing.

*Source :
[MacGeneration](http://www.macgeneration.com/news/voir/131197/synchronisation-totale-entre-ical-et-google-calendar)*
