---
layout: post
title: "NTFS sous Linux"
date: 2006-07-15 16:46
---

Un driver <acronym title="New Technology File System">NTFS</acronym>
libre pour Linux vient de [faire son
apparition](http://sourceforge.net/mailarchive/forum.php?thread_id=23836054&forum_id=2697) ;
il est dérivé du projet [linux-ntfs](http://www.linux-ntfs.org/) (qui ne
permettait que la lecture), et authorise la lecture et l'écriture sur un
système de fichiers au [format NTFS](http://en.wikipedia.org/wiki/NTFS).

Un tel driver était attendu comme le messie depuis plus de dix ans. Le
système NTFS, utilisé en standard par les systèmes Microsoft depuis
Windows NT, est très complexe et non documenté ; on arrivait sans trop
de peine à lire une partition au format NTFS, mais jusqu'à présent
impossible de créer, modifier ou supprimer des fichiers. Il y a un an,
[Paragon Software](http://www.ntfs-linux.com/) lançait une technologie
permettant de lire de manière transparente un système de fichier NTFS,
mais cette solution était hélas propriétaire, chère, et surtout très
lente. On pouvait également tenter d'enrober le driver Windows original
d'une surcouche permettant de le faire fonctionner sous un système
Linux, mais cela restait propriétaire et peu performant.

Avec ce nouveau driver, les performances semblent être de 20 à 50 fois
meilleures qu'avec la solution commerciale de Paragon - sans qu'aucun
travail d'optimisation ait encore été fait. L'interopérabilité
Windows/Linux fait donc un bond en avant, puisque enfin les partitions
Windows seront librement accessibles depuis Linux ; ceci éliminera le
besoin de partitions d'échanges en FAT32 pour transférer ses fichiers de
Linux à Windows.
