---
layout: post
title: "Première formation C#"
date: 2007-11-22 20:18
---

La première formation C\# dispensée par le Club Microsoft s'est déroulée
aujourd'hui à l'Isep. Au programme : démonstrations, exemples, TP, et un
peu de théorie, parce qu'il faut bien. Le programme a pu être bouclé
malgré les retards et les grèves, mais j'ai la voix cassée pour la
soirée \^\^ Deuxième séance la semaine prochaine — n'oubliez pas de vous
inscrire !

Voici le diaporama de cette première formation, ainsi que les exemples
de code et solution des TP.  
[Diaporama Formation C\#- Partie
1](http://www.slideshare.net/kemenaran/formation-c-cours-1-introduction-premiers-pas-concepts-186982)  
[Formation C\# - Exemples et
TPs](http://winosx.com/hosted_files/TP%20et%20Exercices%20CSharp.zip)
