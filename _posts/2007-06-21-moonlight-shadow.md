---
layout: post
title: "Moonlight shadow..."
date: 2007-06-21 20:22
---

Miguel de Icaza, principal contributeur du projet
[Mono](http://www.mono-project.com/Main_Page), raconte en détail le
[''hackatlon''
Moonlight](http://tirania.org/blog/archive/2007/Jun-21.html). Objectif :
implémenter les fonctionnalités de Silverlight sous Linux en 21 jours.
Récit.
