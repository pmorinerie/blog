---
layout: post
title: "Où est Charlie ?"
date: 2008-04-08 17:08
---

S'ajoutant à la longue série des "Google Earth-bombing", voici le
Charlie géant ! L'idée est de faire photographier cette [reproduction de
16 mètres de long de
Charlie](http://whereonearthiswaldo.wordpress.com/about/) par les
satellites de Google Earth. Gag : non, on ne sait pas où cette
installation se trouve ; il va bien falloir trouver Charlie.

[![Où est Charlie vu du
ciel](/images/divers/ou%20est%20charlie.jpg)](http://whereonearthiswaldo.wordpress.com/about/)
