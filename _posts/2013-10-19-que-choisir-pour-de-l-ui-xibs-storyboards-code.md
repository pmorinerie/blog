---
layout: post
title: "Que choisir pour de l'UI : XIBs / Storyboards / Code"
date: 2013-10-19 10:16
---

> Écrire ses interfaces graphiques en code, ou utiliser des XIBs ou des
> Storyboards : chaque méthode a ses forces et ses faiblesses. Que
> choisir pour quel cas d'usage ?
>
> Ce diaporama a servi de support à un débat collectif lors de la
> réunion CocoaHeads Paris d'octobre 2013.

<script async class="speakerdeck-embed" data-id="4ebf69801ac30131688f0eeb8e09da02" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>
<noscript>
https://speakerdeck.com/kemenaran/code
</noscript>
