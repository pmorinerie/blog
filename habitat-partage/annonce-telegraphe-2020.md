---
layout: post
title: "Rejoindre un habitat partagé rue du Télégraphe"
date: 2020-01-29 10:27
---

Bonjour,

Dans notre petit habitat collectif, pour cause de moment de transition (une rupture amoureuse qui a causé un déménagement, et du coup petit moment de transition le temps que tout le monde décide dans quelle chambre iel veut s'installer à plus long terme, et/ou qui veut rester ou partir), nous avons une chambre libre pour quelques mois **à partir de maintenant**. **Durée flexible, entre deux à quatre mois**, mais en revanche la chambre n'est probablement pas dispo à long terme. C'est à **Paris, XX<sup>e</sup> arrondissement, au métro Télégraphe**.

**Le loyer serait 450 €, qui peut être discuté en fonction de votre situation/vos revenus**. Nous avons coutume d'appliquer une solidarité financière entre nous tous, et si nécessaire, ce principe peut être étendu à la personne qui viendra partager avec nous ces quelques mois. (Si vous pensez avoir des revenus faibles qui donneraient lieu à une baisse de loyer, vous pouvez nous décrire la situation, et on vous dira ce qui est possible de notre côté et quelle serait la baisse de loyer correspondante.)

## Le collectif

- une maison à trois chambre (pour quatre habitants) + un appart' à une chambre (une habitante actuellement),
- un chat du côté appart', et d'ailleurs quelques autres chats qui habitent autour et aiment bien venir en visite,
- tous les espaces qui ne sont pas des chambres sont partagés (salles de bain, cuisines, salon...),
- à l'extérieur, un micro-jardin et une terrasse partagée aussi,
- quatre habitants en ce moment, jusqu'à six dans les moments les plus remplis,
- on essaye d'habiter ensemble même si tout le monde est très occupé : un dîner de coloc' par semaine où tout le monde essaye d'être présent, mais aussi une solidarité morale et financière entre nous,

## La chambre qui est libre

- c'est la plus petite chambre de la maison : 9 m<sup>2</sup>,
- meublée avec notamment un futon une place et demie et un bureau pliable (et on peut adapter les meubles aux souhaits de la personne).

Si tu es intéressé·e, écris-nous à [la.commune.libre.du.telegraphe@gmail.com](mailto:la.commune.libre.du.telegraphe@gmail.com) :
tu nous dis pourquoi tu as envie de venir habiter ici, et puis on se présente tous un peu plus, on envoie des photos de la maison, on se voit pour en parler (ou on peut skyper si tu n'es pas à Paris), etc.

Élise, Pierre, Aurore, Marina :-)
