---
layout: post
title: "Rejoindre un habitat partagé rue du Télégraphe"
date: 2016-11-09 10:27
---

Salut !

**On habite à Paris, dans le 20<sup>ème</sup>, rue du Télégraphe – et on cherche deux personnes pour compléter notre habitat partagé.**

La "coloc'" est répartie sur deux espaces communs : un appartement (avec une chambre) au rez-de-chaussée et une petite maison (avec trois chambres) sur trois niveaux, l'un et l'autre voisins, et partageant donc une très grande terrasse et un petit jardin. Au total, les deux espaces font 95 m<sup>2</sup> intérieur + 35 m<sup>2</sup> extérieur à eux deux.

- Une chambre disponible tout de suite est toute petite ; elle fait environ 9 m<sup>2</sup>.
- L'autre chambre fait plutôt 11 m<sup>2</sup>, et se libèrera au mois de mai ou juin.
- _-> On peut imaginer **une personne dans chaque chambre, ou un couple utilisant en commun l'espace des deux chambres**, (ou un non-couple utilisant l'espace des deux chambres, ou ce que vous voulez, au fond : venez nous dire) avec l'une en chambre et l'autre en bureau. (Mais difficilement un couple dans chaque chambre : c'est quand même pas bien grand !)_

Pour des **photos** de la maison et de la plus petite des chambres, voir ici : [https://www.airbnb.fr/rooms/7549868](https://www.airbnb.fr/rooms/7549868)
(et sinon : venez visiter et faire connaissance.)

L'idée est **d'habiter ensemble, mais pas seulement**. On a envie d'un espace qui soit plus qu'un lieu où dormir : un endroit ouvert, où discuter, inviter des amis, cuisiner ensemble, et puis un endroit solidaire, où héberger des copains, se filer des coups de main en informatique ou en langues, monter des projets, se soutenir les uns les autres - et surtout, un endroit sans discrimination de genre, race, âge, ou autres : par exemple, ici, les filles peuvent bricoler ou apprendre à bricoler sans qu'on se moque d'elles, et les tâches ménagères sont réparties également entre tous.

En pratique, ça veut dire aussi qu'on paye des **loyers solidaires** (modérés et calculés en fonction de nos revenus respectifs ; si vous venez visitez, on vous expliquera la formule, et vous verrez combien ça fait pour vous en ce moment). On dîne à plusieurs quand nos horaires coïncident, mais de toute façon, tous ensemble un soir dans la semaine - en ce moment, c'est le mercredi. On entretient et on répare ensemble ce qui se casse dans la maison. Et de temps en temps, on aime bien organiser une activité à plusieurs : une simple sortie, ou carrément un coup de main sur un repas associatif.

## Quelques mots sur nous

**Aurore** a 25 ans. Elle fait actuellement un service civique. Elle a étudié dans le domaine du cinéma et aimerait y travailler. Elle s'intéresse à l'art en général et aime entre autres aller à des expositions de peinture, regarder des films et réfléchir dessus, les analyser...

**Alexandre** a 31 ans. D'une formation juridique et passionné par la protection de notre planète, il s'est naturellement spécialisé en droit de l'environnement. Curieux, il aime voyager, aller au cinéma, découvrir de nouvelles saveurs. S'il aime flâner dans Paris, rien ne vaut les balades dans la nature, que ce soit en forêt, montagne ou son cher bord de mer.

**Élise** a 33 ans. Elle rédige une thèse en histoire médiévale. Elle participe à des projets musicaux ou théâtraux. Elle est impliquée aussi dans des projets de solidarité internationale.

**Pierre** a 31 ans. Dans la vie, il fait partie d'une coopérative de développeurs informatiques ; et en ce moment, il fait des sites web pour le service public. Ça lui laisse le temps de travailler sur sa licence de philosophie, et filer un coup de main à quelques associations. Plutôt introverti, il aime refaire le monde en petits groupes, parler d'autogestion et de transports en communs.

## Pour en parler

Si le projet vous tente, parlons-en !

**Envoyez un message à Alexandre** (<a href='m&#97;ilto&#58;th%6&#53;_new_gunne&#114;&#64;&#104;otma&#105;l&#46;%63o&#37;&#54;D'>th&#101;_new_gunner&#64;&#104;&#111;t&#109;a&#105;l&#46;com</a>), en décrivant si possible qui vous êtes, et ce qui vous intéresse dans l'idée. On pourra se rencontrer pour que vous voyez le lieu, et qu'on fasse tous connaissance !

_Si vous ne vous sentez pas à l'aise par écrit en français, vous pouvez aussi écrire en anglais, allemand, espagnol, italien, grec moderne ou espéranto :) Cela dit dans la vie de tous les jours la maison est francophone._
